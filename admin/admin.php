<?
	require('db.php');
	mysqli_query($db,"SET NAMES UTF8");
	session_start();

	//logged?
	if($_GET['action']=='logout'){
		$q = "UPDATE admin SET session_id='' WHERE session_id='".session_id()."'";
		$r = mysqli_query($db,$q);
	}

	$q = "select login from admin where session_id='".session_id()."'";
	$r = mysqli_query($db,$q);
	$c = mysqli_num_rows($r);
	if($c){
		$logged = 1;
		$f = mysqli_fetch_array($r);
		$admin_name = $f['login'];
	}else{
		$logged = 0;
	}

?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-language" content="en" />
    <meta name="robots" content="index,follow" />
    <meta name="googlebot" content="index,follow,snippet,archive" />
    <meta name="author" content="ELIVE CZ s.r.o. (c) 2010" />
    <?
        if(!$logged) {
            die('<meta http-equiv="refresh" content="0;url=index.php" />');

        }
    ?>

    <link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="css/smoothness/jquery-ui-1.7.2.custom.css" />

    <script src="https://www.shanore.com/wysiwyg/bootstrap-wysihtml5-master/lib/js/wysihtml5-0.3.0.js"></script>


    <!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <!--<script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>-->

    <script src="https://www.shanore.com/wysiwyg/bootstrap-wysihtml5-master/lib/js/prettify.js"></script>
    <script src="https://www.shanore.com/wysiwyg/bootstrap-wysihtml5-master/lib/js/bootstrap.js"></script>
    <script src="https://www.shanore.com/wysiwyg/bootstrap-wysihtml5-master/src/bootstrap-wysihtml5.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>

    <!--<script type="text/javascript" src="js/jquery.ui.datepicker-cs.js"></script>-->
    <!--<script type="text/javascript" src="js/jquery.simpleTooltip.js"></script>-->
    <!--<script type="text/javascript" src="js/jquery.flot.pack.js"></script>-->
    <!--[if IE]><script language="javascript" type="text/javascript" src="js/excanvas.pack.js"></script><![endif]-->
    <!--<script src="js/jquery.wysiwyg.js" type="text/javascript" charset="utf-8"></script>-->
    <!--<script src="js/jquery.wysiwyg.controls.js" type="text/javascript" charset="utf-8"></script>-->

    <script type="text/javascript" src="js/jquery.highlight-1.1.source.js"></script>

    <link rel="stylesheet" type="text/css" href="https://www.shanore.com/wysiwyg/bootstrap-wysihtml5-master/lib/css/bootstrap.min.css"></link>
    <link rel="stylesheet" type="text/css" href="https://www.shanore.com/wysiwyg/bootstrap-wysihtml5-master/lib/css/prettify.css"></link>
    <link rel="stylesheet" type="text/css" href="https://www.shanore.com/wysiwyg/bootstrap-wysihtml5-master/src/bootstrap-wysihtml5.css"></link>

    <script type="text/javascript" src="js/submenu.js"></script>
    <script type="text/javascript" src="js/xadmin.js"></script>
    <script type="text/javascript" src="js/jquery.simplemodal.js"></script>
    <script type="text/javascript" src="js/admin.js"></script>




    <title>ShanOre.com ADMIN</title>
    <link href="css/jquery.wysiwyg.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
		body,td,th {
			font-family: Arial, sans-serif;
		}
    </style>
    </head>
<body>
        <div id="header">
            <h1>ShanOre.com</h1>
            <div id="user"><a href="?action=logout">Logout</a>

                <br />
            </div>

            <div id="mainMenuWrapper">
<ul id="mainMenu">
                    <li class="active"><a href="admin.php">Catalog</a></li>
                    <li><a href="orders.php">ORDERS</a></li>
                    <li><a href="customers.php">Customers</a><a href="editor.html"></a></li>
                    <li><a href="affiliates.php">Affiliates</a><a href="editor.html"></a></li>
</ul>
            </div>
        </div>
        <div id="contentWrapper">
            <div id="leftMenu">
                <strong>Menu</strong>
              <div id="menu">
                    <ul>
                        <li class="active"><a href="admin.php" class="item">Products</a></li>
                        <li>
                            <a href="#" class="item">Product Options</a>
                            <ul>
                            	<li><a href="options.php?what=categories">Categories</a></li>
                                <li><a href="options.php?what=brands">Brands</a></li>
                                <?
									$q = "SELECT name, id FROM product_options_types";
									$r = mysqli_query($db,$q);
									$c = mysqli_num_rows($r);
									for($i=0; $i<$c; $i++){
										$f = mysqli_fetch_row($r);
										echo('<li><a href="options.php?what='.$f[1].'&multi=yes">'.$f[0].'</a></li>');
									}
								?>

                            </ul>
                       </li>
                       <li>
                            <a href="#" class="item" id="collections_menu">Collections</a>
                            <ul>
                            		<li><a href="categories-seo.php">CATEGORIES SEO</a></li><hr />
                            		<li><a href="collections-assign.php">COLLECTIONS ADMIN</a></li><hr />
                                <?
									$q = "SELECT name, id FROM collections";
									$r = mysqli_query($db,$q);
									$c = mysqli_num_rows($r);
									for($i=0; $i<$c; $i++){
										$f = mysqli_fetch_row($r);
										echo('<li><a href="collections.php?what='.$f[1].'">'.$f[0].'</a></li>');
									}
								?>

                            </ul>
                        </li>
                        <li><a href="brands.php" class="item">Brands</a></li>
                        <li><a href="deleted_products.php" class="item">Deleted Products</a></li>
                        <li><a href="pages.php" class="item">Pages</a></li>
                        <li><a href="currencies.php" class="item">Currencies</a></li>
                        <li><a href="images.php" class="item">Images</a></li>
                        <li><a href="sort.php" class="item">Sort Products</a></li>
                        <li><a href="reports.php" class="item">Reports</a></li>
                        <li>
                            <a href="#" class="item" id="pdf_menu">PDF catalogues</a>
                            <ul>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=1" target="_new">Engagement Rings</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=10" target="_new">Wedding Rings</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=96" target="_new">Claddagh Rings</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=94" target="_new">Celtic Rings</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=16" target="_new">Pendants</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=37" target="_new">Crosses</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=95" target="_new">Bracelets</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=84" target="_new">Earrings</a></li>


                                <!--<li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=85" target="_new">Tara Beads</a></li>-->
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=85" target="_new">Tara Beads</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=91" target="_new">Tara Charms</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=83" target="_new">Tara Bracelets</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=92" target="_new">Tara Necklaces</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=93" target="_new">Tara Stacking Rings</a></li>

                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=99" target="_new">Little Miss</a></li>

                            </ul>
                        </li>
                        <li><a href="discount_codes.php" class="item">Discount codes</a></li>
                        <li><a href="reassign.php" class="item">Reassign products</a></li>
                    </ul>
            </div>

            <div class="filter">
              <h3>DISPLAY FILTERING</h3>
              <form action="" method="get" enctype="multipart/form-data">
                    Word / Phrase
                    <input type="text" name="search_txt" style="width:165px;" value="<? echo($_GET['search_txt']); ?>" />

                    Brand<br />
					<select name="brand_filter">
                    	<option value="">All</option>
                        <?
							$q = "SELECT id, name FROM brands";
							$r = mysqli_query($db,$q);
							$c = mysqli_num_rows($r);
							for($i=0; $i<$c; $i++){
								$f = mysqli_fetch_row($r);
								if($_GET['brand_filter']==$f[0]){
									$selected = ' SELECTED';
								}else{
									$selected = '';
								}
								echo('<option value="'.$f[0].'"'.$selected.'>'.$f[1].'</option>');
							}
						?>
                    </select>

                     Category<br />
					<select name="category_filter">
                    	<option value="">All</option>
                        <?
							$q = "SELECT id, name FROM categories";
							$r = mysqli_query($db,$q);
							$c = mysqli_num_rows($r);
							for($i=0; $i<$c; $i++){
								$f = mysqli_fetch_row($r);
								if($_GET['category_filter']==$f[0]){
									$selected = ' SELECTED';
								}else{
									$selected = '';
								}
								echo('<option value="'.$f[0].'"'.$selected.'>'.$f[1].'</option>');
							}
						?>
                    </select>

                    <input type="submit" value="Submit" />

              </form>
            </div>

            <div class="filter">
            	<h3>BULK EDIT BASKET</h3>
                <div id="edit_basket">
            	<?
					$q = "SELECT id, product_id, product_name FROM bulk_edit_basket WHERE admin_name='".$admin_name."'";
					$r = mysqli_query($db,$q);
					$c = mysqli_num_rows($r);
                                        
                                        
					for($i=0; $i<$c; $i++){
						$f = mysqli_fetch_row($r);
						echo("
							<script>
								$(document).ready(function() {
									$('#tobulk_".$f[1]."').css('display','none');
								});
							</script>
						");
						echo('<span><img src="img/datagridDel.png" style="cursor:pointer;" onclick="remove_from_bulk(\''.$f[0].'\',\''.$admin_name.'\','.$f[1].');">&nbsp;'.$f[2].'</span><br/>');
					}
					if($c){
						echo('<br/><button onclick="edit_bulk();">Edit All</button>');
					}else{
						echo('empty');
					}
				?>
                </div>
            </div>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            <strong> </strong></div>
            <div id="content">
<?

	if($_GET['action'] == 'editproduct'){

		$_POST['edit_name'] = mysqli_real_escape_string($db,$_POST['edit_name']);
		$_POST['desc'] = mysqli_real_escape_string($db,$_POST['desc']);

		$_POST['desc'] = str_replace("®","&reg;",$_POST['desc']);
		$_POST['desc'] = str_replace("é","&eacute;",$_POST['desc']);
		$_POST['desc'] = str_replace("í","&iacute;",$_POST['desc']);
		$_POST['desc'] = str_replace("’","&#39;",$_POST['desc']);
		$_POST['desc'] = str_replace("'","&#39;",$_POST['desc']);
		$_POST['desc'] = str_replace('"',"&quot;",$_POST['desc']);
        
                if(!$_POST['edit_category']) $_POST['edit_category'] = 0;
                if(!$_POST['edit_price_sterling']) $_POST['edit_price_sterling'] = 0;
                if(!$_POST['edit_price_wholesale']) $_POST['edit_price_wholesale'] = 0;
                if(!$_POST['edit_price_wholesale_sterling']) $_POST['edit_price_wholesale_sterling'] = 0;
                
        //start: korny ---
        $flds = 'price,price_dollar,price_sterling,price_wholesale,price_wholesale_dollar,price_wholesale_sterling';
        foreach (explode(',',$flds) as $field) {
            $field = 'edit_'.$field; //set prefix
            
            if ($_POST[$field] == '')
                $_POST[$field] = 0;
        }
        $flds = 'price_old,price_dollar_old,public_sale,public_sale_dollar';
        foreach (explode(',',$flds) as $field) {
            if ($_POST[$field] == '')
                $_POST[$field] = 0;
        }
        //end: korny ----- 
                
                
		$q = "UPDATE products SET name='".$_POST['edit_name']."', price='".$_POST['edit_price']."', description='".$_POST['desc']."', category='".$_POST['edit_category']."', brand='".$_POST['edit_brand']."', sku='".$_POST['edit_sku']."', sku2='".$_POST['edit_sku2']."', edit_datetime=now(), price_dollar='".$_POST['edit_price_dollar']."', price_sterling='".$_POST['edit_price_sterling']."', price_wholesale='".$_POST['edit_price_wholesale']."', price_wholesale_dollar='".$_POST['edit_price_wholesale_dollar']."', price_wholesale_sterling='".$_POST['edit_price_wholesale_sterling']."', price_old='".$_POST['edit_price_old']."', price_dollar_old='".$_POST['edit_price_dollar_old']."', public_sale='".$_POST['public_sale']."', public_sale_dollar='".$_POST['public_sale_dollar']."' WHERE id=".$_POST['id'];
                
                //echo($q);
                
                
                $r = mysqli_query($db,$q) or $message = '<div class="flash error">
											  <div class="top"></div>
											  <div class="text">Error talking to database: '.mysqli_error($db).'</div>
											  <div class="bottom"></div>
											</div>';
                                            
                //echo $q;

                //echo '<pre>'; print_r($db); echo '</pre>';
                //print_r(mysqli_error($db));

		$product_id = $_POST['id'];

		if($_FILES['file']['tmp_name']){
				if(file_exists("../img/products/product/".$product_id.".jpg")) unlink("../img/products/product/".$product_id.".jpg");
				$filename = $product_id.".jpg";
				copy ($_FILES['file']['tmp_name'], "../img/products/product/".$filename);

				var_dump(error_get_last());

				/*$newwidth = 144;
				list($width, $height) = getimagesize("../img/products/product/".$filename);
				$ratio = $width / $height;
				$newheight = ($newwidth / $width) * $height;

				unlink("../img/products/product/tmb/tmb_".$filename);
				$thumb = imagecreatetruecolor($newwidth, $newheight);
				$source = imagecreatefromjpeg("../img/products/product/".$filename);
				imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
				imagejpeg($thumb,"../img/products/product/tmb/tmb_".$filename) or die("Couldn't save thumbnail .../img/products/product/tmb/tmb_".$filename);*/

			}

			if($_FILES['thumb']['tmp_name']){
				echo('thumbnail found');
				if(file_exists("../img/products/product/tmb/tmb_".$product_id.".jpg")) unlink("../img/products/product/tmb/tmb_".$product_id.".jpg");
				$filename = "tmb_".$product_id.".jpg";
				echo('<br>copying '.$filename);
				copy ($_FILES['thumb']['tmp_name'], "../img/products/product/tmb/".$filename);

			}

			if($_FILES['file2']['tmp_name']){
				echo('image 2 found');
				if(file_exists("../img/products/product/".$product_id."._2.jpg")) unlink("../img/products/product/".$product_id."._2jpg");
				$filename = $product_id."_2.jpg";
				echo('<br>copying '.$filename);
				unlink("../img/products/product/".$filename);
				copy ($_FILES['file2']['tmp_name'], "../img/products/product/".$filename);
				echo('<br>copied<br>error:');

			}

			if($_FILES['file3']['tmp_name']){
				if(file_exists("../img/products/product/".$product_id."._3.jpg")) unlink("../img/products/product/".$product_id."._3jpg");
				$filename = $product_id."_3.jpg";
				unlink("../img/products/product/".$filename);
				copy ($_FILES['file3']['tmp_name'], "../img/products/product/".$filename);
			}

			if($_FILES['file4']['tmp_name']){
				if(file_exists("../img/products/product/".$product_id."._4.jpg")) unlink("../img/products/product/".$product_id."._4jpg");
				$filename = $product_id."_4.jpg";
				unlink("../img/products/product/".$filename);
				copy ($_FILES['file4']['tmp_name'], "../img/products/product/".$filename);
			}

			if($_FILES['file5']['tmp_name']){
				if(file_exists("../img/products/product/".$product_id."._5.jpg")) unlink("../img/products/product/".$product_id."._5jpg");
				$filename = $product_id."_5.jpg";
				unlink("../img/products/product/".$filename);
				copy ($_FILES['file5']['tmp_name'], "../img/products/product/".$filename);
			}


		if(!$message) {
			$message = '<div class="flash ok">
							  <div class="top"></div>
							  <div class="text">Existing product has been successfuly saved</div>
							  <div class="bottom"></div>
							</div>';
		}
	}




	if($_GET['action'] == 'bulkedit'){

            


		if(($_POST['edit_category']) OR ($_POST['edit_brand']) OR ($_POST['edit_price'])){

		$values = '';

			if($_POST['edit_category']){
				$values = "category=".$_POST['edit_category'];
			}

			if($_POST['edit_brand']){
				if($values) $values = ", ".$values;
				$values = "brand=".$_POST['edit_brand'];
			}

			if($_POST['edit_price']){
				if($values) $values = ", ".$values;
				$values = "price=".$_POST['edit_price'];
			}


			$q = "SELECT product_id FROM bulk_edit_basket WHERE admin_name='".$admin_name."'";
			$r = mysqli_query($db,$q);
			$c = mysqli_num_rows($r);
			for($i=0; $i<$c; $i++){
				$f = mysqli_fetch_row($r);

				$q_edit = "UPDATE products SET ".$values." WHERE id=".$f[0];
		
                    
		$r_edit = mysqli_query($db,$q_edit) or $message = '<div class="flash error">
																  <div class="top"></div>
																  <div class="text">Error talking to database: '.mysqli_error($db).'</div>
																  <div class="bottom"></div>
																</div>';

			}
		}
         
		if(!$message){
			$message = '<div class="flash ok">
						  <div class="top"></div>
						  <div class="text">Existing products have been successfuly saved</div>
						  <div class="bottom"></div>
						</div>';
		}
	}







	if($_GET['action']=='newproduct'){

		$_POST['name'] = mysqli_real_escape_string($db,$_POST['name']);
		$_POST['desc'] = mysqli_real_escape_string($db,$_POST['desc']);
		$_POST['category'] = mysqli_real_escape_string($db,$_POST['category']);
        
		$q = "SELECT id FROM products WHERE sku='".$_POST['sku']."'";
		$r = mysqli_query($db,$q);
		$c = mysqli_num_rows($r);
		if($c){
			// sku exists already... don't do anything as it was probably just a page refresh after adding a product

		}else{
			//inserting a product
			if(!$_POST['brand']) $_POST['brand'] = '0';
            
            //start: korny ---
            $flds = 'price,price_dollar,price_sterling,price_wholesale,price_wholesale_dollar,price_wholesale_sterling';
            foreach (explode(',',$flds) as $field) {
                if ($_POST[$field] == '')
                    $_POST[$field] = 0;
            }
            //end: korny ---

			$q = "INSERT INTO products (sku, sku2, name, price, description, brand, insert_datetime, price_dollar, price_sterling, price_wholesale, price_wholesale_dollar, price_wholesale_sterling) values('".$_POST['sku']."','".$_POST['sku2']."','".$_POST['name']."','".$_POST['price']."','".$_POST['desc']."',".$_POST['brand'].",now(),'".$_POST['price_dollar']."','".$_POST['price_sterling']."','".$_POST['price_wholesale']."','".$_POST['price_wholesale_dollar']."','".$_POST['price_wholesale_sterling']."')";

			echo($q);

			$r = mysqli_query($db,$q) or die(mysqli_error($db));

			//getting it's ID
			$q = "select id from products where sku='".$_POST['sku']."'";
			$r = mysqli_query($db,$q);
			$f = mysqli_fetch_row($r);
			$product_id = $f[0];



			//inserting 4 basic options

			// STYLE
			$q = "SELECT id FROM product_options_types WHERE name='Style'";
			$r = mysqli_query($db,$q);
			if($f = mysqli_fetch_row($r)){
				$style_id = $f[0];

				// GET value ID
				$q = "SELECT id FROM product_options_values WHERE name='".$_POST['style']."'";
				$r = mysqli_query($db,$q);
				if($f = mysqli_fetch_row($db,$r)){

					$q = "INSERT INTO product_options (product_id, option_value_id, option_type_id) values(".$product_id.",".$f[0].",".$style_id.") ";
					$r = mysqli_query($db,$q);
				}

			}


			// OCCASION
			$q = "SELECT id FROM product_options_types WHERE name='Occasion'";
			$r = mysqli_query($db,$q);
			if ($f = mysqli_fetch_row($r)){
				$occasion_id = $f[0];

				// GET value ID
				$q = "SELECT id FROM product_options_values WHERE name='".$_POST['occasion']."'";
				$r = mysqli_query($db,$q);
				if($f = mysqli_fetch_row($r)){

					$q = "INSERT INTO product_options (product_id, option_value_id, option_type_id) values(".$product_id.",".$f[0].",".$occasion_id.") ";
					$r = mysqli_query($db,$q);
				}
			}



			// MATERIAL
			$q = "SELECT id FROM product_options_types WHERE name='Material'";
			$r = mysqli_query($db,$q);
			if($f = mysqli_fetch_row($r)){
				$material_id = $f[0];

				$q = "INSERT INTO product_options (product_id, option_value_id, option_type_id) values(".$product_id.",".$_POST['material'].",".$material_id.") ";
				$r = mysqli_query($db,$q) or die (mysqli_error($db));

			}



			// SETTING
			$q = "SELECT id FROM product_options_types WHERE name='Setting'";
			$r = mysqli_query($db,$q);
			if($f = mysqli_fetch_row($r)){
				$setting_id = $f[0];

				// GET value ID
				$q = "SELECT id FROM product_options_values WHERE name='".$_POST['setting']."'";
				$r = mysqli_query($db,$q);
				if($f = mysqli_fetch_row($r)){

					$q = "INSERT INTO product_options (product_id, option_value_id, option_type_id) values(".$product_id.",".$f[0].",".$setting_id.") ";
					$r = mysqli_query($db,$q);
				}

			}



			// COLLECTION

			$q = "INSERT INTO product_collections (product_id, collection_id) values(".$product_id.",".$_POST['collection_select'].")";
			$r = mysqli_query($db,$q);


			// IMAGES

			if(file_exists("../img/products/product/".$product_id.".jpg")) unlink("../img/products/product/".$product_id.".jpg");
			if(file_exists("../img/products/product/".$product_id."._2jpg")) unlink("../img/products/product/".$product_id."._2jpg");
			if(file_exists("../img/products/product/".$product_id."._3jpg")) unlink("../img/products/product/".$product_id."._3jpg");
			if(file_exists("../img/products/product/".$product_id."._4jpg")) unlink("../img/products/product/".$product_id."._4jpg");
			if(file_exists("../img/products/product/".$product_id."._5jpg")) unlink("../img/products/product/".$product_id."._5jpg");



			if($_FILES['file']['tmp_name']){
				$filename = $product_id.".jpg";

				//echo('copying '.$_FILES['file']['tmp_name'].'   --->>    '."../img/products/product/".$filename);

				copy ($_FILES['file']['tmp_name'], "../img/products/product/".$filename);



				/*$newwidth = 144;
				list($width, $height) = getimagesize("../img/products/product/".$filename);
				$ratio = $width / $height;
				$newheight = ($newwidth / $width) * $height;

					if(file_exists("../img/products/product/tmb/tmb_".$filename)) unlink("../img/products/product/tmb/tmb_".$filename);


				$thumb = imagecreatetruecolor($newwidth, $newheight);
				$source = imagecreatefromjpeg("../img/products/product/".$filename);
				imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
				imagejpeg($thumb,"../img/products/product/tmb/tmb_".$filename) or die("Couldn't save thumbnail .../img/products/product/tmb/tmb_".$filename);*/

				//echo("<br/>thumb copy --- ../img/products/product/tmb/tmb_".$filename."<br/>");

					//print_r(error_get_last());

				//echo('file 2 = '.$_FILES['file2']['tmp_name']);
			}

			if($_FILES['thumb']['tmp_name']){
				echo('thumbnail found');
				if(file_exists("../img/products/product/tmb/tmb_".$product_id.".jpg")) unlink("../img/products/product/tmb/tmb_".$product_id.".jpg");
				$filename = "tmb_".$product_id.".jpg";
				echo('<br>copying '.$filename);
				copy ($_FILES['thumb']['tmp_name'], "../img/products/product/tmb/".$filename);

			}


			if($_FILES['file2']['tmp_name']){
				$filename = $product_id."_2.jpg";

				copy ($_FILES['file2']['tmp_name'], "../img/products/product/".$filename);

				echo('copying '.$_FILES['file2']['tmp_name'].'   --->>    '."../img/products/product/".$filename) or die(error_get_last());


			}
			if($_FILES['file3']['tmp_name']){
				$filename = $product_id."_3.jpg";
				copy ($_FILES['file3']['tmp_name'], "../img/products/product/".$filename);
			}
			if($_FILES['file4']['tmp_name']){
				$filename = $product_id."_4.jpg";
				copy ($_FILES['file4']['tmp_name'], "../img/products/product/".$filename);
			}
			if($_FILES['file5']['tmp_name']){
				$filename = $product_id."_5.jpg";
				copy ($_FILES['file5']['tmp_name'], "../img/products/product/".$filename);
			}

			$message = '<div class="flash ok">
							  <div class="top"></div>
							  <div class="text">New product has been successfuly saved</div>
							  <div class="bottom"></div>
							</div>';
		}
	}
?>

              <div id="inner"><? echo($message); ?><a href="#" class="add" id="addnew_clicker">Add a new product</a>
           	    <div class="add">
<div class="textbox">

    	            	<form action="?action=newproduct" enctype="multipart/form-data" method="post" id="addform">
                			<table width="100%" border="0" cellspacing="0" cellpadding="0">
                			  <tr>
                			    <td width="27%" valign="top">Product name <br />
                                  <input name="name" id="name" />
                                  <br />
Our Code<br />
<input name="sku" id="sku" />
<br />
Brand Code<br />
<input name="sku2" id="sku2" />
<br />
Price (&euro;) / from price<br /><input name="price" id="price" />
<br />
Price ($) / from price<br /><input name="price_dollar" id="price_dollar" />
<!--<br />
Price (&pound;)<br /><input name="price_sterling" id="price_sterling" />-->
<br />
Price Wholesale (&euro;)<br /><input name="price_wholesale" id="price_wholesale" />
<br />
Price Wholesale ($)<br /><input name="price_wholesale_dollar" id="price_wholesale_dollar" />
<!--<br />
Price Wholesale (&pound;)<br /><input name="price_wholesale_sterling" id="price_wholesale_sterling" />-->
<br />
Add to a collection<br />
<select name="collection_select" id="collection_select" onChange="check_form();">
	<option value="">None</option>
    <?
		$q = "SELECT id, name FROM collections";
		$r = mysqli_query($db,$q);
		$c = mysqli_num_rows($r);
		for($i=0; $i<$c; $i++){
			$f = mysqli_fetch_row($r);
			echo('<option value="'.$f[0].'">'.$f[1].'</option>');
		}
	?>
</select>
<br />
<br />

<?php

$q = "SELECT id
FROM collections";
$r = mysqli_query($db,$q);
$c = mysqli_num_rows($r);
for($i=0; $i<$c; $i++){
	$f = mysqli_fetch_row($r);
}

var_dump($c);

echo $f[0];

 ?>

<div class="red" id="mandatory">- <strong>product name</strong> is mandatory<br />
  - <strong>SKU</strong> is mandatory<br />
  - <strong>SKU</strong> must be unique <br />
- <strong>price</strong> is mandatory<br /></div>

</td>
<td width="31%" valign="top">
<!--Category<br />
  <select name="category" id="category">
     <option value="0">Choose</option>
  <?
  	$q = "SELECT id, name FROM categories WHERE parent_id=0";
	$r = mysqli_query($db,$q);
	$c = mysqli_num_rows($r);
	for($i=0; $i<$c; $i++){
		$f = mysqli_fetch_row($r);
		echo('<option value="'.$f[0].'">'.$f[1].'</option>');

		$q_sub = "SELECT id, name FROM categories WHERE parent_id=".$f[0];
		$r_sub = mysqli_query($db,$q_sub);
		$c_sub = mysqli_num_rows($r_sub);
		for($j=0; $j<$c_sub; $j++){
			$f_sub = mysqli_fetch_row($r_sub);
			echo('<option value="'.$f_sub[0].'">- '.$f_sub[1].'</option>');
		}

	}
  ?>
</select>
<br />-->
Brand<br />
<select name="brand" id="brand">
  <option value="0">Choose</option>
  <?
  	$q = "SELECT id, name FROM brands";
	$r = mysqli_query($db,$q);
	$c = mysqli_num_rows($r);
	for($i=0; $i<$c; $i++){
		$f = mysqli_fetch_row($r);
		echo('<option value="'.$f[0].'">'.$f[1].'</option>');
	}
  ?>
</select>
<br />
Style<br />
<select name="style" id="style">
  <option value="0">Choose</option>
  <?
  	$q = "SELECT id, name FROM product_options_values WHERE type_id=2";
	$r = mysqli_query($db,$q);
	$c = mysqli_num_rows($r);
	for($i=0; $i<$c; $i++){
		$f = mysqli_fetch_row($r);
		echo('<option value="'.$f[0].'">'.$f[1].'</option>');
	}
  ?>
</select>
<br />
Material<br />
<select name="material" id="material">
  <option value="0">Choose</option>
  <?
  	$q = $q = "SELECT id, name FROM product_options_values WHERE type_id=1";
	$r = mysqli_query($db,$q);
	$c = mysqli_num_rows($r);
	for($i=0; $i<$c; $i++){
		$f = mysqli_fetch_row($r);
		echo('<option value="'.$f[0].'">'.$f[1].'</option>');
	}
  ?>
</select>
<br />
Occasion<br />
<select name="occasion" id="occasion">
  <option value="0">Choose</option>
  <?
  	$q = $q = "SELECT id, name FROM product_options_values WHERE type_id=3";
	$r = mysqli_query($db,$q);
	$c = mysqli_num_rows($r);
	for($i=0; $i<$c; $i++){
		$f = mysqli_fetch_row($r);
		echo('<option value="'.$f[0].'">'.$f[1].'</option>');
	}
  ?>
</select>
<br />
Setting<br />
<select name="setting" id="setting">
  <option value="0">Choose</option>
  <?
  	$q = $q = "SELECT id, name FROM product_options_values WHERE type_id=4";
	$r = mysqli_query($db,$q);
	$c = mysqli_num_rows($r);
	for($i=0; $i<$c; $i++){
		$f = mysqli_fetch_row($r);
		echo('<option value="'.$f[0].'">'.$f[1].'</option>');
	}
  ?>
</select></td>
                			    <td width="42%" valign="top">Main image<br />
               			        <input type="file" name="file" id="file" />
               			        <br />
                                Main Thumbnail (142 x 142 px)<br />
               			        <input type="file" name="thumb" id="thumb" />
               			        <br />
               			        image 2<br />
               			        <input type="file" name="file2" id="file2" />
               			        <br />
               			        image 3<br />
               			        <input type="file" name="file3" id="file3" />
                                <br />
               			        image 4<br />
               			        <input type="file" name="file4" id="file4" />
                                <br />
               			        image 5<br />
               			        <input type="file" name="file5" id="file5" />
               			        <br />
               			        <br />
               			        <br />
               			        Product description<br />               			        <textarea name="desc" cols="30" rows="6" id="newdesc"></textarea></td>
              			      </tr>
                			  <tr>
                			    <td><input type="submit" id="submitbtn" value="Save this product" style="width:120px; position:absolute; right:30px; top:60px; background-color:#F90;">
               			        <img src="img/loading.gif" alt="loading" width="16" height="16" class="loading" style="margin-top:3px; display:none;" id="loading" /></td>
                			    <td>&nbsp;</td>
                			    <td>&nbsp;</td>
              			      </tr>
              			  </table><br />
   	            	  </form>
                    <div class="textboxFooter"></div></div></div>

               	<div class="datagrid">
                	  <div style="float:right"><a href="google_csv.php" style="color:#FFF; position:relative; top:10px; right:15px;">All products - CSV for Google</a></div>
<?
						$q_all = "SELECT id FROM products WHERE deleted=0 ORDER BY id DESC";
						$r_all = mysqli_query($db,$q_all);
						$products = mysqli_num_rows($r_all);
?>
                      <h2>Products table (<?=$products?> Products in the database)</h2>
                	  <form method="post" action="">
                	    <table>
                	      <tr>
                	        <th style="text-align:left">&nbsp;</th>
                	        <th><!--Our -->Code<!--<br />
                	          (brand code)--></th>
                	        <th>Price</th>
                            <th>Price</th>
              	        </tr>
                        <?

						if($_GET['search_txt']){
							$search_str = " (name LIKE '%".$_GET['search_txt']."%' OR sku LIKE '%".$_GET['search_txt']."%' OR sku2 LIKE '%".$_GET['search_txt']."%') AND";
						}

						if($_GET['brand_filter']){
							$search_str .= " brand = '".$_GET['brand_filter']."' AND";
						}

						if($_GET['category_filter']){
							$search_str .= " category = '".$_GET['category_filter']."' AND";
						}

							$q = "SELECT id FROM products WHERE".$search_str." deleted=0";
							$r = mysqli_query($db,$q);
							$all_count = mysqli_num_rows($r);
							$all_pages = ceil($all_count / 20);

							$page = 1;
							if($_GET['page']){
								$offset = " OFFSET ".strval((intval($_GET['page'])-1) * 20);
								$page = $_GET['page'];
							}

							$q = "SELECT id, name, sku, category, price, edit_datetime, sku2, purchasable, brand, price_dollar, price_sterling, price_wholesale, price_wholesale_dollar, price_wholesale_sterling, gold_item, allow_info, configurable, is_pendant, insert_datetime, price_old, price_dollar_old, public_sale, public_sale_dollar FROM products WHERE".$search_str." deleted=0 ORDER BY id DESC LIMIT 20".$offset;
							//$q = "SELECT id, name, sku, category, price, edit_datetime, sku2, purchasable, brand, price_dollar, price_sterling, price_wholesale, price_wholesale_dollar, price_wholesale_sterling, gold_item, allow_info, configurable, is_pendant, insert_datetime, price_old, price_dollar_old, public_sale, public_sale_dollar FROM products WHERE".$search_str." deleted=0 ORDER BY id DESC";
							$r = mysqli_query($db,$q);
							$c = mysqli_num_rows($r);

							$sudalicha = 'odd';



							for($i=0; $i<$c; $i++){
								$f = mysqli_fetch_row($r);

								$item_gold = $f[14];
								$item_info = $f[15];
								$item_configurable = $f[16];
								$is_pendant = $f[17];

								$inserted = $f[18];

								if($is_pendant){
									$display_pendant_pricing = 'block';
								}else{
									$display_pendant_pricing = 'none';
								}



								if($sudalicha == 'odd'){
									$sudalicha = 'even';
								}else{
									$sudalicha = 'odd';
								}


								$q_brand = "SELECT name FROM brands WHERE id=".$f[8];
								$r_brand = mysqli_query($db,$q_brand);
								if($f_brand = mysqli_fetch_row($r_brand)){
									$brand = $f_brand[0];
								}else{
									$brand = '';
								}


								$q_cat = "SELECT name FROM categories WHERE id=".$f[3];
								$r_cat = mysqli_query($db,$q_cat);
								$f_cat = mysqli_fetch_row($r_cat);

								if(file_exists('../img/products/product/'.$f[0].'.jpg')){
									$image_str = '<img src="../img/products/product/'.$f[0].'.jpg?x='.strval(rand(1000000,9999999)).'" width="170" style="margin-bottom:20px; clear:both; float:left; margin-left:50px;" />';
								}else{
									$image_str = '';

								}

								$sub_images_str = '';

								if(file_exists("../img/products/product/".$f[0]."_2.jpg")){
								  $sub_images_str = '<br><div style="text-align:center; float:left; width:80px;"><img src="../img/products/product/'.$f[0].'_2.jpg?x='.strval(rand(1000000,9999999)).'" width="80" style="margin-bottom:4px;" alt="'.$f[0].'_2.jpg" /><br/><span style="cursor:pointer; text-decoration:underline;" onclick="ajax_delimg(\''.$f[0].'_2.jpg\')">delete</a></div>';
								}else{
									$sub_images_str = '<br><div style="text-align:center; float:left; width:80px;"><img src="small_noimage.jpg" width="80" style="margin-bottom:4px;" alt="'.$f[0].'_2.jpg" /></div>';
								}

								if(file_exists("../img/products/product/".$f[0]."_3.jpg")){
									$sub_images_str .= '<div style="text-align:center; float:left; width:80px;"><img src="../img/products/product/'.$f[0].'_3.jpg?x='.strval(rand(1000000,9999999)).'" width="80" style="margin-bottom:4px; float:left;" alt="'.$f[0].'_3.jpg" /><br/><span style="cursor:pointer; text-decoration:underline;" onclick="ajax_delimg(\''.$f[0].'_3.jpg\')">delete</a></div>';
								}else{
									$sub_images_str .= '<div style="text-align:center; float:left; width:80px;"><img src="small_noimage.jpg" width="80" style="margin-bottom:4px;" alt="'.$f[0].'_3.jpg" /></div>';
								}

								if(file_exists("../img/products/product/".$f[0]."_4.jpg")){
									$sub_images_str .= '<div style="text-align:center; float:left; width:80px;"><img src="../img/products/product/'.$f[0].'_4.jpg?x='.strval(rand(1000000,9999999)).'" width="80" style="margin-bottom:4px; float:left;" alt="'.$f[0].'_4.jpg" /><br/><span style="cursor:pointer; text-decoration:underline;" onclick="ajax_delimg(\''.$f[0].'_4.jpg\')">delete</a></div>';
								}else{
									$sub_images_str .= '<div style="text-align:center; float:left; width:80px;"><img src="small_noimage.jpg" width="80" style="margin-bottom:4px;" alt="'.$f[0].'_2.jpg" /></div>';
								}

								if(file_exists("../img/products/product/".$f[0]."_5.jpg")){
									$sub_images_str .= '<div style="text-align:center; float:left; width:80px;"><img src="../img/products/product/'.$f[0].'_5.jpg?x='.strval(rand(1000000,9999999)).'" width="80" style="margin-bottom:4px; float:left;" alt="'.$f[0].'_5.jpg" /><br/><span style="cursor:pointer; text-decoration:underline;" onclick="ajax_delimg(\''.$f[0].'_5.jpg\')">delete</a></div>';
								}else{
									$sub_images_str .= '<div style="text-align:center; float:left; width:80px;"><img src="small_noimage.jpg" width="80" style="margin-bottom:4px;" alt="'.$f[0].'_2.jpg" /></div>';
								}

								$edited_time = intval(strtotime($f[5]));
								$now_time = intval(time());
								if(($now_time - $edited_time) < 86400){
									$edited_today = '<span class="edited_today"> (EDITED TODAY)</span>';
								}else{
									$edited_today = '';
								}

								if(strlen($f[2])>10){
									$sku = substr($f[2],0,7).'...';
								}else{
									$sku = $f[2];
								}

								if(strlen($f[1])>40){
									$name = substr($f[1],0,37).'...';
								}else{
									$name = $f[1];
								}

								$options_str = '';

								$q_opt = "SELECT id, option_value_id, option_type_id, sizeadd FROM product_options WHERE product_id=".$f[0];
								$r_opt = mysqli_query($db,$q_opt);
								$c_opt = mysqli_num_rows($r_opt);
								for($i_opt=0; $i_opt<$c_opt; $i_opt++){
									$f_opt = mysqli_fetch_row($r_opt);

									$r_name = mysqli_query($db,"SELECT name FROM product_options_types WHERE id=".$f_opt[2]);
									$f_name = mysqli_fetch_row($r_name);
									$opt_name = $f_name[0];

									$r_value = mysqli_query($db,"SELECT name FROM product_options_values WHERE id=".$f_opt[1]);
									$f_value = mysqli_fetch_row($r_value);
									$value = $f_value[0];

									if($f_opt[3]){
										$sizeadd = ' (+'.$f_opt[3].'&euro;)';
									}else{
										$sizeadd = '';
									}

									$options_str .= '<span><strong>'.$opt_name.'</strong> - '.$value.$sizeadd.'</span><br/>';
								}

								$collection = '';

								$q_opt2 = "SELECT DISTINCT collection_id FROM product_collections WHERE product_id=".$f[0];
								$r_opt2 = mysqli_query($db,$q_opt2);
								$c_opt2 = mysqli_num_rows($r_opt2);
								for($i_opt2 = 0; $i_opt2 < $c_opt2; $i_opt2++){
									$frnda = mysqli_fetch_row($r_opt2);
									$collection_id = $frnda[0];

									$r_col = mysqli_query($db,"SELECT name FROM collections WHERE id=".$collection_id);
									$f_col = mysqli_fetch_row($r_col);
									$collection = $f_col[0];

									$options_str .= 'collection: <strong>'.$collection.'</strong><br/>';
								}


								//wear it with
								$q_wear = "SELECT wear_with FROM wear_it_with WHERE product_id=".$f[0];
								$r_wear = mysqli_query($db,$q_wear);
								$c_wear = mysqli_num_rows($r_wear);
								$options_str .= 'Has <strong>'.$c_wear.'</strong> Wear With links<br/>';


								if(!$options_str){
									$options_str = 'No Aditional Options Found';
								}

								// sale on/off image preparation
								if($f[7]){
									$onoff = "sale_on";
								}else{
									$onoff = "sale_off";
								}

								$name_esc = str_replace("'","&#32;",$name);
								$name_esc = str_replace('"','&#34;',$name_esc);

								if($f[6]){
									$sku2 = "<br/>(".$f[6].")";
								}else{
									$sku2 = "";
								}

								if(!trim($brand)) $brand = "no brand selected";


								$q_new = "SELECT * FROM products WHERE id=".$f[0];
								$r_new = mysqli_query($db,$q_new);
								$f_new = mysqli_fetch_assoc($r_new);


								echo('<tr class="odd" id="tr_'.$f[0].'">
										<td style="cursor:default; text-align:left; padding-top:20px;" alt="'.$f[1].'" title="'.$f[1].'">
										<span class="name">&nbsp;&nbsp;'.$name.'</span> ('.$brand.') '.$edited_today.'
<br/>Inserted: '.$inserted.'&nbsp;&nbsp;&nbsp;'.$f_cat[0].'<br/><img src="img/my_to_bulk.png" style="float:left; cursor:pointer; margin-top:20px;" id="tobulk_'.$f[0].'" onclick="to_bulk(\''.$admin_name.'\','.$f[0].',\''.$name_esc.'\');" />


										'.$image_str.'
										<div style="margin:20px; clear:both;">
											'.$options_str.'
										</div>'.$sub_images_str.'
										<td style="padding-top:20px;">'.$sku.$sku2.'</td>
										<td style="padding-top:20px;"><strong>Retail</strong><br>&euro; '.$f[4].'<br>$ '.$f[9].'
										<br><br>
										<strong>Sale Price (hidden)</strong><br />
										<br>&euro; '.$f[19].'<br>$ '.$f[20].'
										<br><br>
										<strong>Sale Price (public)</strong><br />
										<br>&euro; '.$f[21].'<br>$ '.$f[22].'


										<div style="width:200px;"></div>

										<div id="pendant_pricing_'.$f[0].'" style="display:'.$display_pendant_pricing.';">
											<br><br>
											<strong>Pendant Pricing</strong><br />
											<table cellpadding="2" cellspacing="2">
												<tr>
													<th>Retail</th>
													<th>Wholesale</th>
												</tr>
												<tr>
													<td style="padding-top:0;">2 [&euro;] <input name="2_euro_'.$f[0].'" id="2_euro_'.$f[0].'" style="width:40px;" value="'.$f_new['pendant_price_2'].'" /></td>
													<td style="padding-top:0;">2 [&euro;] <input name="2_euro_wholesale_'.$f[0].'" id="2_euro_wholesale_'.$f[0].'" style="width:40px;" value="'.$f_new['pendant_price_wholesale_2'].'" /></td>
												</tr>
												<tr>
													<td style="padding-top:0;">4 [&euro;] <input name="4_euro_'.$f[0].'" id="4_euro_'.$f[0].'" style="width:40px;" value="'.$f_new['pendant_price_4'].'" /></td>
													<td style="padding-top:0;">4 [&euro;] <input name="4_euro_wholesale_'.$f[0].'" id="4_euro_wholesale_'.$f[0].'" style="width:40px;" value="'.$f_new['pendant_price_wholesale_4'].'" /></td>
												</tr>
												<tr>
													<td style="padding-top:0;">6 [&euro;] <input name="6_euro_'.$f[0].'" id="6_euro_'.$f[0].'" style="width:40px;" value="'.$f_new['pendant_price_6'].'" /></td>
													<td style="padding-top:0;">6 [&euro;] <input name="6_euro_wholesale_'.$f[0].'" id="6_euro_wholesale_'.$f[0].'" style="width:40px;" value="'.$f_new['pendant_price_wholesale_6'].'" /></td>
												</tr>
												<tr>
													<td style="padding-top:0;">2 [$] <input name="2_usd_'.$f[0].'" id="2_usd_'.$f[0].'" style="width:40px;" value="'.$f_new['pendant_price_usd_2'].'" /></td>
													<td style="padding-top:0;">2 [$] <input name="2_usd_wholesale_'.$f[0].'" id="2_usd_wholesale_'.$f[0].'" style="width:40px;" value="'.$f_new['pendant_price_wholesale_usd_2'].'" /></td>
												</tr>
												<tr>
													<td style="padding-top:0;">4 [$] <input name="4_usd_'.$f[0].'" id="4_usd_'.$f[0].'" style="width:40px;" value="'.$f_new['pendant_price_usd_4'].'"  /></td>
													<td style="padding-top:0;">4 [$] <input name="4_usd_wholesale_'.$f[0].'" id="4_usd_wholesale_'.$f[0].'" style="width:40px;" value="'.$f_new['pendant_price_wholesale_usd_4'].'" /></td>
												</tr>
												<tr>
													<td style="padding-top:0;">6 [$] <input name="6_usd_'.$f[0].'" id="6_usd_'.$f[0].'" style="width:40px;" value="'.$f_new['pendant_price_usd_6'].'" /></td>
													<td style="padding-top:0;">6 [$] <input name="6_usd_euro_'.$f[0].'" id="6_usd_wholesale_'.$f[0].'" style="width:40px;" value="'.$f_new['pendant_price_wholesale_usd_6'].'" /></td>
												</tr>
												<tr>
													<td colspan="2" style="padding-top:0;">
														<div style="cursor:pointer; background-color:#5e5e5e; color:#FFFFFF; padding:5px;" onclick="save_pendant_pricing(\''.$f[0].'\');">Save Pricing</div>
													</td>
												</tr>
											</table>
										</div>



										</td><td style="padding-top:20px;"><strong>Wholesale</strong><br>&euro; '.$f[11].'<br>$ '.$f[12].'<div class="my_actions"><br/><a href="../product.php?id='.$f[0].'" target="_new" style="margin-top:15px; float:right;"><img src="img/preview.png" /></a>
											<img src="img/my_delete.png" width="59" height="58" alt="Delete" class="delete_btn" style="cursor:pointer; margin-top:15px; float:right;" onclick="delete_product('.$f[0].');" /><br/>
											<img src="img/my_edit.png" width="59" height="58" alt="Edit" class="edit_btn" style="cursor:pointer; float:right; clear:both;" onclick="edit_product('.$f[0].','.$page.',\''.$_GET['search_txt'].'\',\''.$_GET['brand_filter'].'\',\''.$_GET['category_filter'].'\');" /><br><img src="img/loading_bar.gif" id="edit_loading_'.$f[0].'" style="float:right; margin-right:5px; display:none;" width="50px">



											<img src="img/gold_'.$item_gold.'.png" width="59" height="58" alt="Gold/Silver" class="" style="cursor:pointer; float:right;" onclick="gold_product('.$f[0].');" id="gold_'.$f[0].'" />



											<br/>



											<img src="img/info_'.$item_info.'.png" width="59" height="58" alt="Info" class="" style="cursor:pointer; float:right;" onclick="info_product('.$f[0].');" id="info_'.$f[0].'" />


											<img src="img/pendant_'.$is_pendant.'.png" width="59" height="58" alt="Info" class="" style="cursor:pointer; float:right;" onclick="pendant_product('.$f[0].');" id="pendant_'.$f[0].'" />

											<img src="img/config_'.$item_configurable.'.png" width="59" height="58" alt="Info" class="" style="cursor:pointer; float:right;" onclick="config_product('.$f[0].');" id="config_'.$f[0].'" />



											<img src="img/'.$onoff.'.png" width="59" height="58" alt="Toggle Sale" class="onoff_btn" style="cursor:pointer; float:right; clear:both;" onclick="sale_product('.$f[0].');" id="sale_'.$f[0].'" />

											<img src="img/seo.png" width="59" height="58" alt="SEO" class="onoff_btn" style="cursor:pointer; float:right; clear:both;" onclick=\'window.open("seo.php?product_id='.$f[0].'&ssid='.session_id().'","mywindow","menubar=1,resizable=1,width=880,height=570");\' />


										</div></td>
									</tr>');
							}

						?>
                	    </table>


                	    <div class="datagridActions">
                	      <div class="paginator">

   <?
							if($page == 1){
                                echo('<a href="#" class="active">1</a> <a href="?page=2&search_txt='.$_GET['search_txt'].'&brand_filter='.$_GET['brand_filter'].'&category_filter='.$_GET['category_filter'].'">2</a> <a href="?page=3&search_txt='.$_GET['search_txt'].'&brand_filter='.$_GET['brand_filter'].'&category_filter='.$_GET['category_filter'].'">3</a> ... <a href="?page='.$all_pages.'&search_txt='.$_GET['search_txt'].'&brand_filter='.$_GET['brand_filter'].'&category_filter='.$_GET['category_filter'].'">'.$all_pages.'</a><a href="?page='.strval(($page)+1).'&search_txt='.$_GET['search_txt'].'&brand_filter='.$_GET['brand_filter'].'&category_filter='.$_GET['category_filter'].'">Next &raquo;</a>');
							}

							if(($page > 1) AND($page < $all_pages)){
                                echo('<a href="?page='.strval(intval($page)-1).'&search_txt='.$_GET['search_txt'].'&brand_filter='.$_GET['brand_filter'].'&category_filter='.$_GET['category_filter'].'">&laquo; Previous</a><a href="?page=1&search_txt='.$_GET['search_txt'].'&brand_filter='.$_GET['brand_filter'].'&category_filter='.$_GET['category_filter'].'">1</a> ... <a href="#" class="active">'.$page.'</a> ... <a href="?page='.$all_pages.'&search_txt='.$_GET['search_txt'].'&brand_filter='.$_GET['brand_filter'].'&category_filter='.$_GET['category_filter'].'">'.$all_pages.'</a><a href="?page='.strval(($page)+1).'&search_txt='.$_GET['search_txt'].'&brand_filter='.$_GET['brand_filter'].'&category_filter='.$_GET['category_filter'].'">Next &raquo;</a>');
							}

							if($page == $all_pages){
								echo('<a href="?page='.strval(intval($page)-1).'&search_txt='.$_GET['search_txt'].'&brand_filter='.$_GET['brand_filter'].'&category_filter='.$_GET['category_filter'].'">&laquo; Previous</a><a href="?page=1&search_txt='.$_GET['search_txt'].'&brand_filter='.$_GET['brand_filter'].'&category_filter='.$_GET['category_filter'].'">1</a> ... <a href="#" class="active">'.$all_pages.'</a>');
							}

						  ?>








                          </div>

              	      </div>
              	    </form>
          </div>
              </div>
            </div>
            <hr class="cleaner" />

      <div id="basic-modal-content">
      		<!-- edit form here -->

	  </div>
      <div id="basic-modal-content-2">
      		<!-- edit form here -->

	  </div>


</div>

        <div id="footer">

        </div>
    </body>

<?
	if($_GET['search_txt']){
		echo("<script>
					$('td').highlight('".$_GET['search_txt']."','highlight');
			  </script>");
	}
?>

</html>
