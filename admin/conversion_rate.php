<?
	require('db.php');
	$conv_rate =0.736918; //1USD = 0.736918€ //////Adrien
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Shanore statistics</title>
    <link href="screen.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
</head>

<body style="padding:25px;">
<div class="content">
	<div class="onglet"><a href="stats.php">Statistics</a></div>
	<div class="onglet sel"><a href="#">Fx rates</a></div> 
	<div class="onglet"><a href="visits.php">Visits</a></div> 
	<div class="tab_content" id="tab_content_0">
	<?
		$month_2 = mktime(0,0,0,date('n'), date('j'), date('Y'));
		if(!$_POST['visits_1_from']) $_POST['visits_1_from'] = '01 '.date('M',strtotime("-1 month")).' '.date('Y');
		if(!$_POST['visits_1_to']) $_POST['visits_1_to'] = date('t').' '.date('M').' '.date('Y');
		if(!$_POST['visits_2_from']) $_POST['visits_2_from'] = '01 '.date('M', strtotime("-1 month")).' '.date('Y');
		if(!$_POST['visits_2_to']) $_POST['visits_2_to'] =  date('t', $month_2).' '.date('M').' '.date('Y');
		if(!$_POST['values_per']) $_POST['values_per'] = 'Daily';
		if(!$_POST['values_type']) $_POST['values_type'] = 'Transactions';
	?>
	
	<section id="left_col">
    <form action="conversion_rate.php?tab=0" method="post">
		<h2>Attribute conversion rate to a period</h2>
        <div class="select_area">
           	<h1 style="margin-top:0;">Dollars To Euro </h1>
			<input type="text" id="conv_rate_input" name="conv_rate_input" value="<?=$conv_rate ?>" style="font-size:28px; width:100%;" /><br/>
			<label for='from'><h1>FROM</h1></label>
				<select id='from'  name="month" style="font-size:28px; width:100%;">
					<option value="01">January</option>
					<option value="02">February</option>
					<option value="03">March</option>
					<option value="04">April</option>
					<option value="05">May</option>
					<option value="06">June</option>
					<option value="07">July</option>
					<option value="08">August</option>
					<option value="09">September</option>
					<option value="10">October</option>
					<option value="11">November</option>
					<option value="12">December</option>
				</select>
			<br/>
			<label for='nb'><h1>Number of months</h1></label>
				<select id='nb' name="nb_month" style="font-size:28px; width:100%;">
					<?
						for($m=01; $m<=12; $m++){
							echo "<option value='$m'>$m</option>";
						}
					?>
				</select>
			<br/>
			<label for='y'><h1>Years</h1></label>
				<select id='y' name="year" style="font-size:28px; width:100%;">
					<?
						for($m=-5; $m<=5; $m++){
							if ($m==0){
								echo "<option value='" .(date(Y)+$m) ."' selected=\"selected\">" .(date(Y)+$m) ."</option>";
							} else {
								echo "<option value='" .(date(Y)+$m) ."'>" .(date(Y)+$m) ."</option>";
							}
						}
					?>
				</select>
        </div>
        <input type="submit" value="Submit" />     
    </form>
	
	
	</section>
	<section id ="right_col">
		<table class="rate_table">
			<tr><thead><td>Month</td><td>Year</td><td>Rate</td></thead><tr>
		
<?	
//traitment
	if (isset($_POST['month']) && isset($_POST['nb_month']) && isset($_POST['year']))
	{
		// recup datas
		$rate = floatval($_POST['conv_rate_input']);
		$month = $_POST['month'];
		$nb_month = ($_POST['nb_month']-1);
		$year = $_POST['year'];
		
		$arr = array();
		if ($rate !=0 AND $rate != NULL)
		{
			for($i=$month; $i<=($month+$nb_month); $i++){
				
				//verify and ask for changes (onsubmit javascript)
				/*
				$sql="SELECT rate,YEAR(insert_datetime),MONTH(insert_datetime) FROM calendar_rate WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."'";
				$resp  = mysql_query($sql) or die(mysql_error());
				$res = mysql_num_rows($resp);
				if ($res !=0 || $res != NULL)
				{
					echo "<script>confirm ('There is already a values. Continue?');</script>";
				}
				*/
				
				//del old values
				$sql =" DELETE FROM calendar_rate WHERE MONTH(insert_datetime)='$i' AND YEAR(insert_datetime)='$year';";
				$resp = mysql_query($sql)or die(mysql_error());
				
				//foreach month, a value will be set. The day will be the first, obligatory
				$sql = "INSERT INTO calendar_rate VALUES ('', '$year-$i-01', '$rate');";
				$resp = mysql_query($sql)or die(mysql_error());
				$arr[] = $resp;
			}
		
			$arr = array_unique($arr);
			if (count($arr) ==1 AND $arr[0] ==  "bool(true)")
			{
					echo "<script>alert('Your values have been sent.');</script>";
			}
		}
	}
	
//show
$monthes = array("January", "February", "March", "April", "May", "June", "July","August", "September", "November", "December");
	$sql = "SELECT * FROM calendar_rate ORDER BY insert_datetime;";
	$resp = mysql_query($sql) or die(mysql_error);
	while ($r = mysql_fetch_array($resp))
	{
		$i=intval(substr($r[1],5,2));
		echo "<tr><td>".$monthes[$i]."</td><td>" .substr($r[1],0,4)."</td><td>".$r[2]."</td></tr>";
	}
?>
		</table>
	</section>
    </div>
 
</div>
    
    
</body>
</html>