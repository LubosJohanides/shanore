<?
	require('db.php');
	mysqli_query($db,"SET NAMES UTF8");
	session_start();
	
	//logged?
	if($_GET['action']=='logout'){
		$q = "UPDATE admin SET session_id='' WHERE session_id='".session_id()."'";
		$r = mysqli_query($db,$q);
	}
	
	$q = "select login from admin where session_id='".session_id()."'";
	$r = mysqli_query($db,$q);
	$c = mysqli_num_rows($r);
	if($c){
		$logged = 1;
		$f = mysqli_fetch_row($r);
		$admin_name = $f[0];
	}else{
		$logged = 0;
	}
	
	

?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-language" content="en" />
        <meta name="robots" content="index,follow" />
        <meta name="googlebot" content="index,follow,snippet,archive" />
        <meta name="author" content="ELIVE CZ s.r.o. (c) 2010" />
		<?
			if(!$logged) {
				die('<meta http-equiv="refresh" content="0;url=index.php" />');
				
			}
			
			
			if($_GET['action'] == 'savetext'){
				if($_POST['id']){
					
					$_POST['text'] = addslashes($_POST['text']);
					
					$q = "UPDATE collections SET text='".$_POST['text']."' WHERE id=".$_POST['id'];
					$r = mysqli_query($db,$q);
				}
			}
			
			
		?>


        <link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/smoothness/jquery-ui-1.7.2.custom.css" />

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
        <script type="text/javascript" src="js/jquery.ui.datepicker-cs.js"></script>
        <script type="text/javascript" src="js/jquery.simpleTooltip.js"></script>
        <script type="text/javascript" src="js/jquery.flot.pack.js"></script>
        <!--[if IE]><script language="javascript" type="text/javascript" src="js/excanvas.pack.js"></script><![endif]-->
        <script type="text/javascript" src="js/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="js/submenu.js"></script>
        <script type="text/javascript" src="js/xadmin.js"></script>
        <script type="text/javascript" src="js/admin.js"></script>
        
        

        <title>Shanore Admin</title>
    </head>
    <body>
        <div id="header">
            <h1>ShanOre.com</h1>
            <div id="user"><a href="?action=logout">Logout</a>

                <br />
            </div>
            <div id="mainMenuWrapper">
              <ul id="mainMenu">
                <li class="active"><a href="admin.php">Catalog</a></li>
                <li><a href="orders.php">ORDERS</a></li>
                <li><a href="customers.php">Customers</a><a href="editor.html"></a></li>
                <li><a href="affiliates.php">Affiliates</a><a href="editor.html"></a></li>
              </ul>
            </div>
        </div>
        <div id="contentWrapper">
          <div id="leftMenu"> <strong>Menu</strong>
            <div id="menu">
              <ul>
                <li><a href="admin.php" class="item">Products</a></li>
                <li> <a href="#" class="item">Product Options</a>
                  <ul>
                    <li><a href="options.php?what=categories">Categories</a></li>
                    <li><a href="options.php?what=brands">Brands</a></li>
                    <?
									$q = "SELECT name, id FROM product_options_types";
									$r = mysqli_query($db,$q);
									$c = mysqli_num_rows($r);
									for($i=0; $i<$c; $i++){
										$f = mysqli_fetch_row($r);
										echo('<li><a href="options.php?what='.$f[1].'&multi=yes">'.$f[0].'</a></li>');
									}
								?>
                  </ul>
                </li>
                <li class="active"> <a href="#" class="item" id="collections_menu">Collections</a>
                  <ul>
                    <li><a href="categories-seo.php">CATEGORIES SEO</a></li><hr />
                    <li><a href="collections-assign.php">COLLECTIONS ADMIN</a></li><hr />
                    <?
									$q = "SELECT name, id FROM collections";
									$r = mysqli_query($db,$q);
									$c = mysqli_num_rows($r);
									for($i=0; $i<$c; $i++){
										$f = mysqli_fetch_row($r);
										echo('<li><a href="collections.php?what='.$f[1].'">'.$f[0].'</a></li>');
									}
								?>
                  </ul>
                </li>
                <li><a href="brands.php" class="item">Brands</a></li>
                <li><a href="deleted_products.php" class="item">Deleted Products</a></li>
                <li><a href="pages.php" class="item">Pages</a></li>
                <li><a href="currencies.php" class="item">Currencies</a></li>
                <li><a href="images.php" class="item">Images</a></li>
                <li><a href="sort.php" class="item">Sort Products</a></li>
              </ul>
            </div>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <strong> </strong></div>
          <div id="content">
              <div id="inner"><a href="#" class="add" id="addnew_clicker">Add a new Collection</a>
           	    <div class="add">
<div class="textbox">
                    
    	            	<form action="?&action=add" enctype="multipart/form-data" method="post" id="addform">
                			<table width="100%" border="0" cellspacing="0" cellpadding="0">
                			  <tr>
                			    <td valign="top">New collection name <br />
                                  <input name="name" />
                                  <input type="submit" value="Add" /></td>
                			    <td width="37%" valign="top">&nbsp;</td>
              			      </tr>
              			  </table>
  </form>
                    <div class="textboxFooter"></div></div></div>
         
         <?
		 		
		 		if($_GET['action'] == 'add'){
						if($_POST['name']){
							$q = "INSERT INTO collections (name) values('".mysqli_real_escape_string($db,$_POST['name'])."')";
							$r = mysqli_query($db,$q) or die('<div class="flash error">
													  <div class="top"></div>
													  <div class="text">'.mysqli_error($db).'</div>
													  <div class="bottom"></div>
													</div>');
													
							//GET ID for the redirect
							$q = "SELECT id FROM collections WHERE name='".$_POST['name']."'";
							$r = mysqli_query($db,$q);
							$f = mysqli_fetch_row($r);
							$id = $f[0];
														
							echo('<script>window.location = "collections.php?what='.$id.'&action=added"</script>');
						}else{
							echo('<div class="flash error">
								  <div class="top"></div>
								  <div class="text">No name inserted</div>
								  <div class="bottom"></div>
								</div>');
						}
					
				}
				
		 ?>
  
<div class="datagrid">
<h2>Collections administration</h2>
                	    <table>
                	      <tr>
                	        <th>Collection name<br />
                	          (display name)</th>
                	        <th>Belong to categories</th>
                	        <th>Belong to brands</th>
              	          </tr>
                        
                        
                     	<?
						$q = "SELECT id, name, display_name, text FROM collections";
						$r = mysqli_query($db,$q);
						$c = mysqli_num_rows($r);
						$sudalicha = 'odd';
						for($i=0; $i<$c; $i++){
							$f = mysqli_fetch_row($r);
							$collection_id = $f[0];
							$collection_name = $f[1];
							$display_name = $f[2];
							$text = $f[3];
							
							// GET CATEGORY ASSIGNMENTS
							$list = '';
							$q_prod = "SELECT category_id FROM category_collection WHERE collection_id=".$f[0];
							$r_prod = mysqli_query($db,$q_prod);
							$c_prod = mysqli_num_rows($r_prod);
							for($j=0; $j<$c_prod; $j++){
								$f_prod = mysqli_fetch_row($r_prod);
								$category_id = $f_prod[0];
								
								// GET CAT NAME
								$q_cat = "SELECT name FROM categories WHERE id=".$category_id;
								$r_cat = mysqli_query($db,$q_cat);
								$f_cat = mysqli_fetch_row($r_cat);
								$category_name = $f_cat[0];
								
								$list .= '<span id="assign_'.$category_id.'_'.$collection_id.'"><img src="img/datagridDel.png" style="cursor:pointer;" onclick="delete_collection_assignment('.$category_id.','.$collection_id.');">&nbsp;<strong>'.$category_name.'<br/></span>';
							}
							
							// GET BRAND ASSIGNMENTS
							$listb = '';
							$q_prod = "SELECT brand_id FROM brand_collection WHERE collection_id=".$f[0];
							$r_prod = mysqli_query($db,$q_prod);
							$c_prod = mysqli_num_rows($r_prod);
							for($j=0; $j<$c_prod; $j++){
								$f_prod = mysqli_fetch_row($r_prod);
								$category_id = $f_prod[0];
								
								// GET CAT NAME
								$q_cat = "SELECT name FROM brands WHERE id=".$category_id;
								$r_cat = mysqli_query($db,$q_cat);
								$f_cat = mysqli_fetch_row($r_cat);
								$category_name = $f_cat[0];
								
								$listb .= '<span id="assignb_'.$category_id.'_'.$collection_id.'"><img src="img/datagridDel.png" style="cursor:pointer;" onclick="delete_collection_assignmentb('.$category_id.','.$collection_id.');">&nbsp;<strong>'.$category_name.'<br/></span>';
							}
							
							
							
							$options = '';
							$q_cat = "SELECT name, id FROM categories";
							$r_cat = mysqli_query($db,$q_cat);
							$c_cat = mysqli_num_rows($r_cat);
							for ($k=0; $k<$c_cat; $k++){
								$f_cat = mysqli_fetch_row($r_cat);
								$options .= '<option value="'.$f_cat[1].'">'.$f_cat[0].'</option>';
							}
							
							$optionsb = '';
							$q_cat = "SELECT name, id FROM brands";
							$r_cat = mysqli_query($db,$q_cat);
							$c_cat = mysqli_num_rows($r_cat);
							for ($k=0; $k<$c_cat; $k++){
								$f_cat = mysqli_fetch_row($r_cat);
								$optionsb .= '<option value="'.$f_cat[1].'">'.$f_cat[0].'</option>';
							}
							
							
							$form = '<select id="collection_select_'.$collection_id.'" style="width:150px;"><option value="">select...</option>'.$options.'</select><img src="img/submit_btn.png" onclick="assign_collection(\''.$collection_id.'\');" style="position:relative; top:7px; cursor:pointer; margin-left:10px;" />';
							
							$formb = '<select id="collection_selectb_'.$collection_id.'" style="width:150px;"><option value="">select...</option>'.$optionsb.'</select><img src="img/submit_btn.png" onclick="assign_collectionb(\''.$collection_id.'\');" style="position:relative; top:7px; cursor:pointer; margin-left:10px;" />';
														
							echo('<tr class="odd">
									<td><h3 style="color:red; margin-bottom:0;">'.$collection_name.'</h3>
									
									<img src="img/seo.png" width="59" height="58" alt="SEO" class="onoff_btn" style="cursor:pointer; float:right; clear:both;" onclick=\'window.open("seo.php?collection_id='.$collection_id.'&ssid='.session_id().'&intro=1","mywindow","menubar=1,resizable=1,width=880,height=570");\' />
									
									
									</td>
									<td class="actions"><div id="collection_list_'.$collection_id.'">'.$list.'</div>'.$form.'</td>
									<td class="actions"><div id="collection_listb_'.$collection_id.'">'.$listb.'</div>'.$formb.'</td>
								</tr>
								<tr>
									<td colspan="3">
										<form action="?action=savetext" method="post" enctype="multipart/form-data">
											Introductory Text<br />
											<textarea name="text" style="width:720px; height:120px;">'.$text.'</textarea><br />
											<input type="hidden" name="id" value="'.$collection_id.'" />
											<input type="submit" value="save" />
										</form>
									</td>
								</tr>
								
								');
						}
						
							
						?>
                	      
                        
                        
              	      </table>
          </div>
              </div>
            </div>
            <hr class="cleaner" />
        </div>

        <div id="footer">

        </div>
    </body>
</html>
