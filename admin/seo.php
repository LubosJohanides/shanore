<?
	require('db.php');
	mysqli_query($db,"SET NAMES UTF8");
		
	$ssid = $_GET['ssid'];
	
	
	//logged?
	if($_GET['action']=='logout'){
		$q = "UPDATE admin SET session_id='' WHERE session_id='".$ssid."'";
		$r = mysqli_query($db,$q);
	}
	
	$q = "select login from admin where session_id='".$ssid."'";
	$r = mysqli_query($db,$q);
	$c = mysqli_num_rows($r);
	if($c){
		$logged = 1;
		$f = mysqli_fetch_row($r);
		$admin_name = $f[0];
	}else{
		$logged = 0;
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SEO settings</title>
<style type="text/css">
body {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	background-color: #CEE6EA;
}
</style>
</head>

<body>
<?
	if($logged){
		
		// ************************ PRODUCT *********************
		if($_GET['product_id']){
			$product_id = $_GET['product_id'];
			if($_GET['action'] == 'save'){
				$q_save = "UPDATE products SET seo_name='".mysqli_real_escape_string($db,$_POST['page_title'])."', seo_description='".mysqli_real_escape_string($db,$_POST['meta_description'])."', seo_keywords='".mysqli_real_escape_string($db,$_POST['keywords'])."', img_alt='".mysqli_real_escape_string($db,$_POST['img_alt'])."' WHERE id=".$product_id;
				$r_save = mysqli_query($db,$q_save) or die(mysqli_error($db));
				$message = "VALUES SAVED. You can close this window now.";
			}
			
			$q = "SELECT sku, sku2, name, price, description, category, brand, purchasable, seo_name, seo_description, seo_keywords, img_alt FROM products WHERE id=".$product_id;
			$r = mysqli_query($db,$q);
			$f = mysqli_fetch_row($r);
			
			//get category
			$q_cat = "SELECT name FROM categories WHERE id=".$f[5];
			$r_cat = mysqli_query($db,$q_cat);
			$f_cat = mysqli_fetch_row($r_cat);
			$product_category = $f_cat[0];
			
			//get brand
			$q_cat = "SELECT name FROM brands WHERE id=".$f[6];
			$r_cat = mysqli_query($db,$q_cat);
			$f_cat = mysqli_fetch_row($r_cat);
			$product_brand = $f_cat[0];
			
			$title = $f[8];
			$title_default = strip_tags($f[2].' - '.$product_category.' from '.$product_brand);
			$keywords = $f[10];
			$keywords_default = strip_tags($f[2].','.$product_category.','.$product_brand);
			$description = $f[9];
			$description_default = strip_tags($f[4]);
			$alt_tag = $f[11];
			$alt_tag_default = $product_brand." ".$f[2];
			$alt_tag_disable = '';
		}
		
		// ************************ BRAND *********************
		if($_GET['brand_id']){
			$brand_id = $_GET['brand_id'];
			
			if($_GET['action'] == 'save'){
				$q_save = "UPDATE brands SET seo_name='".$_POST['page_title']."', seo_description='".$_POST['meta_description']."', seo_keywords='".$_POST['keywords']."', img_alt='".$_POST['img_alt']."' WHERE id=".$brand_id;
				$r_save = mysqli_query($db,$q_save) or die(mysqli_error($db));
				$message = "VALUES SAVED. You can close this window now.";
			}
			
			$q = "SELECT seo_name, seo_description, seo_keywords, img_alt, name FROM brands WHERE id=".$brand_id;
			$r = mysqli_query($db,$q);
			$f = mysqli_fetch_row($r);
			
			$title = $f[0];
			$title_default = strip_tags('Shanore Jewellery - '.$f[4]);
			$keywords = $f[2];
			$keywords_default = strip_tags($f[4].'Shanore, Tara\'s Diary, Jewellers, earrings, bracelets, rings, diamonds, gold');
			$description = $f[1];
			$description_default = strip_tags($f[4].' in Shanore Jewellery, uniques stores filled with engagement rings, diamond rings, wedding bands, all top branded jewellery.');
			$alt_tag = $f[3];
			$alt_tag_default = $f[4];
			$alt_tag_disable = '';
		}
		
		if($_GET['collection_id']){
			$collection_id = $_GET['collection_id'];
			
			if($_GET['action'] == 'save'){
				$q_del = "DELETE FROM seo_collections WHERE collections_id=".$collection_id;
				$r_del = mysqli_query($db,$q_del) or die(mysqli_error($db));
				
				
				$q_save = "INSERT INTO seo_collections (title, description, keywords, collections_id) VALUES('".$_POST['page_title']."', '".$_POST['meta_description']."', '".$_POST['keywords']."', '".$collection_id."' )";
				$r_save = mysqli_query($db,$q_save) or die(mysqli_error($db));
				$message = "VALUES SAVED. You can close this window now.";
			}
			
			
			$q = "SELECT title, description, keywords FROM seo_collections WHERE collections_id=".$collection_id;
			$r = mysqli_query($db,$q);
			$f = mysqli_fetch_row($r);
			
			$title = $f[0];
			$title_default = '';
			$keywords = $f[2];
			$keywords_default = '';
			$description = $f[1];
			$description_default = '';
			$alt_tag = $f[3];
			$alt_tag_default = $f[4];
			$alt_tag_disable = '';
			
			
		}
		
?>
<form action="?action=save&product_id=<? echo($product_id); ?>&ssid=<? echo($ssid); ?>&brand_id=<? echo($brand_id); ?>&collection_id=<? echo($collection_id); ?>"  enctype="multipart/form-data" method="post">
<table width="800" border="0" cellspacing="7" cellpadding="5">
  <tr>
    <td height="33"><h3>SEO settings: </h3>
      <span style="font-size:12px;">Leave the fields empty to use the default values.</span></td>
    <td height="33"><h3>Default Values</h3></td>
  </tr>
  <tr>
    <td width="400">Page Title<br /><input name="page_title" style="width:400px;" value="<? echo($title); ?>" /></td>
    <td width="359" valign="middle"><br />      <? echo($title_default); ?></td>
  </tr>
  <tr>
    <td><br />
      Keywords (comma separated)<br /><input name="keywords" style="width:400px;"  value="<? echo($keywords); ?>" /></td>
    <td width="359" valign="middle"><br />      
    <? echo($keywords_default); ?></td>
  </tr>
  <tr>
    <td><br />
      META description<br /><textarea name="meta_description" style="width:400px;"><? echo($description); ?></textarea></td>
    <td width="359" valign="middle"><br />      <? echo($description_default); ?></td>
  </tr>
  <tr>
    <td>Product Image ALT tag<br />
      <input name="img_alt" style="width:400px;" <? echo($alt_tag_disable); ?>  value="<? echo($alt_tag); ?>" /></td>
    <td valign="middle"><? echo($alt_tag_default); ?></td>
  </tr>
  <tr>
    <td align="right">
    <?
	
		if($message){
			echo($message.'<br>');
		}
	
	?>
    <input type="submit" value="Save" /></td>
    <td width="359" valign="middle">&nbsp;</td>
  </tr>
</table>
</form>
<?	
	}else{
		echo('NOT LOGED IN');
	}
?>
</body>
</html>