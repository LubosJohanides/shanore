<?
	require('db.php');
	session_start();
	
	//logged?
	if($_GET['action']=='logout'){
		$q = "UPDATE admin SET session_id='' WHERE session_id='".session_id()."'";
		$r = mysqli_query($db,$q);
	}
	
	$q = "select login from admin where session_id='".session_id()."'";
	$r = mysqli_query($db,$q);
	$c = mysqli_num_rows($r);
	if($c){
		$logged = 1;
		$f = mysqli_fetch_row($r);
		$admin_name = $f[0];
	}else{
		$logged = 0;
	}
	
	

?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-language" content="en" />
        <meta name="robots" content="index,follow" />
        <meta name="googlebot" content="index,follow,snippet,archive" />
        <meta name="author" content="ELIVE CZ s.r.o. (c) 2010" />
		<?
			if(!$logged) {
				die('<meta http-equiv="refresh" content="0;url=index.php" />');
				
			}
		?>


        <link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/smoothness/jquery-ui-1.7.2.custom.css" />

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
        <script type="text/javascript" src="js/jquery.ui.datepicker-cs.js"></script>
        <script type="text/javascript" src="js/jquery.simpleTooltip.js"></script>
        <script type="text/javascript" src="js/jquery.flot.pack.js"></script>
        <!--[if IE]><script language="javascript" type="text/javascript" src="js/excanvas.pack.js"></script><![endif]-->
        <script src="js/jquery.wysiwyg.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/jquery.wysiwyg.controls.js" type="text/javascript" charset="utf-8"></script>
        <script type="text/javascript" src="js/jquery.highlight-1.1.source.js"></script>

<script type="text/javascript" src="js/submenu.js"></script>
        <script type="text/javascript" src="js/xadmin.js"></script>
        <script type="text/javascript" src="js/jquery.simplemodal.js"></script>
    <script type="text/javascript" src="js/admin.js"></script>
        
        

        <title>Shanore Admin</title>
    </head>
    <body>
        <div id="header">
            <h1>ShanOre.com</h1>
            <div id="user"><a href="?action=logout">Logout</a>

                <br />
            </div>
            <div id="mainMenuWrapper">
              <ul id="mainMenu">
                <li class="active"><a href="admin.php">Catalog</a></li>
                <li><a href="orders.php">ORDERS</a></li>
                <li><a href="customers.php">Customers</a><a href="editor.html"></a></li>
                <li><a href="affiliates.php">Affiliates</a><a href="editor.html"></a></li>
              </ul>
            </div>
        </div>
        <div id="contentWrapper" style="width:1200px;">
          <div id="leftMenu"> <strong>Menu</strong>
            <div id="menu">
              <ul>
                <li><a href="admin.php" class="item">Products</a></li>
                <li> <a href="#" class="item">Product Options</a>
                  <ul>
                    <li><a href="options.php?what=categories">Categories</a></li>
                    <li><a href="options.php?what=brands">Brands</a></li>
                    <?
									$q = "SELECT name, id FROM product_options_types";
									$r = mysqli_query($db,$q);
									$c = mysqli_num_rows($r);
									for($i=0; $i<$c; $i++){
										$f = mysqli_fetch_row($r);
										echo('<li><a href="options.php?what='.$f[1].'&multi=yes">'.$f[0].'</a></li>');
									}
								?>
                  </ul>
                </li>
                <li> <a href="#" class="item" id="collections_menu">Collections</a>
                  <ul>
                  	<li><a href="categories-seo.php">CATEGORIES SEO</a></li><hr />
                    <li><a href="collections-assign.php">COLLECTIONS ADMIN</a></li><hr />
                    <?
									$q = "SELECT name, id FROM collections";
									$r = mysqli_query($db,$q);
									$c = mysqli_num_rows($r);
									for($i=0; $i<$c; $i++){
										$f = mysqli_fetch_row($r);
										echo('<li><a href="collections.php?what='.$f[1].'">'.$f[0].'</a></li>');
									}
								?>
                  </ul>
                </li>
                <li><a href="brands.php" class="item">Brands</a></li>
                <li><a href="deleted_products.php" class="item">Deleted Products</a></li>
                <li><a href="pages.php" class="item">Pages</a></li>
                <li><a href="currencies.php" class="item">Currencies</a></li>
                <li><a href="images.php" class="item">Images</a></li>
                <li class="active"><a href="sort.php" class="item">Sort Products</a></li>
                <li> <a href="#" class="item" id="pdf_menu">PDF catalogues</a>
                  <ul>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=1" target="_new">Engagement Rings</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=10" target="_new">Wedding Rings</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=96" target="_new">Claddagh Rings</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=94" target="_new">Celtic Rings</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=16" target="_new">Pendants</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=37" target="_new">Celtic Crosses</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=95" target="_new">Bracelets</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=84" target="_new">Earrings</a></li>

                                
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=85" target="_new">Tara Beads</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=91" target="_new">Tara Charms</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=83" target="_new">Tara Bracelets</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=92" target="_new">Tara Necklaces</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=93" target="_new">Tara Stacking Rings</a></li>

                            </ul>
                </li>
              </ul>
            </div>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <strong> </strong></div>
          <div id="content">
              <div id="inner">
  
<div class="datagrid" style="width:850px;">
<h2 style="background-color:#666">Sort Products</h2>
<form action="" method="post" enctype="multipart/form-data">
	<select name="collections">
    	<option value="">Select Collection</option>
<?
		$q = "SELECT id, name FROM collections ORDER BY name";
		$r = mysqli_query($db,$q);
		$c = mysqli_num_rows($r);
		for($i=0; $i<$c; $i++){
			$f = mysqli_fetch_row($r);
			$select = '';
			if($_POST['collections'] == $f[0]) $select = ' SELECTED';
			
			echo('<option value="'.$f[0].'"'.$select.'>'.$f[1].'</option>');
			
		}
?>
    </select>
    <input type="submit" value="GO" />
</form>

<form action="" method="post" enctype="multipart/form-data">
	<select name="categories">
    	<option value="">Select Category</option>
<?
		$q = "SELECT id, name FROM categories ORDER BY name";
		$r = mysqli_query($db,$q);
		$c = mysqli_num_rows($r);
		for($i=0; $i<$c; $i++){
			$f = mysqli_fetch_row($r);
			$select = '';
			if($_POST['categories'] == $f[0]) $select = ' SELECTED';
			
			echo('<option value="'.$f[0].'"'.$select.'>'.$f[1].'</option>');
			
		}
?>
    </select>
    <input type="submit" value="GO" />
</form>

<script type="text/javascript">
$(document).ready(function(){ 
						   
	$(function() {
		$("#sort").sortable({ opacity: 0.6, cursor: 'move', update: function() {
			var order = $(this).sortable("serialize") + '&action=updateRecordsListings'; 
			$.post("ajax_save_collection_order.php?collection=<? echo($_POST['collections']); ?>&category=<? echo($_POST['categories']); ?>", order, function(theResponse){
				//alert(theResponse);
			}); 															 
		}								  
		});
	});

});	
</script>

                	    
                        
<?
		if($_POST['collections']){
			$q = "SELECT products.id, products.name, products.sku, product_collections.id FROM products INNER JOIN product_collections ON product_collections.product_id = products.id WHERE product_collections.collection_id=".$_POST['collections'].' AND products.deleted=0 ORDER BY product_collections.sort';
			$r = mysqli_query($db,$q) or die(mysqli_error($db));
			$c = mysqli_num_rows($r);
			echo('<ul id="sort" style="padding:0; margin:0;">');
			for($i=0; $i<$c; $i++){
				$f = mysqli_fetch_row($r);
				echo('<li id="collection-'.$f[3].'" style="background-color:#cee6ea; margin: 5px; padding: 5px; float:left; width:220px; height:240px; text-align:center; list-style:none; cursor:move;">
					'.$f[1].'<br/>
					<img src="../products_images/tmb/tmb_'.$f[0].'.jpg" width="150" />
					<br/>'.$f[2].'
				</li>');
			}
			echo('</ul><br style="clear:both;" />');
		}
		
?>


<?
		if($_POST['categories']){
			
			$q = "SELECT id, name, sku FROM products WHERE category=".$_POST['categories'].' AND deleted=0 ORDER BY sort';
			$r = mysqli_query($db,$q) or die(mysqli_error($db));
			$c = mysqli_num_rows($r);
			echo('<ul id="sort" style="padding:0; margin:0;">');
			
			
			for($i=0; $i<$c; $i++){
				
				$f = mysqli_fetch_row($r);
				echo('<li id="category-'.$f[0].'" style="background-color:#cee6ea; margin: 5px; padding: 5px; float:left; width:220px; height:240px; text-align:center; list-style:none; cursor:move;">
					'.$f[1].'<br/>
					<img src="../products_images/tmb/tmb_'.$f[0].'.jpg" width="150" />
					<br/>'.$f[2].'
				</li>');
			}
			echo('</ul><br style="clear:both;" />');
		}
		
?>
	





<?

	if($_GET['action'] == 'upload'){
		
		$id = $_POST['id'];
		$link = $_POST['link'];
		$text = $_POST['text'];
		
		$q = "UPDATE page_images SET text='".$text."', link='".$link."' WHERE id=".$id;
		$r = mysqli_query($db,$q) or die(mysql_error($db));
		
		$q = "SELECT file FROM page_images WHERE id=".$id;
		$r = mysqli_query($db,$q);
		$f = mysqli_fetch_row($r);
		
		echo('<script type="text/javascript">alert("saved");</script>');
		
		if($_FILES['image']['tmp_name']){

			if(file_exists("../images_configurable/".$f[0])) unlink("../images_configurable/".$f[0]);
			
			copy ($_FILES['image']['tmp_name'], "../images_configurable/".$f[0]);
			
		}
		
	}



	$q = "SELECT id, file, text, link, name FROM page_images WHERE page='front' ORDER BY sort";
	$r = mysqli_query($db,$q);
	$c = mysqli_num_rows($r);
	for($i=0; $i<$c; $i++){
		$f = mysqli_fetch_row($r);
		
		echo('<li id="'.$f[4].'" style="background-color:#cee6ea; margin: 5px; padding: 5px; float:left; width:220px; height:240px; text-align:center; list-style:none;">
				<img src="../images_configurable/'.$f[0].'.jpg?x='.rand().'" height="100" /><br/>
				
				<form action="?action=upload" enctype="multipart/form-data" method="post">
					<table cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td>caption:</td>
							<td><input name="text" value="'.$f[2].'" /></td>
						</tr>
						<tr>
							<td>link:</td>
							<td><input name="link" value="'.$f[3].'" /></td>
						</tr>
						<tr>
							<td colspan="2"><input type="file" name="image" /><input type="hidden" name="id" value="'.$f[0].'" /></td>
						</tr>
						<tr>
							<td colspan="2"><input type="submit" value="upload / save"></td>
						</tr>
						
					</table>
				</form>
			</li>');
		
	}

?>

</ul>

                	      
                        
          </div>
              </div>
            </div>
            <hr class="cleaner" />
        </div>

        <div id="footer">

        </div>
    </body>
</html>
