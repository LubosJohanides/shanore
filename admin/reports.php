<?
	require('db.php');

	session_start();

	require('benefit_func.php');
	
	//logged?
	if($_GET['action']=='logout'){
		$q = "UPDATE admin SET session_id='' WHERE session_id='".session_id()."'";
		$r = mysql_query($q);
	}
	
	$q = "select login from admin where session_id='".session_id()."'";
	$r = mysqli_query($db,$q);
	$c = mysqli_num_rows($r);
	if($c){
		$logged = 1;
		$f = mysqli_fetch_row($r);
		$admin_name = $f[0];
	}else{
		$logged = 0;
	}
	
	

?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-language" content="en" />
        <meta name="robots" content="index,follow" />
        <meta name="googlebot" content="index,follow,snippet,archive" />
        <meta name="author" content="ELIVE CZ s.r.o. (c) 2010" />
		<?
			if(!$logged) {
				die('<meta http-equiv="refresh" content="0;url=index.php" />');
				
			}
		?>


        <link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/smoothness/jquery-ui-1.7.2.custom.css" />

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
        <script type="text/javascript" src="js/jquery.ui.datepicker-cs.js"></script>
        <script type="text/javascript" src="js/jquery.simpleTooltip.js"></script>
        <script type="text/javascript" src="js/jquery.flot.pack.js"></script>
        <!--[if IE]><script language="javascript" type="text/javascript" src="js/excanvas.pack.js"></script><![endif]-->
        <script type="text/javascript" src="js/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="js/submenu.js"></script>
        <script type="text/javascript" src="js/xadmin.js"></script>
        <script type="text/javascript" src="js/admin.js"></script>

<script type="text/javascript">
	function printDiv(){
	  var divToPrint=document.getElementById('datagrid');
	  newWin= window.open("");
	  newWin.document.write(divToPrint.outerHTML);
	  newWin.print();
	  newWin.close();
	}
</script>       

        <title>Shanore Admin</title>
    </head>
    <body>
        <div id="header">
            <h1>ShanOre.com</h1>
            <div id="user"><a href="?action=logout">Logout</a>

                <br />
            </div>
            <div id="mainMenuWrapper">
              <ul id="mainMenu">
                <li class="active"><a href="admin.php">Catalog</a></li>
                <li><a href="orders.php">ORDERS</a></li>
                <li><a href="customers.php">Customers</a><a href="editor.html"></a></li>
                <li><a href="affiliates.php">Affiliates</a><a href="editor.html"></a></li>
              </ul>
            </div>
        </div>
        <div id="contentWrapper" style="width:1250px;">
            <div id="leftMenu">
                <strong>Menu</strong>
                <div id="menu">
                  <ul>
                    <li><a href="admin.php" class="item">Products</a></li>
                    <li> <a href="#" class="item">Product Options</a>
                      <ul>
                        <li><a href="options.php?what=categories">Categories</a></li>
                        <li><a href="options.php?what=brands">Brands</a></li>
                        <?
									$q = "SELECT name, id FROM product_options_types";
									$r = mysqli_query($db,$q);
									$c = mysqli_num_rows($r);
									for($i=0; $i<$c; $i++){
										$f = mysqli_fetch_row($r);
										echo('<li><a href="options.php?what='.$f[1].'&multi=yes">'.$f[0].'</a></li>');
									}
								?>
                      </ul>
                    </li>
                    <li> <a href="#" class="item" id="collections_menu">Collections</a>
                      <ul>
                      	<li><a href="categories-seo.php">CATEGORIES SEO</a></li><hr />
                        <li><a href="collections-assign.php">COLLECTIONS ADMIN</a></li><hr />
                        <?
									$q = "SELECT name, id FROM collections";
									$r = mysqli_query($db,$q);
									$c = mysqli_num_rows($r);
									for($i=0; $i<$c; $i++){
										$f = mysqli_fetch_row($r);
										echo('<li><a href="collections.php?what='.$f[1].'">'.$f[0].'</a></li>');
									}
								?>
                      </ul>
                    </li>
                    <li><a href="brands.php" class="item">Brands</a></li>
                    <li><a href="deleted_products.php" class="item">Deleted Products</a></li>
                    <li><a href="pages.php" class="item">Pages</a></li>
                    <li><a href="currencies.php" class="item">Currencies</a></li>
                    <li><a href="images.php" class="item">Images</a></li>
                    <li><a href="sort.php" class="item">Sort Products</a></li>
                    <li class="active"><a href="reports.php" class="item">Reports</a></li>
                    
                  </ul>
                </div>
              <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <div class="contact">
                    <strong>Support</strong>
                    <address>
                        Matrix Internet Solutions<br />
                        <a href="http://www.matrixinternet.ie/">www.matrixinternet.ie</a><br />
                        <br />
                        info@matrixinternet.ie<br />
                        +353 (0) 1 6711884
                  </address>
              </div>
            </div>
            <div id="content">
              <div id="inner">
                <div class="datagrid" style="width:900px;" id="datagrid">
<h2 style="background-color:#5e5e5e"><form action="" method="get" enctype="multipart/form-data" id="filterfrm">
						<!-- Year: <select name="year" id="year" onchange="$('#filterfrm').submit();" style="position:relative; top:10px;">
                        		<option <?php //if ($_GET['year'] == ""){ echo ' selected="selected"'; } ?> value="" SELECTED>Select Year</option>
								<option <?php //if ($_GET['year'] == "2012"){ echo ' selected="selected"'; } ?> value="2012">2012</option>                               
                              </select>-->
                              
                       <!-- Change: pull year(s) from DB, Done by Manuel Salazar on 07/01/2013 -->
					  Year: <select name="year" id="year" onchange="$('#filterfrm').submit();" style="position:relative; top:10px;">
                        	<option value="">select year...</option>
                        	<?php
								$q = "SELECT DISTINCT YEAR(insert_datetime) as year FROM orders_master ORDER BY year DESC";
								$r = mysqli_query($db,$q) or die(mysqli_error($db));
								$c = mysqli_num_rows($r);
								for($i=0; $i<$c; $i++){
									$f = mysqli_fetch_row($r);
									if($f[0] == $_GET['year']){
										$sel = "SELECTED";
									}else{
										$sel = '';
									}
									echo('<option value="'.$f[0].'" '.$sel.'>'.$f[0].'</option>');
								}
							?>
                        </select>                                           
                        &nbsp;&nbsp;Month: <select name="month" id="month" onchange="$('#filterfrm').submit();"style="position:relative; top:10px;" >
                                    <option <?php if ($_GET['month'] == "0"){ echo ' selected="selected"'; } ?>     value="0" >All Year</option>
                                    <option <?php if ($_GET['month'] == "1"){ echo ' selected="selected"'; } ?> value="1" >Jan</option>
                                    <option <?php if ($_GET['month'] == "2"){ echo ' selected="selected"'; } ?> value="2" >Feb</option>
                                    <option <?php if ($_GET['month'] == "3"){ echo ' selected="selected"'; } ?> value="3" >Mar</option>
                                    <option <?php if ($_GET['month'] == "4"){ echo ' selected="selected"'; } ?> value="4" >Apr</option>
                                    <option <?php if ($_GET['month'] == "5"){ echo ' selected="selected"'; } ?> value="5" >May</option>
                                    <option <?php if ($_GET['month'] == "6"){ echo ' selected="selected"'; } ?> value="6" >Jun</option>
                                    <option <?php if ($_GET['month'] == "7"){ echo ' selected="selected"'; } ?> value="7" >Jul</option>
                                    <option <?php if ($_GET['month'] == "8"){ echo ' selected="selected"'; } ?> value="8" >Aug</option>
                                    <option <?php if ($_GET['month'] == "9"){ echo ' selected="selected"'; } ?> value="9" >Sep</option>
                                    <option <?php if ($_GET['month'] == "10"){ echo ' selected="selected"'; } ?> value="10">Oct</option>
                                    <option <?php if ($_GET['month'] == "11"){ echo ' selected="selected"'; } ?> value="11">Nov</option>
                                    <option <?php if ($_GET['month'] == "12"){ echo ' selected="selected"'; } ?> value="12">Dec</option>
                               </select>
                         &nbsp;&nbsp;Currency: <select name="slc_currency" id="slc_currency" onchange="$('#filterfrm').submit();" style="position:relative; top:10px;">
                        					<option <?php if ($_GET['slc_currency'] == ""){ echo ' selected="selected"'; } ?> value="">Select Currency</option>
											<option <?php if ($_GET['slc_currency'] == "dolar"){ echo ' selected="selected"'; } ?> value="dolar">Dollar</option>           
                                            <option <?php if ($_GET['slc_currency'] == "euro"){ echo ' selected="selected"'; } ?> value="euro">Euro</option>
                                            <!--<option <?php if ($_GET['slc_currency'] == "sterling"){ echo ' selected="selected"'; } ?>-->
                                            value="sterling">Sterling</option>
                                   </select> 
 <span style="cursor:pointer; float:right;" onclick="printDiv()"><img src="img/print.png" width="59" height="58" alt="Print" /></span>
 <span style="cursor:pointer; float:right;"><a href="stats.php" target="_new"><img src="img/statistics.png" width="59" height="58" alt="Print" /></a></span>
</form></h2>

                  
                	    <table style="width:900px;" cellspacing="0" border="1">
                	      <tr>
                	        <th>#</th>
                	        <th>Date</th>
                            <th>Order no.</th>
                            <th>Customer Name</th>
                            <th>State/Country</th>
                            <th>Country</th>
                            <th>QTY items</th>
                            <th>Currency</th>
                            <th>Amount</th>
                            <th width="100">Benefit</th>
                            <th align="center">Affiliate</th>
              	        </tr>
                            	<?
								//currency
								if($_GET['slc_currency']){
							$search_str .= " om.currency = '".$_GET['slc_currency']."' AND";
						 		}
								//year
								if($_GET['year']){
							$search_str .= " YEAR(om.insert_datetime) = '".$_GET['year']."' AND";
						 		}
								//month
								if($_GET['month']){
							$search_str .= " MONTH(om.insert_datetime) = '".$_GET['month']."' AND";
						 		}
								
								$total_benefit = 0.0;
								
								//query string
								//$str_sql_qry = "SELECT om.insert_datetime, om.id, concat(om.name,' ',om.surname), om.county, om.country, om.currency, om.total, a.name FROM orders_master om INNER JOIN affiliates_benefit ab INNER JOIN affiliates a WHERE".$search_str." om.status=2 AND om.id=ab.orders_master_id AND ab.affiliates_id=a.id ORDER BY om.id desc";
								
								$str_sql_qry = "SELECT om.insert_datetime, om.id, concat(om.name,' ',om.surname), om.county, om.country, om.currency, om.total FROM orders_master om  WHERE".$search_str." om.status in(1,2) ORDER BY om.id";
								
								//echo($str_sql_qry); //display query content
								//query recordset	
								$str_rst = mysqli_query($db,$str_sql_qry);
								//row container
								$int_rows_no = mysqli_num_rows($str_rst);
								//row layout
								$str_row_layout = 'odd';
								//row counter
								$int_row_counter = 0;
								$total=0; //sets totals to 0
								for($i=0; $i<$int_rows_no; $i++){
									//row fetch
									$row = mysqli_fetch_row($str_rst);
									
									//echo($row[1].' - ');
									
									/*$str_sql_qry_a = "SELECT a.name FROM orders_master om INNER JOIN affiliates_benefit ab INNER JOIN affiliates a WHERE om.id=".$row[1]." AND om.status=2 AND om.id=ab.orders_master_id AND ab.affiliates_id=a.id ORDER BY om.id desc";	
									$str_rst_a = mysql_query($str_sql_qry_a);
									$row_a = mysql_fetch_row($str_rst_a);*/
									
									$q = "SELECT affiliates_id FROM affiliates_benefit WHERE orders_master_id=".$row[1];
									$r = mysqli_query($db,$q);
									$c = mysqli_num_rows($r);
									$row_a = '';
									if($c){
										$f = mysqli_fetch_row($r);
										
										$q = "SELECT name FROM affiliates WHERE id=".$f[0];
										$r = mysqli_query($db,$q);
										
										$row_a = mysqli_fetch_row($r);
										
									}
									
									
									     
									if($str_row_layout == 'odd'){
										$str_row_layout = 'odd';
									}else{
										$str_row_layout = 'odd';
									}
									//increase by 1
									$int_row_counter ++;
									//currency correction (change dolar to Dollar)
									if ($row[5] == "dolar") {$cur='Dollar';}else{$cur=$row[5];}
									//sums amount totals
									$total = $total + $row[6];
									// calculating affiliate benefit
									$benefit = get_benefit($row[1]);
									
									$total_benefit = $total_benefit + $benefit;
									
									$q_items = "SELECT id FROM orders WHERE order_master_id=".$row[1];
									$r_items = mysqli_query($db,$q_items) or die(mysql_error());
									$items = mysqli_num_rows($r_items);
									
									//display container
									echo('<tr class="'.$str_row_layout.'">
											<td>'.strval($int_row_counter).'</td>
											<td>' .date('d-m-Y',strtotime($row[0])).'</td>
											<td align="center">' .$row[1].'</td>
											<td>' .$row[2].'</td>
											<td>' .$row[3].'</td>
											<td>' .$row[4].'</td>
											<td>' .$items.'</td>
											<td>' .$cur   .'</td>
											<td>' .$row[6].'</td>
											<td>' .number_format($benefit,2,'.',' ').'</td>
											<td>' .stripslashes($row_a[0]).'</td>
										</tr>');
								}//for   
								if($_GET['slc_currency']) echo('<tr class="'.$str_row_layout.'">
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td><b>TOTAL: </b></td>
											<td><b>' .number_format($total,2,'.',' '). '</b></td>
											<td><strong>'.number_format($total_benefit,2,'.',' ').'</strong></td>
											<td></td>
										</tr>');
								?>
              	      </table>
        </div>
              </div>
            </div>
            <hr class="cleaner" />
        </div>

        <div id="footer">

        </div>
    </body>
</html>

