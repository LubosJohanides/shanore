<?
	require('db.php');
	session_start();
	
	//logged?
	if($_GET['action']=='logout'){
		$q = "UPDATE admin SET session_id='' WHERE session_id='".session_id()."'";
		$r = mysqli_query($db,$q);
	}
	
	$q = "select login from admin where session_id='".session_id()."'";
	$r = mysqli_query($db,$q);
	$c = mysqli_num_rows($r);
	if($c){
		$logged = 1;
		$f = mysqli_fetch_row($r);
		$admin_name = $f[0];
	}else{
		$logged = 0;
	}
	
	

?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-language" content="en" />
        <meta name="robots" content="index,follow" />
        <meta name="googlebot" content="index,follow,snippet,archive" />
        <meta name="author" content="ELIVE CZ s.r.o. (c) 2010" />
		<?
			if(!$logged) {
				die('<meta http-equiv="refresh" content="0;url=index.php" />');
				exit();
			}
		?>


        <link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/smoothness/jquery-ui-1.7.2.custom.css" />

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
        <script type="text/javascript" src="js/jquery.ui.datepicker-cs.js"></script>
        <script type="text/javascript" src="js/jquery.simpleTooltip.js"></script>
        <script type="text/javascript" src="js/jquery.flot.pack.js"></script>
        <!--[if IE]><script language="javascript" type="text/javascript" src="js/excanvas.pack.js"></script><![endif]-->
        <script src="js/jquery.wysiwyg.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/jquery.wysiwyg.controls.js" type="text/javascript" charset="utf-8"></script>
        <script type="text/javascript" src="js/jquery.highlight-1.1.source.js"></script>
        

<script type="text/javascript" src="js/submenu.js"></script>
        <script type="text/javascript" src="js/xadmin.js"></script>
        <script type="text/javascript" src="js/jquery.simplemodal.js"></script>
    <script type="text/javascript" src="js/admin.js"></script>
        

    <title>ShanOre.com ADMIN</title>
    <link href="css/jquery.wysiwyg.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
		body,td,th {
			font-family: Arial, sans-serif;
		}
    </style>
    </head>
<body>
        <div id="header">
            <h1>ShanOre.com</h1>
            <div id="user"><a href="?action=logout">Logout</a>

                <br />
            </div>
            
            <div id="mainMenuWrapper">
<ul id="mainMenu">
                    <li><a href="admin.php">Catalog</a></li>
                    <li><a href="orders.php">ORDERS</a></li>
                    <li><a href="customers.php">Customers</a></li>
                    <li class="active"><a href="affiliates.php">Affiliates</a><a href="editor.html"></a></li>
</ul>
            </div>
        </div>
        <div id="contentWrapper">
            <div id="leftMenu">
                <strong>Menu</strong>
              <div id="menu">
                    <ul>
                        <li><a href="affiliates.php" class="item">Affiliates List</a></li>
                        <li><a href="affiliates-orders.php" class="item">Orders</a></li>
                        <li><a href="affiliates-orders-resolved.php" class="item">Resolved Orders</a></li>
                        <li><a href="affiliates-reports.php" class="item">Reports</a></li>
                        <li class="active"><a href="affiliates-links.php" class="item">Permanent Links</a></li>
                    </ul>
            </div>
            <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                
                <div class="filter">
              <h3>Quick Search</h3>
              <form action="" method="get" enctype="multipart/form-data">
                    Word / Phrase
                    <input type="text" name="search_txt" style="width:165px;" value="<? echo($_GET['search_txt']); ?>" />
                    <br />
                    <input type="submit" value="Search" />
                    
              </form>
            </div>
                
            </div>
            <div id="content">

              
              <div id="inner"><? echo($message); ?>
                <div class="datagrid">
       	  <h2>Affiliates to Consumer permanent links</h2>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <th width="41%">Affiliate</th>
            <th width="17%">Customer email</th>
            <th>Created</th>
            </tr>
          
<?

			$q = "SELECT affiliates_id, user_email, insert_datetime FROM affiliates_permalinks ORDER BY insert_datetime DESC";
			$r = mysqli_query($db,$q) or die(mysqli_error($db));
			$c = mysqli_num_rows($r);
			for($i=0; $i<$c; $i++){
				$f = mysqli_fetch_row($r);
				
				// getting aff name
				$q_name = "SELECT name FROM affiliates WHERE id=".$f[0];
				$r_name = mysqli_query($db,$q_name);
				$f_name = mysqli_fetch_row($r_name);				
				$aff_name = $f_name[0];
				
				if($f[1]){
					$email = $f[1];
				}else{
					$email = 'n/a - no order made';
				}
				
				$stamp = strtotime($f[2]);
				$date = date('d M Y',$stamp);
				
				echo('<tr><td valign="top"><h3 style="margin-top:0;">'.$aff_name.'</h3></td>
					<td valign="top">'.$email.'</td>
					<td valign="top">'.$date.'</td></tr>');
			}

?>
                      
             </table>

          </div>
              </div>
            </div>
            <hr class="cleaner" />
            
      <div id="basic-modal-content"> 
      		<!-- edit form here -->
            
	  </div>
      <div id="basic-modal-content-2"> 
      		<!-- edit form here -->
            
	  </div>
            
            
</div>

        <div id="footer">

        </div> 
    </body>

<?
	if($_GET['search_txt']){
		echo("<script>
					$('td').highlight('".$_GET['search_txt']."','highlight');
			  </script>");
	}
?>    

</html>
