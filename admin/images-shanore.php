<?
	require('db.php');
	MySQL_Query("SET NAMES UTF8");
	session_start();
	
	//logged?
	if($_GET['action']=='logout'){
		$q = "UPDATE admin SET session_id='' WHERE session_id='".session_id()."'";
		$r = mysql_query($q);
	}
	
	$q = "select login from admin where session_id='".session_id()."'";
	$r = mysql_query($q);
	$c = mysql_num_rows($r);
	if($c){
		$logged = 1;
		$f = mysql_fetch_row($r);
		$admin_name = $f[0];
	}else{
		$logged = 0;
	}
	
	

?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-language" content="en" />
        <meta name="robots" content="index,follow" />
        <meta name="googlebot" content="index,follow,snippet,archive" />
        <meta name="author" content="ELIVE CZ s.r.o. (c) 2010" />
		<?
			if(!$logged) {
				die('<meta http-equiv="refresh" content="0;url=index.php" />');
				
			}
		?>


        <link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/smoothness/jquery-ui-1.7.2.custom.css" />

        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
        <script type="text/javascript" src="js/jquery.ui.datepicker-cs.js"></script>
        <script type="text/javascript" src="js/jquery.simpleTooltip.js"></script>
        <script type="text/javascript" src="js/jquery.flot.pack.js"></script>
        <!--[if IE]><script language="javascript" type="text/javascript" src="js/excanvas.pack.js"></script><![endif]-->
        <script type="text/javascript" src="js/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="js/submenu.js"></script>
        <script type="text/javascript" src="js/xadmin.js"></script>
        <script type="text/javascript" src="js/admin.js"></script>
        
        

        <title>Shanore Admin</title>
    </head>
    <body>
        <div id="header">
            <h1>ShanOre.com</h1>
            <div id="user"><a href="?action=logout">Logout</a>

                <br />
            </div>
            <div id="mainMenuWrapper">
              <ul id="mainMenu">
                <li class="active"><a href="admin.php">Catalog</a></li>
                <li><a href="orders.php">ORDERS</a></li>
                <li><a href="customers.php">Customers</a><a href="editor.html"></a></li>
                <li><a href="affiliates.php">Affiliates</a><a href="editor.html"></a></li>
              </ul>
            </div>
        </div>
        <div id="contentWrapper" style="width:1200px;">
          <div id="leftMenu"> <strong>Menu</strong>
            <div id="menu">
              <ul>
                <li><a href="admin.php" class="item">Products</a></li>
                <li> <a href="#" class="item">Product Options</a>
                  <ul>
                    <li><a href="options.php?what=categories">Categories</a></li>
                    <li><a href="options.php?what=brands">Brands</a></li>
                    <?
									$q = "SELECT name, id FROM product_options_types";
									$r = mysql_query($q);
									$c = mysql_num_rows($r);
									for($i=0; $i<$c; $i++){
										$f = mysql_fetch_row($r);
										echo('<li><a href="options.php?what='.$f[1].'&multi=yes">'.$f[0].'</a></li>');
									}
								?>
                  </ul>
                </li>
                <li> <a href="#" class="item" id="collections_menu">Collections</a>
                  <ul>
                  	<li><a href="categories-seo.php">CATEGORIES SEO</a></li><hr />
                    <li><a href="collections-assign.php">COLLECTIONS ADMIN</a></li><hr />
                    <?
									$q = "SELECT name, id FROM collections";
									$r = mysql_query($q);
									$c = mysql_num_rows($r);
									for($i=0; $i<$c; $i++){
										$f = mysql_fetch_row($r);
										echo('<li><a href="collections.php?what='.$f[1].'">'.$f[0].'</a></li>');
									}
								?>
                  </ul>
                </li>
                <li><a href="brands.php" class="item">Brands</a></li>
                <li><a href="deleted_products.php" class="item">Deleted Products</a></li>
                <li><a href="pages.php" class="item">Pages</a></li>
                <li><a href="currencies.php" class="item">Currencies</a></li>
                <li class="active"><a href="#" class="item">Images</a>
                    <ul>
                        <li><a href="images.php">Front page</a></li>
                        <li><a href="images-shanore.php">Shanore Landing</a></li>
                        <li><a href="images-tara.php">Tara Landing</a></li>
                        <li><a href="images-product.php">Product Page</a></li>
                    </ul>
                <li><a href="sort.php" class="item">Sort Products</a></li>
                <li> <a href="#" class="item" id="pdf_menu">PDF catalogues</a>
                  <ul>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=1" target="_new">Engagement Rings</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=10" target="_new">Wedding Rings</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=96" target="_new">Claddagh Rings</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=94" target="_new">Celtic Rings</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=16" target="_new">Pendants</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=37" target="_new">Celtic Crosses</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=95" target="_new">Bracelets</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=84" target="_new">Earrings</a></li>

                                
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=85" target="_new">Tara Beads</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=91" target="_new">Tara Charms</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=83" target="_new">Tara Bracelets</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=92" target="_new">Tara Necklaces</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=93" target="_new">Tara Stacking Rings</a></li>

                            </ul>
                </li>
              </ul>
              </li>
            </div>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <strong> </strong></div>
          <div id="content">
              <div id="inner">
  
<div class="datagrid" style="width:850px;">
<h2 style="background-color:#666">Shanore Landing Page Images</h2>
<?
	// ACTION
	
	if($_GET['delimg']){
		$q = "DELETE FROM images WHERE id=".$_GET['delimg'];
		$r = mysql_query($q) or die(mysql_error());
	}
	
	if($_GET['action'] == 'upload'){
		
		if($_POST['tag']){
		
			//if(file_exists("../products_images/".$product_id.".jpg")) unlink("../products_images/".$product_id.".jpg");
			
			$q = "INSERT INTO images (name, tag, link) VALUES('".$_POST['name']."','".$_POST['tag']."','".$_POST['link']."')";
			$r = mysql_query($q) or die(mysql_error());
			
			if($_FILES['file']['tmp_name']){
				$filename = mysql_insert_id().'.jpg';
				copy ($_FILES['file']['tmp_name'], "../images-configurable/".$filename);
			}
		}
	}
	
	if($_GET['action'] == 'uploadmain'){
		
		if($_POST['tag']){
		
			//if(file_exists("../products_images/".$product_id.".jpg")) unlink("../products_images/".$product_id.".jpg");
			
			$q = "INSERT INTO images (name, tag, link) VALUES('".$_POST['name']."','".$_POST['tag']."','".$_POST['link']."')";
			$r = mysql_query($q) or die(mysql_error());
			
			if($_FILES['file']['tmp_name']){
				$filename = mysql_insert_id().'.jpg';
				copy ($_FILES['file']['tmp_name'], "../images-configurable/".$filename);
			}
		}
	}

?>

<h3>Shanore Landing Page - Bottom Icons</h3>
          <h4>Upload new image</h4>
          <form action="?action=upload" method="post" enctype="multipart/form-data" style="background-color:#EEE;">
          		<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="7%">&nbsp;</td>
    <td width="45%" style="text-align:left;">Name / ALT tag<br />
      <input name="name" /><br />
		Links to<br />
		<input name="link" />
      <br />
Choose file <br />
<input type="file" name="file" />
<input type="hidden" name="tag" value="shanore_landing" />
<br />
<input type="submit" value="Upload" /></td>
    <td width="48%" style="text-align:left;">Make sure your image is of following size:<br />
		Width: 303px<br />
		Height: 151px</td>
  </tr>
</table>

          </form>
<?
		$q = "SELECT filename, name, link, id FROM images WHERE tag='shanore_landing'";
		$r = mysql_query($q);
		$c = mysql_num_rows($r);
		for($i=0; $i<$c; $i++){
			$f = mysql_fetch_row($r);
			echo('<div style="float:left;">
					<img src="../images-configurable/'.$f[3].'.jpg" alt="'.$f[1].'" width="150" border="0" class="handover" /><br />
					link: '.$f[2].'<br />
					<a href="?delimg='.$f[3].'">delete this image</a>
					
				  </div>');
		}

?>
                 
           <hr style="clear:both;" />
           
<h3>Shanore Landing Page - Top Slider</h3>
          <h4>Upload new image</h4>
          <form action="?action=uploadmain" method="post" enctype="multipart/form-data" style="background-color:#EEE;">
          		<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="7%">&nbsp;</td>
    <td width="45%" style="text-align:left;">Name / ALT tag<br />
      <input name="name" /><br />
		Links to<br />
		<input name="link" />
      <br />
Choose file <br />
<input type="file" name="file" />
<input type="hidden" name="tag" value="shanore_top" />
<br />
<input type="submit" value="Upload" /></td>
    <td width="48%" style="text-align:left;">Make sure your image is of following size:<br />
		Width: 940px<br />
		Height: 370px</td>
  </tr>
</table>

          </form>
<?
		$q = "SELECT filename, name, link, id FROM images WHERE tag='shanore_top'";
		$r = mysql_query($q);
		$c = mysql_num_rows($r);
		for($i=0; $i<$c; $i++){
			$f = mysql_fetch_row($r);
			echo('<div style="float:left;">
					<img src="../images-configurable/'.$f[3].'.jpg" alt="'.$f[1].'" width="150" border="0" class="handover" /><br />
					link: '.$f[2].'<br />
					<a href="?delimg='.$f[3].'">delete this image</a>
					
				  </div>');
		}

?>    
           
           

        </div>
    </body>
</html>
