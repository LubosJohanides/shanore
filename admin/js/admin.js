$(document).ready(function() {

	// hide all the popups
	window.setTimeout(function() {
		 $('.flash').fadeOut();
	}, 3000);

	$('#name').keyup(function(){
		check_form();
	});
	$('#sku').keyup(function(){
		check_form();
	});

	$('#price').keyup(function(){
		check_form();
	});
	$('#price_dollar').keyup(function(){
		check_form();
	});
	$('#price_sterling').keyup(function(){
		check_form();
	});
	$('#price_wholesale').keyup(function(){
		check_form();
	});
	$('#price_wholesale_dollar').keyup(function(){
		check_form();
	});

	$('#category').change(function(){
		check_form();
	});

	//$('#newdesc').wysiwyg({autoGrow:true, controls:"bold,italic,|,undo,redo"});
	//$('#newdesc').wysihtml5();

	$('.delete_btn').hover(
		function(){
			$(this).attr('src','img/my_delete_over.png');
		},
		function(){
			$(this).attr('src','img/my_delete.png');
		}
	);

	$('.edit_btn').hover(
		function(){
			$(this).attr('src','img/my_edit_over.png');
		},
		function(){
			$(this).attr('src','img/my_edit.png');
		}
	);

});

function gold_product(product_id){
	$.ajax({
		type: 'POST',
		data: "product_id=" + product_id,
		url: "ajax_gold_product.php",
		success: function(msg){
			$('#gold_' + product_id).attr('src','img/gold_' + msg + '.png');
		}
	});
}


function pendant_product(product_id){
	$.ajax({
		type: 'POST',
		data: "product_id=" + product_id,
		url: "ajax_pendant_product.php",
		success: function(msg){
			$('#pendant_' + product_id).attr('src','img/pendant_' + msg + '.png');
			if(msg == '1'){
				$('#pendant_pricing_' + product_id).show();
			}else{
				$('#pendant_pricing_' + product_id).hide();
			}
		}
	});
}

function save_pendant_pricing(product_id){

	var pendant_price_2 = $('#2_euro_' + product_id).val();
	var pendant_price_4 = $('#4_euro_' + product_id).val();
	var pendant_price_6 = $('#6_euro_' + product_id).val();
	var pendant_price_wholesale_2 = $('#2_euro_wholesale_' + product_id).val();
	var pendant_price_wholesale_4 = $('#4_euro_wholesale_' + product_id).val();
	var pendant_price_wholesale_6 = $('#6_euro_wholesale_' + product_id).val();

	var pendant_price_usd_2 = $('#2_usd_' + product_id).val();
	var pendant_price_usd_4 = $('#4_usd_' + product_id).val();
	var pendant_price_usd_6 = $('#6_usd_' + product_id).val();
	var pendant_price_wholesale_usd_2 = $('#2_usd_wholesale_' + product_id).val();
	var pendant_price_wholesale_usd_4 = $('#4_usd_wholesale_' + product_id).val();
	var pendant_price_wholesale_usd_6 = $('#6_usd_wholesale_' + product_id).val();


	$.ajax({
		type: 'POST',
		data: "pendant_price_2=" + pendant_price_2 + "&pendant_price_4=" + pendant_price_4 + "&pendant_price_6=" + pendant_price_6 + "&pendant_price_wholesale_2=" + pendant_price_wholesale_2 + "&pendant_price_wholesale_4=" + pendant_price_wholesale_4 + "&pendant_price_wholesale_6=" + pendant_price_wholesale_6 + "&pendant_price_usd_2=" + pendant_price_usd_2 + "&pendant_price_usd_4=" + pendant_price_usd_4 + "&pendant_price_usd_6=" + pendant_price_usd_6 + "&pendant_price_wholesale_usd_2=" + pendant_price_wholesale_usd_2 + "&pendant_price_wholesale_usd_4=" + pendant_price_wholesale_usd_4 + "&pendant_price_wholesale_usd_6=" + pendant_price_wholesale_usd_6 + "&product_id=" + product_id,
		url: "ajax_save_pendant_price.php",
		success: function(msg){
			$('#pendant_' + product_id).attr('src','img/pendant_' + msg + '.png');
			if(msg == 'ok'){
				alert('Pendant pricing has been saved.');
			}else{
				alert(msg);
			}
		}
	});
}


function info_product(product_id){
	$.ajax({
		type: 'POST',
		data: "product_id=" + product_id,
		url: "ajax_info_product.php",
		success: function(msg){
			$('#info_' + product_id).attr('src','img/info_' + msg + '.png');
		}
	});
}


function config_product(product_id){
	$.ajax({
		type: 'POST',
		data: "product_id=" + product_id,
		url: "ajax_config_product.php",
		success: function(msg){
			$('#config_' + product_id).attr('src','img/config_' + msg + '.png');
		}
	});
}


function solve_aff_order(mid){
	$.ajax({
		type: 'POST',
		data: "mid=" + mid,
		url: "ajax_solve_order_affiliate.php",
		success: function(msg){
			if(msg == 'ok'){
				$('#line_' + mid).remove();
			}else{
				alert(msg);
			}
		}
	});
}

function solve_order(mid){

	// pop up asking to fill in trcking code and select a provider
	var tracking_code = $('#tracking_code').val();
	var shipping_provider = $('#shipping_provider').val();

	if(!shipping_provider){
		var n = jQuery('<div id="overlay"><div id="overlayin"><div class="closeme" onclick="$(\'#overlay\').remove();"><img src="img/close.png" style="position:absolute; right:0; top:0;" /></div>Please select provider<br/><select name="shipping_provider" id="shipping_provider"><option value="Fedex">Fedex</option><option value="USPS">USPS</option><option value="An Post">An Post</option><option value="Fastway">Fastway</option><option value="DPD">DPD</option></select><br/><br/>Type in the tracking code<br/><input name="tracking_code" id="tracking_code" /><br/><img src="img/solved.png" style="cursor:pointer;" onclick="solve_order(' + mid + ')" id="submit_shipping" /></div></div>');
		if(!shipping_provider){
        	n.appendTo(document.body);
		}else{
			$('#line_'+mid).slideUp();
			$('#submit_shipping').slideUp();
		}
		return false
	}

	$.ajax({
		type: 'POST',
		data: "mid=" + mid + "&provider=" + shipping_provider + "&tracking_code=" + tracking_code,
		url: "ajax_solve_order.php",
		success: function(msg){
			if(msg.trim() == 'ok'){
				$('#line_' + mid).remove();
				alert('order resolved');

				$('#overlay').remove();

			}else{
				alert(msg);
			}
		}
	});
}

function process_order(mid){
	$.ajax({
		type: 'POST',
		data: "mid=" + mid,
		url: "ajax_process_order.php",
		success: function(msg){
			if(msg == 'ok'){
				$('#line_' + mid).remove();
				alert('order is being processed');
			}else{
				alert(msg);
			}
		}
	});
}

function ajax_delimg(file){
	$.ajax({
		type: 'POST',
		data: "file=" + file,
		url: "ajax_delete_image.php",
		success: function(msg){
			//alert(msg);
			$('img[alt="' + file + '"]').attr('src','small_noimage.jpg');
		}
	});
}


function reset_pass(user_id){
	var pass1 = prompt("Please enter new password");
	var pass2 = prompt("Please enter new password AGAIN");

	if(pass1 == pass2){
		$.ajax({
			type: 'POST',
			data: "uid=" + user_id + "&newpass=" + pass1,
			url: "ajax_change_pass.php",
			success: function(msg){
				if(msg == 'ok'){
					alert('Password chaged.');
				}else{
					alert(msg);
				}
			}
		});
	}else{
		alert("The passwords don't match.");
	}
}

function cancel_order(mid){
	var response = confirm('Are you sure you want to cancel the order?');

	if (response){
		$.ajax({
			type: 'POST',
			data: "mid=" + mid,
			url: "ajax_cancel_order.php",
			success: function(msg){
				if(msg == 'ok'){
					$('#line_' + mid).remove();
					alert('order canceled');
				}else{
					alert(msg);
				}
			}
		});
	}
}


function pay_order(mid){
	var response = confirm('Are you sure you want to mark this order as PAID?');

	if (response){
		$.ajax({
			type: 'POST',
			data: "mid=" + mid,
			url: "ajax_pay_order.php",
			success: function(msg){
				if(msg == 'ok'){
					$('#line_' + mid).remove();
					alert('order marked as PAID');
				}else{
					alert(msg);
				}
			}
		});
	}
}

function sale_product(product_id){
	$.ajax({
		type: 'POST',
		data: "product_id=" + product_id,
		url: "ajax_sale_product.php",
		success: function(msg){
			if(msg == '0'){
				$('#sale_' + product_id).attr('src','img/sale_off.png');
			}else if(msg == '1'){
				$('#sale_' + product_id).attr('src','img/sale_on.png');
			}else{
				alert(msg);
			}
		}
	});
}

function publish_review(review_id){
	$.ajax({
		type: 'POST',
		data: "review_id=" + review_id,
		url: "ajax_publish_review.php",
		success: function(msg){
			if(msg == '0'){
				$('#review_' + review_id).attr('src','img/published_off.png');
			}else if(msg == '1'){
				$('#review_' + review_id).attr('src','img/published_on.png');
			}else{
				alert(msg);
			}
		}
	});
}

function delete_collection_assignment(category,collection){
	$.ajax({
		type: 'POST',
		data: "collection_id=" + collection + "&category_id=" + category,
		url: "ajax_unassign_collection.php",
		success: function(msg){
			$('#collection_list_' + collection).html(msg);
		}
	});
}

function save_collection_dispname(collection_id){
	display_name = $('#display_name_' + collection_id).val();
	$.ajax({
		type: 'POST',
		data: "collection_id=" + collection_id + '&display_name=' + display_name,
		url: "ajax_save_collection_dispname.php",
		success: function(msg){
			if(msg=='ok'){
				$('#display_name_' + collection_id).css('background-color','#0F0');
				$('#display_name_' + collection_id).animate({ backgroundColor: "#FFF" }, "slow");
			}else{
				alert(msg);
			}
		}
	});
}

function delete_collection_assignmentb(brand,collection){
	$.ajax({
		type: 'POST',
		data: "collection_id=" + collection + "&brand_id=" + brand,
		url: "ajax_unassign_collection_brand.php",
		success: function(msg){
			$('#collection_listb_' + collection).html(msg);
		}
	});
}

function assign_collection(collection_id){
	category_id = $('#collection_select_' + collection_id).val();
	$.ajax({
		type: 'POST',
		data: "collection_id=" + collection_id + "&category_id=" + category_id,
		url: "ajax_assign_collection.php",
		success: function(msg){
			$('#collection_list_' + collection_id).html(msg);
		}
	});
}

function assign_collectionb(collection_id){
	brand_id = $('#collection_selectb_' + collection_id).val();
	$.ajax({
		type: 'POST',
		data: "collection_id=" + collection_id + "&brand_id=" + brand_id,
		url: "ajax_assign_collection_brand.php",
		success: function(msg){
			$('#collection_listb_' + collection_id).html(msg);
		}
	});
}

function delete_collection_bulk(collection_id){
	$.ajax({
		type: 'POST',
		data: "collection_id=" + collection_id,
		url: "ajax_delete_collection_bulk.php",
		success: function(msg){
			if(msg == 'ok'){
				$('#basic-modal-content-2').find('#collection_' + collection_id).slideUp();
			}else{
				alert(msg);
			}
		}
	});
}

function existing_collection_bulk(){
	collection_id = $('#basic-modal-content-2').find('#collection_name_select').val();

	$.ajax({
		type: 'POST',
		data: "collection_id=" + collection_id,
		url: "ajax_old_collection_bulk.php",
		beforeSend: function(){
			$('#basic-modal-content-2').find('#collection_name_select').val('')
		},
		success: function(msg){
			$('#basic-modal-content-2').find('#common_collections').append(msg)
		}
	});
}


function new_collection_bulk(){

	collection_name = $('#basic-modal-content-2').find('#collection_name').val();

	$.ajax({
		type: 'POST',
		data: "collection_name=" + collection_name,
		url: "ajax_new_collection_bulk.php",
		beforeSend: function(){
			$('#basic-modal-content-2').find('#collection_name').val('')
		},
		success: function(msg){
			if(msg == 'exists'){
				alert('Error - this collection exists already.');
			}else{
				$('#basic-modal-content-2').find('#common_collections').append(msg)
			}
		}
	});
}

function edit_bulk(){
	$.ajax({
		type: 'POST',
		data: "",
		url: "ajax_edit_bulk.php",
		beforeSend: function(){
			$('#bulk_edit_loading').css('display','');
		},
		success: function(msg){
			$('#bulk_edit_loading').css('display','none');
			$('#basic-modal-content-2').html(msg);
			$('#basic-modal-content-2').modal();
		}
	});
}

function remove_from_bulk(record_id, admin_name, product_id){
	$.ajax({
		type: 'POST',
		data: "id=" + record_id + '&admin_name=' + admin_name,
		url: "ajax_remove_bulk.php",
		success: function(msg){
			$('#edit_basket').html(msg);
			$('#tobulk_' + product_id).attr('src','img/my_to_bulk.png');
			$('#tobulk_' + product_id).css('display','');
		}
	});
}

function to_bulk(admin_name, product_id, product_name){
	$.ajax({
		type: 'POST',
		data: "product_id=" + product_id + "&admin_name=" + admin_name + "&product_name=" + product_name,
		url: "ajax_to_bulk.php",
		beforeSend: function(){
			$('#tobulk_' + product_id).attr('src','img/loading_bar.gif');
		},
		success: function(msg){
			$('#edit_basket').html(msg);
			$('#tobulk_' + product_id).css('display','none');
		}
	});
}

function add_option(product_id){

	option_type = $('#basic-modal-content').find('#option_name').val();
	option_value = $('#basic-modal-content').find('#option_value').val();

	option_value.replace("&","&amp;");

	if($('#basic-modal-content').find('#sizeadd').length > 0){
		var sizeadd = $('#basic-modal-content').find('#sizeadd').val();
	}else{
		var sizeadd = 0;
	}

	$.ajax({
		type: 'POST',
		data: "product_id=" + product_id + "&option_type=" + option_type + '&option_value=' + option_value + "&sizeadd=" + sizeadd,
		url: "ajax_add_option.php",
		beforeSend: function(){
			$('#basic-modal-content').find('#option_values_select').html('<img src="img/loading_bar.gif" style="float:right; margin-right:5px; display:none;" width="50px">');
			$('#basic-modal-content').find('#option_name').val('');
		},
		success: function(msg){
			$('#basic-modal-content').find('#other_options').append(msg);
			$('#basic-modal-content').find('#option_values_select').html('');
		}
	});

}

function add_option_bulk(){

	option_type = $('#basic-modal-content-2').find('#option_name-2').val();
	option_value = $('#basic-modal-content-2').find('#option_value-2').val();
	if($('#basic-modal-content-2').find('#sizeadd-2').length > 0){
		var sizeadd = $('#basic-modal-content-2').find('#sizeadd-2').val();
	}else{
		var sizeadd = 0;
	}

	$.ajax({
		type: 'POST',
		data: "option_type=" + option_type + '&option_value=' + option_value + '&sizeadd=' + sizeadd,
		url: "ajax_add_option_bulk.php",
		beforeSend: function(){
			$('#basic-modal-content-2').find('#option_values_select-2').html('<img src="img/loading_bar.gif" style="float:right; margin-right:5px; display:none;" width="50px">');
			$('#basic-modal-content-2').find('#option_name-2').val('');
		},
		success: function(msg){
			$('#basic-modal-content-2').find('#other_options-2').append(msg);
			$('#basic-modal-content-2').find('#option_values_select_bulk-2').html('');
		}
	});

}

function weartogether(){

	var linkname = $('#link-name').val();
	if(linkname == ""){
		alert('please input a name for this new link');
		return false;
	}

	$.ajax({
		url: "ajax_wear_together_bulk.php?linkname=" + linkname,
		success: function(msg){
			alert(msg);
		}
	});
}


function addtowear(){

	var linkname = $('#add-link-name').val();
	if(linkname == ""){
		alert('please select a link from the drop-down above');
		return false;
	}

	$.ajax({
		url: "ajax_wear_together_bulk_add.php?linkname=" + linkname,
		success: function(msg){
			alert(msg);
		}
	});
}


function collection_to_bulk(col_id){
	$.ajax({
		type: 'POST',
		data: "id=" + col_id,
		url: "ajax_collection_to_bulk.php",
		success: function(msg){
			alert(msg);
		}
	});
}

function donotwear(){
	$.ajax({
		url: "ajax_do_not_wear_bulk.php",
		success: function(msg){
			alert(msg);
		}
	});
}

function delete_option(option_id){
	$.ajax({
		type: 'POST',
		data: "id=" + option_id,
		url: "ajax_delete_option.php",
		success: function(msg){
			if(msg == 'ok'){
				$('#basic-modal-content').find('#option_' + option_id).replaceWith('');
			}else{
				alert(msg);
			}
		}
	});
}

function delete_option_bulk(option_value,option_type){
	$.ajax({
		type: 'POST',
		data: "option_value=" + option_value + "&option_type=" + option_type,
		url: "ajax_delete_option_bulk.php",
		success: function(msg){
			if(msg == 'ok'){
				$('#basic-modal-content-2').find('#option_' + option_value + '_' + option_type).replaceWith('');
			}else{
				alert(msg);
			}
		}
	});
}

function get_option_values(option_id, product_id){

	if(!option_id){
		$('#basic-modal-content').find('#option_values_select').html('');
		return false;
	}

	$.ajax({
		type: 'POST',
		data: "id=" + option_id + "&product_id=" + product_id,
		url: "ajax_get_option_values.php",
		success: function(msg){
			//alert('before = ' + $('#basic-modal-content').find('#option_values_select').html());
			$('#basic-modal-content').find('#option_values_select').html(msg);
		}
	});
}

function get_option_values_bulk(option_id){

	if(!option_id){
		$('#basic-modal-content-2').find('#option_values_select-2').html('');
		return false;
	}

	$.ajax({
		type: 'POST',
		data: "id=" + option_id,
		url: "ajax_get_option_values_bulk.php",
		success: function(msg){
			//alert('before = ' + $('#basic-modal-content').find('#option_values_select').html());
			$('#basic-modal-content-2').find('#option_values_select_bulk-2').html(msg);
		}
	});
}

function delete_product(id){
	$.ajax({
		type: 'POST',
		data: "id=" + id,
		url: "ajax_delete_product.php",
		success: function(msg){
			if(msg == 'ok'){
				$('#tr_' + id).slideUp();
			}else{
				alert('error: ' + msg);
			}
		}
	});
}

function refresh_product(id){
	$.ajax({
		type: 'POST',
		data: "id=" + id,
		url: "ajax_refresh_product.php",
		success: function(msg){
			if(msg == 'ok'){
				$('#tr_' + id).slideUp();
			}else{
				alert('error: ' + msg);
			}
		}
	});
}

function edit_product(id, page, search_text, brand_filter, category_filter){
	$.ajax({
		type: 'POST',
		data: "id=" + id + "&page=" + page + "&search_txt=" + search_text + "&brand_filter=" + brand_filter + "&category_filter=" + category_filter,
		url: "ajax_get_product_edit.php",
		beforeSend: function(){
			$('#edit_loading_' + id).css('display','');
		},
		success: function(msg){
			$('#edit_loading_' + id).css('display','none');
			$('#basic-modal-content').html(msg);
			$('#basic-modal-content').modal();
			//$('#wysiwyg').wysiwyg({autoGrow:true, controls:"bold,italic,|,undo,redo"});

		}
	});
}

function check_form(){

	$('#loading').css('display','');
	$('#mandatory').html('');

	var name = $('#name').val();
	var sku = $('#sku').val();
	var price = $('#price').val();
	var price_dollar = $('#price_dollar').val();
	var collection = $('#collection_select').val();

	var category = $('#category').val();
	var halt = 0;

	// check name
	if(name == ''){
		$('#mandatory').html($('#mandatory').html() + '- <strong>product name</strong> is mandatory<br />');

		halt = 1;
		$('#name').css('background-color','#F99');
	}else{
		//check duplicate name
		$.ajax({
			type: 'POST',
			data: "name=" + name,
		  	url: "ajax_check_name_duplicate.php",
		  	success: function(msg){
				if(msg=='ok'){
					$('#name').css('background-color','');
				}else{
					$('#mandatory').html($('#mandatory').html() + '- <strong>NAME</strong> is duplicate<br />');
					halt = 1;
					$('#submitbtn').hide()
					$('#name').css('background-color','#F99');
				}
		  	}
		});



	}

	// check sku
	if(sku == ''){
		$('#mandatory').html($('#mandatory').html() + '- <strong>SKU</strong> is mandatory<br />');
		halt = 1;
		$('#sku').css('background-color','#F99');
	}else{
		$.ajax({
			type: 'POST',
			data: "sku=" + sku,
		  	url: "ajax_check_sku.php",
		  	success: function(msg){
				if(msg=='ok'){
					$('#sku').css('background-color','');
				}else{
					$('#mandatory').html($('#mandatory').html() + '- <strong>SKU</strong> must be unique <br />- this SKU already exists<br />');
					halt = 1;
					$('#sku').css('background-color','#F99');
				}
		  	}
		});
	}

	if(price == ''){
		$('#mandatory').html($('#mandatory').html() + '- <strong>price</strong> is mandatory<br />');
		halt = 1;
		$('#price').css('background-color','#F99');
	}else{
		if (! (/^\d*(?:\.\d{0,2})?$/.test(price))) {
			$('#mandatory').html($('#mandatory').html() + '- <strong>price</strong> is not in correct format<br />');
			$('#price').css('background-color','#F99');
		}else{
			$('#price').css('background-color','');
		}

	}

	if(price_dollar == ''){
		$('#mandatory').html($('#mandatory').html() + '- <strong>price-dollar</strong> is mandatory<br />');
		halt = 1;
		$('#price_dollar').css('background-color','#F99');
	}else{
		if (! (/^\d*(?:\.\d{0,2})?$/.test(price_dollar))) {
			$('#mandatory').html($('#mandatory').html() + '- <strong>price-dollar</strong> is not in correct format<br />');
			$('#price_dollar').css('background-color','#F99');
		}else{
			$('#price_dollar').css('background-color','');
		}

	}

	if(collection == ''){
		$('#mandatory').html($('#mandatory').html() + '- <strong>collection</strong> is mandatory<br />');
		halt = 1;
		$('#collection_select').css('background-color','#F99');
	}else{
		$('#collection_select').css('background-color','');
	}


	if(halt == 0){
		//$('#submitbtn').removeAttr('disabled');
		$('#submitbtn').show();

	}else{
		//$('#submitbtn').attr('disabled','disabled');
		$('#submitbtn').hide()
	}

	$('#loading').css('display','none');

}


function manage_images(){
	var id = $('#id_edit').val();
	window.open('images.php?id='+id,'Spr�va_obr�zku', 'width=600, height=500, scrollbars=1');
}
