$(document).ready(function() { 

	$('#login_btn').click(function() { 
		
		$.ajax({
			type: 'POST',
			data: "login=" + $('#name').val() + "&password=" + $('#password').val(),
		  	url: "login.php",
		  	success: function(response){
				if(response == 'not found'){
					$('#error').css({ visibility: 'visible' });
				}else{
					$('#error').css({ visibility: 'hidden' });
					window.location.replace("admin.php");
				}
		  	}
		});

	});

});