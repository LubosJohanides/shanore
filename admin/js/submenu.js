function subMenu() {
    $('#menu ul').hide();
    $('#menu ul:first').show();


	
	$('#menu li.active ul').slideDown('normal');

	$('#menu ul li a.item').click(
        function() {
            $('#menu ul li').removeClass('active');
            $(this).parent().addClass('active');

            $('#menu ul li ul:visible').slideUp('normal');
            
            var checkElement = $(this).next();
            
            if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
                return false;
            }
            if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
                checkElement.slideDown('normal');
                return false;
            }
        }
    );
}