function simple_tooltip(target_items, name){
    $(target_items).each(function(i){
        $("body").append("<div class='"+name+"' id='"+name+i+"'><p><img src='"+$(this).attr('rel')+"' alt='' /></p></div>");
        var my_tooltip = $("#"+name+i);

        if($(this).attr("rel") != "" && $(this).attr("rel") != "undefined" ){

        $(this).removeAttr("title").mouseover(function(){
                my_tooltip.css({display:"block"});
        }).mousemove(function(kmouse){
                var border_top = $(window).scrollTop();
                var border_right = $(window).width();
                var left_pos;
                var top_pos;
                var offset = 15;
                if(border_right - (offset *2) >= my_tooltip.width() + kmouse.pageX){
                    left_pos = kmouse.pageX+offset;
                    } else{
                    left_pos = kmouse.pageX-my_tooltip.width()-offset;
                    }

                if(border_top + offset>= kmouse.pageY - my_tooltip.height()){
                    top_pos = kmouse.pageY+offset;
                    } else{
                    top_pos = kmouse.pageY-my_tooltip.height()-offset;
                    }


                my_tooltip.css({left:left_pos, top:top_pos});
        }).mouseout(function(){
                my_tooltip.css({left:"-9999px"});
        });

        }
    });
}
