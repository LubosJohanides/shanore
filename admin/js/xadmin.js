$(document).ready(function() {

	$('#datepicker').datepicker({inline: true});
    $('#dialog').dialog({
        buttons: {'OK': function() {$(this).dialog('close');}},
        title: 'Vítejte v xadminu'
    });

    $('div.flash').click(function() {
        $(this).fadeOut(700)
    });

    $('input[title]').each(function() {
        if($(this).val() === '') {
            $(this).val($(this).attr('title'));
        }

        $(this).focus(function() {
            if($(this).val() === $(this).attr('title')) {
                $(this).val('').addClass('focused');
            }
        });

        $(this).blur(function() {
            if($(this).val() === '') {
                $(this).val($(this).attr('title')).removeClass('focused');
            }
        });
    });

    $('div.add').hide();
    $('a.add').toggle(
        function() {
            $(this).next().slideDown();
            return false;
        },

        function() {
            $(this).next().slideUp();
            return false;
        }
    );

    //simple_tooltip('div.images a', 'tooltip');
    subMenu();
});
