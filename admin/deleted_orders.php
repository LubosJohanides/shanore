<?
	require('db.php');
	session_start();
	
	//logged?
	if($_GET['action']=='logout'){
		$q = "UPDATE admin SET session_id='' WHERE session_id='".session_id()."'";
		$r = mysqli_query($db,$q);
	}
	
	$q = "select login from admin where session_id='".session_id()."'";
	$r = mysqli_query($db,$q);
	$c = mysqli_num_rows($r);
	if($c){
		$logged = 1;
		$f = mysqli_fetch_row($r);
		$admin_name = $f[0];
	}else{
		$logged = 0;
	}
	
	

?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-language" content="en" />
        <meta name="robots" content="index,follow" />
        <meta name="googlebot" content="index,follow,snippet,archive" />
        <meta name="author" content="ELIVE CZ s.r.o. (c) 2010" />
		<?
			if(!$logged) {
				die('<meta http-equiv="refresh" content="0;url=index.php" />');
				
			}
		?>


        <link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/smoothness/jquery-ui-1.7.2.custom.css" />

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
        <script type="text/javascript" src="js/jquery.ui.datepicker-cs.js"></script>
        <script type="text/javascript" src="js/jquery.simpleTooltip.js"></script>
        <script type="text/javascript" src="js/jquery.flot.pack.js"></script>
        <!--[if IE]><script language="javascript" type="text/javascript" src="js/excanvas.pack.js"></script><![endif]-->
        <script src="js/jquery.wysiwyg.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/jquery.wysiwyg.controls.js" type="text/javascript" charset="utf-8"></script>
        <script type="text/javascript" src="js/jquery.highlight-1.1.source.js"></script>
        

<script type="text/javascript" src="js/submenu.js"></script>
        <script type="text/javascript" src="js/xadmin.js"></script>
        <script type="text/javascript" src="js/jquery.simplemodal.js"></script>
    <script type="text/javascript" src="js/admin.js"></script>
        

    <title>ShanOre.com ADMIN</title>
    <link href="css/jquery.wysiwyg.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
		body,td,th {
			font-family: Arial, sans-serif;
		}
    </style>
    </head>
<body>
        <div id="header">
            <h1>ShanOre.com</h1>
            <div id="user"><a href="?action=logout">Logout</a>

                <br />
            </div>
            <div id="mainMenuWrapper">
              <ul id="mainMenu">
                <li><a href="admin.php">Catalog</a></li>
                <li class="active"><a href="orders.php">ORDERS</a></li>
                <li><a href="customers.php">Customers</a><a href="editor.html"></a></li>
                <li><a href="affiliates.php">Affiliates</a><a href="editor.html"></a></li>
              </ul>
            </div>
        </div>
        <div id="contentWrapper">
            <div id="leftMenu">
                <strong>Menu</strong>
              <div id="menu">
                    <ul>
                    	<li><a href="orders_installment.php" class="item">Installment Orders</a></li>
                        <li><a href="orders.php" class="item">Confirmed Orders</a></li>
                        <li><a href="processing_orders.php" class="item">Processing Orders</a></li>
                        <li><a href="resolved_orders.php" class="item">Completed Orders</a></li>
                        <li class="active"><a href="deleted_orders.php" class="item">Canceled Orders</a></li>
                        <li><a href="failed_orders.php" class="item">Failed Orders</a></li>
                        <li><a href="orders_unpaid.php" class="item">Unpaid Orders</a></li>
                        <li><a href="abandoned_carts.php" class="item">Abandoned Carts</a></li>
                    </ul>
            </div>
            <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            <strong> </strong></div>
            <div id="content">

              
              <div id="inner"><? echo($message); ?>
                <div class="datagrid">
       	  <h2>Canceled Orders</h2>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <th width="13%">#</th>
            <th width="41%" style="text-align:left">Products Ordered</th>
            <th width="17%">Billing</th>
            <th width="14%">Total &euro;</th>
            <th width="15%">Actions</th>
          </tr>
          
<?

		$q = "SELECT id, title, name, surname, email, company, address1, address2, city, county, post_code, country, phone_day, phone_evening, ship_address1, ship_address2, ship_city, ship_county, ship_post_code, ship_country, total, delivery_type, insert_datetime FROM orders_master WHERE status=8 AND paid=1 ORDER BY id DESC";
		$r = mysqli_query($db,$q) or die(mysqli_error($db));
		$c = mysqli_num_rows($r);
		
		for($i=0; $i<$c; $i++){
			$f = mysqli_fetch_row($r);
			
			// lets harvest products
			$q_sub = "SELECT id, product_id, qty, price, size, wrapping FROM orders WHERE order_master_id='".$f[0]."'";
			$r_sub = mysqli_query($db,$q_sub) or die(mysqli_error($db));
			$c_sub = mysqli_num_rows($r_sub);
			$products = '';
			for($j=0; $j<$c_sub; $j++){
				$ff = mysqli_fetch_row($r_sub);
				
				$q_detail = "SELECT sku2, name FROM products WHERE id=".$ff[1];
				$r_detail = mysqli_query($db,$q_detail) or die(mysqli_error($db));
				$fff = mysqli_fetch_row($r_detail);
				
				if($ff[4]) $ff[4] = '<br />SIZE '.$ff[4];
				
				$products .= $ff[2].' x <strong><a href="https://www.shanore.com/detail.php?id=a'.$ff[1].'" target="_new">'.$fff[1].'</a></strong>'.$ff[4].'<br/>';
			}
			
			echo('<tr id="line_'.$f[0].'">
					<td valign="top">'.$f[0].'<br>'.$f[22].'</td>
					<td valign="top" style="text-align:left;">'.$products.'</td>
					<td valign="top">'.$f[1].' '.$f[2].' '.$f[3]);
			if($f[5]){
				echo('<br />'.$f[5]);
			}
			
			if($f[7]) $f[7] .= '<br>';
			if($f[9]) $f[9] .= '<br>';
			if($f[10]) $f[10] .= '<br>';
			
			echo('<br />'.$f[6].'<br />
					  '.$f[7].'
					  '.$f[8].'<br />
					  '.$f[9].'
					  '.$f[10].'
					  '.$f[11].'<br/>
					  <strong>'.$f[21].'</strong>
					  <p><a href="mailto:'.$f[4].'">'.$f[4].'</a><br />'.$f[12].'
					</p></td>
					<td valign="top">'.number_format($f[20],2).'</td>
					<td valign="top">
					<!--<img src="img/cancel.png" style="float:right; cursor:pointer; clear:both;" onclick="cancel_order(\''.$f[0].'\')">
					<img src="img/solved.png" style="float:right; cursor:pointer; clear:both;" onclick="solve_order(\''.$f[0].'\')"> -->
					<img src="img/print.png" style="float:right; cursor:pointer; clear:both;" onclick="window.open(\'print_order.php?mid='.$f[0].'\',\'Print order '.$f[0].'\',\'width=980,resizable=0,scrollbars=1\')"></td>
				  </tr>');
		}

?>
                      
             </table>

          </div>
              </div>
            </div>
            <hr class="cleaner" />
            
      <div id="basic-modal-content"> 
      		<!-- edit form here -->
            
	  </div>
      <div id="basic-modal-content-2"> 
      		<!-- edit form here -->
            
	  </div>
            
            
</div>

        <div id="footer">

        </div> 
    </body>

<?
	if($_GET['search_txt']){
		echo("<script>
					$('td').highlight('".$_GET['search_txt']."','highlight');
			  </script>");
	}
?>    

</html>
