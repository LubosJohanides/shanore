<?
	require('db.php');
    mysqli_query($db,"SET NAMES UTF8");
	session_start();
	
	//logged?
	if($_GET['action']=='logout'){
		$q = "UPDATE admin SET session_id='' WHERE session_id='".session_id()."'";
		$r = mysqli_query($db,$q);
	}
	
	$q = "select login from admin where session_id='".session_id()."'";
	$r = mysqli_query($db,$q);
	$c = mysqli_num_rows($r);
	if($c){
		$logged = 1;
		$f = mysqli_fetch_row($r);
		$admin_name = $f[0];
	}else{
		$logged = 0;
	}
	
	

?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-language" content="en" />
        <meta name="robots" content="index,follow" />
        <meta name="googlebot" content="index,follow,snippet,archive" />
        <meta name="author" content="ELIVE CZ s.r.o. (c) 2010" />
		<?
			if(!$logged) {
				die('<meta http-equiv="refresh" content="0;url=index.php" />');
				
			}
		?>


        <link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/smoothness/jquery-ui-1.7.2.custom.css" />

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
        <script type="text/javascript" src="js/jquery.ui.datepicker-cs.js"></script>
        <script type="text/javascript" src="js/jquery.simpleTooltip.js"></script>
        <script type="text/javascript" src="js/jquery.flot.pack.js"></script>
        <!--[if IE]><script language="javascript" type="text/javascript" src="js/excanvas.pack.js"></script><![endif]-->
        <script type="text/javascript" src="js/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="js/submenu.js"></script>
        <script type="text/javascript" src="js/xadmin.js"></script>
        <script type="text/javascript" src="js/admin.js"></script>
        
        

        <title>Shanore Admin</title>
    </head>
    <body>
        <div id="header">
            <h1>ShanOre.com</h1>
            <div id="user"><a href="?action=logout">Logout</a>

                <br />
            </div>
            <div id="mainMenuWrapper">
              <ul id="mainMenu">
                <li class="active"><a href="admin.php">Catalog</a></li>
                <li><a href="orders.php">ORDERS</a></li>
                <li><a href="customers.php">Customers</a><a href="editor.html"></a></li>
                <li><a href="affiliates.php">Affiliates</a><a href="editor.html"></a></li>
              </ul>
            </div>
        </div>
        <div id="contentWrapper">
          <div id="leftMenu"> <strong>Menu</strong>
            <div id="menu">
              <ul>
                <li><a href="admin.php" class="item">Products</a></li>
                <li> <a href="#" class="item">Product Options</a>
                  <ul>
                    <li><a href="options.php?what=categories">Categories</a></li>
                    <li><a href="options.php?what=brands">Brands</a></li>
                    <?
									$q = "SELECT name, id FROM product_options_types";
									$r = mysqli_query($db,$q);
									$c = mysqli_num_rows($r);
									for($i=0; $i<$c; $i++){
										$f = mysqli_fetch_row($r);
										echo('<li><a href="options.php?what='.$f[1].'&multi=yes">'.$f[0].'</a></li>');
									}
								?>
                  </ul>
                </li>
                <li class="active"> <a href="#" class="item" id="collections_menu">Collections</a>
                  <ul>
                  	<li><a href="categories-seo.php">CATEGORIES SEO</a></li><hr />
                    <li><a href="collections-assign.php">COLLECTIONS ADMIN</a></li><hr />
<?
									$q = "SELECT name, id FROM collections";
									$r = mysqli_query($db,$q);
									$c = mysqli_num_rows($r);
									for($i=0; $i<$c; $i++){
										$f = mysqli_fetch_row($r);
										echo('<li><a href="collections.php?what='.$f[1].'">'.$f[0].'</a></li>');
									}
?>
                  </ul>
                </li>
                <li><a href="brands.php" class="item">Brands</a></li>
                <li><a href="deleted_products.php" class="item">Deleted Products</a></li>
                <li><a href="pages.php" class="item">Pages</a></li>
                <li><a href="currencies.php" class="item">Currencies</a></li>
                <li><a href="images.php" class="item">Images</a></li>
                <li><a href="sort.php" class="item">Sort Products</a></li>
              </ul>
            </div>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <strong> </strong></div>
          <div id="content">
              <div id="inner"><a href="#" class="add" id="addnew_clicker">Add a new Collection</a>
           	    <div class="add">
<div class="textbox">
                    
    	            	<form action="?&action=add" enctype="multipart/form-data" method="post" id="addform">
                			<table width="100%" border="0" cellspacing="0" cellpadding="0">
                			  <tr>
                			    <td valign="top">New collection name <br />
                                  <input name="name" />
                                  <input type="submit" value="Add" /></td>
                			    <td width="37%" valign="top">&nbsp;</td>
              			      </tr>
              			  </table>
  </form>
                    <div class="textboxFooter"></div></div></div>
         
         <?
		 		if($_GET['deleteid']){
					$q = "DELETE FROM product_collections WHERE collection_id=".$_GET['what']." AND product_id=".$_GET['deleteid'];
					$r = mysqli_query($db,$q) or die(mysqli_error($db));
					echo('<div class="flash ok">
							  <div class="top"></div>
							  <div class="text">Product removed</div>
							  <div class="bottom"></div>
							</div>');
					
				}
				
				if($_GET['action'] == 'delete_collection'){
					$q = "DELETE FROM collections WHERE id=".$_GET['what'];
					$r = mysqli_query($db,$q) or die(mysqli_errno());
					
					echo('<script>window.location = "admin.php"</script>');
					
				}
				
		 		if($_GET['action'] == 'add'){
						if($_POST['name']){
							$q = "INSERT INTO collections (name) values('".$_POST['name']."')";
							$r = mysqli_query($db,$q) or die('<div class="flash error">
													  <div class="top"></div>
													  <div class="text">'.mysqli_error($db).'</div>
													  <div class="bottom"></div>
													</div>');
													
							//GET ID for the redirect
							$q = "SELECT id FROM collections WHERE name='".$_POST['name']."'";
							$r = mysqli_query($db,$q);
							$f = mysqli_fetch_row($r);
							$id = $f[0];
														
							echo('<script>window.location = "collections-assign.php"</script>');
						}else{
							echo('<div class="flash error">
								  <div class="top"></div>
								  <div class="text">No name inserted</div>
								  <div class="bottom"></div>
								</div>');
						}
					
				}
				
		 ?>
  
<div class="datagrid">
<h2><? 
		$q = "SELECT name FROM collections WHERE id=".$_GET['what'];
		$r = mysqli_query($db,$q);
		$f = mysqli_fetch_row($r);
		echo($f[0]);
		
	?> - products <img src="img/my_to_bulk.png" alt="All to bulk" width="92" height="30" style="position:relative; top:10px; cursor:pointer;" onclick="collection_to_bulk(<? echo($_GET['what']); ?>);" /></h2>
                	  <form method="post" action="">
                	    <table>
                	      <tr>
                	        <th>Products</th>
                	        <th>Actions</th>
              	        </tr>
                        
                        
                     	<?
						$q = "SELECT id, product_id FROM product_collections WHERE collection_id=".$_GET['what'];
						$r = mysqli_query($db,$q);
						$c = mysqli_num_rows($r);
						$sudalicha = 'odd';
						for($i=0; $i<$c; $i++){
							$f = mysqli_fetch_row($r);
							
							// GET PRODUCT DETAILS
							$q_prod = "SELECT name, sku, sku2 FROM products WHERE id=".$f[1];
							$r_prod = mysqli_query($db,$q_prod);
							$f_prod = mysqli_fetch_row($r_prod);
                            
                            //echo '<pre>'; print_r($f_prod); echo '</pre>';
							
							if($sudalicha == 'odd'){
								$sudalicha = 'even';
							}else{
								$sudalicha = 'odd';
							}
                            
                            $desc = [$f_prod[1],$f_prod[2]];
                            if ($desc[1] == '')
                                unset($desc[1]);
							
							echo('<tr class="'.$sudalicha.'">
									<td>'.$f_prod[0].' ('.implode(',',$desc).')</td>
									<td class="actions"><a href="?what='.$_GET['what'].'&deleteid='.$f[1].'" class="button delete"><span>Delete</span></a></td>
								</tr>');
						}
						
						if(!$c){
							echo('<tr class="'.$sudalicha.'">
									<td colspan="2">There are no products in this calegory. You can add product using bulk edit in <a href="admin.php">PRODUCTS</a> section <br/>or you can <a href="?what='.$_GET['what'].'&action=delete_collection">DELETE THIS COLLECTION</a>.</td>
								</tr>');
						}
							
						?>
                	      
                        
                        
              	      </table>
              	    </form>
          </div>
              </div>
            </div>
            <hr class="cleaner" />
        </div>

        <div id="footer">

        </div>
    </body>
</html>
