<?
	require('db.php');
	session_start();
	
	//logged?
	if($_GET['action']=='logout'){
		$q = "UPDATE admin SET session_id='' WHERE session_id='".session_id()."'";
		$r = mysqli_query($db,$q);
	}
	
	$q = "select login from admin where session_id='".session_id()."'";
	$r = mysqli_query($db,$q);
	$c = mysqli_num_rows($r);
	if($c){
		$logged = 1;
		$f = mysqli_fetch_row($r);
		$admin_name = $f[0];
	}else{
		$logged = 0;
	}
	
	

?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-language" content="en" />
        <meta name="robots" content="index,follow" />
        <meta name="googlebot" content="index,follow,snippet,archive" />
        <meta name="author" content="ELIVE CZ s.r.o. (c) 2010" />
		<?
			if(!$logged) {
				die('<meta http-equiv="refresh" content="0;url=index.php" />');
				exit();
			}
		?>


        <link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/smoothness/jquery-ui-1.7.2.custom.css" />

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
        <script type="text/javascript" src="js/jquery.ui.datepicker-cs.js"></script>
        <script type="text/javascript" src="js/jquery.simpleTooltip.js"></script>
        <script type="text/javascript" src="js/jquery.flot.pack.js"></script>
        <!--[if IE]><script language="javascript" type="text/javascript" src="js/excanvas.pack.js"></script><![endif]-->
        <script src="js/jquery.wysiwyg.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/jquery.wysiwyg.controls.js" type="text/javascript" charset="utf-8"></script>
        <script type="text/javascript" src="js/jquery.highlight-1.1.source.js"></script>
        

<script type="text/javascript" src="js/submenu.js"></script>
        <script type="text/javascript" src="js/xadmin.js"></script>
        <script type="text/javascript" src="js/jquery.simplemodal.js"></script>
    <script type="text/javascript" src="js/admin.js"></script>
        

    <title>ShanOre.com ADMIN</title>
    <link href="css/jquery.wysiwyg.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
		body,td,th {
			font-family: Arial, sans-serif;
		}
    </style>
    </head>
<body>
        <div id="header">
            <h1>ShanOre.com</h1>
            <div id="user"><a href="?action=logout">Logout</a>

                <br />
            </div>
            
            <div id="mainMenuWrapper">
<ul id="mainMenu">
                    <li><a href="admin.php">Catalog</a></li>
                    <li><a href="orders.php">ORDERS</a></li>
                    <li><a href="customers.php">Customers</a></li>
                    <li class="active"><a href="affiliates.php">Affiliates</a><a href="editor.html"></a></li>
</ul>
            </div>
        </div>
        <div id="contentWrapper">
            <div id="leftMenu">
                <strong>Menu</strong>
              <div id="menu">
                    <ul>
                        <li><a href="affiliates.php" class="item">Affiliates List</a></li>
                        <li><a href="affiliates-orders.php" class="item">Orders</a></li>
                        <li class="active"><a href="affiliates-orders-resolved.php" class="item">Resolved Orders</a></li>
                        <li><a href="affiliates-reports.php" class="item">Reports</a></li>
                        <li><a href="affiliates-links.php" class="item">Permanent Links</a></li>
                    </ul>
            </div>
            <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            <strong> </strong></div>
            <div id="content">

              
              <div id="inner"><? echo($message); ?>
                <div class="datagrid">
       	  <h2>All Existing Orders from Affiliates</h2>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <th width="41%" style="text-align:left;">Affiliate</th>
            <th width="17%" style="text-align:left;">Items</th>
            <th width="15%">Gross Wholesale Price</th>
          </tr>
          
<?
	// listing unsolved only now
	$q = "SELECT DISTINCT affiliates_id FROM affiliates_orders WHERE submitted=2 ORDER BY id DESC";
	$r = mysqli_query($db,$q);
	$c = mysqli_num_rows($r);
	for($i=0; $i<$c; $i++){
		
		$f = mysqli_fetch_row($r);
	
		$aff_id = $f[0];
	
		$q_sub = "SELECT id, product_id, qty, price_one, insert_datetime, size, order_number FROM affiliates_orders WHERE affiliates_id=".$aff_id." AND submitted=2";
		$r_sub = mysqli_query($db,$q_sub) or die(mysqli_error($db));
		$c_sub = mysqli_num_rows($r_sub);
		
		$items = '';
		$price = 0;
		
		$previous_time = '';
		
		for($j=0; $j<$c_sub; $j++){
			$f_sub = mysqli_fetch_row($r_sub);
			
			$q_pro = "SELECT name, sku FROM products WHERE id=".$f_sub[1];
			$r_pro = mysqli_query($db,$q_pro);
			$f_pro = mysqli_fetch_row($r_pro);
			
			if($f_sub[5]){
				$size = $f_sub[5];
			}else{
				$size = 'none';
			}
			
			if($previous_number != $f_sub[6]){
				$previous_number = $f_sub[6];
				$items .= '</div><h2 style="color:black;">Order # '.$f_sub[6].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="cursor:pointer; color:#BB0000;" onclick="$(\'#'.$f_sub[0].'\').slideToggle(); $(\'#'.$aff_id.'_add\').slideToggle();  $(\'#'.$aff_id.'_acc\').slideToggle();">open / close</span></h2><div id="'.$f_sub[0].'" style="display:none">';
			}
			
			$items .= 'ordered: <strong>'.$f_sub[4].'</strong><br>'.$f_sub[2].' X '.$f_pro[0].' ('.$f_pro[1].')<br/>SIZE:'.$size.'<hr/>';
			$price = $price + (intval($f_sub[3]) * $f_sub[2]);
			
		}
		
		$q_aff = "SELECT debtor_no, name, address1, address2, address3, contact, phone, email FROM affiliates WHERE id=".$aff_id;
		$r_aff = mysqli_query($db,$q_aff);
		$f_aff = mysqli_fetch_row($r_aff);
		
		echo('<tr id="line_'.$aff_id.'" class="odd">
					<td valign="top" align="left" style="text-align:left;">
						<strong>'.$f_aff[1].'</strong><br />
						<strong>Debtor no</strong>: '.$f_aff[0].'<br/><br/>
						<div id="'.$aff_id.'_add" style="display:none">
						<strong>ADDRESS</strong><br />
						'.$f_aff[2].'<br/>
						'.$f_aff[3].'<br/>
						'.$f_aff[4].'<br/><br/>
						<strong>CONTACT</strong><br/>'.$f_aff[5].'
						<br/>'.$f_aff[6].'
						<br/>'.$f_aff[7].'<br/>
						<img src="img/print.png" style="float:left; cursor:pointer; clear:both;" onclick="window.open(\'print_aff_order.php?ordernumber='.$f_sub[6].'\',\'\',\'width=980,resizable=0,scrollbars=1\')">
						</div>
					</td>
					<td valign="top" style="text-align:left;">'.$items.'</td>
					<td valign="top">$ '.$price.'</td>
					</td></tr>');
		
	}

?>
                      
             </table>

          </div>
              </div>
            </div>
            <hr class="cleaner" />
            
      <div id="basic-modal-content"> 
      		<!-- edit form here -->
            
	  </div>
      <div id="basic-modal-content-2"> 
      		<!-- edit form here -->
            
	  </div>
            
            
</div>

        <div id="footer">

        </div> 
    </body>

<?
	if($_GET['search_txt']){
		echo("<script>
					$('td').highlight('".$_GET['search_txt']."','highlight');
			  </script>");
	}
?>    

</html>
