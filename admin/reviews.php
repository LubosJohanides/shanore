<?
	require('db.php');
	session_start();
	
	//logged?
	if($_GET['action']=='logout'){
		$q = "UPDATE admin SET session_id='' WHERE session_id='".session_id()."'";
		$r = mysqli_query($db,$q);
	}
	
	$q = "select login from admin where session_id='".session_id()."'";
	$r = mysqli_query($db,$q);
	$c = mysqli_num_rows($r);
	if($c){
		$logged = 1;
		$f = mysqli_fetch_row($r);
		$admin_name = $f[0];
	}else{
		$logged = 0;
	}
	
	

?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-language" content="en" />
        <meta name="robots" content="index,follow" />
        <meta name="googlebot" content="index,follow,snippet,archive" />
        <meta name="author" content="ELIVE CZ s.r.o. (c) 2010" />
		<?
			if(!$logged) {
				die('<meta http-equiv="refresh" content="0;url=index.php" />');
				
			}
		?>


        <link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/smoothness/jquery-ui-1.7.2.custom.css" />

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
        <script type="text/javascript" src="js/jquery.ui.datepicker-cs.js"></script>
        <script type="text/javascript" src="js/jquery.simpleTooltip.js"></script>
        <script type="text/javascript" src="js/jquery.flot.pack.js"></script>
        <!--[if IE]><script language="javascript" type="text/javascript" src="js/excanvas.pack.js"></script><![endif]-->
        <script src="js/jquery.wysiwyg.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/jquery.wysiwyg.controls.js" type="text/javascript" charset="utf-8"></script>
        <script type="text/javascript" src="js/jquery.highlight-1.1.source.js"></script>
        

<script type="text/javascript" src="js/submenu.js"></script>
        <script type="text/javascript" src="js/xadmin.js"></script>
        <script type="text/javascript" src="js/jquery.simplemodal.js"></script>
    <script type="text/javascript" src="js/admin.js"></script>
        

    <title>ShanOre.com ADMIN</title>
    <link href="css/jquery.wysiwyg.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
		body,td,th {
			font-family: Arial, sans-serif;
		}
    </style>
    </head>
<body>
        <div id="header">
            <h1>ShanOre.com</h1>
            <div id="user"><a href="?action=logout">Logout</a>

                <br />
            </div>
            
            <div id="mainMenuWrapper">
<ul id="mainMenu">
                    <li><a href="admin.php">Catalog</a></li>
                    <li><a href="orders.php">ORDERS</a></li>
                    <li class="active"><a href="admin-options.php">Customers</a></li>
                    <li><a href="affiliates.php">Affiliates</a><a href="editor.html"></a></li>
</ul>
            </div>
        </div>
        <div id="contentWrapper">
            <div id="leftMenu">
                <strong>Menu</strong>
              <div id="menu">
                    <ul>
                        <li><a href="customers.php" class="item">Customers list</a></li>
                        <li class="active"><a href="reviews.php" class="item">Reviews</a></li>
                    </ul>
            </div>
            <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            <strong> </strong></div>
            <div id="content">

              
              <div id="inner"><? echo($message); ?>
                <div class="datagrid">
       	  <h2>Reviews</h2>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <th align="center">name</th>
            <th>real client name / email</th>
            <th>review</th>
            <th>actions</th>
          </tr>
<?
		$q = "SELECT id, user_id, name, text, published FROM reviews";
		$r = mysqli_query($db,$q) or die(mysqli_error($db));
		$c = mysqli_num_rows($r);
		
		for($i=0; $i<$c; $i++){
			$f = mysqli_fetch_row($r);
			
			if($f[4]){
				$onoff = "published_on";
			}else{
				$onoff = "published_off";
			}
			
			$q_name = "SELECT title, name, surname, email FROM customers WHERE id=".$f[1];
			$r_name = mysqli_query($db,$q_name);
			$f_name = mysqli_fetch_row($r_name);
			
			echo('<tr id="line_'.$f[0].'">
					<td valign="top">&nbsp;<strong>'.$f[2].'</strong></td>
					<td valign="top">'.$f_name[0].' '.$f_name[1].' '.$f_name[2].'<br/>'.$f_name[3].'</td>
					<td valign="top">'.$f[3].'</td>
					
					
					<td valign="top">
					<img src="img/'.$onoff.'.png" width="59" height="58" alt="Toggle Sale" class="onoff_btn" style="cursor:pointer; float:right; clear:both;" onclick="publish_review('.$f[0].');" id="review_'.$f[0].'" />
				  </tr>');
		}

?>
                      
             </table>

          </div>
              </div>
            </div>
            <hr class="cleaner" />
            
      <div id="basic-modal-content"> 
      		<!-- edit form here -->
            
	  </div>
      <div id="basic-modal-content-2"> 
      		<!-- edit form here -->
            
	  </div>
            
            
</div>

        <div id="footer">

        </div> 
    </body>

<?
	if($_GET['search_txt']){
		echo("<script>
					$('td').highlight('".$_GET['search_txt']."','highlight');
			  </script>");
	}
?>    

</html>
