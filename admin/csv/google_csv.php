<?
	require('../db.php');
	
	  
	$csvTitle = "Shanore Products for Google";
	$titleArray = array_keys($data[0]);
	$delimiter = "\t";
	  
	$filename="Search_Export.xls";
	  
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=$filename");
	header("Pragma: no-cache");
	header("Expires: 0");
	  
	 echo("title\tlink\tdescription\tid\tcondition\tprice\tavailability\timage_link\tbrand\tgoogle_product_category\titem_group_id\tshipping\tproduct_type\tgender\tage_group\tcolor\tadwords_grouping\r\n");
	 
	  
	$q = "SELECT * FROM products WHERE deleted=0";
	$r = mysql_query($q);
	$c = mysql_num_rows($r);
	for($i=0; $i<$c; $i++){
		$f = mysql_fetch_assoc($r);
		
		// getting URL
		$urlname = strtolower(str_replace(' ','-',$f['name']));
		$urlname = str_replace("'","",$urlname);
		$urlname = str_replace("/","-",$urlname);
		$urlname = strtolower(str_replace('.','',$urlname));
		$urlname = str_replace(",","",$urlname);
		$urlname = str_replace("\"","",$urlname);
		$urlname = str_replace("(","",$urlname);
		$urlname = str_replace(")","",$urlname);
		$urlname = str_replace("&","and",$urlname);
		$urlname = str_replace("%","percent",$urlname);
		$link = $urlname.'-product-'.$f['id'].'.html';
		
		$dolar_price = $f['price_dollar'];
		
		$description = preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', strip_tags($f['description']));
		
		$q_col = "SELECT collection_id FROM product_collections WHERE product_id=".$f['id'];
		$r_col = mysql_query($q_col);
		if($f_col = mysql_fetch_row($r_col)){
			$collection_id = $f_col[0];
			
			// get name
			$q_colname = "SELECT name FROM collections WHERE id=".$collection_id;
			$r_colname = mysql_query($q_colname);
			$f_colname = mysql_fetch_row($r_colname);
			$collection_name = $f_colname[0];
			
		}else{
			$collection_name = '';
		}
		
		if($f['gold_item']){
			$shipping = 'US::Fedex:0.00 USD';
		}else{
			$shipping = 'US::USPS:0.00 USD';
		}
		
		// gender - id 21
		$q = "SELECT option_value_id FROM product_options WHERE option_type_id = 21 AND product_id=".$f['id'];
		$r_opt = mysql_query($q);
		$c_opt = mysql_num_rows($r_opt);
		if($c_opt){
			$f_opt = mysql_fetch_row($r_opt);
			
			$q_tmp = "SELECT name FROM product_options_values WHERE id=".$f_opt[0];
			$r_tmp = mysql_query($q_tmp);
			$f_tmp = mysql_fetch_row($r_tmp);
			
			$gender = $f_tmp[0];
			
		}else{
			$gender = '';
		}
		
		
		// age_group - id 22
		$q = "SELECT option_value_id FROM product_options WHERE option_type_id = 22 AND product_id=".$f['id'];
		$r_opt = mysql_query($q);
		$c_opt = mysql_num_rows($r_opt);
		if($c_opt){
			$f_opt = mysql_fetch_row($r_opt);
			
			$q_tmp = "SELECT name FROM product_options_values WHERE id=".$f_opt[0];
			$r_tmp = mysql_query($q_tmp);
			$f_tmp = mysql_fetch_row($r_tmp);
			
			$age_group = $f_tmp[0];
			
		}else{
			$age_group = '';
		}
		
		// color - id 24
		$q = "SELECT option_value_id FROM product_options WHERE option_type_id = 24 AND product_id=".$f['id'];
		$r_opt = mysql_query($q);
		$c_opt = mysql_num_rows($r_opt);
		if($c_opt){
			$f_opt = mysql_fetch_row($r_opt);
			
			$q_tmp = "SELECT name FROM product_options_values WHERE id=".$f_opt[0];
			$r_tmp = mysql_query($q_tmp);
			$f_tmp = mysql_fetch_row($r_tmp);
			
			$color = $f_tmp[0];
			
		}else{
			$color = '';
		}
		
		// adwords_grouping - id 25
		$q = "SELECT option_value_id FROM product_options WHERE option_type_id = 25 AND product_id=".$f['id'];
		$r_opt = mysql_query($q);
		$c_opt = mysql_num_rows($r_opt);
		if($c_opt){
			$f_opt = mysql_fetch_row($r_opt);
			
			$q_tmp = "SELECT name FROM product_options_values WHERE id=".$f_opt[0];
			$r_tmp = mysql_query($q_tmp);
			$f_tmp = mysql_fetch_row($r_tmp);
			
			$adwords_grouping = $f_tmp[0];
			
		}else{
			$adwords_grouping = '';
		}
		
		// google_category - id 23
		$q = "SELECT option_value_id FROM product_options WHERE option_type_id = 23 AND product_id=".$f['id'];
		$r_opt = mysql_query($q);
		$c_opt = mysql_num_rows($r_opt);
		if($c_opt){
			$f_opt = mysql_fetch_row($r_opt);
			
			$q_tmp = "SELECT name FROM product_options_values WHERE id=".$f_opt[0];
			$r_tmp = mysql_query($q_tmp);
			$f_tmp = mysql_fetch_row($r_tmp);
			
			$category = $f_tmp[0];
			
		}else{
			$category = '';
		}
		
		
		
		if($f['configurable']){
			
			
				// round or princess or both?
				// option_type = 15
				// round = 401
				// princess = 402
				
				$q_cut = "SELECT option_value_id FROM product_options WHERE product_id=".$f['id']." AND option_type_id=15";
				$r_cut = mysql_query($q_cut);
				if($f_cut = mysql_fetch_row($r_cut)){
					if($f_cut[0] == '401'){
						// round cut
						$q_stones = "SELECT price, name FROM stone_prices WHERE currency='dolar' AND name like '%round%'";
					}
					if($f_cut[0] == '402'){
						// princess cut
						$q_stones = "SELECT price, name FROM stone_prices WHERE currency='dolar' AND name like '%princess%'";
					}
				}else{
					// no cuts defined, hence using both
					$q_stones = "SELECT price, name FROM stone_prices WHERE currency='dolar'";
				}
				
				$r_stones = mysql_query($q_stones);
				$c_stones = mysql_num_rows($r_stones);
				for($i_stones=0; $i_stones < $c_stones; $i_stones++){
					$f_stones = mysql_fetch_assoc($r_stones);
					
					$pricefull = floatval($f['price_dollar']) + floatval($f_stones['price']);
					
					$stone_description = '';
					
					if(strpos($f_stones['name'],'round') > -1) $stone_description .= 'Round Cut, ';
					if(strpos($f_stones['name'],'princess') > -1) $stone_description .= 'Princess Cut, ';
					
					if(strpos($f_stones['name'],'-a-') > -1) $stone_description .= 'Diamond Colour F, Diamond Clarity Si1/Si2, ';
					if(strpos($f_stones['name'],'-b-') > -1) $stone_description .= 'Diamond Colour H, Diamond Clarity Si1/Si2, ';
					if(strpos($f_stones['name'],'-c-') > -1) $stone_description .= 'Diamond Colour J, Diamond Clarity Si1/Si2, ';
					
					if(strpos($f_stones['name'],'25') > -1) $stone_description .= 'Diamond Weight: 25ct. ';
					if(strpos($f_stones['name'],'33') > -1) $stone_description .= 'Diamond Weight: 33ct. ';
					if(strpos($f_stones['name'],'50') > -1) $stone_description .= 'Diamond Weight: 50ct. ';
					
					//$stone_description .= ' PPP = ('.$f['price_dollar'].') + ('.$f_stones['price'].')';
					
					$item_group_id = $f['sku'];
					echo($f['name']."\thttp://www.shanore.com/".$link."\t".$stone_description.$description."\t".$f['sku']."-".$f_stones['name']."\tnew\t".$pricefull." USD\tavailable for order\thttp://www.shanore.com/products_images/".$f['id'].".jpg\tShanore\t".$category."\t".$item_group_id."\t".$shipping."\t".$collection_name."\t".$gender."\t".$age_group."\t".$color."\t".$adwords_grouping."\r\n");
					
				}
			
			
		}else{
			$item_group_id = '';
			echo($f['name']."\thttp://www.shanore.com/".$link."\t".$description."\t".$f['sku']."\tnew\t".$f['price_dollar']." USD\tavailable for order\thttp://www.shanore.com/products_images/".$f['id'].".jpg\tShanore\t".$category."\t".$item_group_id."\t".$shipping."\t".$collection_name."\t".$gender."\t".$age_group."\t".$color."\t".$adwords_grouping."\r\n");
		}
		
		
		
	}

	

?>