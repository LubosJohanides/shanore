<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    	<meta http-equiv="Content-language" content="en" />
        <meta name="robots" content="index,follow" />
        <meta name="googlebot" content="index,follow,snippet,archive" />
        <meta name="author" content="ELIVE CZ s.r.o. (c) 2010" />

        <link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/smoothness/jquery-ui-1.7.2.custom.css" />

        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
        <script type="text/javascript" src="js/jquery.ui.datepicker-cs.js"></script>
        <script type="text/javascript" src="js/jquery.simpleTooltip.js"></script>
        <script type="text/javascript" src="js/jquery.flot.pack.js"></script>
        <!--[if IE]><script language="javascript" type="text/javascript" src="js/excanvas.pack.js"></script><![endif]-->
        <script src="js/jquery.wysiwyg.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/jquery.wysiwyg.controls.js" type="text/javascript" charset="utf-8"></script>
        <script type="text/javascript" src="js/jquery.highlight-1.1.source.js"></script>

		<script type="text/javascript" src="js/submenu.js"></script>
        <script type="text/javascript" src="js/xadmin.js"></script>
        <script type="text/javascript" src="js/jquery.simplemodal.js"></script>
    	<script type="text/javascript" src="js/admin.js"></script>


    <title>ShanOre.com ADMIN</title>
    <link href="css/jquery.wysiwyg.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
		body,td,th {
			font-family: Arial, sans-serif;
		}
    </style>
    </head>
<body>

            <h1>ShanOre.com</h1>
<!--             <div id="content">-->
<?php

		$timestamp = $_POST['TIMESTAMP'];
		$merchant_id = $_POST['MERCHANT_ID'];
		$account = $_POST['ACCOUNT'];
		$master_order_id = $_POST['ORDER_ID']; //Defines OrderId and Monthly Payment

		$curr = $_POST['CURRENCY'];
		$total = $_POST['AMOUNT'];
		$payerref = $_POST['PAYER_REF'];
		$pmt_ref = $_POST['PMT_REF'];
		$md5hash = $_POST['MD5HASH'];
		$tmp = $_POST['TMP'];
		//$result = $_POST['RESULT'];
		//$message = $_POST['MESSAGE'];
		//$authcode = $_POST['AUTHCODE'];

	//Hash Variables
//	echo('Time: '.$timestamp.'<br>');
//	echo('Merchant: '.$merchant_id.'<br>');
//	echo('Order Id: '.$master_order_id.'<br>');
//	echo('Account: '.$account.'<br>');
//	echo('Total: '.$total.'<br>');
//	echo('Currency: '.$curr.'<br>');
//	echo('PAYER_REF: '.$payerref.'<br>');
//	echo('PMT_REF: '.$pmt_ref.'<br>');
//	echo('HASH: '.$md5hash.'<br>');
//	echo('TMP HASH: '.$tmp.'<br>');

$url = "https://epage.payandshop.com/epage-remote-plugins.cgi";

$post_string = '<request type="receipt-in" timestamp="'.$timestamp.'">
<merchantid>'.$merchant_id.'</merchantid>
 <account>'.$account.'</account>
<orderid>'.$master_order_id.'</orderid>
<paymentdata>
 <cvn>
 <number></number>
 </cvn>
</paymentdata>
<amount currency="'.$curr.'">'.$total.'</amount>
<payerref>'.$payerref.'</payerref>
<paymentmethod>'.$payerref.'</paymentmethod>
<autosettle flag="0" />
<md5hash></md5hash>
<sha1hash>'.$md5hash.'</sha1hash>
<comments>
<comment id="1" />
<comment id="2" />
</comments>
<tssinfo>
<address type="billing">
<code />
<country />
</address>
<address type="shipping">
<code />
<country />
</address>
<custnum></custnum>
<varref></varref>
<prodid></prodid>
</tssinfo>
</request>';

//echo('<pre>'.$post_string.'</pre>');

 /*
   * XML Sender/Client.
   */
  // Get our XML. You can declare it here or even load a file.

  // We send XML via CURL using POST with a http header of text/xml.
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
  curl_setopt($ch, CURLOPT_REFERER, '$url');
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  $ch_result = curl_exec($ch);
  curl_close($ch);
  // Print CURL result.
  //echo $ch_result;

$doc= new DOMDocument();
$doc->loadXML($ch_result);

//echo('ERROR:<br>');
$sourceNode = $doc->getElementsByTagName("response timestamp");
$timestamp_error = $sourceNode->item(0)->nodeValue;
//echo($timestamp_error.'<br>');

$sourceNode = $doc->getElementsByTagName("merchantid");
$merchantid_error = $sourceNode->item(0)->nodeValue;
//echo($merchantid_error.'<br>');

$sourceNode = $doc->getElementsByTagName("account");
$account_error = $sourceNode->item(0)->nodeValue;
//echo($account_error.'<br>');

$sourceNode = $doc->getElementsByTagName("result");
$result_error = $sourceNode->item(0)->nodeValue;
//echo($result_error.'<br>');

$sourceNode = $doc->getElementsByTagName("message");
$message_error = $sourceNode->item(0)->nodeValue;
//echo($message_error.'<br>');


//echo('OK:<br>');
$sourceNode = $doc->getElementsByTagName("response timestamp");
$timestamp = $sourceNode->item(0)->nodeValue;
//echo($timestamp.'<br>');

$sourceNode = $doc->getElementsByTagName("merchantid");
$merchantid_ok = $sourceNode->item(0)->nodeValue;
//echo($merchantid_ok.'<br>');

$sourceNode = $doc->getElementsByTagName("account");
$account_ok = $sourceNode->item(0)->nodeValue;
//echo($account_ok.'<br>');

$sourceNode = $doc->getElementsByTagName("orderid");
$orderid_tmp = $sourceNode->item(0)->nodeValue;
//echo($orderid_tmp.'<br>');

$sourceNode = $doc->getElementsByTagName("result");
$result = $sourceNode->item(0)->nodeValue;
//echo($result.'<br>');

$sourceNode = $doc->getElementsByTagName("message");
$message = $sourceNode->item(0)->nodeValue;
//echo($message.'<br>');

$sourceNode = $doc->getElementsByTagName("pasref");
$pasref = $sourceNode->item(0)->nodeValue;
//echo($pasref.'<br>');

$sourceNode = $doc->getElementsByTagName("md5hash");
$realexmd5 = $sourceNode->item(0)->nodeValue;
//echo($realexmd5.'<br>');

$sourceNode = $doc->getElementsByTagName("sha1hash");
$sha1hash_ok = $sourceNode->item(0)->nodeValue;
//echo($sha1hash_ok.'<br>');


		$orderid_tmp = explode("-",$orderid_tmp); //Defines OrderId and Monthly Payment
		$orderid = $orderid_tmp[0]; 	//Gets OrderId
		$install_pay = $orderid_tmp[1]; //Gets Montly Payment status
		$id = $orderid_tmp[2];			//Gets $id of new table order_payment_Installments if comes from admin, otherwise, 0 value


if ($result == "00") {
		require('db.php');
		echo('install_pay: '.$install_pay);
		//Added By Manuel
		//$install_pay=="2"  means, the payment comes from admin
 		if($install_pay=="2"){

			//settlest payment in order_payment_Installments table
			$q = "UPDATE order_payment_Installments SET Payment_Settled=1 WHERE id='".$id."'";
			//echo($q);
			$r = mysql_query($q) or die(mysql_error());

			//checks if all payments have been paid
			$q = "SELECT id, order_id  FROM order_payment_Installments WHERE order_id='".$orderid."' AND Payment_Settled=0";
			$r = mysql_query($q) or die(mysql_error());
			$c = mysql_num_rows($r);
			//echo($c);
				//Updates status to 2 if the sum of all number of payments is 11.
				if($c == 0){
					//////////////////////////////////////////////////////////////////////////
					// status is set up to 2 in orders_master and orders indicating that payment has been paid
					$q = "UPDATE orders_master SET status=1, paid=1, insert_datetime=now() WHERE id='".$orderid."'";
					$r = mysql_query($q) or die(mysql_error());

					$q = "UPDATE orders SET status=1, insert_datetime=now() WHERE order_master_id='".$orderid."'";
					$r = mysql_query($q) or die(mysql_error());
					/////////////////////////////////////////////////////////////////////////

					//else, if still some payments to do then set status to 3 (pending)
				}else{
					/////////////////////////////////////////////////////////////////////////////////
					// as beeing an Installment payment, status must be set up to 3 in orders_master and orders indicating that payment is still pending
					$q = "UPDATE orders_master SET status=3 WHERE id='".$orderid."' AND status<>3";
					$r = mysql_query($q) or die(mysql_error());

					$q = "UPDATE orders SET status=3 WHERE order_master_id='".$orderid."' AND status<>3";
					$r = mysql_query($q) or die(mysql_error());
					/////////////////////////////////////////////////////////////////////////////////
				}//if
		}//if
?>
			 <div id="header">
             <br/><br/><br/>
			The payment was successful.<br/><br/>

            Result No: <?= $result_error; ?><br />
            Message: <?= $message ; ?><br /><br/>

			Back to Payment Installments <a href="orders_installment.php"><b><u>click here </u></b></a>
			<br/><br/>
            </div>
            <?
            //*************************** SEND EMAILS HERE ****************************
            ?>

            <script type="text/javascript">
				//window.location = "https://www.shanore.com/confirmation.php?mid=<? //echo($orderid); ?>";
			</script>

<?
	//If error
	} else {
		require('db.php');

		// update status
		//$q = "UPDATE orders SET status=9 WHERE order_master_id='".$orderid."'";
		//$r = mysql_query($q) or die(mysql_error());

		// update paid status and master status
		//$q = "UPDATE orders_master SET status=9 WHERE id='".$orderid."'";
		//$r = mysql_query($q) or die(mysql_error());
?>
	 <div id="header">
            <br/><br/><br/>
            There was an error processing your subscription<br /><br />
            Error Result No: <?= $result_error; ?><br />
            Message: <?= $message ; ?><br /><br />

            <span class="italic">To try again please <a href="orders_installment.php"><b><u>click here</u></b></a><br><BR>
           <!-- Please contact our customer care department at <a href="mailto:info@shanore.com"><b><u>info@shanore.com</u></b></a>-->
            </span>
	 </div>
<?
	}//if
?>


  </body>
</html>
