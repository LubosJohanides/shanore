<?
	require('db.php');
	//MySQL_Query("SET NAMES UTF8");
	session_start();

	//logged?
	if($_GET['action']=='logout'){
		$q = "UPDATE admin SET session_id='' WHERE session_id='".session_id()."'";
		$r = mysqli_query($db,$q);
	}

	$q = "select login from admin where session_id='".session_id()."'";
	$r = mysqli_query($db,$q);
	$c = mysqli_num_rows($r);
	if($c){
		$logged = 1;
		$f = mysqli_fetch_row($r);
		$admin_name = $f[0];
	}else{
		$logged = 0;
	}



?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-language" content="en" />
        <meta name="robots" content="index,follow" />
        <meta name="googlebot" content="index,follow,snippet,archive" />
        <meta name="author" content="ELIVE CZ s.r.o. (c) 2010" />
		<?
			if(!$logged) {
				die('<meta http-equiv="refresh" content="0;url=index.php" />');

			}
		?>


        <link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/smoothness/jquery-ui-1.7.2.custom.css" />

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
        <script type="text/javascript" src="js/jquery.ui.datepicker-cs.js"></script>
        <script type="text/javascript" src="js/jquery.simpleTooltip.js"></script>
        <script type="text/javascript" src="js/jquery.flot.pack.js"></script>
        <!--[if IE]><script language="javascript" type="text/javascript" src="js/excanvas.pack.js"></script><![endif]-->
        <script src="js/jquery.wysiwyg.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/jquery.wysiwyg.controls.js" type="text/javascript" charset="utf-8"></script>
        <script type="text/javascript" src="js/jquery.highlight-1.1.source.js"></script>


<script type="text/javascript" src="js/submenu.js"></script>
        <script type="text/javascript" src="js/xadmin.js"></script>
        <script type="text/javascript" src="js/jquery.simplemodal.js"></script>
    <script type="text/javascript" src="js/admin.js"></script>


    <title>ShanOre.com ADMIN</title>
    <link href="css/jquery.wysiwyg.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
		body,td,th {
			font-family: Arial, sans-serif;
		}
    </style>
    </head>
<body>
        <div id="header">
            <h1>ShanOre.com</h1>
            <div id="user"><a href="?action=logout">Logout</a>

                <br />
            </div>
            <div id="mainMenuWrapper">
              <ul id="mainMenu">
                <li><a href="admin.php">Catalog</a></li>
                <li class="active"><a href="orders.php">ORDERS</a></li>
                <li><a href="customers.php">Customers</a><a href="editor.html"></a></li>
                <li><a href="affiliates.php">Affiliates</a><a href="editor.html"></a></li>
              </ul>
            </div>
        </div>
        <div id="contentWrapper">
            <div id="leftMenu">
                <strong>Menu</strong>
              <div id="menu">
                    <ul>
                    	<li><a href="orders_installment.php" class="item">Installment Orders</a></li>
                        <li><a href="orders.php" class="item">Confirmed Orders</a></li>
                        <li><a href="processing_orders.php" class="item">Processing Orders</a></li>
                        <li><a href="resolved_orders.php" class="item">Completed Orders</a></li>
                        <li><a href="deleted_orders.php" class="item">Canceled Orders</a></li>
                        <li><a href="failed_orders.php" class="item">Failed Orders</a></li>
                        <li><a href="orders_unpaid.php" class="item">Unpaid Orders</a></li>
                        <li class="active"><a href="abandoned_carts.php" class="item">Abandoned Carts</a></li>
                    </ul>
            </div>
            <br />
                <br />
                <br />
                <br />
                <br />
                <br />

            <div class="filter">
              <h3>Quick Search</h3>
              <form action="" method="get" enctype="multipart/form-data">
                    Word / Phrase
                    <input type="text" name="search_txt" style="width:165px;" value="<? echo($_GET['search_txt']); ?>" />
                    <br />
                    <input name="year" value="<? echo($_GET['year']); ?>" type="hidden" />
                    <input name="month" value="<? echo($_GET['month']); ?>" type="hidden" />
                    <input type="submit" value="Search" />

              </form>
            </div>

            </div>
            <div id="content">


              <div id="inner"><? echo($message); ?>
                <div class="datagrid">
       	  <h2>Abandoned Carts</h2>

          <?
						if($_GET['year']){
							$search_str .= " AND YEAR(insert_datetime)='".$_GET['year']."'";
							$year = $_GET['year'];
						}else{
							$search_str .= " AND YEAR(insert_datetime)='".date('Y')."'";
							$year = date('Y');
						}

						if($_GET['month']){
							$search_str .= " AND MONTH(insert_datetime)='".$_GET['month']."'";
							$month = $_GET['month'];
						}else{
							if($_GET['month'] != '0'){
								$search_str .= " AND MONTH(insert_datetime)='".date('n')."'";
								$month = date('n');
							}
						}

						if($_GET['search_txt']){
							$_GET['search_txt'] = strtolower($_GET['search_txt']);

							$search_str .= " AND (LOWER(name) like '%".$_GET['search_txt']."%' OR LOWER(surname) like '%".$_GET['search_txt']."%' OR LOWER(email) like '%".$_GET['search_txt']."%' OR LOWER(company) like '%".$_GET['search_txt']."%' OR LOWER(address1) like '%".$_GET['search_txt']."%' OR LOWER(address2) like '%".$_GET['search_txt']."%' OR LOWER(city) like '%".$_GET['search_txt']."%' OR LOWER(county) like '%".$_GET['search_txt']."%' OR LOWER(country) like '%".$_GET['search_txt']."%') ";
						}



?>


          <form action="" method="get" enctype="multipart/form-data" id="filterfrm">
          Show only year: <select name="year" id="year" onChange="$('#filterfrm').submit();">
                        					<option value="">This Year</option>
<?
							$q = "SELECT distinct year FROM affiliates_benefit ORDER BY year DESC";
							$r = mysqli_query($db,$q);
							$c = mysqli_num_rows($r);
							for($i=0; $i<$c; $i++){
								$f = mysqli_fetch_row($r);
								$selected="";
								if($f[0] == $_GET['year']) $selected = " SELECTED";

								echo('<option value="'.$f[0].'"'.$selected.'>'.$f[0].'</option>');
							}
?>
                        </select>

                         Month: <select name="month" id="month" onChange="$('#filterfrm').submit();" >
                                    <option value="0" <? if($month == '0') echo('SELECTED'); ?>>All Year</option>
                                    <option value="1" <? if($month == '1') echo('SELECTED'); ?>>Jan</option>
                                    <option value="2" <? if($month == '2') echo('SELECTED'); ?>>Feb</option>
                                    <option value="3" <? if($month == '3') echo('SELECTED'); ?>>Mar</option>
                                    <option value="4" <? if($month == '4') echo('SELECTED'); ?>>Apr</option>
                                    <option value="5" <? if($month == '5') echo('SELECTED'); ?>>May</option>
                                    <option value="6" <? if($month == '6') echo('SELECTED'); ?>>Jun</option>
                                    <option value="7" <? if($month == '7') echo('SELECTED'); ?>>Jul</option>
                                    <option value="8" <? if($month == '8') echo('SELECTED'); ?>>Aug</option>
                                    <option value="9" <? if($month == '9') echo('SELECTED'); ?>>Sep</option>
                                    <option value="10" <? if($month == '10') echo('SELECTED'); ?>>Oct</option>
                                    <option value="11" <? if($month == '11') echo('SELECTED'); ?>>Nov</option>
                                    <option value="12" <? if($month == '12') echo('SELECTED'); ?>>Dec</option>
                        </select>
            </form>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <th width="13%">Order #</th>
            <th width="41%" style="text-align:left">Products Ordered</th>
            <th width="17%">Billing</th>
            <th width="14%">Total &euro;</th>
            <th width="15%">Actions</th>
          </tr>

<?

		$q = "SELECT id, title, name, surname, email, company, address1, address2, city, county, post_code, country, phone_day, phone_evening, ship_address1, ship_address2, ship_city, ship_county, ship_post_code, ship_country, total, delivery_type, insert_datetime, currency, ip, tracking_code FROM orders_master WHERE status=0 AND paid=0 AND email<>'' ".$search_str." ORDER BY id DESC";
		$r = mysqli_query($db,$q) or die(mysqli_error($db));
		$c = mysqli_num_rows($r);

		for($i=0; $i<$c; $i++){
			$f = mysqli_fetch_row($r);

			$q_country = "SELECT country FROM ip_to_country WHERE ip='".$f[24]."'";
			$r_country = mysqli_query($db,$q_country);
			$f_country = mysqli_fetch_row($r_country);
			$country = $f_country[0];

			// lets harvest products
			$q_sub = "SELECT id, product_id, qty, price, size, wrapping FROM orders WHERE order_master_id='".$f[0]."'";
			$r_sub = mysqli_query($db,$q_sub) or die(mysqli_error());
			$c_sub = mysqli_num_rows($r_sub);
			$products = '';
			for($j=0; $j<$c_sub; $j++){
				$ff = mysqli_fetch_row($r_sub);

				$q_detail = "SELECT sku2, name FROM products WHERE id=".$ff[1];
				$r_detail = mysqli_query($db,$q_detail) or die(mysqli_error());
				$fff = mysqli_fetch_row($r_detail);

				if($ff[4]) $ff[4] = '<br />SIZE '.$ff[4];

				$products .= $ff[2].' x <strong><a href="https://www.shanore.com/detail.php?id=a'.$ff[1].'" target="_new">'.$fff[1].'</a></strong>'.$ff[4].'<br/>';
			}

			$datetime = $f[22];

			$val = explode(" ",$f[22]);
		   	$date = explode("-",$val[0]);
		   	$time = explode(":",$val[1]);
		   	$datetime = date('D jS F Y',mktime($time[0],$time[1],$time[2],$date[1],$date[2],$date[0]));

			echo('<tr id="line_'.$f[0].'" class="odd">
					<td valign="top">'.$f[0].'</td>
					<td valign="top" style="text-align:left;"><h2 style="color:#000;margin:0; padding:0;">'.$datetime.'</h2>'.$products.'</td>
					<td valign="top">'.$f[2].' '.$f[3]);
			if(($f[5]) AND ($f[5] != 'undefined')){
				echo('<br />'.$f[5]);
			}

			if($f[7]) $f[7] .= '<br>';
			if($f[9]) $f[9] .= '<br>';
			if($f[10]) $f[10] .= '<br>';

			echo('<br />'.$f[6].'<br />
					  '.$f[7].'
					  '.$f[8].'<br />
					  '.$f[9].'
					  '.$f[10].'
					  '.$f[11].'<br/>
					  <strong>'.$f[21].'</strong><br />
					  Track #: '.$f[25].'
					  <p><a href="mailto:'.$f[4].'">'.$f[4].'</a><br />'.$f[12].'
					</p>
					<img src="https://www.shanore.com/img/flags/'.strtolower($country).'.png" height="20" />&nbsp;&nbsp;'.$country.'<br/>IP: '.$f[24].'
					</td>
					<td valign="top">'.number_format($f[20],2).'<br/>['.$f[23].']</td>
					<td valign="top">
						&nbsp;
					</td>
				  </tr>');
		}

?>

             </table>

          </div>
              </div>
            </div>
            <hr class="cleaner" />

      <div id="basic-modal-content">
      		<!-- edit form here -->

	  </div>
      <div id="basic-modal-content-2">
      		<!-- edit form here -->

	  </div>


</div>

        <div id="footer">

        </div>
    </body>

<?
	if($_GET['search_txt']){
		echo("<script>
					$('td').highlight('".$_GET['search_txt']."','highlight');
			  </script>");
	}
?>

</html>
