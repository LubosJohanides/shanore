<?
	require('db.php');
	MySQL_Query("SET NAMES UTF8");
	session_start();
	
	//logged?
	if($_GET['action']=='logout'){
		$q = "UPDATE admin SET session_id='' WHERE session_id='".session_id()."'";
		$r = mysql_query($q);
	}
	
	$q = "select login from admin where session_id='".session_id()."'";
	$r = mysql_query($q);
	$c = mysql_num_rows($r);
	if($c){
		$logged = 1;
		$f = mysql_fetch_row($r);
		$admin_name = $f[0];
	}else{
		$logged = 0;
	}
	
	

?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-language" content="en" />
        <meta name="robots" content="index,follow" />
        <meta name="googlebot" content="index,follow,snippet,archive" />
        <meta name="author" content="ELIVE CZ s.r.o. (c) 2010" />
		<?
			if(!$logged) {
				die('<meta http-equiv="refresh" content="0;url=index.php" />');
				
			}
		?>


        <link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/smoothness/jquery-ui-1.7.2.custom.css" />

        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
        <script type="text/javascript" src="js/jquery.ui.datepicker-cs.js"></script>
        <script type="text/javascript" src="js/jquery.simpleTooltip.js"></script>
        <script type="text/javascript" src="js/jquery.flot.pack.js"></script>
        <!--[if IE]><script language="javascript" type="text/javascript" src="js/excanvas.pack.js"></script><![endif]-->
        <script src="js/jquery.wysiwyg.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/jquery.wysiwyg.controls.js" type="text/javascript" charset="utf-8"></script>
        <script type="text/javascript" src="js/jquery.highlight-1.1.source.js"></script>
        

<script type="text/javascript" src="js/submenu.js"></script>
        <script type="text/javascript" src="js/xadmin.js"></script>
        <script type="text/javascript" src="js/jquery.simplemodal.js"></script>
    <script type="text/javascript" src="js/admin.js"></script>
        

    <title>ShanOre.com ADMIN</title>
    <link href="css/jquery.wysiwyg.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
		body,td,th {
			font-family: Arial, sans-serif;
		}
    </style>
    </head>
<body>
        <div id="header">
            <h1>ShanOre.com</h1>
            <div id="user"><a href="?action=logout">Logout</a>

                <br />
            </div>
            
            <div id="mainMenuWrapper">
<ul id="mainMenu">
                    <li><a href="admin.php">Catalog</a></li>
                    <li><a href="orders.php">ORDERS</a></li>
                    <li class="active"><a href="admin-options.php">options</a><a href="editor.html"></a></li>
                    <li><a href="typography.html">SOON TO COME</a><a href="photo.html"></a></li>
                </ul>
            </div>
        </div>
        <div id="contentWrapper">
            <div id="leftMenu">
                <strong>Menu</strong>
              <div id="menu">
                    <ul>
                        <li class="active"><a href="admin.php" class="item">Front Page Slider IMGs</a></li>
                    </ul>
            </div>
                
            <div class="filter">
              <h3>DISPLAY FILTERING</h3>
              <form action="" method="get" enctype="multipart/form-data">
                    Word / Phrase
                    <input type="text" name="search_txt" style="width:165px;" value="<? echo($_GET['search_txt']); ?>" />
                    
                    Brand<br />
					<select name="brand_filter">
                    	<option value="">All</option>
                        <?
							$q = "SELECT id, name FROM brands";
							$r = mysql_query($q);
							$c = mysql_num_rows($r);
							for($i=0; $i<$c; $i++){
								$f = mysql_fetch_row($r);
								if($_GET['brand_filter']==$f[0]){
									$selected = ' SELECTED';
								}else{
									$selected = '';
								}
								echo('<option value="'.$f[0].'"'.$selected.'>'.$f[1].'</option>');
							}
						?>
                    </select>
                    
                     Category<br />
					<select name="category_filter">
                    	<option value="">All</option>
                        <?
							$q = "SELECT id, name FROM categories";
							$r = mysql_query($q);
							$c = mysql_num_rows($r);
							for($i=0; $i<$c; $i++){
								$f = mysql_fetch_row($r);
								if($_GET['category_filter']==$f[0]){
									$selected = ' SELECTED';
								}else{
									$selected = '';
								}
								echo('<option value="'.$f[0].'"'.$selected.'>'.$f[1].'</option>');
							}
						?>
                    </select>
                                        
                    <input type="submit" value="Submit" />
                    
              </form>
            </div>
            
            <div class="filter">
            	<h3>BULK EDIT BASKET</h3>
                <div id="edit_basket">
            	<?
					$q = "SELECT id, product_id, product_name FROM bulk_edit_basket WHERE admin_name='".$admin_name."'";
					$r = mysql_query($q);
					$c = mysql_num_rows($r);
					for($i=0; $i<$c; $i++){
						$f = mysql_fetch_row($r);
						echo("
							<script>
								$(document).ready(function() {
									$('#tobulk_".$f[1]."').css('display','none');
								});
							</script>
						");
						echo('<span><img src="img/datagridDel.png" style="cursor:pointer;" onclick="remove_from_bulk(\''.$f[0].'\',\''.$admin_name.'\','.$f[1].');">&nbsp;'.$f[2].'</span><br/>');
					}
					if($c){
						echo('<br/><button onclick="edit_bulk();">Edit All</button>');
					}else{
						echo('empty');
					}
				?>
                </div>
            </div>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            <strong> </strong></div>
            <div id="content">
<?

	if($_GET['action'] == 'editproduct'){
		
		$q = "UPDATE products SET name='".$_POST['edit_name']."', price='".$_POST['edit_price']."', description='".$_POST['desc']."', category='".$_POST['edit_category']."', brand='".$_POST['edit_brand']."', sku='".$_POST['edit_sku']."', sku2='".$_POST['edit_sku2']."', edit_datetime=now() WHERE id=".$_POST['id'];
		$r = mysql_query($q) or $message = '<div class="flash error">
											  <div class="top"></div>
											  <div class="text">Error talking to database: '.mysql_error().'</div>
											  <div class="bottom"></div>
											</div>';
		
		$product_id = $_POST['id'];
		
		if($_FILES['file']['tmp_name']){
				if(file_exists("../products_images/".$product_id.".jpg")) unlink("../products_images/".$product_id.".jpg");
				$filename = $product_id.".jpg";
				copy ($_FILES['file']['tmp_name'], "../products_images/".$filename);
			}
			
			if($_FILES['file2']['tmp_name']){
				if(file_exists("../products_images/".$product_id."._2.jpg")) unlink("../products_images/".$product_id."._2jpg");
				$filename = $product_id."_2.jpg";
				copy ($_FILES['file2']['tmp_name'], "../products_images/".$filename);
			}
			
			if($_FILES['file3']['tmp_name']){
				if(file_exists("../products_images/".$product_id."._3.jpg")) unlink("../products_images/".$product_id."._3jpg");
				$filename = $product_id."_3.jpg";
				copy ($_FILES['file3']['tmp_name'], "../products_images/".$filename);
			}
			
			if($_FILES['file4']['tmp_name']){
				if(file_exists("../products_images/".$product_id."._4.jpg")) unlink("../products_images/".$product_id."._4jpg");
				$filename = $product_id."_4.jpg";
				copy ($_FILES['file4']['tmp_name'], "../products_images/".$filename);
			}
			
			if($_FILES['file5']['tmp_name']){
				if(file_exists("../products_images/".$product_id."._5.jpg")) unlink("../products_images/".$product_id."._5jpg");
				$filename = $product_id."_5.jpg";
				copy ($_FILES['file5']['tmp_name'], "../products_images/".$filename);
			}
		
		
		$message = '<div class="flash ok">
							  <div class="top"></div>
							  <div class="text">Existing product has been successfuly saved</div>
							  <div class="bottom"></div>
							</div>';
	}
	
	
	
	
	if($_GET['action'] == 'bulkedit'){
		
		if(($_POST['edit_category']) OR ($_POST['edit_brand']) OR ($_POST['edit_price'])){
			
			$values = '';
			
			if($_POST['edit_category']){
				$values = "category=".$_POST['edit_category'];
			}
			
			if($_POST['edit_brand']){
				if($values) $values = ", ".$values;
				$values = "brand=".$_POST['edit_brand'];
			}
			
			if($_POST['edit_price']){
				if($values) $values = ", ".$values;
				$values = "price=".$_POST['edit_price'];
			}
			
			
			
			$q = "SELECT product_id FROM bulk_edit_basket WHERE admin_name='".$admin_name."'";
			$r = mysql_query($q);
			$c = mysql_num_rows($r);
			for($i=0; $i<$c; $i++){
				$f = mysql_fetch_row($r);
				
				$q_edit = "UPDATE products SET ".$values." WHERE id=".$f[0];
				$r_edit = mysql_query($q_edit) or $message = '<div class="flash error">
																  <div class="top"></div>
																  <div class="text">Error talking to database: '.mysql_error().'</div>
																  <div class="bottom"></div>
																</div>';
				
			}
		}
		
		if(!$message){
			$message = '<div class="flash ok">
						  <div class="top"></div>
						  <div class="text">Existing products have been successfuly saved</div>
						  <div class="bottom"></div>
						</div>';
		}
	}
	
	
	
	
	
	
	
	if($_GET['action']=='newproduct'){
		
		$q = "SELECT id FROM products WHERE sku='".$_POST['sku']."'";
		$r = mysql_query($q);
		$c = mysql_num_rows($r);
		if($c){
			// sku exists already... don't do anything as it was probably just a page refresh after adding a product
			
		}else{
			//inserting a product
			$q = "INSERT INTO products (sku, sku2, name, price, description, category, brand, insert_datetime) values('".$_POST['sku']."','".$_POST['sku2']."','".$_POST['name']."','".$_POST['price']."','".$_POST['desc']."',".$_POST['category'].",".$_POST['brand'].",now())";
			$r = mysql_query($q) or die(mysql_error());
			
			//getting it's ID
			$q = "select id from products where sku='".$_POST['sku']."'";
			$r = mysql_query($q);
			$f = mysql_fetch_row($r);
			$product_id = $f[0];
			

			
			//inserting 4 basic options
			
			// STYLE
			$q = "SELECT id FROM product_options_types WHERE name='Style'";
			$r = mysql_query($q);
			if($f = mysql_fetch_row($r)){
				$style_id = $f[0];
				
				// GET value ID
				$q = "SELECT id FROM product_options_values WHERE name='".$_POST['style']."'";
				$r = mysql_query($q);
				if($f = mysql_fetch_row($r)){
					
					$q = "INSERT INTO product_options (product_id, option_value_id, option_type_id) values(".$product_id.",".$f[0].",".$style_id.") ";
					$r = mysql_query($q);
				}
				
			}


			// OCCASION
			$q = "SELECT id FROM product_options_types WHERE name='Occasion'";
			$r = mysql_query($q);
			if($f = mysql_fetch_row($r)){
				$occasion_id = $f[0];
				
				// GET value ID
				$q = "SELECT id FROM product_options_values WHERE name='".$_POST['occasion']."'";
				$r = mysql_query($q);
				if($f = mysql_fetch_row($r)){
					
					$q = "INSERT INTO product_options (product_id, option_value_id, option_type_id) values(".$product_id.",".$f[0].",".$occasion_id.") ";
					$r = mysql_query($q);
				}
			}



			// MATERIAL
			$q = "SELECT id FROM product_options_types WHERE name='Material'";
			$r = mysql_query($q);
			if($f = mysql_fetch_row($r)){
				$material_id = $f[0];

				$q = "INSERT INTO product_options (product_id, option_value_id, option_type_id) values(".$product_id.",".$_POST['material'].",".$material_id.") ";
				$r = mysql_query($q) or die (mysql_error());

			}



			// SETTING
			$q = "SELECT id FROM product_options_types WHERE name='Setting'";
			$r = mysql_query($q);
			if($f = mysql_fetch_row($r)){
				$setting_id = $f[0];
				
				// GET value ID
				$q = "SELECT id FROM product_options_values WHERE name='".$_POST['setting']."'";
				$r = mysql_query($q);
				if($f = mysql_fetch_row($r)){
					
					$q = "INSERT INTO product_options (product_id, option_value_id, option_type_id) values(".$product_id.",".$f[0].",".$setting_id.") ";
					$r = mysql_query($q);
				}
				
			}
			


			// COLLECTION
			
			$q = "INSERT INTO product_collections (product_id, collection_id) values(".$product_id.",".$_POST['collection_select'].")";
			$r = mysql_query($q);
			
			
			// IMAGES

			if(file_exists("../products_images/".$product_id.".jpg")) unlink("../products_images/".$product_id.".jpg");
			if(file_exists("../products_images/".$product_id."._2jpg")) unlink("../products_images/".$product_id."._2jpg");
			if(file_exists("../products_images/".$product_id."._3jpg")) unlink("../products_images/".$product_id."._3jpg");
			if(file_exists("../products_images/".$product_id."._4jpg")) unlink("../products_images/".$product_id."._4jpg");
			if(file_exists("../products_images/".$product_id."._5jpg")) unlink("../products_images/".$product_id."._5jpg");


			
			if($_FILES['file']['tmp_name']){
				$filename = $product_id.".jpg";
				
				echo('copying '.$_FILES['file']['tmp_name'].'   --->>    '."../products_images/".$filename);
				
				copy ($_FILES['file']['tmp_name'], "../products_images/".$filename);
								
			}



			if($_FILES['file2']['tmp_name']){
				$filename = $product_id."_2.jpg";
				
				echo('copying '.$_FILES['file2']['tmp_name'].'   --->>    '."../products_images/".$filename);
				
				copy ($_FILES['file2']['tmp_name'], "../products_images/".$filename);
			}
			if($_FILES['file3']['tmp_name']){
				$filename = $product_id."_3.jpg";
				copy ($_FILES['file3']['tmp_name'], "../products_images/".$filename);
			}
			if($_FILES['file4']['tmp_name']){
				$filename = $product_id."_4.jpg";
				copy ($_FILES['file4']['tmp_name'], "../products_images/".$filename);
			}
			if($_FILES['file5']['tmp_name']){
				$filename = $product_id."_5.jpg";
				copy ($_FILES['file5']['tmp_name'], "../products_images/".$filename);
			}
			
			$message = '<div class="flash ok">
							  <div class="top"></div>
							  <div class="text">New product has been successfuly saved</div>
							  <div class="bottom"></div>
							</div>';
		}
	}
?>
              
              <div id="inner"><? echo($message); ?><a href="#" class="add" id="addnew_clicker">Add a new product</a>
           	    <div class="add">
<div class="textbox">
                    
    	            	<form action="?action=newproduct" enctype="multipart/form-data" method="post" id="addform">
                			<table width="100%" border="0" cellspacing="0" cellpadding="0">
                			  <tr>
                			    <td width="27%" valign="top">Product name <br />
                                  <input name="name" id="name" />
                                  <br />
Rocks Code<br />
<input name="sku" id="sku" />
<br />
Brand Code<br />
<input name="sku2" id="sku2" />
<br />
Price (&euro;)<br /><input name="price" id="price" />
<br />
Add to a collection<br />
<select name="collection_select" id="collection_select" >
	<option value="">None</option>
    <?
		$q = "SELECT id, name FROM collections";
		$r = mysql_query($q);
		$c = mysql_num_rows($r);
		for($i=0; $i<$c; $i++){
			$f = mysql_fetch_row($r);
			echo('<option value="'.$f[0].'">'.$f[1].'</option>');	
		}
	?>
</select>
<br />
<br />

<div class="red" id="mandatory">- <strong>product name</strong> is mandatory<br />
  - <strong>SKU</strong> is mandatory<br />
  - <strong>SKU</strong> must be unique <br />
- <strong>price</strong> is mandatory<br />
- <strong>category</strong> is mandatory </div>

</td>
<td width="31%" valign="top">Category<br />
  <select name="category" id="category">
     <option value="0">Choose</option>
  <?
  	$q = "SELECT id, name FROM categories";
	$r = mysql_query($q);
	$c = mysql_num_rows($r);
	for($i=0; $i<$c; $i++){
		$f = mysql_fetch_row($r);
		echo('<option value="'.$f[0].'">'.$f[1].'</option>');
	}
  ?>
</select>
<br />
Brand<br />
<select name="brand" id="brand">
  <option value="0">Choose</option>
  <?
  	$q = "SELECT id, name FROM brands";
	$r = mysql_query($q);
	$c = mysql_num_rows($r);
	for($i=0; $i<$c; $i++){
		$f = mysql_fetch_row($r);
		echo('<option value="'.$f[0].'">'.$f[1].'</option>');
	}
  ?>
</select>
<br />
Style<br />
<select name="style" id="style">
  <option value="0">Choose</option>
  <?
  	$q = "SELECT id, name FROM product_options_values WHERE type_id=2";
	$r = mysql_query($q);
	$c = mysql_num_rows($r);
	for($i=0; $i<$c; $i++){
		$f = mysql_fetch_row($r);
		echo('<option value="'.$f[0].'">'.$f[1].'</option>');
	}
  ?>
</select>
<br />
Material<br />
<select name="material" id="material">
  <option value="0">Choose</option>
  <?
  	$q = $q = "SELECT id, name FROM product_options_values WHERE type_id=1";
	$r = mysql_query($q);
	$c = mysql_num_rows($r);
	for($i=0; $i<$c; $i++){
		$f = mysql_fetch_row($r);
		echo('<option value="'.$f[0].'">'.$f[1].'</option>');
	}
  ?>
</select>
<br />
Occasion<br />
<select name="occasion" id="occasion">
  <option value="0">Choose</option>
  <?
  	$q = $q = "SELECT id, name FROM product_options_values WHERE type_id=3";
	$r = mysql_query($q);
	$c = mysql_num_rows($r);
	for($i=0; $i<$c; $i++){
		$f = mysql_fetch_row($r);
		echo('<option value="'.$f[0].'">'.$f[1].'</option>');
	}
  ?>
</select>
<br />
Setting<br />
<select name="setting" id="setting">
  <option value="0">Choose</option>
  <?
  	$q = $q = "SELECT id, name FROM product_options_values WHERE type_id=4";
	$r = mysql_query($q);
	$c = mysql_num_rows($r);
	for($i=0; $i<$c; $i++){
		$f = mysql_fetch_row($r);
		echo('<option value="'.$f[0].'">'.$f[1].'</option>');
	}
  ?>
</select></td>
                			    <td width="42%" valign="top">Main image<br />
               			        <input type="file" name="file" id="file" />
               			        <br />
               			        Second image<br />
               			        <input type="file" name="file2" id="file2" />
               			        <br />
               			        Third image<br />
               			        <input type="file" name="file3" id="file3" />
                                <br />
               			        Third image<br />
               			        <input type="file" name="file4" id="file4" />
                                <br />
               			        Third image<br />
               			        <input type="file" name="file5" id="file5" />
               			        <br />
               			        <br />
               			        <br />
               			        Product description<br />               			        <textarea name="desc" cols="30" rows="6" id="newdesc">&nbsp;</textarea></td>
              			      </tr>
                			  <tr>
                			    <td><input type="submit" id="submitbtn" value="Save this product" style="width:120px; position:absolute; right:30px; top:60px; background-color:#F90;" disabled="disabled">
               			        <img src="img/loading.gif" alt="loading" width="16" height="16" class="loading" style="margin-top:3px; display:none;" id="loading" /></td>
                			    <td>&nbsp;</td>
                			    <td>&nbsp;</td>
              			      </tr>
              			  </table><br />
   	            	  </form>
                    <div class="textboxFooter"></div></div></div>
                
               	<div class="datagrid">
                	  <h2>Products table</h2>
                	  <form method="post" action="">
                	    <table>
                	      <tr>
                	        <th>Product</th>
                	        <th>Rocks Code<br />
                	          (brand code)</th>
                	        <th>Category</th>
                	        <th>Price</th>
              	        </tr>
                        <?
						
						if($_GET['search_txt']){
							$search_str = " (name LIKE '%".$_GET['search_txt']."%' OR sku LIKE '%".$_GET['search_txt']."%' OR sku2 LIKE '%".$_GET['search_txt']."%') AND";
						}
						
						if($_GET['brand_filter']){
							$search_str .= " brand = '".$_GET['brand_filter']."' AND";
						}
						
						if($_GET['category_filter']){
							$search_str .= " category = '".$_GET['category_filter']."' AND";
						}
							
							$q = "SELECT id FROM products WHERE".$search_str." deleted=0";
							$r = mysql_query($q);
							$all_count = mysql_num_rows($r);
							$all_pages = ceil($all_count / 20);
							
							$page = 1;
							if($_GET['page']){
								$offset = " OFFSET ".strval((intval($_GET['page'])-1) * 20);
								$page = $_GET['page'];
							}
							
							$q = "SELECT id, name, sku, category, price, edit_datetime, sku2, purchasable, brand  FROM products WHERE".$search_str." deleted=0 ORDER BY id DESC LIMIT 20".$offset;
							$r = mysql_query($q);
							$c = mysql_num_rows($r);
							
							$sudalicha = 'odd';
							
							
							
							for($i=0; $i<$c; $i++){
								$f = mysql_fetch_row($r);
								if($sudalicha == 'odd'){
									$sudalicha = 'even';
								}else{
									$sudalicha = 'odd';
								}
								
								
								$q_brand = "SELECT name FROM brands WHERE id=".$f[8];
								$r_brand = mysql_query($q_brand);
								if($f_brand = mysql_fetch_row($r_brand)){
									$brand = $f_brand[0];
								}else{
									$brand = '';
								}
								
								
								$q_cat = "SELECT name FROM categories WHERE id=".$f[3];
								$r_cat = mysql_query($q_cat);
								$f_cat = mysql_fetch_row($r_cat);
								
								if(file_exists('../products_images/'.$f[0].'.jpg')){
									$image_str = '<br><img src="../products_images/'.$f[0].'.jpg?x='.strval(rand(1000000,9999999)).'" width="100" style="margin-bottom:20px;" />';
								}else{
									$image_str = '';
								}
								
								
								$edited_time = intval(strtotime($f[5]));
								$now_time = intval(time());
								if(($now_time - $edited_time) < 86400){
									$edited_today = '<span class="edited_today"> (EDITED TODAY)</span>';
								}else{
									$edited_today = '';
								}
								
								if(strlen($f[2])>10){
									$sku = substr($f[2],0,7).'...';
								}else{
									$sku = $f[2];
								}
								
								if(strlen($f[1])>40){
									$name = substr($f[1],0,37).'...';
								}else{
									$name = $f[1];
								}
								
								$options_str = '';
								
								$q_opt = "SELECT id, option_value_id, option_type_id, sizeadd FROM product_options WHERE product_id=".$f[0];
								$r_opt = mysql_query($q_opt);
								$c_opt = mysql_num_rows($r_opt);
								for($i_opt=0; $i_opt<$c_opt; $i_opt++){
									$f_opt = mysql_fetch_row($r_opt);
									
									$r_name = mysql_query("SELECT name FROM product_options_types WHERE id=".$f_opt[2]);
									$f_name = mysql_fetch_row($r_name);
									$opt_name = $f_name[0];
									
									$r_value = mysql_query("SELECT name FROM product_options_values WHERE id=".$f_opt[1]);
									$f_value = mysql_fetch_row($r_value);
									$value = $f_value[0];
									
									if($f_opt[3]){
										$sizeadd = ' (+'.$f_opt[3].'&euro;)';
									}else{
										$sizeadd = '';
									}
									
									$options_str .= '<span><strong>'.$opt_name.'</strong> - '.$value.$sizeadd.'</span><br/>';
								}
								
								$collection = '';
								
								$q_opt2 = "SELECT DISTINCT collection_id FROM product_collections WHERE product_id=".$f[0];
								$r_opt2 = mysql_query($q_opt2);
								$c_opt2 = mysql_num_rows($r_opt2);
								for($i_opt2 = 0; $i_opt2 < $c_opt2; $i_opt2++){
									$frnda = mysql_fetch_row($r_opt2);
									$collection_id = $frnda[0];
									
									$r_col = mysql_query("SELECT name FROM collections WHERE id=".$collection_id);
									$f_col = mysql_fetch_row($r_col);
									$collection = $f_col[0];
									
									$options_str .= 'collection: <strong>'.$collection.'</strong><br/>';
								}

								
								//wear it with
								$q_wear = "SELECT wear_with FROM wear_it_with WHERE product_id=".$f[0];
								$r_wear = mysql_query($q_wear);
								$c_wear = mysql_num_rows($r_wear);
								$options_str .= 'Has <strong>'.$c_wear.'</strong> Wear With links<br/>';
																
								
								if(!$options_str){
									$options_str = 'No Aditional Options Found';
								}
								
								// sale on/off image preparation
								if($f[7]){
									$onoff = "sale_on";
								}else{
									$onoff = "sale_off";
								}
								
								echo('<tr class="odd" id="tr_'.$f[0].'">
										<td style="cursor:default; text-align:left;" alt="'.$f[1].'" title="'.$f[1].'">
										<span class="name">&nbsp;&nbsp;'.$name.'</span> ('.$brand.') '.$edited_today.'
										<table>
											<tr>
												<td style="background-image:none;" valign="left">
													<div class="prop_shadow">
														'.$options_str.'
													</div>
													<img src="img/my_to_bulk.png" style="float:left; cursor:pointer;" id="tobulk_'.$f[0].'" onclick="to_bulk(\''.$admin_name.'\','.$f[0].',\''.$name.'\');" />
												</td>
												<td style="background-image:none;">
													'.$image_str.'
												</td>
											</tr>
										</table>
										<td>'.$sku.'<br/>('.$f[6].')</td>
										<td>'.$f_cat[0].'</a></td>
										<td>&euro; '.$f[4].'<div class="my_actions">
											<img src="img/my_delete.png" width="59" height="58" alt="Delete" class="delete_btn" style="cursor:pointer; margin-top:15px; float:right;" onclick="delete_product('.$f[0].');" /><br/>
											
											<img src="img/my_edit.png" width="59" height="58" alt="Edit" class="edit_btn" style="cursor:pointer; float:right; clear:both;" onclick="edit_product('.$f[0].','.$page.');" /><br><img src="img/loading_bar.gif" id="edit_loading_'.$f[0].'" style="float:right; margin-right:5px; display:none;" width="50px">
											
											<img src="img/'.$onoff.'.png" width="59" height="58" alt="Edit" class="onoff_btn" style="cursor:pointer; float:right; clear:both;" onclick="sale_product('.$f[0].');" id="sale_'.$f[0].'" />
											
										</div></td>
									</tr>');
							}
							
						?>
                	    </table>
                	    

                	    <div class="datagridActions">
                	      <div class="paginator"> 
                          
   <?						  	
							if($page == 1){
                                echo('<a href="#" class="active">1</a> <a href="?page=2&search_txt='.$_GET['search_txt'].'&brand_filter='.$_GET['brand_filter'].'&category_filter='.$_GET['category_filter'].'">2</a> <a href="?page=3&search_txt='.$_GET['search_txt'].'&brand_filter='.$_GET['brand_filter'].'&category_filter='.$_GET['category_filter'].'">3</a> ... <a href="?page='.$all_pages.'&search_txt='.$_GET['search_txt'].'&brand_filter='.$_GET['brand_filter'].'&category_filter='.$_GET['category_filter'].'">'.$all_pages.'</a><a href="?page='.strval(($page)+1).'&search_txt='.$_GET['search_txt'].'&brand_filter='.$_GET['brand_filter'].'&category_filter='.$_GET['category_filter'].'">Next &raquo;</a>');
							}
							
							if(($page > 1) AND($page < $all_pages)){
                                echo('<a href="?page='.strval(intval($page)-1).'&search_txt='.$_GET['search_txt'].'&brand_filter='.$_GET['brand_filter'].'&category_filter='.$_GET['category_filter'].'">&laquo; Previous</a><a href="?page=1&search_txt='.$_GET['search_txt'].'&brand_filter='.$_GET['brand_filter'].'&category_filter='.$_GET['category_filter'].'">1</a> ... <a href="#" class="active">'.$page.'</a> ... <a href="?page='.$all_pages.'&search_txt='.$_GET['search_txt'].'&brand_filter='.$_GET['brand_filter'].'&category_filter='.$_GET['category_filter'].'">'.$all_pages.'</a><a href="?page='.strval(($page)+1).'&search_txt='.$_GET['search_txt'].'&brand_filter='.$_GET['brand_filter'].'&category_filter='.$_GET['category_filter'].'">Next &raquo;</a>');
							}
						  	
							if($page == $all_pages){
								echo('<a href="?page='.strval(intval($page)-1).'&search_txt='.$_GET['search_txt'].'&brand_filter='.$_GET['brand_filter'].'&category_filter='.$_GET['category_filter'].'">&laquo; Previous</a><a href="?page=1&search_txt='.$_GET['search_txt'].'&brand_filter='.$_GET['brand_filter'].'&category_filter='.$_GET['category_filter'].'">1</a> ... <a href="#" class="active">'.$all_pages.'</a>');
							}
							
						  ?>
                          
                          
                          
                          
                          	
                          
                          
                          
                          </div>
                          
              	      </div>
              	    </form>
          </div>
              </div>
            </div>
            <hr class="cleaner" />
            
      <div id="basic-modal-content"> 
      		<!-- edit form here -->
            
	  </div>
      <div id="basic-modal-content-2"> 
      		<!-- edit form here -->
            
	  </div>
            
            
</div>

        <div id="footer">

        </div> 
    </body>

<?
	if($_GET['search_txt']){
		echo("<script>
					$('td').highlight('".$_GET['search_txt']."','highlight');
			  </script>");
	}
?>    

</html>
