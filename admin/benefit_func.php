<?
	function get_benefit($master_order_id, $display = null){
		
		$q = "SELECT id FROM affiliates_benefit WHERE orders_master_id=".$master_order_id;
		$r = mysqli_query($q);
		$c = mysqli_num_rows($r);
		if($c){
			
			if($display) echo('<table>
								<tr>
									<th>product</th>
									<th>price</th>
									<th>bank fees 3%</th>
									<th>shipping</th>
									<th>benefit %</th>
									<th>benefit</th>
								</tr>
								');
			
			// OK ORDER HAS A BENEFITIARY 
			
			$q = "SELECT product_id, qty, price FROM orders WHERE order_master_id=".$master_order_id;
			$r = mysqli_query($q) or die(mysqli_error());
			$c = mysqli_num_rows($r);
			
			$total = 0.0;
			
			for($i=0; $i<$c; $i++){
				$f = mysqli_fetch_row($r);
				
				// getting product details
				//$q_prod = "SELECT price_dollar, configurable, gold_item, name, sku FROM products WHERE id=".$f[0];
				$q_prod = "SELECT  orders.price, products.configurable, products.gold_item, products.name, products.sku, products.price_dollar FROM products INNER JOIN orders ON orders.product_id = products.id WHERE products.id=".$f[0]." AND orders.order_master_id=".$master_order_id;
				$r_prod = mysqli_query($q_prod);
				$f_prod = mysqli_fetch_row($r_prod);
				
				// getting real price from orders table
				$price = floatval($f_prod[0]);
				
				$orig_price = floatval($f_prod[5]);
				
				$full_price = floatval($f[2]);
				
				
				if(!$f_prod[1]){
					$orig_price = $full_price;
				}
				
				//$orig_price = floatval($f[2]);
				
				//echo('orig price = '.$orig_price);
				//echo('<br>full price = '.$full_price);
				
				for($j=0; $j<$f[1]; $j++){
					
					if(!$orig_price) continue;
					
					$price = $orig_price * 0.97;
					
					if($f_prod[2]){
						// gold
						$price = $price - 30;
						$shipping = 30;
					}else{
						// not gold
						$price = $price - 7;
						$shipping = 7;
					}
					
					$price = $price * 0.4;
					$benefitproc = '40%';
					
					if($f_prod[1]){
						// CONFIGURABLE - + 25 for stone
						
						$stone_price = $full_price - $orig_price;
						
						
						
						$stoneline = '<tr>
											<td>Diamond for the ring above</td>
											<td>$'.$stone_price.'</td>
											<td>-$'.strval($stone_price*0.03).'</td>
											<td></td>
											<td>25%</td>
											<td>$'.(($stone_price-($stone_price*0.03))*0.25).'</td>
									  </tr>';
						
							$total = $total + (($stone_price-($stone_price*0.03))*0.25);
						
					}else{
						// standard jewelery
						$stoneline = '';
						//$price = floatval($f_prod[0]); // not configurable, using price from orders table
						$orig_price = floatval($f_prod[0]);
					}
					
					
					
					$total = $total + $price;
					
					if($display) echo('<tr>
							<td>'.substr($f_prod[3],0,20).'... ('.$f_prod[4].')</td>
							<td>$'.$orig_price.'</td>
							<td>-$'.number_format(0.03*$orig_price,2).'</td>
							<td>-$'.$shipping.'</td>
							<td>'.$benefitproc.'</td>
							<td>$'.number_format($price,2,'.',' ').'</td>
						</tr>'.$stoneline);
						
				}
			}
			
			if($display) echo('</table>');
			
			return $total;
			
		}else{
			return '-';
		}
		
	}
?>