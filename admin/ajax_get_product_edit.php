<?
	require('db.php');
	
	$q = "SELECT sku, name, price, description, category, brand, sku2, price_dollar, price_sterling, price_wholesale, price_wholesale_dollar, price_wholesale_sterling, sort, price_old, price_dollar_old, public_sale, public_sale_dollar FROM products WHERE id=".$_POST['id'];
	$r = mysqli_query($db,$q);
	$c = mysqli_num_rows($r);
	if($c){
		$f = mysqli_fetch_row($r);
		$desc = str_replace("®","&reg;",$f[3]);
		$desc = str_replace("é","&eacute;",$desc);
		$desc = str_replace("í","&iacute;",$desc);
		$desc = str_replace("’","&#39;",$desc);
		$desc = str_replace("'","&#39;",$desc);
		
	}
	
	$q = "SELECT styles_id, occasions_id, materials_id, settings_id FROM product_options WHERE product_id=".$_POST['id'];
	$r = mysqli_query($db,$q);
	$c = mysqli_num_rows($r);
	if($c){
		$f_opt = mysqli_fetch_row($r);
	}

?>
                <form action="?action=editproduct&page=<? echo($_POST['page']); ?>&search_txt=<? echo($_POST['search_txt']); ?>&brand_filter=<? echo($_POST['brand_filter']); ?>&category_filter=<? echo($_POST['category_filter']); ?>" method="post" enctype="multipart/form-data" style="margin:0; padding:0;">
                <input type="submit" value="Save the Product" style="background-color:#F90; font-weight:bold; width:125px; height:25px; position:absolute; right:15px; top:33px;" />
                  <table width="680" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td colspan="2" align="left" valign="top">Name<br />
                        <input name="edit_name" value="<? echo($f[1]); ?>" style="width:400px;" />
                        <input type="hidden" name="id" value="<? echo($_POST['id']); ?>" />
                        <br />
                        Your Code<br />
                        <input name="edit_sku" value="<? echo($f[0]); ?>" style="width:400px;" />
                        <br />
                        Brand Code<br />
                        <input name="edit_sku2" value="<? echo($f[6]); ?>" style="width:400px;" />
                        <br /></td>
                    </tr>
                    <tr>
                      <td width="19%" valign="top">
                        
                        <table width="100%" border="0" cellpadding="5">
                          <tr>
                            <td> <p>Retail Price<br />
                        &euro;<input name="edit_price" value="<? echo($f[2]); ?>" style="width:30px" /> $<input name="edit_price_dollar" value="<? echo($f[7]); ?>" style="width:30px" /> <!--&pound;<input name="edit_price_sterling" value="<? //echo($f[8]); ?>" style="width:30px" />--></td>
                            <td><p>Wholesale Price<br />
                        &euro;<input name="edit_price_wholesale" value="<? echo($f[9]); ?>" style="width:30px" /> $<input name="edit_price_wholesale_dollar" value="<? echo($f[10]); ?>" style="width:30px" /> <!--&pound;<input name="edit_price_wholesale_sterling" value="<? //echo($f[11]); ?>" style="width:30px" />--></td>
                          </tr>
                        </table>

                        
                        
                        <table width="100%" border="0" cellpadding="5">
                          <tr>
                            <td><p>Sale Price (hidden)<br />
                        &euro;<input name="edit_price_old" value="<? echo($f[13]); ?>" style="width:30px" /> $<input name="edit_price_dollar_old" value="<? echo($f[14]); ?>" style="width:30px" /></td>
                            <td><p>Sale Price (public)<br />
                        &euro;<input name="public_sale" value="<? echo($f[15]); ?>" style="width:30px" /> $<input name="public_sale_dollar" value="<? echo($f[16]); ?>" style="width:30px" /></td>
                          </tr>
                        </table>

                        
						
                        <br />
                        
                        
						
                        
                        
                        <p>Category<br />
                        <select name="edit_category">
                          <option value="">NONE</option>
                          <?
                        $q = "SELECT id, name FROM categories";
                        $r = mysqli_query($db,$q);
                        $c = mysqli_num_rows($r);
                        for($i=0; $i<$c; $i++){
                            $f2 = mysqli_fetch_row($r);
							if($f2[0] == $f[4]){
								$selected = ' SELECTED';
							}else{
								$selected = '';
							}
                            echo('<option value="'.$f2[0].'" '.$selected.'>'.$f2[1].'</option>');
                        }
                      ?>
                        </select>
                        <br />
Brand<br />
<select name="edit_brand">
  <option value="0">NONE</option>
  <?
                        $q = "SELECT id, name FROM brands";
                        $r = mysqli_query($db,$q);
                        $c = mysqli_num_rows($r);
                        for($i=0; $i<$c; $i++){
                            $f2 = mysqli_fetch_row($r);
							if($f2[0] == $f[5]){
								$selected = ' SELECTED';
							}else{
								$selected = '';
							}
                            echo('<option value="'.$f2[0].'" '.$selected.'>'.$f2[1].'</option>');
                        }
                      ?>
</select>
<br />
                          <br />
                          Sort Priority <br />
                          <input name="sort_priority" maxlength="12" value="<? echo($f[12]); ?>" />
                        <div class="options_shadow">
Other options:
<div id="other_options">
<?
	$q = "SELECT id, option_value_id, option_type_id FROM product_options WHERE product_id=".$_POST['id'];
	$r = mysqli_query($db,$q);
	$c = mysqli_num_rows($r);
	for($i=0; $i<$c; $i++){
		$f = mysqli_fetch_row($r);
		
		$r_name = mysqli_query($db,"SELECT name FROM product_options_types WHERE id=".$f[2]);
		$f_name = mysqli_fetch_row($r_name);
		$name = $f_name[0];
		
		$r_value = mysqli_query($db,"SELECT name FROM product_options_values WHERE id=".$f[1]);
		$f_value = mysqli_fetch_row($r_value);
		$value = $f_value[0];
		
		echo('<span id="option_'.$f[0].'"><img src="img/datagridDel.png" style="cursor:pointer;" onclick="delete_option('.$f[0].');">&nbsp;<strong>'.$name.'</strong> - '.$value.'<br/></span>');
	}
	if(!$c){
		echo('<br/><strong>NO OPTIONS FOUND</strong><br/><br/>');
	}
?>
</div>


<br />Add an option:<br />
<select id="option_name" onchange="get_option_values($(this).val(),<? echo($_POST['id']); ?>);">
	<option value="">Option</option>
    <?
		$q = "SELECT id, name FROM product_options_types";
		$r = mysqli_query($db,$q);
		$c = mysqli_num_rows($r);
		for($i=0; $i<$c; $i++){
			$f2 = mysqli_fetch_row($r);
			echo('<option value="'.$f2[0].'">'.$f2[1].'</option>');
		}
	?>
</select>
<div id="option_values_select">
</div>
</div>

                      </p></td>
                      <td width="81%" align="center" valign="top">
                      <!--<span style="text-decoration:underline; cursor:pointer;" onclick="toggle_images();" >IMAGES - show / hide</span>-->
                      <div id="edit_image_place" style="display:block !important;">
                        <?
								if(file_exists("../img/products/product/".$_POST['id'].".jpg")){
									echo('<div class="edit_shadow"><h4>Main Image</h4><img id="img1" src="../img/products/product/'.$_POST['id'].'.jpg?x='.strval(rand(1000000,9999999)).'" style="margin-bottom:40px;max-width:100px;max-height:140px" />
									<div style="position:relative; bottom: 32px; left:3px;">
										<input type="file" name="file" style="width:80px; position:absolute; left:4px; z-index:2; opacity:0;" />
										<img src="img/my_choose_file.png" width="95" height="27" alt="Browse" style="position:absolute;left:0; cursor:pointer;" />
									</div>
								</div>');
								}else{
									echo('<div class="edit_shadow"><h4>Main Image</h4><img id="img1" src="img/noimage.jpg" height="90" style="margin-bottom:40px;max-width:100px;max-height:140px" /><div style="position:relative; bottom: 32px; left:3px">
										<input type="file" name="file" style="width:80px; position:absolute; left:4px; z-index:2; opacity:0;" />
										<img src="img/my_choose_file.png" width="95" height="27" alt="Browse" style="position:absolute; left:0; cursor:pointer;" />
									</div></div>');
								}
								
								
								
								if(file_exists("../img/products/product/".$_POST['id']."_2.jpg")){
									echo('<div class="edit_shadow"><h4>Second Image</h4><img id="img2" src="../img/products/product/'.$_POST['id'].'_2.jpg?x='.strval(rand(1000000,9999999)).'" style="max-width:100px;max-height:140px" /><div style="position:relative; bottom: 32px; left:3px;">
										<input type="file" name="file2" style="width:80px; position:absolute; left:4px; z-index:2; opacity:0;" />
										<img src="img/my_choose_file.png" width="95" height="27" alt="Browse" style="position:absolute;left:0; cursor:pointer;" />
									</div></div>');
								}else{
									echo('<div class="edit_shadow"><h4>Second Image</h4><img id="img2" src="img/noimage.jpg" height="90" style="margin-bottom:40px;max-width:100px;max-height:140px" /><div style="position:relative; bottom: 32px; left:3px;">
										<input type="file" name="file2" style="width:80px; position:absolute; left:4px; z-index:2; opacity:0;" />
										<img src="img/my_choose_file.png" width="95" height="27" alt="Browse" style="position:absolute;left:0; cursor:pointer;" />
									</div></div>');
								}
								
								if(file_exists("../img/products/product/".$_POST['id']."_3.jpg")){
									echo('<div class="edit_shadow"><h4>Third Image</h4><img id="img3" src="../img/products/product/'.$_POST['id'].'_3.jpg?x='.strval(rand(1000000,9999999)).'" style="max-width:100px;max-height:140px" /><div style="position:relative; bottom: 32px; left:3px">
										<input type="file" name="file3" style="width:80px; position:absolute; left:4px; z-index:2; opacity:0;" />
										<img src="img/my_choose_file.png" width="95" height="27" alt="Browse" style="position:absolute;left:0; cursor:pointer;" />
									</div></div>');
								}else{
									echo('<div class="edit_shadow"><h4>Third Image</h4><img id="img3" src="img/noimage.jpg" style="margin-bottom:40px;max-width:100px;max-height:140px" /><div style="position:relative; bottom: 32px; left:3px;">
										<input type="file" name="file3" style="width:80px; position:absolute; left:4px; z-index:2; opacity:0;" />
										<img src="img/my_choose_file.png" width="95" height="27" alt="Browse" style="position:absolute;left:0; cursor:pointer;" />
									</div></div>');
								}
								
								if(file_exists("../img/products/product/".$_POST['id']."_4.jpg")){
									echo('<div class="edit_shadow"><h4>Fourth Image</h4><img id="img4" src="../img/products/product/'.$_POST['id'].'_4.jpg?x='.strval(rand(1000000,9999999)).'" style="max-width:100px;max-height:140px" /><div style="position:relative; bottom: 32px; left:3px;">
										<input type="file" name="file4" style="width:80px; position:absolute; left:4px; z-index:2; opacity:0;" />
										<img src="img/my_choose_file.png" width="95" height="27" alt="Browse" style="position:absolute;left:0; cursor:pointer;" />
									</div></div>');
								}else{
									echo('<div class="edit_shadow"><h4>Fourth Image</h4><img id="img4" src="img/noimage.jpg" height="90" style="margin-bottom:40px;max-width:100px;max-height:140px" /><div style="position:relative; bottom: 32px; left:3px;">
										<input type="file" name="file4" style="width:80px; position:absolute; left:4px; z-index:2; opacity:0;" />
										<img src="img/my_choose_file.png" width="95" height="27" alt="Browse" style="position:absolute;left:0; cursor:pointer;" />
									</div></div>');
								}
									
								if(file_exists("../img/products/product/".$_POST['id']."_5.jpg")){
									echo('<div class="edit_shadow"><h4>Fifth Image</h4><img id="img5" src="../img/products/product/'.$_POST['id'].'_5.jpg?x='.strval(rand(1000000,9999999)).'" style="max-width:100px;max-height:140px" /><div style="position:relative; bottom: 32px; left:3px;">
										<input type="file" name="file5" style="width:80px; position:absolute; left:4px; z-index:2; opacity:0;" onclick="$(\'img5\').attr(\'src\',\'img/image_changed.jpg\');" />
										<img src="img/my_choose_file.png" width="95" height="27" alt="Browse" style="position:absolute;left:0; cursor:pointer;" />
									</div></div>');
								}else{
									echo('<div class="edit_shadow"><h4>Fifth Image</h4><img id="img5" src="img/noimage.jpg" height="90" style="margin-bottom:40px;" /><div style="position:relative; bottom: 32px; left:3px;">
										<input type="file" name="file5" style="width:80px; position:absolute; left:4px; z-index:2; opacity:0;"  onclick="$(\'img5\').attr(\'src\',\'img/image_changed.jpg\');"/>
										<img src="img/my_choose_file.png" width="95" height="27" alt="Browse" style="position:absolute;left:0; cursor:pointer;" />
									</div></div>');
								}
								
								if(file_exists("../img/products/product/tmb/tmb_".$_POST['id'].".jpg")){
									echo('<div class="edit_shadow"><h4>THUMBNAIL</h4><img id="thumb" src="../img/products/product/tmb/tmb_'.$_POST['id'].'.jpg?x='.strval(rand(1000000,9999999)).'" style="margin-bottom: 40px;max-width:100px;max-height:140px" style="height:90px;" /><div style="position:relative; bottom: 32px; left:3px;">
										<input type="file" name="thumb" style="width:80px; position:absolute; left:4px; z-index:2; opacity:0;" onclick="$(\'thumb\').attr(\'src\',\'img/image_changed.jpg\');" />
										<img src="img/my_choose_file.png" width="95" height="27" alt="Browse" style="position:absolute;left:0; cursor:pointer;" />
									</div></div>');
								}else{
									echo('<div class="edit_shadow"><h4>THUMBNAIL</h4><img id="thumb" src="img/noimage.jpg" style="margin-bottom:40px;max-width:100px;max-height:140px" /><div style="position:relative; bottom: 32px; left:3px;">
										<input type="file" name="thumb" style="width:80px; position:absolute; left:4px; z-index:2; opacity:0;"  onclick="$(\'thumb\').attr(\'src\',\'img/image_changed.jpg\');"/>
										<img src="img/my_choose_file.png" width="95" height="27" alt="Browse" style="position:absolute;left:0; cursor:pointer;" />
									</div></div>');
								}
								
							?>
                      </div>
                      
                      
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2"><br />
                      
                      
                      <div id="toolbar" style="display:none; background-image:url(http://www.shanore.com/wysiwyg/controls-bg.jpg); height:35px;">
                        <a data-wysihtml5-command-value="h2" href="javascript:;" data-wysihtml5-command="formatBlock">
                          <img src="http://www.shanore.com/wysiwyg/h2.jpg" alt="H2" width="75" height="35" />
                        </a>
                        <a data-wysihtml5-command-value="h3" href="javascript:;" data-wysihtml5-command="formatBlock">
                          <img src="http://www.shanore.com/wysiwyg/h3.jpg" alt="H3" width="75" height="35" />
                        </a>
                        <a data-wysihtml5-command="createLink" href="javascript:;">
                          <img src="http://www.shanore.com/wysiwyg/link.jpg" alt="H1" width="47" height="35" />
                        </a>
                        <a data-wysihtml5-command="insertUnorderedList">
                        	<img src="http://www.shanore.com/wysiwyg/ul.jpg" alt="H1" width="47" height="35" />
                        </a>
                        <a data-wysihtml5-command="insertOrderedList">
                        	<img src="http://www.shanore.com/wysiwyg/ol.jpg" alt="H1" width="47" height="35" />
                        </a>
                        <a data-wysihtml5-action="change_view">
                        	<img src="http://www.shanore.com/wysiwyg/html.jpg" alt="H1" width="47" height="35" />
                        </a>
                        
                        <div data-wysihtml5-dialog="createLink" style="display: none; background-color: #ffffff; position: relative; padding:20px; border:2px solid #666;">
                          <label>
                            Link:
                            <input data-wysihtml5-dialog-field="href" value="http://">
                          </label>
                          <a data-wysihtml5-dialog-action="save" style="color:#000; cursor:pointer;">OK</a>&nbsp;&nbsp;&nbsp;&nbsp;<a data-wysihtml5-dialog-action="cancel" style="color:#000; cursor:pointer;">Cancel</a>
                        </div>
                        
                        <div data-wysihtml5-dialog="insertImage" style="display: none;">
                          <label>
                            Image:
                            <input data-wysihtml5-dialog-field="src" value="http://">
                          </label>
                          <label>
                            Align:
                            <select data-wysihtml5-dialog-field="className">
                              <option value="">default</option>
                              <option value="wysiwyg-float-left">left</option>
                              <option value="wysiwyg-float-right">right</option>
                            </select>
                          </label>
                          <a data-wysihtml5-dialog-action="save">OK</a>&nbsp;<a data-wysihtml5-dialog-action="cancel">Cancel</a>
                        </div>
                      </div>
                      
                      
                      
                      
						<textarea name="desc" cols="83" rows="12" id="wysiwyg" style="width:670px;"><? echo($desc); ?></textarea></td>
                        
                        
                        <!--<script src="http://www.shanore.com/wysiwyg/xing-wysihtml5-fb0cfe4/parser_rules/simple.js"></script>
                        <script src="http://www.shanore.com/wysiwyg/xing-wysihtml5-fb0cfe4/parser_rules/advanced.js"></script>
						<script src="http://www.shanore.com/wysiwyg/xing-wysihtml5-fb0cfe4/dist/wysihtml5-0.3.0.js"></script>-->
                        
                        
                        
                        <script>
						
							/*var wysihtml5ParserRules = {
							  tags: {
								"a": {
								  "check_attributes": {
									"href": "url" // make sure the entered url is really an url
								  },
								  "set_attributes": {
									"target": "_self"   // optional
								  }
								},
								h3: {},
								p:{},
						      	h2: {}
							  }
							};
						
                          var editor = new wysihtml5.Editor("wysiwyg", {
                            toolbar:      "toolbar",
                            stylesheets:  "/wysiwyg/xing-wysihtml5-fb0cfe4/website/css/stylesheet.css",
                            parserRules:  wysihtml5ParserRules,
							useLineBreaks: true
                          });*/
						  
						  $('#wysiwyg').wysihtml5({
							 "font-styles": true, //Font styling, e.g. h1, h2, etc.
							 "emphasis": false, //Italics, bold, etc.
							 "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers.
							 "html": true, //Button which allows you to edit the generated HTML.
							 "link": true, //Button to insert a link.
							 "image": false, //Button to insert an image.
							 "color": false //Button to change color of font
						});
						  
                        </script>
                        
                        
                    </tr>
                  </table>
                </form>
<script>

	/*function toggle_images(){
		if($('#basic-modal-content').find('#edit_image_place').css('display') == 'none'){
			$('#basic-modal-content').find('#edit_image_place').css('display','block');
		}else{
			$('#basic-modal-content').find('#edit_image_place').css('display','none');
		}
	}*/
	
	function toggle_wear(){
		if($('#basic-modal-content').find('#wear_with').css('display') == 'none'){
			$('#basic-modal-content').find('#wear_with').css('display','block');
		}else{
			$('#basic-modal-content').find('#wear_with').css('display','none');
		}
	}

	//$('#edit_image_place').hide();
	$('#wear_with').hide();
	
</script>