<style type="text/Css">
<!--
h1
{
    text-decoration: none;
    font-size: 20px;
    color: cmyk(0%,0%,0%,100%);
}

h1.title
{
    text-decoration: underline;
    font-size: 50px;
    color: cmyk(0%,0%,0%,100%);
}

td
{
    color: cmyk(0%,0%,0%,100%);
}

-->
</style>

<page backtop="35mm" backbottom="10mm" style="font-size: 14px">

    <page_header>
        <table style="width: 100%; ">
            <tr>
            	<td style="text-align: left; width: 50%; font-size:10px;"><br><br><br><br><br><h1>Celtic Engagement Rings</h1></td>
                <td style="text-align: right; width: 50%"><img src="img/logo.png" height="125" width="250"></td>
            </tr>
            <tr>
                <td colspan="3"><hr></td>
            </tr>
        </table>
    </page_header>
    <page_footer>
        <table style="width: 100%;">
            <tr>
                <td colspan="3"><hr></td>
            </tr>
            <tr>
            	<td style="text-align: left; width: 20%; font-size:10px;">Page [[page_cu]]/[[page_nb]]</td>
                <td style="text-align: center; width: 50%">www.shanore.com</td>
                <td style="text-align: right; width: 30%; font-size:10px;"><?=date("F j, Y, g:i a");?></td>
            </tr>
        </table>
    </page_footer>

<table style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="width: 100%; height: 40px; text-align: center;" colspan="3" bgcolor="#D6D6D6">
      <h1>
        Celtic Engagement Rings
      </h1>
    </td>
  </tr>
  <tr>
    <td height="220" width="220" align="center">
      <img src="../../products_images/449.jpg" style="width:120px" height="120px" />
      <br/>
      BR1 RD
      <br/>
      Celtic engagement ring 14k yellow and white gold diamond round cut.
    </td>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/451.jpg" />
      <br/>
      BR1W RD
      <br/>
      Celtic diamond solitaire 14k white gold round cut.
    </td>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/450.jpg" />
      <br/>
      BR1 PR
      <br/>
      Celtic diamond ring 14k yellow and white gold princess cut.
    </td>
  </tr>
  <tr>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/452.jpg" />
      <br/>
      BR1W PR
      <br/>
      Celtic engagement ring 14k white gold solitaire princess cut diamond.
    </td>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/457.jpg" />
      <br/>
      JP15
      <br/>
      Diamond Celtic Trinity Cluster Engagement Ring
    </td>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/458.jpg" />
      <br/>
      JP15W
      <br/>
      Diamond Celtic Trinity Cluster Engagement Ring
    </td>
  </tr>
  <tr>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/441.jpg" />
      <br/>
      14M4S6YW
      <br/>
      Celtic diamond ring 14k yellow and white gold princess cut.
    </td>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/442.jpg" />
      <br/>
      14M4S6W
      <br/>
      Celtic Solitaire ring 14k white gold princess cut diamond.
    </td>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/445.jpg" />
      <br/>
      JP1
      <br/>
      Celtic engagement ring 14k yellow and white gold round cut solitaire.
    </td>
  </tr>
  <tr>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/446.jpg" />
      <br/>
      JP1W
      <br/>
      Celtic diamond solitaire 14k white gold round cut.
    </td>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/440.jpg" />
      <br/>
      14M1S6W
      <br/>
      Celtic solitaire diamond 14k white gold ring round cut.
    </td>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/439.jpg" />
      <br/>
      14M1S6YW
      <br/>
      Celtic engagement ring 14k yellow and white gold diamond round cut.
    </td>
  </tr>
  <tr>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/469.jpg" />
      <br/>
      14Rc3ST WRD OVAL
      <br/>
      Oval Ruby and Diamond Celtic Trinity 14K Yellow and White Gold 3 Stone
    </td>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/470.jpg" />
      <br/>
      14RC3ST RD OVAL
      <br/>
      Oval Ruby and Diamond Celtic Trinity 14K Yellow and White Gold 3 Stone
    </td>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/430.jpg" />
      <br/>
      14RC3ST WED OVAL
      <br/>
      Oval Emerald and Diamond Celtic Trinity 14K White Gold 3 Stone
    </td>
  </tr>
  <tr>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/429.jpg" />
      <br/>
      14RC3ST ED OVAL
      <br/>
      Oval Emerald and Diamond Celtic Trinity 14K Yellow and White Gold 3 Stone
    </td>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/467.jpg" />
      <br/>
      14RC3ST WSD OVAL
      <br/>
      Oval Sapphire and Diamond Celtic Trinity 14K White Gold 3 Stone
    </td>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/468.jpg" />
      <br/>
      14RC3ST SD OVAL
      <br/>
      Oval Sapphire and Diamond Celtic Trinity 14K Yellow and White Gold 3 Stone
    </td>
  </tr>
  <tr>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/448.jpg" />
      <br/>
      JP4W
      <br/>
      Celtic engagement ring 14k white gold solitaire princess cut diamond.
    </td>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/447.jpg" />
      <br/>
      JP4
      <br/>
      Celtic solitaire diamond ring 14k yellow and white gold princess cut.
    </td>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/428.jpg" />
      <br/>
      14RC3STWD
      <br/>
      Celtic diamond ring 14k white gold 3 stone .25ct
    </td>
  </tr>
  <tr>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/427.jpg" />
      <br/>
      14RC3STD
      <br/>
      Celtic engagement ring 14k yellow and white 3 stone .25ct
    </td>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/63.jpg" />
      <br/>
      JP16W
      <br/>
      Diamond 14K cluster celtic engagement ring
    </td>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/62.jpg" />
      <br/>
      JP16
      <br/>
      Diamond 14K cluster celtic engagement ring
    </td>
  </tr>
  <tr>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/65.jpg" />
      <br/>
      JP17WED
      <br/>
      Emerald and Diamond Cluster Engagement Ring
    </td>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/64.jpg" />
      <br/>
      JP17ED
      <br/>
      Emerald and Diamond Cluster Engagement Ring
    </td>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/71.jpg" />
      <br/>
      JP7W
      <br/>
      Celtic trinity diamond ring 14k white gold 3 stone .33ct
    </td>
  </tr>
  <tr>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/70.jpg" />
      <br/>
      JP7
      <br/>
      Celtic diamond ring 14k yellow and white gold 3 stone .33ct trinity
    </td>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/73.jpg" />
      <br/>
      JP8W
      <br/>
      Celtic engagement ring 14k white gold 3 stone .50ct
    </td>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/72.jpg" />
      <br/>
      JP8
      <br/>
      Celtic 3 stone diamond ring 14k yellow and white gold .50ct
    </td>
  </tr>
  <tr>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/75.jpg" />
      <br/>
      JP9WED
      <br/>
      Diamond and Emerald 14K Trinity Knot Design
    </td>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/74.jpg" />
      <br/>
      JP9ED
      <br/>
      Diamond and Emerald 14K Trinity Knot Design
    </td>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/425.jpg" />
      <br/>
      14RC3STED
      <br/>
      Emerald and diamond celtic trinity 14k yellow and white gold 3 stone
    </td>
  </tr>
  <tr>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/426.jpg" />
      <br/>
      14RC3STWED
      <br/>
      Emerald and diamond celtic ring 14k white gold 3 stone
    </td>
    <td height="220" width="220" align="center">
      <img src="../../products_images_converted/66.jpg" />
      <br/>
      JP18ED
      <br/>
      Celtic 3 stone Emerald and Diamond with Trinity Knot Design
    </td>
  </tr>
</table>

</page>
