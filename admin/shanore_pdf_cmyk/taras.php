<?php

	// get variable
	$catalog = $_GET['catalog'];

	// Session start
	session_start();

	// get variable
	$_SESSION['catalog'] = $catalog;	
	
    // get the HTML
    ob_start();
    
    // Demo
    //include('my_report.php');
    
    // Tomas Script
    include('../catalogue_tara.php');
        
    $content = ob_get_clean();

    // convert in PDF
    require_once('html2pdf.class.php');
    try
    {
        $html2pdf = new HTML2PDF('P', 'A4', 'en');	
        // $html2pdf->setModeDebug();
        $html2pdf->setDefaultFont('Arial');
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        $html2pdf->Output('taras.pdf');
    }
    
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }

?>