<?
	require('db.php');
	MySQL_Query("SET NAMES UTF8");
	session_start();
	
	//logged?
	if($_GET['action']=='logout'){
		$q = "UPDATE admin SET session_id='' WHERE session_id='".session_id()."'";
		$r = mysql_query($q);
	}
	
	$q = "select login from admin where session_id='".session_id()."'";
	$r = mysql_query($q);
	$c = mysql_num_rows($r);
	if($c){
		$logged = 1;
		$f = mysql_fetch_row($r);
		$admin_name = $f[0];
	}else{
		$logged = 0;
	}
	
	

?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-language" content="en" />
        <meta name="robots" content="index,follow" />
        <meta name="googlebot" content="index,follow,snippet,archive" />
        <meta name="author" content="ELIVE CZ s.r.o. (c) 2010" />
		<?
			if(!$logged) {
				die('<meta http-equiv="refresh" content="0;url=index.php" />');
				
			}
		?>


        <link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/smoothness/jquery-ui-1.7.2.custom.css" />

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
        <script type="text/javascript" src="js/jquery.ui.datepicker-cs.js"></script>
        <script type="text/javascript" src="js/jquery.simpleTooltip.js"></script>
        <script type="text/javascript" src="js/jquery.flot.pack.js"></script>
        <!--[if IE]><script language="javascript" type="text/javascript" src="js/excanvas.pack.js"></script><![endif]-->
        <script src="js/jquery.wysiwyg.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/jquery.wysiwyg.controls.js" type="text/javascript" charset="utf-8"></script>
        <script type="text/javascript" src="js/jquery.highlight-1.1.source.js"></script>
        
		<script type="text/javascript" src="js/submenu.js"></script>
        <script type="text/javascript" src="js/xadmin.js"></script>
        <script type="text/javascript" src="js/jquery.simplemodal.js"></script>
    	<script type="text/javascript" src="js/admin.js"></script>
        

    <title>ShanOre.com ADMIN</title>
    <link href="css/jquery.wysiwyg.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
		body,td,th {
			font-family: Arial, sans-serif;
		}
    </style>
    </head>
<body>
        <div id="header">
            <h1>ShanOre.com</h1>
            <div id="user"><a href="?action=logout">Logout</a>

                <br />
            </div>
            <div id="mainMenuWrapper">
              <ul id="mainMenu">
                <li><a href="admin.php">Catalog</a></li>
                <li class="active"><a href="orders.php">ORDERS</a></li>
                <li><a href="customers.php">Customers</a><a href="editor.html"></a></li>
                <li><a href="affiliates.php">Affiliates</a><a href="editor.html"></a></li>
              </ul>
            </div>
        </div>
        <div id="contentWrapper">
            <div id="leftMenu">
                <strong>Menu</strong>
              <div id="menu">
                    <ul>
                        <li class="active"><a href="orders_installment.php" class="item">Installment Orders</a></li>
                        <li><a href="orders.php" class="item">Confirmed Orders</a></li>
                        <li><a href="processing_orders.php" class="item">Processing Orders</a></li>
                        <li><a href="resolved_orders.php" class="item">Completed Orders</a></li>
                        <li><a href="deleted_orders.php" class="item">Canceled Orders</a></li>
                        <li><a href="failed_orders.php" class="item">Failed Orders</a></li>
                        <li><a href="orders_unpaid.php" class="item">Unpaid Orders</a></li>
                        <li><a href="abandoned_carts.php" class="item">Abandoned Carts</a></li>
                    </ul>
            </div>
            <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            <strong> </strong></div>
            <div id="content">

              
              <div id="inner"><? echo($message); ?>
                <div class="datagrid">
       	  <h2>Payment Installment Orders - This month</h2>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <th width="17%">Order #</th>
<!--            <th width="13%">Title</th>
            <th width="13%">Name</th>
            <th width="13%">Surname</th>
            <th width="13%">TokenID</th>-->
            <th width="41%">Products Ordered</th>
            <th width="17%">Delivery</th>
          <!--  <th width="14%">Total</th>-->
          <th width="13%">Mthly. Payment</th>
          <th width="13%">Date</th>
          <th width="15%">Actions</th>
          </tr>
          
<?

		$prev_order = '';


		//$q = "SELECT id, title, name, surname, email, company, address1, address2, city, county, post_code, country, phone_day, phone_evening, ship_address1, ship_address2, ship_city, ship_county, ship_post_code, ship_country, total, delivery_type, currency, insert_datetime FROM orders_master WHERE status=1 AND paid=1 ORDER BY id DESC";
		$q = "SELECT orders_master.id, title, name, surname, email, company, address1, address2, city, county, post_code, country, phone_day, phone_evening, ship_address1, ship_address2, ship_city, ship_county, ship_post_code, ship_country, total, delivery_type, currency, insert_datetime, Token, Monthly_Payment, Payment_Settled, Due_Date, order_payment_Installments.id, first_trans_payer_ref, Payment_No FROM orders_master INNER JOIN order_payment_Installments ON orders_master.id=order_payment_Installments.order_id WHERE status=3 AND paid=0 AND Payment_Settled=0 AND MONTH(Due_Date)<= MONTH(NOW()) ORDER BY Due_Date ASC";
		$r = mysql_query($q) or die(mysql_error());
		$c = mysql_num_rows($r);
		
		for($i=0; $i<$c; $i++){
			$f = mysql_fetch_row($r);
			
			// lets harvest product information only once
			$q_sub = "SELECT id, product_id, qty, price, size, product_desc FROM orders WHERE order_master_id='".$f[0]."' LIMIT 1";
			$r_sub = mysql_query($q_sub) or die(mysql_error());
			$c_sub = mysql_num_rows($r_sub);
			//echo($c_sub.'<br>');
			//echo($f[0]);
			$products = '';
			for($j=0; $j<$c_sub; $j++){
				$ff = mysql_fetch_row($r_sub);
				
				$q_detail = "SELECT sku, name FROM products WHERE id=".$ff[1];
				$r_detail = mysql_query($q_detail) or die(mysql_error());
				$fff = mysql_fetch_row($r_detail);
				
				if($ff[4]) $ff[4] = '<br />SIZE '.$ff[4];
				
				$products .= $ff[2].' x ('.$fff[0].')
				<br />
				<strong><a href="https://www.shanore.com/detail.php?id=a'.$ff[1].'" target="_new">'.$fff[1].'</a></strong>'.$ff[4].'
				<br />
				'.$ff[5].'
				<br />
				<img src="../products_images/tmb/tmb_'.$ff[1].'.jpg" width="80" />';
				
				if(($j+1)<$c_sub) $products .= '<hr/>';
				
			}
			
			$datetime = $f[22];
			
			$val = explode(" ",$f[23]);
		   	$date = explode("-",$val[0]);
		   	$time = explode(":",$val[1]);
		   	$datetime = date('D jS F Y',mktime($time[0],$time[1],$time[2],$date[1],$date[2],$date[0]));
			
			
			
			echo('<tr id="line_'.$f[0].'" class="odd">
					<td valign="top">'.$f[0].'</td>
					<!--<td valign="top">'.$f[24].'</td>-->
					<td valign="top" style="text-align:left;"><h2 style="color:#000;margin:0; padding:0;">'.$datetime.'</h2>'.$products.'</td>
					<td valign="top"><a href="http://maps.google.com/?q='.$f[6].','.$f[7].','.$f[8].','.$f[9].','.$f[10].','.$f[11].'" target="_new"><img src="img/map.png" /></a><br/>'.$f[2].' '.$f[3]);
			
			
			if($f[7]) $f[7] .= '<br>';
			if($f[9]) $f[9] .= '<br>';
			if($f[10]) $f[10] .= '<br>';
			
			// getting $affiliate
			$q_aff = "SELECT affiliates_id, distance FROM affiliates_benefit WHERE orders_master_id=".$f[0];
			$r_aff = mysql_query($q_aff);
			$c_aff = mysql_num_rows($r_aff);
			if($c_aff){
				$f_aff = mysql_fetch_row($r_aff);
				
				//getting name and stuff
				$q_affname = "SELECT debtor_no, name FROM affiliates WHERE id=".$f_aff[0];
				$r_affname = mysql_query($q_affname);
				$f_affname = mysql_fetch_row($r_affname);
				
				$affiliate = '<h4 style="color:red; margin:0;">'.$f_affname[0].' - '.$f_affname[1].'</h4>distance: '.$f_aff[1].' MILES';
			}else{
				$affiliate = '<h4 style="color:red">NONE</h4>';
			}
			
			if($f[22] == 'dolar') $f[22] = 'dollar';
			
			echo('<br />'.$f[6].'<br />
					  '.$f[7].'
					  '.$f[8].'<br />
					  '.$f[9].'
					  '.$f[10].'
					  '.$f[11].'<br/>
					  <strong>'.$f[21].'</strong>
					  <p><a href="mailto:'.$f[4].'">'.$f[4].'</a><br />'.$f[12].'
					</p>
					<h3 style="margin-bottom:0;">beneficiary affiliate</h3>
					'.$affiliate.'
					</td>
					<td valign="top">'.number_format($f[25],2).'<br/>['.$f[22].']</td>
					<td valign="top">'.$f[27].'</td>');
					$now = time(); // current time or date
     				$your_date = strtotime($f[27]);
    				$datediff = $now - $your_date;
     				$diff_days = floor($datediff/(60*60*24));
					
					$val= explode("-",$f[29]); //random token from original transaction. e.g: (1842861181) from this number 3265-1-0-1842861181
					$total = strval(100 * floatval($f[25])); //amount * 100 as realex does not accept decimals
					
					$timestamp = strftime("%Y%m%d%H%M%S");
					mt_srand((double)microtime()*1000000);
					
					//Live 
					$merchant_id = 'flaircraft';
					$secret = 'k5QV4AUq6S';
					
					//Sandbox details
					//$merchant_id = 'flaircrafttest';
					//$secret = 'secret';	
					
					if($f[22] == 'dolar') $curr = 'USD';
					if($f[22] == 'dollar') $curr = 'USD';
					if($f[22] == 'euro') $curr = 'EUR';
					//$curr = 'EUR';
				
					$f[0] = intval($f[0]);
					
					$master_order_id = $f[0].'-2-'.$f[28].'-'.$f[24];
					
					$payerref= $f[29];
					$pmt_ref=$f[24];
					
					
					$tmp  = "$timestamp.$merchant_id.$master_order_id.$total.$curr.$payerref";
					
				
					
						echo('string: '.$tmp.'<br>');
					//$md5hash = md5($tmp);
					$md5hash = sha1($tmp);
						echo('hash 1: '.$md5hash.'<br>');
					$tmp = "$md5hash.$secret";
					$md5hash = sha1($tmp);
						echo('hash 2: '.$md5hash.'<br>');
					
						
						$q_total = "SELECT id FROM order_payment_Installments WHERE order_id=".$f[0];
						$r_total = mysql_query($q_total);
						$total_payments = mysql_num_rows($r_total);
						
						
						// process_order($id,$amount,$master_order_id,$token,$payer_ref, hash)
						
					echo('<td valign="top">
					<img src="img/req_payment_btn.png" style="float:right; cursor:pointer; clear:both;" onclick="process_installment('.$f[28].','.$total.','.$f[0].','.$f[24].',\''.$f[29].'\',\''.$md5hash.'\')" />');
					echo('<br>#[<b>'.$f[30].'/'.$total_payments.'</b>]');
					//echo('<br>DB curr = '.$f[22]);
					
					echo('</td>');
					  //}
				  echo('</tr>');
		}
?>           
             </table>
          </div>
          
          
          <div class="datagrid">
       	  	<h2>All Scheduled Payments</h2>
          	
          	<table width="100%" border="0" cellspacing="3" cellpadding="3">
          <tr>
            <th width="17%">Order #</th>
<!--            <th width="13%">Title</th>
            <th width="13%">Name</th>
            <th width="13%">Surname</th>
            <th width="13%">TokenID</th>-->
            <th width="41%" align="left">Products Ordered</th>
            <th width="17%">Delivery</th>
          <!--  <th width="14%">Total</th>-->
          <th width="13%"> Payment</th>
          <th width="15%">Progress</th>
          </tr>
          
<?

		//$q = "SELECT id, title, name, surname, email, company, address1, address2, city, county, post_code, country, phone_day, phone_evening, ship_address1, ship_address2, ship_city, ship_county, ship_post_code, ship_country, total, delivery_type, currency, insert_datetime FROM orders_master WHERE status=1 AND paid=1 ORDER BY id DESC";
		//$q = "SELECT orders_master.id, title, name, surname, email, company, address1, address2, city, county, post_code, country, phone_day, phone_evening, ship_address1, ship_address2, ship_city, ship_county, ship_post_code, ship_country, total, delivery_type, currency, insert_datetime, Token, Monthly_Payment, Payment_Settled, Due_Date, order_payment_Installments.id, first_trans_payer_ref, Payment_No FROM orders_master INNER JOIN order_payment_Installments ON orders_master.id=order_payment_Installments.order_id WHERE status=3 AND paid=0 AND Payment_Settled=0 ORDER BY Due_Date ASC";
		
		$q = "SELECT orders_master.id, title, name, surname, email, company, address1, address2, city, county, post_code, country, phone_day, phone_evening, ship_address1, ship_address2, ship_city, ship_county, ship_post_code, ship_country, total, delivery_type, currency, insert_datetime, Token, Monthly_Payment, Payment_Settled, Due_Date, order_payment_Installments.id, first_trans_payer_ref, Payment_No FROM orders_master INNER JOIN order_payment_Installments ON orders_master.id=order_payment_Installments.order_id WHERE status=3 AND paid=0 AND Payment_Settled=0 ORDER BY orders_master.id, Due_Date";
		
		
		$r = mysql_query($q) or die(mysql_error());
		$c = mysql_num_rows($r);
		
		for($i=0; $i<$c; $i++){
			$f = mysql_fetch_row($r);
			
			
			if($f[0] == $prev_order){
				continue;
				$prev_order = $f[0];
			}else{
				$prev_order = $f[0];
			}
			
			
			// lets harvest product information only once
			$q_sub = "SELECT id, product_id, qty, price, size, product_desc FROM orders WHERE order_master_id='".$f[0]."' LIMIT 1";
			$r_sub = mysql_query($q_sub) or die(mysql_error());
			$c_sub = mysql_num_rows($r_sub);
			//echo($c_sub.'<br>');
			//echo($f[0]);
			$products = '';
			for($j=0; $j<$c_sub; $j++){
				$ff = mysql_fetch_row($r_sub);
				
				$q_detail = "SELECT sku, name FROM products WHERE id=".$ff[1];
				$r_detail = mysql_query($q_detail) or die(mysql_error());
				$fff = mysql_fetch_row($r_detail);
				
				if($ff[4]) $ff[4] = '<br />SIZE '.$ff[4];
				
				$products .= $ff[2].' x ('.$fff[0].')
				<br />
				<strong><a href="https://www.shanore.com/detail.php?id=a'.$ff[1].'" target="_new">'.$fff[1].'</a></strong>'.$ff[4].'
				<br />
				'.$ff[5];
				
				if(($j+1)<$c_sub) $products .= '<hr/>';
				
			}
			
			$datetime = $f[27];
			
			$val = explode(" ",$f[27]);
		   	$date = explode("-",$val[0]);
		   	$time = explode(":",$val[1]);
		   	$datetime = date('D jS F Y',mktime($time[0],$time[1],$time[2],$date[1],$date[2],$date[0]));
			
			echo('<tr id="line_'.$f[0].'" class="odd">
					<td valign="top">'.$f[0].'</td>
					<!--<td valign="top">'.$f[24].'</td>-->
					<td valign="top" style="text-align:left;"><h2 style="color:#000;margin:0; padding:0;">'.$datetime.'</h2>'.$products.'</td>
					<td valign="top"><a href="http://maps.google.com/?q='.$f[6].','.$f[7].','.$f[8].','.$f[9].','.$f[10].','.$f[11].'" target="_new"><img src="img/map.png" /></a><br/>'.$f[2].' '.$f[3]);
			
			
			if($f[7]) $f[7] .= '<br>';
			if($f[9]) $f[9] .= '<br>';
			if($f[10]) $f[10] .= '<br>';
			
			// getting $affiliate
			$q_aff = "SELECT affiliates_id, distance FROM affiliates_benefit WHERE orders_master_id=".$f[0];
			$r_aff = mysql_query($q_aff);
			$c_aff = mysql_num_rows($r_aff);
			if($c_aff){
				$f_aff = mysql_fetch_row($r_aff);
				
				//getting name and stuff
				$q_affname = "SELECT debtor_no, name FROM affiliates WHERE id=".$f_aff[0];
				$r_affname = mysql_query($q_affname);
				$f_affname = mysql_fetch_row($r_affname);
				
				$affiliate = '<h4 style="color:red; margin:0;">'.$f_affname[0].' - '.$f_affname[1].'</h4>distance: '.$f_aff[1].' MILES';
			}else{
				$affiliate = '<h4 style="color:red">NONE</h4>';
			}
			
			if($f[22] == 'dolar') $f[22] = 'dollar';
			
			echo('<br />'.$f[6].'<br />
					  '.$f[7].'
					  '.$f[8].'<br />
					  '.$f[9].'
					  '.$f[10].'
					  '.$f[11].'<br/>
					  <strong>'.$f[21].'</strong>
					  <p><a href="mailto:'.$f[4].'">'.$f[4].'</a><br />'.$f[12].'
					</p>
					<h3 style="margin-bottom:0;">beneficiary affiliate</h3>
					'.$affiliate.'
					</td>
					<td valign="top">'.number_format($f[25],2).'<br/>['.$f[22].']</td>');
					$curr = 'USD';
					if($f[22] == 'dolar') $curr = 'USD';
					if($f[22] == 'euro') $curr = 'EUR';
					
				
					$master_order_id = $f[0].'-2-'.$f[28].'-'.$f[24];
					$payerref= $f[29];
					$pmt_ref=$f[24];
					
						$q_total = "SELECT id FROM order_payment_Installments WHERE order_id=".$f[0];
						$r_total = mysql_query($q_total);
						$total_payments = mysql_num_rows($r_total);
						
					echo('<td valign="top">');
					echo('<br>#[<b>'.$f[30].'/'.$total_payments.'</b>]');
					echo('<img src="img/print.png" style="float:right; cursor:pointer; clear:both;" onclick="window.open(\'print_order.php?mid='.$f[0].'\',\'\',\'width=980,resizable=0,scrollbars=1\')">');
					echo('</td>');
					  //}
				  echo('</tr>');
		}
?>           
             </table>
          
          </div>
          
          
          
          
          
          
              </div>
            </div>
            <hr class="cleaner" />
            
      <div id="basic-modal-content"> 
      		<!-- edit form here -->
            
	  </div>
      <div id="basic-modal-content-2"> 
      		<!-- edit form here -->
            
	  </div>
            
            
</div>

        <div id="footer">

        </div>
      
<?
	
	
	
	
	//Hash Variables
	//echo('Time: '.$timestamp.'<br>');
	//echo('Merchant: '.$merchant_id.'<br>');
	//echo('Order Id: '.$master_order_id.'<br>');
	//echo('Total: '.$total.'<br>');
	//echo('Currency: '.$curr.'<br>');
	//echo('PAYER_REF: '.$payerref.'<br>');	
	//echo('PMT_REF: '.$pmt_ref.'<br>');
			
	//Ojo aqui, I've commented**********************************************	
	//$tmp = "$timestamp.$merchant_id.$master_order_id.$total.$curr.$payerref.$pmt_ref";
	//$tmp_ = "$timestamp.$merchant_id.$master_order_id.$total.$curr.$payerref.$pmt_ref";
	
	$tmp  = "$timestamp.$merchant_id.$master_order_id.$total.$curr.$payerref";
	$tmp_ = "$timestamp.$merchant_id.$master_order_id.$total.$curr.$payerref";
	
	//$tmp  = "20140417130443.flaircraft.6702-2-38-122020312.26957.USD.6702-1-0-641654490-6";
	//$md5hash = md5($tmp);
	$md5hash = sha1($tmp);
	$tmp = "$md5hash.$secret";
	//$md5hash = md5($tmp);
	$md5hash = sha1($tmp);
	//Live account
	$account = 'internet';
	
	//test account
	//$account = 'internettest';
?> 
<script>
function process_installment($id,$amount,$master_order_id,$token,$payer_ref, hash){
	$tmp =$master_order_id+'-'+'2'+'-'+$id+'-'+$token;
	
	//alert($tmp);
	
	document.getElementById('ORDER_ID').value=$tmp;
	document.getElementById('AMOUNT').value=$amount;
	document.getElementById('PAYER_REF').value=$payer_ref;
	document.getElementById('PMT_REF').value=$token;
	document.getElementById('MD5HASH').value=hash;
	
	//alert($tmp);
	
	document.realex_form.submit();
	
	//alert(document.getElementById('PMT_REF').value);
}//function
</script>

<form method="POST" action="orders_installment_posting.php" id="realex_form" name="realex_form" style="margin-left:20px; margin-top:20px;">
    <input type="hidden" name="MERCHANT_ID" value="<? echo($merchant_id); ?>">
    <input type="hidden" name="ORDER_ID"  id="ORDER_ID" />
    <input type="hidden" name="ACCOUNT" value="<? echo($account); ?>">
    <input type="hidden" name="AMOUNT" id="AMOUNT" />
    <input type="hidden" name="CURRENCY" value="<? echo($curr); ?>">
    <input type="hidden" name="TIMESTAMP" value="<? echo($timestamp); ?>">
    
    <input type="hidden" name="OFFER_SAVE_CARD" value="1"> 
	<input type="hidden" name="PAYER_REF" id="PAYER_REF" />
	<input type="hidden" name="PMT_REF" id="PMT_REF" /> 
	<input type="hidden" name="PAYER_EXIST" value="0">
    
    <input type="hidden" name="MD5HASH" id="MD5HASH" value="<? echo($md5hash);?>">
    <input type="hidden" name="TMP" value="<? echo($tmp_);?>">    
</form>

<!--<input type="hidden" name="AUTO_SETTLE_FLAG" value="1">-->

<!--<img src="images/realex_button.jpg" alt="Pay with Realex" style="cursor:pointer;" onclick="submit_details('realex_form','<? //echo($master_order_id); ?>')" />   -->      
    </body>

<?
	if($_GET['search_txt']){
		echo("<script>
					$('td').highlight('".$_GET['search_txt']."','highlight');
			  </script>");
	}
?>    

</html>
