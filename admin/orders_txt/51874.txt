1<Order Number - <b>4000051874</b>>


Order Date - 2019-12-13 13:52:00
Order Status - 1 - confirmed and paid


2<Products>

<b>TARA'S DIARY</b> - Green Shamrock Silver Crystal Bead (P/N - TD67)
Gift Wrap = NO
Size = -
Price = 58.00
QTY = 1
Total = 58.00

<b>TARA'S DIARY</b> - Silver Celtic Trinity Beaded Heart  (P/N - TD71)
Gift Wrap = NO
Size = -
Price = 56.00
QTY = 1
Total = 56.00

2<Billing and Shipping Information>

Wrap note = If package is too large for my mailbox. Have it placed on the back porch NOT the front porch.

<b>Billing address</b>
 Tracy Wetzel
13726 Karper Rd.

Mercersburg
PA&nbsp;17236
United States

<b>Delivery address</b>
Tracy Wetzel
13726 Karper Rd.

Mercersburg
PA17236
United States

Registered Post (6 - 10 days)

SUB total= 114.00

Delivery= 0.00

<b>TOTAL= 56.00</b>

