1<Order Number - <b>4000037586</b>>


Order Date - 2016-12-14 15:16:04
Order Status - 1 - confirmed and paid


2<Products>

<b>SHANORE</b> - Intricate Celtic Knot Silver Design Necklace (P/N - SP2110CZ)
Gift Wrap = NO
Size = -
Price = 67.00
QTY = 1
Total = 67.00

<b>SHANORE</b> - Silver Celtic Trinity Knot Stud Earrings  (P/N - SE2201)
Gift Wrap = NO
Size = -
Price = 41.00
QTY = 1
Total = 41.00

2<Billing and Shipping Information>

Wrap note = 

<b>Billing address</b>
 Justin Connaughton
52 Hall End Road

Wootton
Beds&nbsp;MK43 9HP
United Kingdom

<b>Delivery address</b>
Justin Connaughton
25 The Green
undefined
Marston Moretaine
BedsMK43 0NF
United Kingdom

Registered mail (10-14 business days)

SUB total= 108.00

Delivery= 0.00

<b>TOTAL= 41.00</b>

