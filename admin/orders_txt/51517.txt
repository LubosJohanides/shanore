1<Order Number - <b>4000051517</b>>


Order Date - 2019-12-01 16:36:15
Order Status - 1 - confirmed and paid


2<Products>

<b>SHANORE</b> - Sterling Silver Celtic Trinity Knot Necklace  (P/N - SP2200)
Gift Wrap = NO
Size = -
Price = 48.45
QTY = 1
Total = 48.45

<b>SHANORE</b> - Silver Celtic Trinity Knot Stud Earrings  (P/N - SE2201)
Gift Wrap = NO
Size = -
Price = 42.50
QTY = 1
Total = 42.50

2<Billing and Shipping Information>

Wrap note = BFS2019

<b>Billing address</b>
 Jessica Ireland
11007 Rougemont Rd

Rougemont
NC&nbsp;27572
United States

<b>Delivery address</b>
Jessica Ireland
11007 Rougemont Rd

Rougemont
NC27572
United States

Registered Post (6 - 10 days)

SUB total= 90.95

Delivery= 0.00

<b>TOTAL= 42.50</b>

