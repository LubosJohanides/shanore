1<Order Number - <b>4000047433</b>>


Order Date - 2018-12-05 13:38:56
Order Status - 1 - confirmed and paid


2<Products>

<b>SHANORE</b> - Claddagh Birthstone August Pendant Adorned With Swarovski Crystal  (P/N - SW101PT)
Gift Wrap = NO
Size = -
Price = 64.00
QTY = 1
Total = 64.00

2<Billing and Shipping Information>

Wrap note = If no answer please leave with a neighbour. 

<b>Billing address</b>
 William Partlan
89 Clarendon Road 

Stockport 
Cheshire &nbsp;SK7 4NS
United Kingdom

<b>Delivery address</b>
William Partlan
89 Clarendon Road 

Stockport 
Cheshire SK7 4NS
United Kingdom

Registered Post (6 - 10 days)

SUB total= 64.00

Delivery= 0.00

<b>TOTAL= 64.00</b>

