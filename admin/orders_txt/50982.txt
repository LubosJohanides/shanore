1<Order Number - <b>4000050982</b>>


Order Date - 2019-10-30 17:45:32
Order Status - 1 - confirmed and paid


2<Products>

<b>SHANORE</b> -  Trinity Gold Plated Cross Embellished With Swarovski Crystals (P/N - SW18)
Gift Wrap = NO
Size = -
Price = 167.00
QTY = 1
Total = 167.00

2<Billing and Shipping Information>

Wrap note = This is a gift for Christmas so please send along a gift receipt if she should decide to choose another of your beautiful products.

<b>Billing address</b>
 Linda McInerney
19733 North Shore Dr.

Spring Lake
MI&nbsp;49456
United States

<b>Delivery address</b>
Linda McInerney
19733 North Shore Dr.

Spring Lake
MI49456
United States

Registered Post (6 - 10 days)

SUB total= 167.00

Delivery= 0.00

<b>TOTAL= 167.00</b>

