1<Order Number - <b>4000049278</b>>


Order Date - 2019-04-28 19:05:53
Order Status - 1 - confirmed and paid


2<Products>

<b>SHANORE</b> - Claddagh February Birthstone Ring (P/N - SL90AY)
Gift Wrap = NO
Size = 8
Price = 77.00
QTY = 1
Total = 77.00

<b>SHANORE</b> - Silver Claddagh Key Pendant Encrusted With Swarovski Crystals (P/N - SW103)
Gift Wrap = NO
Size = -
Price = 87.00
QTY = 1
Total = 87.00

2<Billing and Shipping Information>

Wrap note = 

<b>Billing address</b>
 Sarah Logan
1609 NW Gregory Dr

Vancouver
WA&nbsp;98665
United States

<b>Delivery address</b>
Sarah Logan
1609 NW Gregory Dr

Vancouver
WA98665
United States

Registered Post (6 - 10 days)

SUB total= 164.00

Delivery= 0.00

<b>TOTAL= 87.00</b>

