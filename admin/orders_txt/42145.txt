1<Order Number - <b>4000042145</b>>


Order Date - 2017-10-05 00:01:35
Order Status - 1 - confirmed and paid


2<Products>

<b>SHANORE</b> - Claddagh Stud Earrings Adorned With Swarovski Crystals (P/N - SW47)
Gift Wrap = NO
Size = -
Price = 62.00
QTY = 1
Total = 62.00

<b>SHANORE</b> - Claddagh Birthstone Necklace September (P/N - SP91S)
Gift Wrap = NO
Size = -
Price = 77.00
QTY = 1
Total = 77.00

2<Billing and Shipping Information>

Wrap note = 

<b>Billing address</b>
 Kelly Pittner
2427 Heritage Circle

Navarre
Fl&nbsp;32566
United States

<b>Delivery address</b>
Grace Gasior
105 Schorr Drive

McKees Rocks
PA15136
United States

Registered Post (6 - 10 days)

SUB total= 139.00

Delivery= 0.00

<b>TOTAL= 77.00</b>

