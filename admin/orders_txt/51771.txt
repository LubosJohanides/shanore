1<Order Number - <b>4000051771</b>>


Order Date - 2019-12-08 21:02:22
Order Status - 1 - confirmed and paid


2<Products>

<b>SHANORE</b> - Silver Celtic Stone Set Trinity Bracelet  (P/N - SB2018GR)
Gift Wrap = NO
Size = -
Price = 116.00
QTY = 1
Total = 116.00

<b>SHANORE</b> - Celtic Trinity Stone set Silver Earrings (P/N - SE2017GR)
Gift Wrap = NO
Size = -
Price = 47.00
QTY = 1
Total = 47.00

<b>SHANORE</b> - Celtic Silver Stone Set Trinity Necklace (P/N - SP2016GR)
Gift Wrap = NO
Size = -
Price = 65.00
QTY = 1
Total = 65.00

2<Billing and Shipping Information>

Wrap note = 

<b>Billing address</b>
 Peter McMahon
16a Burston Drive

St Albans
Hertfordshire &nbsp;AL2 2HR
United Kingdom

<b>Delivery address</b>
Peter McMahon
16a Burston Drive

St Albans
Hertfordshire AL2 2HR
United Kingdom

Registered - DPD (2 days)

SUB total= 228.00

Delivery= 0.00

<b>TOTAL= 65.00</b>

