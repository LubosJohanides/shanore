1<Order Number - <b>4000043024</b>>


Order Date - 2017-12-03 21:54:03
Order Status - 1 - confirmed and paid


2<Products>

<b>TARA'S DIARY</b> - Black Leather 16.5 Inch Origin Bracelet (P/N - TD633 16.5 )
Gift Wrap = NO
Size = -
Price = 42.00
QTY = 1
Total = 42.00

<b>TARA'S DIARY</b> - Silver Spiral September Birthstone Bead (P/N - TD107S)
Gift Wrap = NO
Size = -
Price = 67.00
QTY = 1
Total = 67.00

<b>TARA'S DIARY</b> - Silver Spiral January Birthstone Bead (P/N - TD107G)
Gift Wrap = NO
Size = -
Price = 67.00
QTY = 1
Total = 67.00

<b>TARA'S DIARY</b> - Silver Spiral August Birthstone Bead (P/N - TD107P)
Gift Wrap = NO
Size = -
Price = 67.00
QTY = 1
Total = 67.00

<b>TARA'S DIARY</b> - Sterling Silver Claddagh Bead Encrusted With Swarovski Crystal (P/N - TD617)
Gift Wrap = NO
Size = -
Price = 73.00
QTY = 1
Total = 73.00

<b>TARA'S DIARY</b> - Celtic Diamond Set Trinity Knot Dangle Bead (P/N - TD127)
Gift Wrap = NO
Size = -
Price = 53.00
QTY = 1
Total = 53.00

2<Billing and Shipping Information>

Wrap note = 

<b>Billing address</b>
 Ivan  Lewis
PO Box 31

Startup
WA&nbsp;98293
United States

<b>Delivery address</b>
Ivan  Lewis
PO Box 31

Startup
WA98293
United States

Registered Post (6 - 10 days)

SUB total= 369.00

Delivery= 0.00

<b>TOTAL= 53.00</b>

