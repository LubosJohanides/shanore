1<Order Number - <b>4000047243</b>>


Order Date - 2018-11-26 23:26:29
Order Status - 1 - confirmed and paid


2<Products>

<b>SHANORE</b> - Celtic Tribal Silver Trinity Knot Earrings  (P/N - SE2210AY)
Gift Wrap = NO
Size = -
Price = 140.00
QTY = 1
Total = 140.00

<b>SHANORE</b> -  Celtic Tribal Silver Stone Set Trinity Knot Key (P/N - SP2207AY )
Gift Wrap = NO
Size = -
Price = 192.00
QTY = 1
Total = 192.00

<b>OCEAN</b> - Silver Abalone Turtle Pendant Adorned With White Swarovski Crystals (P/N - OC23)
Gift Wrap = NO
Size = -
Price = 129.00
QTY = 1
Total = 129.00

2<Billing and Shipping Information>

Wrap note = 780-991-1422 Cell 

<b>Billing address</b>
 Richard Densmore
624 Crimson Drive

Sherwood Park
Alberta&nbsp;T8H 0B2
Canada

<b>Delivery address</b>
Richard Densmore
624 Crimson Drive

Sherwood Park
AlbertaT8H 0B2
Canada

Fedex (1 - 2 days)

SUB total= 461.00

Delivery= 30.00

<b>TOTAL= 129.00</b>

