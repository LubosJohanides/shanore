1<Order Number - <b>4000047327</b>>


Order Date - 2018-12-01 02:03:34
Order Status - 1 - confirmed and paid


2<Products>

<b>SHANORE</b> - Silver Shamrock Earrings Adorned With Swarovski Crystal (P/N - SW84)
Gift Wrap = NO
Size = -
Price = 73.15
QTY = 1
Total = 73.15

<b>SHANORE</b> - Sterling Silver Claddagh Bracelet Adorned With White Swarovski Crystal  (P/N - SW68)
Gift Wrap = NO
Size = -
Price = 92.15
QTY = 1
Total = 92.15

2<Billing and Shipping Information>

Wrap note = 

<b>Billing address</b>
 Dennis Carney
240 W Ridley Ave

Ridley Park
PA&nbsp;19078-2724
United States

<b>Delivery address</b>
Dennis Carney
240 W Ridley Ave

Ridley Park
PA19078-2724
United States

Registered Post (6 - 10 days)

SUB total= 165.30

Delivery= 0.00

<b>TOTAL= 92.15</b>

