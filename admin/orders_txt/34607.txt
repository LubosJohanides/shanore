1<Order Number - <b>4000034607</b>>


Order Date - 2016-09-13 12:29:56
Order Status - 1 - confirmed and paid


2<Products>

<b>SHANORE</b> - Claddagh Celtic solitaire diamond 14k white gold engagement ring princess cut.  (P/N - BR2W PR)
Gift Wrap = YES
Size = 8
Price = 1,220.00
QTY = 1
Total = 1,220.00

2<Billing and Shipping Information>

Wrap note = 

<b>Billing address</b>
 Scott Mills
511 Stream Mill Ln

Katy
TX&nbsp;77494
United States

<b>Delivery address</b>
Scott Mills
509 N Sam Houston Pkwy E, Suite 400
undefined
Houston
TX77060
United States

Fedex Priority (1-2 business days)

SUB total= 1,220.00

Delivery= 0.00

<b>TOTAL= 1,220.00</b>

