1<Order Number - <b>4000044155</b>>


Order Date - 2018-03-03 19:48:38
Order Status - 1 - confirmed and paid


2<Products>

<b>SHANORE</b> - Claddagh Birthstone March Pendant Adorned With Swarovski Crystal  (P/N - SW101AQ)
Gift Wrap = NO
Size = -
Price = 64.00
QTY = 1
Total = 64.00

<b>SHANORE</b> - 2 Stone Shamrock Mothers Pendant  (P/N - MP2-S)
Gift Wrap = YES
Size = -
Price = 103.00
QTY = 1
Total = 103.00

2<Billing and Shipping Information>

Wrap note = 

<b>Billing address</b>
 Richard Bell
5 Station Avenue, South Witham

Grantham
Lincs&nbsp;NG33 5QF
United Kingdom

<b>Delivery address</b>
Richard Bell
5 Station Avenue, South Witham

Grantham
LincsNG33 5QF
United Kingdom

Registered Post (6 - 10 days)

SUB total= 167.00

Delivery= 0.00

<b>TOTAL= 103.00</b>

