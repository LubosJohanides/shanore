<?
	require('db.php');

	$provider = $_POST['provider'];
	$code = $_POST['tracking_code'];
	$master_order_id = intval($_POST['mid']);

	switch($provider){
		case 'Fedex':
			$link = "https://www.fedex.com/insight/findit/nrp.jsp?tracknumbers=".$code."&language=en&opco=FX&clientype=ivshpalrt";
			$provider = "Fedex Priority";
			break;
		case 'USPS':
			$link = "https://tools.usps.com/go/TrackConfirmAction!input.action?tRef=qt&tLc=1&tLabels=".$code;
			break;
		case 'An Post':
			$link = "http://track.anpost.ie/TrackingResults.aspx?rtt=0&site=website&trackcode=".$code;
			break;
		case 'Fastway':
			$link = "http://www.fastway.ie/courier-services/track-your-parcel?l=".$code;
			break;
			case 'DPD':
				$link = "http://www2.dpd.ie/Services/QuickTrack/tabid/222/ConsignmentID/".$code."/Default.aspx";
				break;
	}


	$q = "SELECT * FROM orders_master WHERE id=".$master_order_id;
	$r = mysqli_query($db,$q) or die(mysqli_error($db));
	$f = mysqli_fetch_assoc($r);

	if($f['currency'] == 'euro'){
		$currency_sign = '&euro;';
	}else{
		$currency_sign = '$';
	}

	$firstname = $f['name'];


	$email_body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>Shanore Jewellery Order Shipping Confirmation</title>
			<style type="text/css">

			</style>
			</head>

			<body style="background: #f7f7f7;margin: 0;padding: 0;color: #000;">
				<div style="background: #3d7e48; height: 30px;">

				</div>


			<div class="container" style="width: 980px;background: #FFF;margin: 0 auto;">
			  <div id="inner_header" style="height: 90px;border-bottom-width: 1px;border-bottom-style: solid;margin-bottom: 30px;padding-top: 24px;padding-right: 0px;padding-bottom: 0px;padding-left: 0px;position: relative;border-bottom-color: #cccccc;">
				<div id="top_logo" style="width: 199px;margin-right: auto;margin-bottom: 0px;margin-left: auto;">
					  <a href="https://www.shanore.com/"><img src="https://www.shanore.com/img/logo.png" alt="shanore jewelery logo" width="199" height="82" border="0"></a>
				</div>
			  </div>
			  <div style="padding-left:30px;">
					<h3>Dear '.$firstname.', </h3>
					<p>We thank you for you online purchase from the Shanore store and are delighted to inform you that we have shipped your Shanore purchase to you today by '.$provider.'. <br/><br/>Your tracking number is <strong>'.$code.'</strong>. You can track you package here: <br/><strong>'.$link.'</strong><br /><br />
					As a thank you for your purchase and a small token of appreciation, we have enclosed a voucher for discount in your package for future website purchases.
					<br />
					<br/>
			In case of any problems with this order please do not hesitate to contact us.</p>
			<p>Your order number is: <strong>'.$master_order_id.'</strong></p>
			<br />
			The Shanore Team<br />
			info@shanore.com

					<br />
			<br />

			  </div>
			  <div class="content" style="width:980px;float: right;padding-top: 0px;padding-right: 0;padding-bottom: 10px;padding-left: 0;">

					<div style="margin-left:40px;">

					  <table width="100%" border="0" cellspacing="0" cellpadding="5" class="bag_table">
				<tr>
				  <th height="40" width="24">&nbsp;</th>
				  <th width="94" align="center">Item</th>
				  <th width="24">&nbsp;</th>
				  <th width="208" align="left">Description</th>
				  <th width="24">&nbsp;</th>
				  <th width="94" align="center">gift wrap</th>
				  <th width="24">&nbsp;</th>
				  <th width="94" align="center">more info</th>
				  <th width="24">&nbsp;</th>
				  <th width="94" align="center">price</th>
				  <th width="24">&nbsp;</th>
				  <th width="94" align="center">quantity</th>
				  <th width="24">&nbsp;</th>
				  <th width="94" align="center">total</th>
				  <th>&nbsp;</th>
				</tr>';


				$q = "SELECT product_id, id, qty, price, size, wrapping, product_desc  FROM orders WHERE order_master_id='".$master_order_id."'";
				$r = mysqli_query($db,$q) or die(mysqli_error($db));
				$c = mysqli_num_rows($r);
				for($i=0; $i<$c; $i++){
					$f = mysqli_fetch_row($r);

					$product_id = $f[0];
					$order_id = $f[1];
					$price = number_format($f[3],2);
					$quantity = $f[2];
					$total = number_format((floatval($f[3]) * intval($quantity)),2);
					$size = $f[4];
					if(!$size) $size = "NA";
					$wrapping = $f[5];
					$more_desc = $f[6];

					if(file_exists('../img/products/product/'.$product_id.'.jpg')){
						$image_size = getimagesize ("../img/products/product/".$product_id.".jpg");
						if($image_size[0] > $image_size[1]){
							$size_restriction = 'width="90"';
						}else{
							$size_restriction = 'height="90"';
						}
						$img = '<img src="https://www.shanore.com/img/products/product/'.$product_id.'.jpg" '.$size_restriction.' alt="product" />';
					}else{
						$img = '<img src="https://www.shanore.com/img/noimage_big.jpg" height="90" alt="no image" />';
					}

					$q_product = "SELECT brand, name FROM products WHERE id=".$product_id;
					$r_product = mysqli_query($db,$q_product);
					$f_product = mysqli_fetch_row($r_product);

					$q_brand = "SELECT name FROM brands WHERE id=".$f_product[0];
					$r_brand = mysqli_query($db,$q_brand);
					$f_brand = mysqli_fetch_row($r_brand);

					if($wrapping){
						$checked = ' checked="yes"';
					}else{
						$checked = '';
					}


					//clear $size of bad stuff

					$size = str_replace("More info: undefined, ","",$size);
					$size = str_replace("More info: ","",$size);
					$size = str_replace("More info:","",$size);


					$email_body .= '<tr id="row_'.$order_id.'_1">
							  <td>&nbsp;</td>
							  <td>'.$img.'</td>
							  <td>&nbsp;</td>
							  <td>
								<span class="brand_name_bag" style="display:block;font-size: 16px;color: #000;">'.strtoupper($f_brand[0]).'</span>
								<span class="product_name_bag" style="display:block;font-size: 14px;clear: both;color: #666666;">'.$f_product[1].'</span>
								<a href="https://www.shanore.com/product.php?id='.$product_id.'" class="seeall" style="margin-top:20px;color: #000 ;font-family: MuseoSlab-500Italic; font-weight: normal;font-style: normal;font-size: 12px;margin-top: 10px;">See item...'.$more_desc.'</a>
							  </td>
							  <td>&nbsp;</td>
							  <td align="center" valign="middle"><input type="checkbox" id="gift_'.$order_id.'" '.$checked.' DISABLED/></td>
							  <td>&nbsp;</td>
							  <td align="center" valign="middle" class="size_bag" style="color: #666666;font-size: 16px;">'.$size.'</td>
							  <td>&nbsp;</td>
							  <td align="center" valign="middle" class="price_bag" style="font-size: 16px;">'.$currency_sign.' <span id="price_'.$order_id.'">'.$price.'</span></td>
							  <td>&nbsp;</td>
							  <td align="center" valign="middle"><input class="bag_quantity" style="width:10px;" id="quantity_'.$order_id.'" value="'.$quantity.'" disabled="disabled" /></td>
							  <td>&nbsp;</td>
							  <td align="center" valign="middle" class="price_bag" style="font-size: 16px;">'.$currency_sign.' <span class="total_price" id="total_'.$order_id.'">'.$total.'</span></td>
							  <td>&nbsp;</td>
						</tr>';
				}

				$q = "SELECT sub_total, delivery, total, ship_address1, ship_address2, ship_city, ship_county, ship_post_code, ship_country, address1, address2, city, county, post_code, country, email, name, surname, shipping_estimation FROM orders_master WHERE id=".$master_order_id;
				$r = mysqli_query($db,$q) or die(mysqli_error($db));
				$f = mysqli_fetch_row($r);

				$delivery_price = $f[1];
				$sub = $f[0];
				$total = $f[2];

				// SHIPPING ADDRESS
				$ship_address1 = $f[3];
				$ship_address2 = $f[4];
				$ship_city = $f[5];
				$ship_county = $f[6];
				$ship_post_code = $f[7];
				$ship_country = $f[8];

				$address1 = $f[9];
				$address2 = $f[10];
				$city = $f[11];
				$county = $f[12];
				$post_code = $f[13];
				$country = $f[14];
				$email = $f[15];

				$name = $f[16].' '.$f[17];

				$shipping_estimation = $f[18];

				$address = "$name<br>$address1<br>$address2<br>$city<br>$county&nbsp;$post_code<br>$country";

				if(($ship_address1 != "<br>") AND ($ship_address1 != "")){
					$ship_address = "$ship_address1<br>$ship_address2<br>$ship_city<br>$ship_county&nbsp;$ship_post_code<br>$ship_country";
				}else{
					$ship_address = $address;
				}




			$email_body .= '

			  </table>
			<br />
			<div class="total_third" style="width:100%;float:right;margin-top: 25px;padding:20px;padding-top:0;">
			  <table border="0" cellspacing="5" cellpadding="5" style="float:right;" width="100%">
				<tr>
				   <td width="350"><h2 style="color: #000;font-size: 14px;width: auto;">Shipping &amp; Delivery</h2></td>
				   <td width="150"><h2 style="color: #000;font-size: 14px;width: auto;">Delivery Address</h2></td>
				   <td width="150"><h2 style="color: #000;font-size: 14px;width: auto;">Billing Address</h2></td>
				   <td colspan="100"><h2 style="color: #000;font-size: 14px;width: auto;">TOTAL AMOUNT</h2></td>
				</tr>
				<tr>
					<td rowspan="4" style="font-size:14px;color: #666666;">'.$shipping_estimation.'</td>
					<td rowspan="4" style="font-size:14px;color: #666666;">'.$ship_address.'</td>
				  <td rowspan="4" style="font-size:14px;color: #666666;">'.$address.'</td>
				  <td><span class="nova_reg" style="font-size:14px;color: #666666;">Sub-Total </span></td>
				  <td><span class="nova_reg" style="font-size:14px;color: #666666;">'.$currency_sign.' <span id="veletotal">'.number_format($sub,2).'</span></span></td>
				</tr>
				<tr>
				  <td><span class="nova_reg" style="font-size:14px;color: #666666;">Delivery</span></td>
				  <td>
					<span class="nova_reg" style="font-size:14px;color: #666666;">'.$currency_sign.' <span id="delivery_price">'.number_format($delivery_price,2).'</span></span>
				  </td>
				</tr>
				<tr>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				</tr>
				<tr>
				  <td><span class="nova_reg_black"  style="font-size:14px;color: #000000;">TOTAL</span></td>
				  <td><span class="nova_reg_black"  style="font-size:14px;color: #000000;">'.$currency_sign.' <span id="master_total">'.number_format($total,2).'</span></span></td>
				</tr>
			</table>
			</div>
			<br />

			<br />



				</div>
				<div class="clearer" style="clear:both;"></div>

			  <!-- end .content -->  </div>
			  <div class="footer" style="position: relative;clear: both;height: 206px;padding-top: 0px;padding-right: 0;padding-bottom: 10px;padding-left: 0;background-color: #f3f3f3;">
				<div class="footer_top" style="background-color: #9c9c9c;margin: 0px;height: 31px;padding-top: 9px;padding-left: 20px;"></div>

				<div id="footer_main" style="height: 125px;padding-top: 20px;padding-bottom: 30px;">
				  <div class="foot_column" style="width: 200px;padding-right: 20px;padding-left: 20px;float: left;"> <span class="foot_heading" style="font-size:18px;margin-bottom: 10px;display: block;">Here to Help</span>
					<a href="https://www.shanore.com/terms.php" style="font-size:14px;text-decoration: none;color: #666666;">Terms & Conditions</a> </div>
				  </div>
				</div>


			  <!-- end .footer --></div>

			</div>

			</body>
			</html>';


             //   EMAIL BODY END ****************
	if($code){

		require '../vendor/autoload.php';
		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->setFrom('info@shanore.com', 'Shanore');

		$mail->addAddress($email, '');
		// $mail->addAddress('shanore@matrix-test.com', 'Shanore');
		$mail->addBCC('tomas@matrixinternet.ie', 'Shanore');
		$mail->addBCC('info@shanore.com', 'Shanore');
		$mail->addBCC('leigh@shanore.com', 'Shanore');
		$mail->addBCC('matrixshanore@gmail.com', 'Shanore');
		// $mail->addBCC('shane@shanore.com', 'Shanore');
		// $mail->addBCC('stuart@shanore.com', 'Shanore');

		$mail->Username = 'AKIAJ5YK2BSGGGL5TVRA';
		$mail->Password = 'Arsu0NnTyiZOEEJnXR7YAW2veGKQ4rv6dGvOKLnXVHkr';
		$mail->Host = 'email-smtp.eu-west-1.amazonaws.com';
		$mail->Subject = 'Order '.$master_order_id.': Shipping Confirmation';
		$mail->Body = $email_body;

		$mail->SMTPAuth = true;
		$mail->SMTPSecure = 'tls';
		$mail->Port = 587;
		$mail->isHTML(true);
		$mail->send();

		/*
			$url = 'https://api.sendgrid.com/';
			$user = 'matrixinternet';
			$pass = 'Sumrt89$';

			$json_string = array(
			  'to' => array(
				'tomas@matrixinternet.ie', 'flaircraft@shanore.com', $email
			  ),
			  'category' => 'Order Confirmations'
			);

			$params = array(
				'api_user'  => $user,
				'api_key'   => $pass,
				'to' 		=> $email,
				'x-smtpapi' => json_encode($json_string),
				'subject'   => 'Order '.$master_order_id.': Shipping Confirmation',
				'html'      => $email_body,
				'from'      => 'info@shanore.com',
			  );

			$request =  $url.'api/mail.send.json';
			$session = curl_init($request);
			curl_setopt ($session, CURLOPT_POST, true);
			curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
			curl_setopt($session, CURLOPT_HEADER, false);
			curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($session);
			curl_close($session);*/
	}


	$q = "UPDATE orders_master SET status=2, tracking_code='".$code."' WHERE id=".intval($_POST['mid']);
	$r = mysqli_query($db,$q) or die(mysqli_error($db));

	$q = "UPDATE orders SET status=2 WHERE order_master_id=".intval($_POST['mid']);
	$r = mysqli_query($db,$q) or die(mysqli_error($db));

	echo('ok');

?>
