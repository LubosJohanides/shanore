<?
	require('db.php');
	session_start();
	
	//logged?
	if($_GET['action']=='logout'){
		$q = "UPDATE admin SET session_id='' WHERE session_id='".session_id()."'";
		$r = mysqli_query($db,$q);
	}
	
	$q = "select login from admin where session_id='".session_id()."'";
	$r = mysqli_query($db,$q);
	$c = mysqli_num_rows($r);
	if($c){
		$logged = 1;
		$f = mysqli_fetch_row($r);
		$admin_name = $f[0];
	}else{
		$logged = 0;
	}
	
	

?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-language" content="en" />
        <meta name="robots" content="index,follow" />
        <meta name="googlebot" content="index,follow,snippet,archive" />
        <meta name="author" content="ELIVE CZ s.r.o. (c) 2010" />
		<?
			if(!$logged) {
				die('<meta http-equiv="refresh" content="0;url=index.php" />');
				
			}
		?>


        <link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/smoothness/jquery-ui-1.7.2.custom.css" />

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
        <script type="text/javascript" src="js/jquery.ui.datepicker-cs.js"></script>
        <script type="text/javascript" src="js/jquery.simpleTooltip.js"></script>
        <script type="text/javascript" src="js/jquery.flot.pack.js"></script>
        <!--[if IE]><script language="javascript" type="text/javascript" src="js/excanvas.pack.js"></script><![endif]-->
        <script src="js/jquery.wysiwyg.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/jquery.wysiwyg.controls.js" type="text/javascript" charset="utf-8"></script>
        
        

<script type="text/javascript" src="js/submenu.js"></script>
        <script type="text/javascript" src="js/xadmin.js"></script>
        <script type="text/javascript" src="js/jquery.simplemodal.js"></script>
    <script type="text/javascript" src="js/admin.js"></script>
        

    <title>ShanOre.com ADMIN</title>
    <link href="css/jquery.wysiwyg.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
    body,td,th {
	font-family: Arial, sans-serif;
}
    </style>
    </head>
<body>
        <div id="header">
            <h1>ShanOre.com</h1>
            <div id="user"><a href="?action=logout">Logout</a>

                <br />
            </div>
            <div id="mainMenuWrapper">
              <ul id="mainMenu">
                <li class="active"><a href="admin.php">Catalog</a></li>
                <li><a href="orders.php">ORDERS</a></li>
                <li><a href="customers.php">Customers</a><a href="editor.html"></a></li>
                <li><a href="affiliates.php">Affiliates</a><a href="editor.html"></a></li>
              </ul>
            </div>
        </div>
        <div id="contentWrapper">
            <div id="leftMenu">
                <strong>Menu</strong>
                <div id="menu">
                  <ul>
                    <li><a href="admin.php" class="item">Products</a></li>
                    <li> <a href="#" class="item">Product Options</a>
                      <ul>
                        <li><a href="options.php?what=categories">Categories</a></li>
                        <li><a href="options.php?what=brands">Brands</a></li>
                        <?
									$q = "SELECT name, id FROM product_options_types";
									$r = mysqli_query($db,$q);
									$c = mysqli_num_rows($r);
									for($i=0; $i<$c; $i++){
										$f = mysqli_fetch_row($r);
										echo('<li><a href="options.php?what='.$f[1].'&multi=yes">'.$f[0].'</a></li>');
									}
								?>
                      </ul>
                    </li>
                    <li> <a href="#" class="item" id="collections_menu">Collections</a>
                      <ul>
                      	<li><a href="categories-seo.php">CATEGORIES SEO</a></li><hr />
                        <li><a href="collections-assign.php">COLLECTIONS ADMIN</a></li><hr />
                        <?
									$q = "SELECT name, id FROM collections";
									$r = mysqli_query($db,$q);
									$c = mysqli_num_rows($r);
									for($i=0; $i<$c; $i++){
										$f = mysqli_fetch_row($r);
										echo('<li><a href="collections.php?what='.$f[1].'">'.$f[0].'</a></li>');
									}
								?>
                      </ul>
                    </li>
                    <li><a href="brands.php" class="item">Brands</a></li>
                    <li class="active"><a href="deleted_products.php" class="item">Deleted Products</a></li>
                    <li><a href="pages.php" class="item">Pages</a></li>
                    <li><a href="currencies.php" class="item">Currencies</a></li>
                    <li><a href="images.php" class="item">Images</a></li>
                    <li><a href="sort.php" class="item">Sort Products</a></li>
                    <li> <a href="#" class="item" id="pdf_menu">PDF catalogues</a>
                      <ul>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=1" target="_new">Engagement Rings</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=10" target="_new">Wedding Rings</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=96" target="_new">Claddagh Rings</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=94" target="_new">Celtic Rings</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=16" target="_new">Pendants</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=37" target="_new">Celtic Crosses</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=95" target="_new">Bracelets</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=84" target="_new">Earrings</a></li>

                                
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=85" target="_new">Tara Beads</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=91" target="_new">Tara Charms</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=83" target="_new">Tara Bracelets</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=92" target="_new">Tara Necklaces</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=93" target="_new">Tara Stacking Rings</a></li>

                            </ul>
                    </li>
                  </ul>
                </div>
              <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            <strong> </strong></div>
            <div id="content">
              <div id="inner"><img src="img/icon_refresh.png" alt="refresh" width="16" height="16" align="top" /> - <strong>UNDELETE</strong><strong></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="img/datagridEdit.gif" alt="edit" width="15" height="15" align="top" /> - <strong>EDIT</strong><br />
                <br />
                <? echo($message); ?>
           	<div class="datagrid">
                	  <h2>Products table</h2>
                	  <form method="post" action="">
                	    <table>
                	      <tr>
                	        <th>Product</th>
                	        <th>Our Code</th>
                	        <th>Category</th>
                	        <th>Price</th>
                	        <th>Actions</th>
              	        </tr>
                        <?
							
							$q = "SELECT id FROM products WHERE deleted=1";
							$r = mysqli_query($db,$q);
							$all_count = mysqli_num_rows($r);
							$all_pages = ceil($all_count / 20);
							
							$page = 1;
							if($_GET['page']){
								$offset = " OFFSET ".strval((intval($_GET['page'])-1) * 20);
								$page = $_GET['page'];
							}
							
							$q = "SELECT id, name, sku, category, price FROM products WHERE deleted=1 ORDER BY id DESC LIMIT 20".$offset;
							$r = mysqli_query($db,$q);
							$c = mysqli_num_rows($r);
							
							$sudalicha = 'odd';
							
							for($i=0; $i<$c; $i++){
								$f = mysqli_fetch_row($r);
								if($sudalicha == 'odd'){
									$sudalicha = 'even';
								}else{
									$sudalicha = 'odd';
								}
								
								$q_cat = "SELECT name FROM categories WHERE id=".$f[3];
								$r_cat = mysqli_query($db,$q_cat);
								$f_cat = mysqli_fetch_row($r_cat);
								
								if(file_exists('../products_images/'.$f[0].'.jpg')){
									$image_str = '<br><img src="../products_images/'.$f[0].'.jpg?x='.strval(rand(1000000,9999999)).'" width="100" />';
								}else{
									$image_str = '';
								}
								
								echo('<tr class="'.$sudalicha.'" id="tr_'.$f[0].'">
										<td>'.$f[1]./*$image_str.*/'</td>
										<td>'.$f[2].'</td>
										<td>'.$f_cat[0].'</a></td>
										<td>'.$f[4].'</td>
										<td class="actions"><a href="javascript:refresh_product('.$f[0].');" class="button refresh"><span>Refresh</span></a></td>
									</tr>');
							}
							
						?>
                	      
                	     
              	      </table>
                	    <div class="datagridActions">
                	      <div class="paginator"> 
                          
   <?						  	
							if($page == 1){
                                echo('<a href="#" class="active">1</a> <a href="?page=2">2</a> <a href="?page=3">3</a> ... <a href="#">'.$all_pages.'</a><a href="?page='.strval(($page)+1).'">Next &raquo;</a>');
							}
							
							if(($page > 1) AND($page < $all_pages)){
                                echo('<a href="?page='.strval(intval($page)-1).'">&laquo; Previous</a><a href="?page=1">1</a> ... <a href="#" class="active">'.$page.'</a> ... <a href="?page='.$all_pages.'">'.$all_pages.'</a><a href="?page='.strval(($page)+1).'">Next &raquo;</a>');
							}
						  	
							if($page == $all_pages){
								echo('<a href="?page='.strval(intval($page)-1).'">&laquo; Previous</a><a href="?page=1">1</a> ... <a href="#" class="active">'.$all_pages.'</a>');
							}
							
						  ?>
                          
                          
                          
                          
                          	
                          
                          
                          
                          </div>
                          
              	      </div>
              	    </form>
          </div>
              </div>
            </div>
            <hr class="cleaner" />
            
      <div id="basic-modal-content"> 
      		<!-- edit form here -->
            
	  </div>
            
            
</div>

        <div id="footer">

        </div> 
    </body>
</html>
