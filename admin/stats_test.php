<?
/*
	require('../google_csv.php');
*/
	require('db.php');
	function find_rate($y,$m)
	{
		$sql="SELECT rate FROM calendar_rate WHERE MONTH(insert_datetime)='".$m."' AND YEAR(insert_datetime)='".$y."'";
		$resp  = mysql_query($sql) or die(mysql_error());
		$result = mysql_fetch_array($resp);
		if ($result[0] ==0 OR $result[0] == NULL){
			$conv_rate =0.736918; //1USD = 0.736918€
		} else {
			$conv_rate = $result[0];
		}
		return $conv_rate;
	}
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Shanore statistics</title>
    <link href="screen.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
</head>

<body style="padding:25px;">
<div class="content">
	<div class="onglet sel"><a href="#">Statistics</a></div>
	<div class="onglet"><a href="conversion_rate.php">Conversion rates</a></div> 
	<div class="onglet"><a href="visits.php">Visits</a></div> 
	<div class="tab_content" id="tab_content_0">
  	<section id="left_col">
	<?
		$month_2 = mktime(0,0,0,date('n'), date('j'), date('Y',strtotime("-1 year")));
		if(!$_POST['visits_1_from']) $_POST['visits_1_from'] = '01 '.date('M',strtotime("-1 month")).' '.date('Y');
		if(!$_POST['visits_1_to']) $_POST['visits_1_to'] = date('t').' '.date('M').' '.date('Y');
		if(!$_POST['visits_2_from']) $_POST['visits_2_from'] = '01 '.date('M', strtotime("-1 month")).' '.date('Y',strtotime("-1 year"));
		if(!$_POST['visits_2_to']) $_POST['visits_2_to'] =  date('t', $month_2).' '.date('M').' '.date('Y',strtotime("-1 year"));
		if(!$_POST['values_per']) $_POST['values_per'] = 'Daily';
		if(!$_POST['values_type']) $_POST['values_type'] = 'Transactions';
	?>
    <form action="stats_test.php?tab=0" method="post">
        <div class="select_area">
           	<h1 style="margin-top:0;">Range 1</h1>
            
                FROM<br />
                <input id="visits_1_from" name="visits_1_from" value="<?=$_POST['visits_1_from'] ?>" style="font-size:28px; width:100%;" /><br />
                TO<br />
                <input id="visits_1_to" name="visits_1_to" value="<?=$_POST['visits_1_to'] ?>" style="font-size:28px; width:100%;" />
        </div>
         <script>
			$(function() {
				$( "#visits_1_from" ).datepicker({
					defaultDate: "+1w",
					changeMonth: true,
					numberOfMonths: 3,
					dateFormat: "d M yy",
					onClose: function( selectedDate ) {
						$( "#visits_1_to" ).datepicker( "option", "minDate", selectedDate );
					}
				});
				
				$( "#visits_1_to" ).datepicker({
					defaultDate: "+1w",
					changeMonth: true,
					numberOfMonths: 3,
					dateFormat: "d M yy",
					onClose: function( selectedDate ) {
						$( "#visits_1_from" ).datepicker( "option", "maxDate", selectedDate );
					}
				});
			});
		</script>
                    
        <div class="select_area" style="margin-top:20px;">
           	<h1 style="margin-top:0;">Range 2</h1>
                FROM<br />
                <input id="visits_2_from" name="visits_2_from" value="<?=$_POST['visits_2_from'] ?>" style="font-size:28px; width:100%;" /><br />
                TO<br />
                <input id="visits_2_to" name="visits_2_to" value="<?=$_POST['visits_2_to'] ?>" style="font-size:28px; width:100%;" />
         </div>
         <script>
			$(function() {
				$( "#visits_2_from" ).datepicker({
					defaultDate: "+1w",
					changeMonth: true,
					numberOfMonths: 3,
					dateFormat: "d M yy",
					onClose: function( selectedDate ) {
						$( "#visits_2_to" ).datepicker( "option", "minDate", selectedDate );
					}
				});
				
				$( "#visits_2_to" ).datepicker({
					defaultDate: "+1w",
					changeMonth: true,
					numberOfMonths: 3,
					dateFormat: "d M yy",
					onClose: function( selectedDate ) {
						$( "#visits_2_from" ).datepicker( "option", "maxDate", selectedDate );
					}
				});
			});
		</script>
         
         <div class="select_area" style="margin-top:20px;">
         	<h1 style="margin-top:0;">Period</h1>
			<select id="values_per" name="values_per" style="font-size:28px; width:100%;">
				<option value="Monthly" <? if($_POST['values_per'] == 'Monthly') echo('selected'); ?>>monthly values</option>
            	<option value="Daily" <? if($_POST['values_per'] == 'Daily') echo('selected'); ?>>daily values</option>
            </select>
         </div>
         <div class="select_area" style="margin-top:20px;">
            <h1 style="margin-top:0;">Type</h1>
			<select id="values_type" name="values_type" style="font-size:28px; width:100%;">
				<option value="Visits" <? if($_POST['values_type'] == 'Visits') echo('selected'); ?>>visits</option>
                <option value="Transactions" <? if($_POST['values_type'] == 'Transactions') echo('selected'); ?>>transactions</option>
				<option value="Transaction_val" <? if($_POST['values_type'] == 'Transaction_val') echo('selected'); ?>>transaction values</option>
                <option value="Conversion" <? if($_POST['values_type'] == 'Conversion') echo('selected'); ?>>conversion rate</option>
				<option value="Products_sold" <? if($_POST['values_type'] == 'Products_sold') echo('selected'); ?>>products sold</option>
				<option value="Abandoned" <? if($_POST['values_type'] == 'Abandoned') echo('selected'); ?>>abandoned carts</option>
				<option value="Av_transactions" <? if($_POST['values_type'] == 'Av_transactions') echo('selected'); ?>>avarage transaction value</option>
				<option value="Av_products" <? if($_POST['values_type'] == 'Av_products') echo('selected'); ?>>avarage product value</option>
				<option value="ten_pop_prod" <? if($_POST['values_type'] == 'ten_pop_prod') echo('selected'); ?>>10 popular products</option>
				<option value="geo_data" <? if($_POST['values_type'] == 'geo_data') echo('selected'); ?>>geo data</option>
            </select>
         </div>
         
         <input type="submit" value="Submit" />
         
         </form>
    </section>
    
		<?
	//10 most popular products sold
	if($_POST['values_type'] == 'ten_pop_prod'){
		echo "<section id ='graph_right'>";
		if($_POST['values_per'] == 'Monthly' || $_POST['values_per'] == 'Daily')
		{
			echo "<section id='range1'>";
			echo "<h3>Range1</h3>";
			function ten_pop_prod($state) {
				// get number of Months between dates
				$date1_stamp = strtotime($_POST['visits_1_from']);
				$date2_stamp = strtotime($_POST['visits_1_to']);
				
				$year1 = date('Y', $date1_stamp);
				$year2 = date('Y', $date2_stamp);
				
				$month1 = date('m', $date1_stamp);
				$month2 = date('m', $date2_stamp);
				
				$months = (($year2 - $year1) * 12) + ($month2 - $month1);
			
				$s = $state;
				$a=array();
				for($i=0; $i<=$months; $i++)
				{
						$disp_date = date("M",strtotime("+".$i." month", $date1_stamp));
						$month = date("n",strtotime("+".$i." month", $date1_stamp));
						$year = date("Y",strtotime("+".$i." month", $date1_stamp));
						
						if ($state == "United _Kindom" || $state == "UK" || $state == "New Zealand" || $state == "Australia" 
							|| $state == "United States" || $state == "US")
						{
							switch ($state){
								case "United Kindom" : $t = "UK";
								break;
								case "United States" : $t = "US";
								break;
								case "New Zealand" : $t = "Australia";
								break;
							}
							$state = "$s' OR orders_master.country = '$t";
						}
						
						
						// TRANSACTIONS
						//select all products sold in this period in this state
						$q = "SELECT orders.product_id FROM orders,orders_master 
								WHERE MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."'
								AND orders.order_master_id= orders_master.id 
								AND orders_master.status=2
								AND (orders_master.country = '$state');";
						$r = mysql_query($q) or die(mysql_error());
						while ($row = mysql_fetch_array($r))
						{
							$a[] = $row[0];
						}
				}
			
				//calcul the frequence of values in array
				$b = array_count_values($a);
				//order and keep the 10 first
				arsort($b);
				$b = array_slice($b, 0, 10, true);
				if ($b !=0 && $b != NULL) 
				{
					?>
					<table id="ten_pop_prod">
						
						<? echo "<h3>$s</h3>"; ?>
						<tbody>
						<tr><td>Product Name</td><td>Number of products sold</td></tr>
						<?
						foreach ($b as $key=>$value)
						{
							$sql = "SELECT name FROM products WHERE id = $key";
							$resp = mysql_query($sql) or die(mysql_error());
							$res = mysql_fetch_array($resp);
							echo "<tr><td>" .$res[0]."($key)</td> <td> $value </td></tr>";
						}
						?>
						</tbody>
					</table>
					<?
				}
			}
			
			ten_pop_prod("United States");
			ten_pop_prod("United Kingdom");
			ten_pop_prod("Mainland Europe");
			ten_pop_prod("Ireland");
			ten_pop_prod("Canada");
			ten_pop_prod("New Zealand");
			ten_pop_prod("South America");
			ten_pop_prod("Africa");
			ten_pop_prod("Asia");
			ten_pop_prod("Middle East");
			echo "</section>";
		
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//GRAPH2
		////////////////////////////////////////////////////////////////////////
		
		
			echo "<section id='range2'>";
			echo "<h3>Range2</h3>";
			function ten_pop_prodb($state) 
			{
				// get number of Months between dates
				$date1_stamp = strtotime($_POST['visits_2_from']);
				$date2_stamp = strtotime($_POST['visits_2_to']);
				
				$year1 = date('Y', $date1_stamp);
				$year2 = date('Y', $date2_stamp);
				
				$month1 = date('m', $date1_stamp);
				$month2 = date('m', $date2_stamp);
				
				$months = (($year2 - $year1) * 12) + ($month2 - $month1);
			
				$s = $state;
				$a=array();
				for($i=0; $i<=$months; $i++)
				{
						$disp_date = date("M",strtotime("+".$i." month", $date1_stamp));
						$month = date("n",strtotime("+".$i." month", $date1_stamp));
						$year = date("Y",strtotime("+".$i." month", $date1_stamp));
						
						if ($state == "United _Kindom" || $state == "UK" || $state == "New Zealand" || $state == "Australia" 
							|| $state == "United States" || $state == "US")
						{
							switch ($state){
								case "United Kindom" : $t = "UK";
								break;
								case "United States" : $t = "US";
								break;
								case "New Zealand" : $t = "Australia";
								break;
							}
							$state = "$s' OR orders_master.country = '$t";
						}
						
						
						// TRANSACTIONS
						//select all products sold in this period in this state
						$q = "SELECT orders.product_id FROM orders,orders_master 
								WHERE MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."'
								AND orders.order_master_id= orders_master.id 
								AND orders_master.status=2
								AND (orders_master.country = '$state');";
						$r = mysql_query($q) or die(mysql_error());
						while ($row = mysql_fetch_array($r))
						{
							$a[] = $row[0];
						}
				}
			
				//calcul the frequence of values in array
				$b = array_count_values($a);
				//order and keep the 10 first
				arsort($b);
				$b = array_slice($b, 0, 10, true);
				if ($b !=0 && $b != NULL) 
				{
					?>
					<table id="ten_pop_prod" id="_ten_pop_prod_right">
						
						<? echo "<h3>$s</h3>"; ?>
						<tbody>
						<tr><td>Product Name</td><td>Number of products sold</td></tr>
						<?
						foreach ($b as $key=>$value)
						{
							$sql = "SELECT name FROM products WHERE id = $key";
							$resp = mysql_query($sql) or die(mysql_error());
							$res = mysql_fetch_array($resp);
							echo "<tr><td>" .$res[0]."($key)</td> <td> $value </td></tr>";
						}
						?>
						</tbody>
					</table>
					<?
					}
			}
			
			ten_pop_prodb("United States");
			ten_pop_prodb("United Kingdom");
			ten_pop_prodb("Mainland Europe");
			ten_pop_prodb("Ireland");
			ten_pop_prodb("Canada");
			ten_pop_prodb("New Zealand");
			ten_pop_prodb("South America");
			ten_pop_prodb("Africa");
			ten_pop_prodb("Asia");
			ten_pop_prodb("Middle East");
			echo "</section>";
		}
		echo "</section>";
		
	}
	// END 10 most popular products
	?>
	
	<!-- geo map -->
	<?
	if($_POST['values_type'] == 'geo_data'){
		?>
		<script type='text/javascript'>
		google.load('visualization', '1', {'packages': ['geochart']});
		google.setOnLoadCallback(drawRegionsMap);

		function drawRegionsMap() {
			var data = google.visualization.arrayToDataTable([
			  ['Country', 'Number of orders'],
			  <?
				if($_POST['values_per'] == 'Monthly' || $_POST['values_per'] == 'Daily'){ //because it's exactly the same
					// get number of Months between dates
					$date1_stamp = strtotime($_POST['visits_1_from']);
					$date2_stamp = strtotime($_POST['visits_1_to']);
					$year1 = date('Y', $date1_stamp);
					$year2 = date('Y', $date2_stamp);
					$month1 = date('m', $date1_stamp);
					$month2 = date('m', $date2_stamp);
					$months = (($year2 - $year1) * 12) + ($month2 - $month1);
				
					$arr = array();
					for($i=0; $i<=$months; $i++){
						$disp_date = date("M",strtotime("+".$i." month", $date1_stamp));
						$month = date("n",strtotime("+".$i." month", $date1_stamp));
						$year = date("Y",strtotime("+".$i." month", $date1_stamp));
						
						$q = "SELECT COUNT(orders.id),orders_master.country FROM orders,orders_master 
								WHERE MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."'
								AND orders.order_master_id= orders_master.id 
								AND orders_master.status=2
								GROUP BY orders_master.country;";
						$r = mysql_query($q) or die(mysql_error());
						while ($row = mysql_fetch_array($r))
						{
							//if the country is in the array : concat
							if( array_key_exists($row[1], $a)==true) {
								$a[$row[1]] += intval($row[0]);
								
							}else {
							//if not, add new country
								$a[$row[1]] = intval($row[0]);
							}
						}
					}
				}
				foreach ($a as $key=>$value)
				{
					$arr[] = "['".$key."', ".$value."]";
				}
				$output = implode(',',$arr);
				echo($output);
			  ?>
			]);

			var options = {};

			var chart = new google.visualization.GeoChart(document.getElementById('chart_diva'));
			chart.draw(data, options);
		};

		</script>
		<div id="chart_diva"></div>	
		
		<!-- SECOND MAP -->
		<script type='text/javascript'>
		google.load('visualization', '1', {'packages': ['geochart']});
		google.setOnLoadCallback(drawRegionsMap);

		function drawRegionsMap() {
			var data = google.visualization.arrayToDataTable([
			  ['Country', 'Number of orders'],
			  <?
				if($_POST['values_per'] == 'Monthly' || $_POST['values_per'] == 'Daily'){ //because it's exactly the same
					// get number of Months between dates
					$date1_stamp = strtotime($_POST['visits_2_from']);
					$date2_stamp = strtotime($_POST['visits_2_to']);
					$year1 = date('Y', $date1_stamp);
					$year2 = date('Y', $date2_stamp);
					$month1 = date('m', $date1_stamp);
					$month2 = date('m', $date2_stamp);
					$months = (($year2 - $year1) * 12) + ($month2 - $month1);
				
					$arr = array();
					for($i=0; $i<=$months; $i++){
						$disp_date = date("M",strtotime("+".$i." month", $date1_stamp));
						$month = date("n",strtotime("+".$i." month", $date1_stamp));
						$year = date("Y",strtotime("+".$i." month", $date1_stamp));
						
						$q = "SELECT COUNT(orders.id),orders_master.country FROM orders,orders_master 
								WHERE MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."'
								AND orders.order_master_id= orders_master.id 
								AND orders_master.status=2
								GROUP BY orders_master.country;";
						$r = mysql_query($q) or die(mysql_error());
						while ($row = mysql_fetch_array($r))
						{
							//if the country is in the array : concat
							if( array_key_exists($row[1], $a)==true) {
								$a[$row[1]] += intval($row[0]);
								
							}else {
							//if not, add new country
								$a[$row[1]] = intval($row[0]);
							}
						}
					}
				}
				foreach ($a as $key=>$value)
				{
					$arr[] = "['".$key."', ".$value."]";
				}
				$output = implode(',',$arr);
				echo($output);
			  ?>
			]);

			var options = {};

			var chart = new google.visualization.GeoChart(document.getElementById('chart_divb'));
			chart.draw(data, options);
		};

		</script>
		<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
		<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
		<div id="chart_divb"></div>	
	<? } ?>
	<!--END geo map -->

	

	<?
	if($_POST['values_type'] != 'geo_data' AND $_POST['values_type'] != 'ten_pop_prod'){
	?>
        <script type="text/javascript">
		  google.load("visualization", "1", {packages:["corechart"]});
		  google.setOnLoadCallback(drawChart);
		  function drawChart() {
			var data = google.visualization.arrayToDataTable([
			  ['<?=$_POST['values_type'] ?>', 'Range 1', 'Range 2'],
<?
				if($_POST['values_per'] == 'Daily'){
					// get number of days between dates
					$date1_stamp = strtotime($_POST['visits_1_from']);
					$date2_stamp = strtotime($_POST['visits_1_to']);
					$datediff = $date2_stamp - $date1_stamp;
					$days = floor($datediff/(60*60*24));
				
					$arr = array();
					
					for($i=0; $i<$days; $i++){
						$day_date = date("j M",$date1_stamp + ($i * (60*60*24)));
						$day = date("j",$date1_stamp + ($i * (60*60*24)));
						$month = date("n",$date1_stamp + ($i * (60*60*24)));
						$year = date("Y",$date1_stamp + ($i * (60*60*24)));
						
						if($_POST['values_type'] == 'Visits'){
							// VISITS
							$q = "SELECT id FROM visits WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."'";
							$r = mysql_query($q) or die(mysql_error());
							$c = mysql_num_rows($r);
						}
						
						if($_POST['values_type'] == 'Transactions'){
							// TRANSACTIONS
							$q = "SELECT id FROM orders_master WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status=2";
							$r = mysql_query($q) or die(mysql_error());
							$c = mysql_num_rows($r);
						}
						
						if($_POST['values_type'] == 'Conversion'){
							// VISITS
							$q = "SELECT id FROM visits WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."'";
							$r = mysql_query($q) or die(mysql_error());
							$cv = mysql_num_rows($r);
							// TRANSACTIONS
							$q = "SELECT id FROM orders_master WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status=2";
							$r = mysql_query($q) or die(mysql_error());
							$ct = mysql_num_rows($r);
							
							$c = $ct / ($cv / 100);
							$c = number_format($c,3);
							
						}
						
						//////Adrien
						if($_POST['values_type'] == 'Products_sold'){
							// TRANSACTIONS
							$q = "SELECT orders.id FROM orders, orders_master WHERE DAY(orders_master.insert_datetime)='".$day."' AND MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."'
														AND orders_master.id = orders.order_master_id  
														AND orders_master.status=2";
							$r = mysql_query($q) or die(mysql_error());
							$c = mysql_num_rows($r);
						
						}
						if($_POST['values_type'] == 'Abandoned'){
							// TRANSACTIONS
							$q = "SELECT id FROM orders_master WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status=0";
							$r = mysql_query($q) or die(mysql_error());
							$c = mysql_num_rows($r);
						
						}
						if($_POST['values_type'] == 'Av_transactions'){
							// TRANSACTIONS
							//all the transaction (€ and $)
							$u=array();
							$w=array();
							$x=array();
							$y=0;
							$o= "SELECT id FROM orders_master WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND (currency='euro' OR currency='dolar')";
							$p = mysql_query($o) or die(mysql_error());
							$divisor = mysql_num_rows($p);
							
							//thirst array in €
							$q = "SELECT total FROM orders_master WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status=2 AND currency='euro'";
							$r = mysql_query($q) or die(mysql_error());
							while ($row = mysql_fetch_array($r))
							{
								$u[]= $row[0];
							}
							
							//second array in dolar, with conversion foreach transactions
							$s = "SELECT total FROM orders_master WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status=2 AND currency='dolar'";
							$t = mysql_query($s) or die(mysql_error());
							while ($row = mysql_fetch_array($t))
							{
								//convert to euros
								$conv_rate = find_rate($year, $month);
								$w[]= $row[0] * $conv_rate;
							}
							//merging the 2 arrays
							$x = array_merge($u , $w);
							
							//avarage of all transactions in the month
							$y = array_sum($x);
							$y = $y/$divisor;
							
							//not a decimal number
							$c = number_format($y,0,'','');
							
						}
						if($_POST['values_type'] == 'Av_products'){
							// TRANSACTIONS
							//all the transaction (€ and $)
							$u=array();
							$w=array();
							$x=array();
							$y=0;
							$o = "SELECT orders.id FROM orders, orders_master 
												WHERE DAY(orders_master.insert_datetime)='".$day."' AND MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."'
												AND orders_master.id = orders.order_master_id  
												AND orders_master.status=2
												AND (orders_master.currency='euro' OR orders_master.currency='dolar')";
							$p = mysql_query($o) or die(mysql_error());
							$divisor = mysql_num_rows($p);
							
							//thirst array in €
							$q = "SELECT orders.price FROM orders,orders_master 
									WHERE orders.order_master_id= orders_master.id 
									AND DAY(orders_master.insert_datetime)='".$day."' AND MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."'
									AND orders_master.status=2 AND orders_master.currency='euro'";
							$r = mysql_query($q) or die(mysql_error());
							while ($row = mysql_fetch_array($r))
							{
								$u[]= $row[0];
							}
							
							//second array in dolar, with conversion foreach transactions
							$s = "SELECT orders.price FROM orders,orders_master 
									WHERE orders.order_master_id= orders_master.id 
									AND DAY(orders_master.insert_datetime)='".$day."' AND MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."'
									AND orders_master.status=2 AND orders_master.currency='dolar'";
							$t = mysql_query($s) or die(mysql_error());
							while ($row = mysql_fetch_array($t))
							{
								//convert to euros
								$conv_rate = find_rate($year, $month);
								$w[]= $row[0] * $conv_rate;
							}
							//merging the 2 arrays
							$x = array_merge($u , $w);
							
							//avarage of all transactions in the month
							$y = array_sum($x);
							$y = $y/$divisor;
							
							//not a decimal number
							$c = number_format($y,0,'','');
						}
						if($_POST['values_type'] == 'Transaction_val'){
							// TRANSACTIONS
							//all the transaction (€ and $)
							$u=array();
							$w=array();
							$x=array();
							$y=0;
							
							//thirst array in €
							$q = "SELECT total FROM orders_master WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status=2 AND currency='euro'";
							$r = mysql_query($q) or die(mysql_error());
							while ($row = mysql_fetch_array($r))
							{
								$u[]= $row[0];
							}
							
							//second array in dolar, with conversion foreach transactions
							$s = "SELECT total FROM orders_master WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status=2 AND currency='dolar'";
							$t = mysql_query($s) or die(mysql_error());
							while ($row = mysql_fetch_array($t))
							{
								//convert to euros
								$conv_rate = find_rate($year, $month);
								$w[]= $row[0] * $conv_rate;
							}
							//merging the 2 arrays
							$x = array_merge($u , $w);
							
							//avarage of all transactions in the month
							$y = array_sum($x);
							
							//not a decimal number
							$c = number_format($y,0,'','');
						}
						
						//////Adrien
						
						//$arr[] = "['".$day_date."', ".$c."]";
						$array_datea[] = $day_date;
						$array_range1 [] = $c;
					}
				} // daily
				
				
				if($_POST['values_per'] == 'Monthly'){
					// get number of Months between dates
					$date1_stamp = strtotime($_POST['visits_1_from']);
					$date2_stamp = strtotime($_POST['visits_1_to']);
					
					$year1 = date('Y', $date1_stamp);
					$year2 = date('Y', $date2_stamp);
					
					$month1 = date('m', $date1_stamp);
					$month2 = date('m', $date2_stamp);
					
					$months = (($year2 - $year1) * 12) + ($month2 - $month1);
				
					$arr = array();
					
					for($i=0; $i<=$months; $i++){
						$disp_date = date("M",strtotime("+".$i." month", $date1_stamp));
						$month = date("n",strtotime("+".$i." month", $date1_stamp));
						$year = date("Y",strtotime("+".$i." month", $date1_stamp));
						
						if($_POST['values_type'] == 'Visits'){
							// VISITS
							$q = "SELECT id FROM visits WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."'";
							$r = mysql_query($q) or die(mysql_error());
							$c = mysql_num_rows($r);
						}
						
						if($_POST['values_type'] == 'Transactions'){
							// TRANSACTIONS
							$q = "SELECT id FROM  orders_master WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status=2";
							$r = mysql_query($q) or die(mysql_error());
							$c = mysql_num_rows($r);
						}
						
						if($_POST['values_type'] == 'Conversion'){
							// VISITS
							$q = "SELECT id FROM visits WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."'";
							$r = mysql_query($q) or die(mysql_error());
							$cv = mysql_num_rows($r);
							// TRANSACTIONS
							$q = "SELECT id FROM orders_master WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status=2";
							$r = mysql_query($q) or die(mysql_error());
							$ct = mysql_num_rows($r);
							
							$c = $ct / ($cv / 100);
							$c = number_format($c,3);
							
						}
						
						//////Adrien
						if($_POST['values_type'] == 'Products_sold'){
							// TRANSACTIONS
							$q = "SELECT orders.id FROM orders, orders_master WHERE MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."' 
														AND orders_master.id = orders.order_master_id  
														AND orders_master.status=2";
							$r = mysql_query($q) or die(mysql_error());
							$c = mysql_num_rows($r);
							
						}
						if($_POST['values_type'] == 'Abandoned'){
							// TRANSACTIONS
							$q = "SELECT id FROM orders_master WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."'AND status=0";
							$r = mysql_query($q) or die(mysql_error());
							$c = mysql_num_rows($r);
						}
						if($_POST['values_type'] == 'Av_transactions'){
							// TRANSACTIONS
							//all the transaction (euro and $)
							$u=array();
							$w=array();
							$x=array();
							$y=0;
							$o= "SELECT id FROM orders_master WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status=2 AND (currency='euro' OR currency='dolar')";
							$p = mysql_query($o) or die(mysql_error());
							$divisor = mysql_num_rows($p);
							
							//thirst array in euro
							$q = "SELECT total FROM orders_master WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status=2 AND currency='euro'";
							$r = mysql_query($q) or die(mysql_error());
							while ($row = mysql_fetch_array($r))
							{
								$u[]= $row[0];
							}
							
							//second array in dolar, with conversion foreach transactions
							$s = "SELECT total FROM orders_master WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status=2 AND currency='dolar'";
							$t = mysql_query($s) or die(mysql_error());
							while ($row = mysql_fetch_array($t))
							{
								//convert to euros
								$conv_rate = find_rate($year, $month);
								$w[]= $row[0] * $conv_rate;
							}
							//merging the 2 arrays
							$x = array_merge($u , $w);
							
							//avarage of all transactions in the month
							$y = array_sum($x);
							$y = $y/$divisor; 
							
							//not a decimal number
							$c = number_format($y,0,'','');
							
						}
						if($_POST['values_type'] == 'Av_products'){
							// TRANSACTIONS
							//all the transaction (€ and $)
							$u=array();
							$w=array();
							$x=array();
							$y=0;
							$o = "SELECT orders.id FROM orders, orders_master WHERE MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."' 
														AND orders_master.id = orders.order_master_id  
														AND orders_master.status=2
														AND (orders_master.currency='euro' OR orders_master.currency='dolar')";
							$p = mysql_query($o) or die(mysql_error());
							$divisor = mysql_num_rows($p);
							
							//thirst array in €
							$q = "SELECT orders.price FROM orders,orders_master 
									WHERE MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."'  
									AND orders.order_master_id= orders_master.id  
									AND orders_master.status=2 AND orders_master.currency='euro'";
							$r = mysql_query($q) or die(mysql_error());
							while ($row = mysql_fetch_array($r))
							{
								$u[]= $row[0];
							}
							
							//second array in dolar, with conversion foreach transactions
							$s = "SELECT orders.price FROM orders,orders_master 
									WHERE MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."'  
									AND orders.order_master_id= orders_master.id 
									AND orders_master.status=2 AND orders_master.currency='dolar'";
							$t = mysql_query($s) or die(mysql_error());
							while ($row = mysql_fetch_array($t))
							{
								//convert to euros
								$conv_rate = find_rate($year, $month);
								$w[]= $row[0] * $conv_rate;
							}
							//merging the 2 arrays
							$x = array_merge($u , $w);
							
							//avarage of all transactions in the month
							$y = array_sum($x);
							$y = $y/$divisor;
							
							//not a decimal number
							$c = number_format($y,0,'','');
						}		
						if($_POST['values_type'] == 'Transaction_val'){
							// TRANSACTIONS
							//all the transaction (euro and $)
							$u=array();
							$w=array();
							$x=array();
							$y=0;
							//thirst array in euro
							$q = "SELECT total FROM orders_master WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status=2 AND currency='euro'";
							$r = mysql_query($q) or die(mysql_error());
							while ($row = mysql_fetch_array($r))
							{
								$u[]= $row[0];
							}
							
							//second array in dolar, with conversion foreach transactions
							$s = "SELECT total FROM orders_master WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status=2 AND currency='dolar'";
							$t = mysql_query($s) or die(mysql_error());
							while ($row = mysql_fetch_array($t))
							{
								//convert to euros
								$conv_rate = find_rate($year, $month);
								$w[]= $row[0] * $conv_rate;
							}
							//merging the 2 arrays
							$x = array_merge($u , $w);
							
							//avarage of all transactions in the month
							$y = array_sum($x);
							
							//not a decimal number
							$c = number_format($y,0,'','');
						}						
						//////Adrien
						
						//$arr[] = "['".$disp_date."', ".$c."]";
						$array_datea[] = $disp_date;
						$array_range1 [] = $c;
					}
				} // monthly
				
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				//////////////GRAPH2
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				if($_POST['values_per'] == 'Daily'){
					// get number of days betwen dates
					$date1_stamp = strtotime($_POST['visits_2_from']);
					$date2_stamp = strtotime($_POST['visits_2_to']);
					$datediff = $date2_stamp - $date1_stamp;
					$days = floor($datediff/(60*60*24));
				
					$arr = array();
					
					for($i=0; $i<$days; $i++){
						$day_date = date("j M",$date1_stamp + ($i * (60*60*24)));
						$day = date("j",$date1_stamp + ($i * (60*60*24)));
						$month = date("n",$date1_stamp + ($i * (60*60*24)));
						$year = date("Y",$date1_stamp + ($i * (60*60*24)));
						
						if($_POST['values_type'] == 'Visits'){
							// VISITS
							$q = "SELECT id FROM visits WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."'";
							$r = mysql_query($q) or die(mysql_error());
							$c = mysql_num_rows($r);
						}
						
						if($_POST['values_type'] == 'Transactions'){
							// VISITS
							$q = "SELECT id FROM orders_master WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status=2";
							$r = mysql_query($q) or die(mysql_error());
							$c = mysql_num_rows($r);
						}
						
						if($_POST['values_type'] == 'Conversion'){
							// VISITS
							$q = "SELECT id FROM visits WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."'";
							$r = mysql_query($q) or die(mysql_error());
							$cv = mysql_num_rows($r);
							// TRANSACTIONS
							$q = "SELECT id FROM orders_master WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status=2";
							$r = mysql_query($q) or die(mysql_error());
							$ct = mysql_num_rows($r);
							
							$c = $ct / ($cv / 100);
							$c = number_format($c,3);
							
						}
						
						//////Adrien
						if($_POST['values_type'] == 'Products_sold'){
							// TRANSACTIONS
							$q = "SELECT orders.id FROM orders, orders_master WHERE DAY(orders_master.insert_datetime)='".$day."' AND MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."'
														AND orders_master.id = orders.order_master_id  
														AND orders_master.status=2";
							$r = mysql_query($q) or die(mysql_error());
							$c = mysql_num_rows($r);
						
						}
						if($_POST['values_type'] == 'Abandoned'){
							// TRANSACTIONS
							$q = "SELECT id FROM orders_master WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."'";
							$r = mysql_query($q) or die(mysql_error());
							$c = mysql_num_rows($r);
						
						}
						if($_POST['values_type'] == 'Av_transactions'){
							// TRANSACTIONS
							//all the transaction (€ and $)
							$u=array();
							$w=array();
							$x=array();
							$y=0;
							$o= "SELECT id FROM orders_master WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."'
									AND status=2 AND (currency='euro' OR currency='dolar')";
							$p = mysql_query($o) or die(mysql_error());
							$divisor = mysql_num_rows($p);
							
							//thirst array in €
							$q = "SELECT total FROM orders_master WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status=2 AND currency='euro'";
							$r = mysql_query($q) or die(mysql_error());
							while ($row = mysql_fetch_array($r))
							{
								$u[]= $row[0];
							}
							
							//second array in dolar, with conversion foreach transactions
							$s = "SELECT total FROM orders_master WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status=2 AND currency='dolar'";
							$t = mysql_query($s) or die(mysql_error());
							while ($row = mysql_fetch_array($t))
							{
								//convert to euros
								$conv_rate = find_rate($year, $month);
								$w[]= $row[0] * $conv_rate;
							}
							//merging the 2 arrays
							$x = array_merge($u , $w);
							
							//avarage of all transactions in the month
							$y = array_sum($x);
							$y = $y/$divisor;
							
							//not a decimal number
							$c = number_format($y,0,'','');
						}
						if($_POST['values_type'] == 'Av_products'){
							// TRANSACTIONS
							//all the transaction (€ and $)
							$u=array();
							$w=array();
							$x=array();
							$y=0;
							$o = "SELECT orders.id FROM orders, orders_master 
												WHERE DAY(orders_master.insert_datetime)='".$day."' AND MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."'
												AND orders_master.id = orders.order_master_id  
												AND orders_master.status=2
												AND (orders_master.currency='euro' OR orders_master.currency='dolar')";
							$p = mysql_query($o) or die(mysql_error());
							$divisor = mysql_num_rows($p);
							
							//thirst array in €
							$q = "SELECT orders.price FROM orders,orders_master 
									WHERE orders.order_master_id= orders_master.id 
									AND DAY(orders_master.insert_datetime)='".$day."' AND MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."'
									AND orders_master.status=2 AND orders_master.currency='euro'";
							$r = mysql_query($q) or die(mysql_error());
							while ($row = mysql_fetch_array($r))
							{
								$u[]= $row[0];
							}
							
							//second array in dolar, with conversion foreach transactions
							$s = "SELECT orders.price FROM orders,orders_master 
									WHERE orders.order_master_id= orders_master.id 
									AND DAY(orders_master.insert_datetime)='".$day."' AND MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."'
									AND orders_master.status=2 AND orders_master.currency='dolar'";
							$t = mysql_query($s) or die(mysql_error());
							while ($row = mysql_fetch_array($t))
							{
								//convert to euros
								$conv_rate = find_rate($year, $month);
								$w[]= $row[0] * $conv_rate;
							}
							//merging the 2 arrays
							$x = array_merge($u , $w);
							
							//avarage of all transactions in the month
							$y = array_sum($x);
							$y = $y/$divisor;
							
							//not a decimal number
							$c = number_format($y,0,'','');
						}
						if($_POST['values_type'] == 'Transaction_val'){
							// TRANSACTIONS
							//all the transaction (€ and $)
							$u=array();
							$w=array();
							$x=array();
							$y=0;
							
							//thirst array in €
							$q = "SELECT total FROM orders_master WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status=2 AND currency='euro'";
							$r = mysql_query($q) or die(mysql_error());
							while ($row = mysql_fetch_array($r))
							{
								$u[]= $row[0];
							}
							
							//second array in dolar, with conversion foreach transactions
							$s = "SELECT total FROM orders_master WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status=2 AND currency='dolar'";
							$t = mysql_query($s) or die(mysql_error());
							while ($row = mysql_fetch_array($t))
							{
								//convert to euros
								$conv_rate = find_rate($year, $month);
								$w[]= $row[0] * $conv_rate;
							}
							//merging the 2 arrays
							$x = array_merge($u , $w);
							
							//avarage of all transactions in the month
							$y = array_sum($x);
							
							//not a decimal number
							$c = number_format($y,0,'','');
						}
						//////Adrien
						
						//$arr[] = "['".$day_date."', ".$c."]";
						$array_dateb[] = $day_date;
						$array_range2[] = $c;
					}
				} // daily
				
				
				if($_POST['values_per'] == 'Monthly'){
					// get number of Months betwen dates
					$date1_stamp = strtotime($_POST['visits_2_from']);
					$date2_stamp = strtotime($_POST['visits_2_to']);
					
					$year1 = date('Y', $date1_stamp);
					$year2 = date('Y', $date2_stamp);
					
					$month1 = date('m', $date1_stamp);
					$month2 = date('m', $date2_stamp);
					
					$months = (($year2 - $year1) * 12) + ($month2 - $month1);
				
					$arr = array();
					
					for($i=0; $i<=$months; $i++){
						$disp_date = date("M",strtotime("+".$i." month", $date1_stamp));
						$month = date("n",strtotime("+".$i." month", $date1_stamp));
						$year = date("Y",strtotime("+".$i." month", $date1_stamp));
						
						if($_POST['values_type'] == 'Visits'){
							// VISITS
							$q = "SELECT id FROM visits WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."'";
							
							//echo($q."/n/r");
							
							$r = mysql_query($q) or die(mysql_error());
							$c = mysql_num_rows($r);
						}
						
						if($_POST['values_type'] == 'Transactions'){
							// VISITS
							$q = "SELECT id FROM orders_master WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status=2";
							
							//echo($q."/n/r");
							
							$r = mysql_query($q) or die(mysql_error());
							$c = mysql_num_rows($r);
						}
						
						if($_POST['values_type'] == 'Conversion'){
							// VISITS
							$q = "SELECT id FROM visits WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."'";
							$r = mysql_query($q) or die(mysql_error());
							$cv = mysql_num_rows($r);
							// TRANSACTIONS
							$q = "SELECT id FROM orders_master WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status=2";
							$r = mysql_query($q) or die(mysql_error());
							$ct = mysql_num_rows($r);
							
							$c = $ct / ($cv / 100);
							$c = number_format($c,3);
							
						}
						
						//////Adrien
						if($_POST['values_type'] == 'Products_sold'){
							// TRANSACTIONS
							$q = "SELECT orders.id FROM orders, orders_master WHERE MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."' 
														AND orders_master.id = orders.order_master_id  
														AND orders_master.status=2";
							$r = mysql_query($q) or die(mysql_error());
							$c = mysql_num_rows($r);
							
						}
						if($_POST['values_type'] == 'Abandoned'){
							// TRANSACTIONS
							$q = "SELECT id FROM orders_master WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."'AND status=0";
							$r = mysql_query($q) or die(mysql_error());
							$c = mysql_num_rows($r);
						}
						if($_POST['values_type'] == 'Av_transactions'){
							// TRANSACTIONS
							//all the transaction (€ and $)
							$u=array();
							$w=array();
							$x=array();
							$y=0;
							$o= "SELECT id FROM orders_master WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status=2 AND (currency='euro' OR currency='dolar')";
							$p = mysql_query($o) or die(mysql_error());
							$divisor = mysql_num_rows($p);
							
							//thirst array in €
							$q = "SELECT total FROM orders_master WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status=2 AND currency='euro'";
							$r = mysql_query($q) or die(mysql_error());
							while ($row = mysql_fetch_array($r))
							{
								$u[]= $row[0];
							}
							
							//second array in dolar, with conversion foreach transactions
							$s = "SELECT total FROM orders_master WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status=2 AND currency='dolar'";
							$t = mysql_query($s) or die(mysql_error());
							while ($row = mysql_fetch_array($t))
							{
								//convert to euros
								$conv_rate = find_rate($year, $month);
								$w[]= $row[0] * $conv_rate;
							}
							//merging the 2 arrays
							$x = array_merge($u , $w);
							
							//avarage of all transactions in the month
							$y = array_sum($x);
							$y = $y/$divisor;
							
							//not a decimal number
							$c = number_format($y,0,'','');
							
						}
						if($_POST['values_type'] == 'Av_products'){
							// TRANSACTIONS
							//all the transaction (€ and $)
							$u=array();
							$w=array();
							$x=array();
							$y=0;
							$o = "SELECT orders.id FROM orders, orders_master WHERE MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."' 
														AND orders_master.id = orders.order_master_id  
														AND orders_master.status=2
														AND (orders_master.currency='euro' OR orders_master.currency='dolar')";
							$p = mysql_query($o) or die(mysql_error());
							$divisor = mysql_num_rows($p);
							
							//thirst array in €
							$q = "SELECT orders.price FROM orders,orders_master 
									WHERE MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."'  
									AND orders.order_master_id= orders_master.id  
									AND	orders_master.status=2 AND orders_master.currency='euro'";
							$r = mysql_query($q) or die(mysql_error());
							while ($row = mysql_fetch_array($r))
							{
								$u[]= $row[0];
							}
							
							//second array in dolar, with conversion foreach transactions
							$s = "SELECT orders.price FROM orders,orders_master 
									WHERE MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."'  
									AND orders.order_master_id= orders_master.id 
									AND orders_master.status=2 AND orders_master.currency='dolar'";
							$t = mysql_query($s) or die(mysql_error());
							while ($row = mysql_fetch_array($t))
							{
								//convert to euros
								$conv_rate = find_rate($year, $month);
								$w[]= $row[0] * $conv_rate;
							}
							//merging the 2 arrays
							$x = array_merge($u , $w);
							
							//avarage of all transactions in the month
							$y = array_sum($x);
							$y = $y/$divisor;
							
							//not a decimal number
							$c = number_format($y,0,'','');
						}
						if($_POST['values_type'] == 'Transaction_val'){
							// TRANSACTIONS
							//all the transaction (euro and $)
							$u=array();
							$w=array();
							$x=array();
							$y=0;
							//thirst array in euro
							$q = "SELECT total FROM orders_master WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status=2 AND currency='euro'";
							$r = mysql_query($q) or die(mysql_error());
							while ($row = mysql_fetch_array($r))
							{
								$u[]= $row[0];
							}
							
							//second array in dolar, with conversion foreach transactions
							$s = "SELECT total FROM orders_master WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status=2 AND currency='dolar'";
							$t = mysql_query($s) or die(mysql_error());
							while ($row = mysql_fetch_array($t))
							{
								//convert to euros
								$conv_rate = find_rate($year, $month);
								$w[]= $row[0] * $conv_rate;
							}
							//merging the 2 arrays
							$x = array_merge($u , $w);
							
							//avarage of all transactions in the month
							$y = array_sum($x);
							
							//not a decimal number
							$c = number_format($y,0,'','');
						}												
						//////Adrien
						
						
						//$arr[] = "['".$disp_date."', ".$c."]";
						$array_dateb[] = $disp_date;
						$array_range2[] = $c;
					}
				} // monthly
				
				//two lines in a graph
				//if periods have different sizes
				
				if ( count($array_datea) != count($array_dateb)){
					foreach ($array_dateb as $key=>$a_d)
					{
						$arr[] = "['".$a_d."', 0, ".$array_range2[$key]."]";
					}
					foreach ($array_datea as $key=>$a_d)
					{
						$arr[] = "['".$a_d."', ".$array_range1[$key].", 0]";
					}
				} else {
				//if periods have the same size
					$array_date = array_merge($array_datea, $array_dateb);
					$array_date = array_unique($array_date);
					foreach ($array_date as $key =>$a_d)
					{
						if ($array_range1[$key] == "" OR $array_range1[$key] == NULL) { $array_range1[$key] =0;}
						if ($array_range2[$key] == "" OR $array_range2[$key] == NULL) { $array_range2[$key] =0;}
						
						$arr[] = "['".$a_d."', ".$array_range1[$key].", ".$array_range2[$key]."]";
					}
				}
				$output = implode(',',$arr);
				echo($output);
?>
			]);
	
			var options = {
				title: '<? echo $_POST['values_type']; ?>'
			};
	
			var chart = new google.visualization.LineChart(document.getElementById('month_conv'));
			chart.draw(data, options);
			/*chart.getChartLayoutInterface().getVAxisValue(300);*/
		  }
		</script>

        <div id="month_conv" style="height:250px; width:80%; margin-left:20%;"></div>
    <?
	}
	?>
        
       
    </div>
 
</div>
	</div>
    
    
</body>
</html>