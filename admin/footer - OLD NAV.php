<?
/*	define('WP_USE_THEMES', false);
	require('./blog/wp-load.php');
	query_posts('showposts=1');*/

/* includes blogs */
define('WP_USE_THEMES', false);
require('./blog/wp-blog-header.php');

?>
<script>
//shows/hide CM form created. (not in use anymore)
function ShowHideForm(divID, imgID){
	if (document.getElementById(divID).style.display == "none") {
		document.getElementById(divID).style.display = "block";
	} else {
		document.getElementById(divID).style.display = "none";
	}//if
}

function ClearTextBox() {
	//clears content of sign up texbox
	document.getElementById('jyyuihr-jyyuihr').value="";
}
</script>
<div class="footer">

	<div class="footer_main">

		<div class="footer_column"><div class="footer_columnbox">
			<div class="footer_ballon">Secure Payment</div>
			<div class="foot_column">
				<img src="images/payments.jpg" width="191" height="221" alt="Secure Payment" />
			</div>		
		</div></div>

		<div class="footer_column"><div class="footer_columnbox">
			<div class="footer_ballon">Stay in Touch</div>
			<div class="foot_column">
				<div id="showform" style="display:block;">
				<form action="http://matrixinternet.createsend.com/t/r/s/jyyuihr/" method="post" id="subForm">
					<!--onclick="ShowHideForm('showform',this.id);"-->
					<img src="images/mail.png" alt="footer mail" align="bottom" id="img" style="vertical-align:top;"/><span class="foot_text" style="vertical-align:top;margin-left:2px;">Join Shanore Ezine</span>
					
					<div style="margin-top:-24px;margin-left:77px;margin-right:-30px;"><!--keep textbox close to icon-->
						<div style="display:inline !important">
						<!--Previos grey clear colour #CCC-->
						<label for="jyyuihr-jyyuihr"></label><input type="text" name="cm-jyyuihr-jyyuihr" id="jyyuihr-jyyuihr"  style="color:#999; max-width:120px; margin-right:1px;" value="your email here ..."  onfocus="ClearTextBox();"/><input type="submit" class="btnGo" value="Go" />
						</div>
					</div>
				</form>
				</div>
			
				<div align="center">
					<a href="https://www.facebook.com/shanorecelticjewelry" target="_new"><img src="images/fb.png"  style="border-radius: 4px;" alt="footer facebook"/></a>
					<a href="#" target="_new"><img src="images/go.png" alt="footer google" style="border-radius: 4px;"/></a>
					<a href="https://twitter.com/Shanore_Jewelry" target="_new"><img src="images/tw.png" alt="footer twitter" style="border-radius: 4px;"/></a>
					<a href="https://pinterest.com/shanorejewelry/pins/" target="_new"><img src="images/pi.png" alt="footer pinterest" style="border-radius: 4px;"/></a>
				</div>
			</div>		
		</div></div>

		<div class="footer_column"><div class="footer_columnbox">
			<div class="footer_ballon">Reach Out</div> 
			<div class="foot_column_end">
				<img src="images/map-footer.jpg"  alt="Location" align="right" />
				<span class="foot_heading">ADDRESS</span> 
				<span class="foot_text">ShanOre &amp; Tara's Diary<br />
				36B Talbot Street<br />
				Dublin 1<br />
				IRELAND<br />
				<br />
				Tel: 011 353 (1) 8555436<br />
				<a href="mailto:info@shanore.com"><br />
				info@shanore.com</a> </span>
			</div>
		</div></div>

	</div>
	
	<div class="footer_main">
		<div class="footer_column"><div class="footer_columnbox">
			<div class="footer_ballon">Useful Info</div>
			<div class="foot_column">     
				<ul>
				<li><a href="sitemap.php"><b>Other Pages</b></a>
					<ul>
					<li><a href="terms.php">Terms &amp; Conditions</a></li>
					<li><a href="returns.php">Returns Policy</a></li>
					<li><a href="privacy.php">Privacy Policy</a></li>
					<li><a href="shipping-and-delivery.php">Shipping & Delivery</a></li>

					<li><a href="diamond-certificate.php">Diamond Certificate</a></li>
					<li><a href="faq.php">FAQ</a></li>
					<li><a href="ring-size-converter.php">Ring Size Converter</a></li>
					<li><a href="http://www.shanore.com/blog/">Shanore BLOG</a></li>
					</ul>
				</li>
				</ul>
				
			</div>
		</div></div>
        
        
	<div class="footer_main">
		<div class="footer_column"><div class="footer_columnbox">
			<div class="footer_ballon"><a href="http://www.shanore.com/blog/" style="color:#3e7f49;">Blog</a></div>
			<div class="foot_column">     
				<ul><li>
                	<a href="/blog/"><strong>Latest Posts</strong></a>
				<ul>
				<?
				query_posts('showposts=4'); 
				while (have_posts()) : the_post();
				?>
			
				
					<li><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>" style="text-decoration:none;"><?php the_title(); ?></a></li>
				
				 <?
				 endwhile;
				 ?>
                 </ul>
                 </li></ul>
				
			</div>
		</div></div>


		<div class="footer_column"><div class="footer_columnbox">
			<div class="footer_ballon">Popular Searches</div>


			<div class="foot_column_end" style="margin-left:5px; width:110px; float:left;" >
				<ul>
					<li><a href="taras-diary.htm">Tara's Diary</a>
						<ul>
						<li><a href="beads.htm">Beads</a></li>
						<li><a href="pendants-tara.htm">Pendants</a></li>
						<!-- <li><a href="necklaces.htm">Bracelets / Necklaces</a></li>-->
						<li><a href="charms.htm">Charms</a></li>
						<li><a href="rings.htm">Stacking Rings</a></li>
						<li><a href="earrings-tara.htm">Earrings</a></li>
						</ul>
					</li>
				</ul>
			 
			</div>
            
            <div class="foot_column_end" style="margin-left:5px; width:130px; float:left;">    
				<ul>
					<li><a href="shanore.htm">Shanore</a>
					  <ul>
						<li><a href="engagement-rings.htm">Engagement Rings</a></li>
						<li><a href="eternity-rings.htm">Eternity Rings</a></li>
						<li><a href="wedding-rings.htm">Wedding Rings</a></li>
						<li><a href="claddagh-rings.htm">Claddagh Rings</a></li>
						<li><a href="retro-designs.htm">Retro Designs</a></li>
						<li><a href="crosses.htm">Celtic Spirit</a></li>
						<li><a href="diamond-designs.htm">Celtic Brilliance</a></li>
						<li><a href="celtic-pearl-designs.htm">Celtic Pearl</a></li>
					  </ul>
					</li>
					</ul>
			</div>
            
		</div></div>
	</div>
    

</div>
    
<? //if (class_exists('MailPress')) MailPress::form(); ?>
    
    <div id="absolute_foot">
    	<a href="http://www.matrixinternet.ie/">Designed &amp; Developed by Matrix Internet </a><a href="http://www.shanore.com/detail.php?id=a553" style="display:block; position:absolute; bottom:0; left:70px;"></a>
    </div>
    
  <!-- end .footer --></div>
  
  
  
 <!-- Google Code for Remarketing tag -->
<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 998357048;
var google_conversion_label = "DzMpCLD0rwQQuPCG3AM";
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/998357048/?value=0&amp;label=DzMpCLD0rwQQuPCG3AM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


<div id="float" style="position:absolute; left:0; top:97px; z-index:99999; width:244px; height:727px; ">
	<img src="images/menutop.png" width="149" height="33" alt="Our Products" style="margin-top:7px;margin-left:40px; z-index:99999; position:relative;" />
    <ul class="shanoremenu">
          <li>Bridal and Engagement
        		<ul>
            	<li class="mainli"><a href="http://www.shanore.com/engagement-rings.htm">All Engagement Rings</a></li>
            	<li><a href="http://www.shanore.com/celtic-engagement-rings.htm">Celtic Engagement Rings</a></li>
                <li><a href="http://www.shanore.com/claddagh-engagement-rings.htm">Claddagh Engagement Rings</a></li>
                <li><a href="http://www.shanore.com/mount-only-rings.htm">Mount only Rings</a></li>
                <li><a href="http://www.shanore.com/solitaire.htm">Solitaire</a></li>
                <li><a href="http://www.shanore.com/diamond-round-cut.htm">Diamond Round Cut</a></li>
                <li><a href="http://www.shanore.com/diamond-princess-cut.htm">Diamond Princess Cut</a></li>
                <li><a href="http://www.shanore.com/diamond-3-stone.htm">Diamond 3 Stone</a></li>
                <li><a href="http://www.shanore.com/colored-stone.htm">Colored Stone</a></li>
                <li><a href="http://www.shanore.com/eternity-rings.htm">Eternity Rings</a></li>
                <li class="mainli"><a href="http://www.shanore.com/wedding-rings.htm">All Wedding Rings</a></li>
                <li><a href="http://www.shanore.com/celtic-wedding-rings.htm">Celtic Wedding Rings</a></li>
                <li><a href="http://www.shanore.com/claddagh-wedding-rings.htm">Claddagh Wedding Rings</a></li>
                <li><a href="http://www.shanore.com/ladies-wedding-rings.htm">Ladies</a></li>
                <li><a href="http://www.shanore.com/gents-wedding-rings.htm">Gents</a></li>
                <li class="mainli"><a href="http://www.shanore.com/favors-and-gifts.htm">Favors &amp; Gifts</a></li>
                <li><a href="http://www.shanore.com/pendants.htm">Pendants</a></li>
                <li><a href="http://www.shanore.com/earrings.htm">Earrings</a></li>
                <li class="mainli"><a href="#">Collections</a></li>
                <li><a href="http://www.shanore.com/aishlin.htm">Aishlin</a></li>
                <li><a href="http://www.shanore.com/coleen.htm">Coleen</a></li>
                <li><a href="http://www.shanore.com/darina.htm">Darina</a></li>
                <li><a href="http://www.shanore.com/errin.htm">Errin</a></li>
            </ul>
    </li>
        <li>Claddagh Rings
            <ul>
              <li class="mainli"><a href="http://www.shanore.com/claddagh-rings.htm">All Claddagh Rings</a></li>
              <li><a href="http://www.shanore.com/silver-claddagh-birthstone.htm">Silver Claddagh Birthstone</a></li>
              <li><a href="http://www.shanore.com/silver-comfort-claddagh.htm">Silver Comfort Claddagh</a></li>
              <li><a href="http://www.shanore.com/silver-stone-set-claddagh.htm">Silver Stone Set Claddagh</a></li>
              <li><a href="http://www.shanore.com/silver-ladies-claddagh.htm">Silver Ladies Claddagh</a></li>
              <li><a href="http://www.shanore.com/silver-gents-claddagh.htm">Silver Gents Claddagh</a></li>
              <li><a href="http://www.shanore.com/gold-claddagh-rings.htm">Gold Claddagh Rings</a></li>
           	</ul>
        </li>
        <li>Shanore Retro
        	<ul>
                <li class="mainli"><a href="http://www.shanore.com/retro-designs.htm">All Designs</a></li>
                <li><a href="http://www.shanore.com/claddagh-pendants-retro.htm">Claddagh Pendants</a></li>
                <li><a href="http://www.shanore.com/celtic-pendants-retro.htm">Celtic Pendants</a></li>
                <li><a href="http://www.shanore.com/shamrock-pendants-retro.htm">Shamrock Pendants</a></li>
                <li><a href="http://www.shanore.com/claddagh-earrings-retro.htm">Claddagh Earrings</a></li>
                <li><a href="http://www.shanore.com/celtic-earrings-retro.htm">Celtic Earrings</a></li>
                <li><a href="http://www.shanore.com/shamrock-earrings-retro.htm">Shamrock Earrings</a></li>
              	<li><a href="http://www.shanore.com/claddagh-bracelet.htm">Claddagh Bracelet</a></li>
           	  	<li><a href="http://www.shanore.com/celtic-bracelet.htm">Celtic Bracelet</a></li>
           	</ul>
        </li>
        <li>Celtic Spirit
        	<ul>
              <li class="mainli"><a href="http://www.shanore.com/crosses.htm">All Crosses</a></li>
              <li><a href="http://www.shanore.com/celtic-crosses.htm">Celtic Crosses</a></li>
              <li><a href="http://www.shanore.com/claddagh-crosses.htm">Claddagh Crosses</a></li>
              <li><a href="http://www.shanore.com/st-brigids-crosses.htm">St Brigid's Crosses</a></li>
              <li><a href="http://www.shanore.com/plain-crosses.htm">Plain Crosses</a></li>
              <li><a href="http://www.shanore.com/stone-set-crosses.htm">Stone Set Crosses</a></li>
            </ul>
        </li>
        <li>Celtic Brilliance
        	<ul>
              <li class="mainli"><a href="http://www.shanore.com/diamond-designs.htm">All Diamond Designs</a></li>
                <li><a href="http://www.shanore.com/claddagh-pendants-brilliance.htm">Claddagh Pendants</a></li>
                <li><a href="http://www.shanore.com/celtic-pendants-brilliance.htm">Celtic Pendants</a></li>
                <li><a href="http://www.shanore.com/claddagh-earrings-brilliance.htm">Claddagh Earrings</a></li>
                <li><a href="http://www.shanore.com/celtic-earrings-brilliance.htm">Celtic Earrings</a></li>
                <li><a href="http://www.shanore.com/shamrock-pendants-brilliance.htm">Shamrock Pendants</a></li>
                <li><a href="http://www.shanore.com/shamrock-earrings-brilliance.htm">Shamrock Earrings</a></li>
            </ul>
        </li>
        <li>Celtic Pearl
        	<ul>
                <li class="mainli"><a href="http://www.shanore.com/celtic-pearl-designs.htm">All Designs</a></li>
              <li><a href="http://www.shanore.com/celtic-pearl-pendant.htm">Celtic Pearl Pendants</a></li>
              <li><a href="http://www.shanore.com/celtic-pearl-earring.htm">Celtic Pearl Earrings</a></li>
              <li><a href="http://www.shanore.com/celtic-pearl-ring.htm">Celtic Pearl Rings</a></li>
            </ul>
        </li>
        <li>Occasions
        	<ul>
            	<li><a href="http://www.shanore.com/fathers-day.htm">Father's Day</a></li>
       		  <li><a href="http://www.shanore.com/st-patricks-day.htm">St Patrick's Day</a></li>
              <li><a href="http://www.shanore.com/mothers-day.htm">Mother's Day</a></li>
            </ul>
        </li>
        <li>Packaging
        	<ul>
       		  <li><a href="http://www.shanore.com/packaging-shanore.php">Award Winning Packaging</a></li>
            </ul>
        </li>
  </ul>
  
  
  <ul class="taramenu">
  		<li>Beads
        	<ul>
            	<li class="mainli"><a href="http://www.shanore.com/beads.htm">All Beads</a></li>
                <li><a href="http://www.shanore.com/celtic-beads.htm">Celtic Beads</a></li>
                <li><a href="http://www.shanore.com/claddagh-beads.htm">Claddagh Beads</a></li>
                <li><a href="http://www.shanore.com/diamond-beads.htm">Diamond Beads</a></li>
                <li><a href="http://www.shanore.com/birthstone.htm">Birthstone</a></li>
                <li><a href="http://www.shanore.com/stoppers.htm">Stoppers</a></li>
                <li><a href="http://www.shanore.com/silver-beads.htm">Silver Beads</a></li>
                <li><a href="http://www.shanore.com/dangles.htm">Dangles</a></li>
                <li><a href="http://www.shanore.com/enamel-and-crystal.htm">Enamel &amp; Crystal</a></li>
                <li><a href="http://www.shanore.com/murano-glass.htm">Murano Style Glass</a></li>
                <li><a href="http://www.shanore.com/initials.htm">Initials</a></li>
                <li><a href="http://www.shanore.com/iconic-beads.htm">Iconic beads</a></li>
            </ul>
        </li>
        <li>Pendants
        	<ul>
            	<li><a href="http://www.shanore.com/pendants-tara.htm">All Pendants</a></li>
            </ul>
        </li>
        <li>Bracelets / Necklaces
        	<ul>
            	<li class="mainli"><a href="http://www.shanore.com/necklaces.htm">All Bracelets and Necklaces</a></li>
                <li><a href="http://www.shanore.com/sterling-silver-bracelets.htm">Sterling Silver Bracelets</a></li>
                <li><a href="http://www.shanore.com/leather-bracelets.htm">Leather Bracelets</a></li>
                <li><a href="http://www.shanore.com/leather-necklaces.htm">Leather Necklaces</a></li>
            </ul>
        </li>
        <li>Charms
        	<ul>
            	<li><a href="http://www.shanore.com/charms.htm">All Charms</a></li>
            </ul>
        </li>
        <li>Stacking Rings
        	<ul>
            	<li class="mainli"><a href="http://www.shanore.com/rings.htm">All Rings</a></li>
                <li><a href="http://www.shanore.com/celtic-stacking-rings.htm">Celtic Stacking Rings</a></li>
                <li><a href="http://www.shanore.com/claddagh-stacking-rings.htm">Claddagh Stacking Rings</a></li>
                <li><a href="http://www.shanore.com/enamel-stacking-rings.htm">Enamel Stacking Rings</a></li>
                <li><a href="http://www.shanore.com/stone-set-stacking-rings.htm">Stone Set Stacking Rings</a></li>
                <li><a href="http://www.shanore.com/matching-sets-stacking-rings.htm">Matching Sets</a></li>
            </ul>
        </li>
        <li>Earrings
        	<ul>
            	<li><a href="http://www.shanore.com/earrings-tara.htm">All Earrings</a></li>
            </ul>
        </li>
        <li>Occasions
        	<ul>
            	<li><a href="http://www.shanore.com/brest-cancer-awareness.htm">Breast Cancer Awareness Month</a></li>
                <li><a href="http://www.shanore.com/st-patricks-day-tara.htm">St.Patrixk's Day</a></li>
            </ul>
        </li>
        <li>Packaging
        	<ul>
            	<li><a href="http://www.shanore.com/packaging-tara.php">Award Winning Packaging</a></li>
            </ul>
        </li>
  </ul>
  </br>
  
    <div class="fb-like" data-href="http://www.facebook.com/shanorecelticjewelry" data-send="false" data-width="220" data-show-faces="false" style=" z-index:99999;"></div>

  </br></br>
  <img src="images/floatbottom.png" alt="cards" width="214" height="185" border="0" usemap="#Map" style="margin-bottom:50px; margin-left:15px;" />
  <map name="Map" id="Map">
    <area shape="circle" coords="169,145,5" href="http://www.shanore.com/detail.php?id=a553" />
    <area shape="rect" coords="10,99,198,164" href="http://www.shanore.com/shipping-and-delivery.php" alt="Delivery" />
    <area shape="rect" coords="9,15,210,80" href="http://www.shanore.com/secure-payment.php" alt="About Us - Payment" />
  </map>
  
  
<!--<div style="width: 170px; font-family: 'Open Sans', sans-serif; font-size: 14px; border: 1px solid #DDD; border-radius: 5px; margin: 0 auto;">
		<div class="pt-gw-sidebar" style="font-weight: bold; position: relative;">
			<div class="pt-gw-title" style="color: #fff; overflow: hidden; clear: both; border-top-left-radius: 5px; border-top-right-radius: 5px;background: url(//s3.amazonaws.com/punchtab-static/nd/images/logo-18.png) no-repeat; background-position: 10px 0; background-color: #434343; padding: 5px 0; min-height: 22px; width: 100%;">
				<div class="inner" style="text-align: center; float: left; margin: 0 30px;">
					Mother's Day Competition - Win a Claddagh Birthstone Pendant of Your Choice!
				</div>
			</div>
		</div>
		<div class="pt-gw-content" style="clear: both; overflow: hidden; position: relative;">
			<div class="pt-gw-description" style="text-align:center;margin-top:10px;">
				Click below to enter our competition!
			</div>
		</div>
		<div class="pt-gw-enter_now" style="margin: 10px auto 5px; min-height: 28px; width: 100%; background: none; border: none; box-shadow: none; padding: 0;">
			<a href="http://ptab.it/LP4c" style="text-decoration: none; background: #2F9AB5; background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#2F9AB5), color-stop(50%,#2F9AB5), color-stop(51%,#267B91), color-stop(100%,#267B91)); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#2f9ab5', endColorstr='#267b91',GradientType=0 ); margin: auto !important; color: white !important; border-radius: 5px !important; border: none !important; font-family: cabin,sans-serif !important; display: block !important; font-weight: bold !important; padding: 5px 15px; padding: 5px 15px; width: 75px; font-size: 15px !important; cursor: pointer;">Enter Now</a>
		</div>
	</div>-->
  
  
<script type="text/javascript">
	$('.shanoremenu li').hover(
		function(){
			$(this).find('ul').css('display','block');
			
		},
		function(){
			$(this).find('ul').css('display','none');
		}
	)
	$('.taramenu li').hover(
		function(){
			$(this).find('ul').css('display','block');
			
		},
		function(){
			$(this).find('ul').css('display','none');
		}
	)
</script>

<?php
	//below switch defines if user is in a new landing page 
	$ContentSwitch=0;
	//Page title names defined as collection_display_name.
	//By the Title, the system will know in what page is, then, will display apprpiate background and ring picture
	$pages=array("Celtic Engagement Rings by Shanore","Celtic Engagement Rings","Claddagh Engagement Rings","Mount Only Rings","Solitaire","Diamond Round Cut","Diamond Princess Cut","Diamond 3 Stone","Colored Stone","Eternity Rings","Celtic Wedding Rings","Claddagh Wedding Rings","Ladies (wedding rings)","Gents (wedding rings)");
	$arrlength=count($pages);
	for($x=0;$x<$arrlength;$x++)
	  {
	  //if the Title is within the array then, the user is a new landing page. $ContentSwitch is set to 1
	  if ($collection_display_name==$pages[$x]){$ContentSwitch=1;}
	  }//for
?>
<!--Landing page code area-->
<?php if ($ContentSwitch==1){ ?>

<a href="http://www.shanore.com/taras-diary.htm"><img src="/images/TaraBtn.jpg" alt="Shanore" border="0"/></a>

<? } ?>
        <map name="Map2" id="Map2">
          <area shape="rect" coords="20,253,189,271" href="contact-shanore.php" />
          <area shape="rect" coords="3,2,211,171" href="shipping-and-delivery.php" alt="Christmas Shipping" />
  </map>
    
</div>