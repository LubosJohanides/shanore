<?

if($_GET['skip'] != 'yes'){
	$url = "print_order.php?mid=".$_GET['mid']."&skip=yes";

	error_reporting(E_ALL);
	set_time_limit(1800);

	include 'pdf/class.ezpdf.php';
	
	// define a clas extension to allow the use of a callback to get the table of contents, and to put the dots in the toc
	class Creport extends Cezpdf {
	
	var $reportContents = array();
	
	function Creport($p,$o){
	  $this->Cezpdf($p,$o);
	}
	
	function rf($info){
	  // this callback records all of the table of contents entries, it also places a destination marker there
	  // so that it can be linked too
	  $tmp = $info['p'];
	  $lvl = $tmp[0];
	  $lbl = rawurldecode(substr($tmp,1));
	  $num=$this->ezWhatPageNumber($this->ezGetCurrentPageNumber());
	  $this->reportContents[] = array($lbl,$num,$lvl );
	  $this->addDestination('toc'.(count($this->reportContents)-1),'FitH',$info['y']+$info['height']);
	}
	
	function dots($info){
	  // draw a dotted line over to the right and put on a page number
	  $tmp = $info['p'];
	  $lvl = $tmp[0];
	  $lbl = substr($tmp,1);
	  $xpos = 520;
	
	  switch($lvl){
		case '1':
		  $size=16;
		  $thick=1;
		  break;
		case '2':
		  $size=12;
		  $thick=0.5;
		  break;
	  }
	
	  $this->saveState();
	  $this->setLineStyle($thick,'round','',array(0,10));
	  $this->line($xpos,$info['y'],$info['x']+5,$info['y']);
	  $this->restoreState();
	  $this->addText($xpos+5,$info['y'],$size,$lbl);
	
	
	}
	
	
	}
	// I am in NZ, so will design my page for A4 paper.. but don't get me started on that.
	// (defaults to legal)
	// this code has been modified to use ezpdf.
	
	//$pdf = new Cezpdf('a4','portrait');
	$pdf = new Creport('a4','portrait');
	
	$pdf -> ezSetMargins(50,70,50,50);
	
	// put a line top and bottom on all the pages
	$all = $pdf->openObject();
	$pdf->saveState();
	$pdf->setStrokeColor(0,0,0,1);
	$pdf->line(20,40,578,40);
	$pdf->line(20,822,578,822);
	$pdf->addText(50,34,6,'http://ros.co.nz/pdf - http://www.sourceforge.net/projects/pdf-php');
	$pdf->restoreState();
	$pdf->closeObject();
	// note that object can be told to appear on just odd or even pages by changing 'all' to 'odd'
	// or 'even'.
	$pdf->addObject($all,'all');
	
	$pdf->ezSetDy(-100);
	
	//$mainFont = './fonts/Helvetica.afm';
	$mainFont = './fonts/Times-Roman.afm';
	$codeFont = './fonts/Courier.afm';
	// select a font
	$pdf->selectFont($mainFont);
	
	$pdf->ezText("PHP Pdf Creation\n",30,array('justification'=>'centre'));
	$pdf->ezText("Module-free creation of Pdf documents\nfrom within PHP\n",20,array('justification'=>'centre'));
	$pdf->ezText("developed by R&OS Ltd\n<c:alink:http://www.ros.co.nz/pdf/>http://www.ros.co.nz/pdf</c:alink>",18,array('justification'=>'centre'));
	$pdf->ezText("\n<c:alink:http://sourceforge.net/projects/pdf-php>http://sourceforge.net/projects/pdf-php</c:alink>\n\nversion 0.09",18,array('justification'=>'centre'));
	
	$pdf->ezSetDy(-100);
	// modified to use the local file if it can
	
	$pdf->openHere('Fit');
	
	function ros_logo(&$pdf,$x,$y,$height,$wl=0,$wr=0){
	  $pdf->saveState();
	  $h=100;
	  $factor = $height/$h;
	  $pdf->selectFont('./fonts/Helvetica-Bold.afm');
	  $text = 'R&OS';
	  $ts=100*$factor;
	  $th = $pdf->getFontHeight($ts);
	  $td = $pdf->getFontDecender($ts);
	  $tw = $pdf->getTextWidth($ts,$text);
	  $pdf->setColor(0.6,0,0);
	  $z = 0.86;
	  $pdf->filledRectangle($x-$wl,$y-$z*$h*$factor,$tw*1.2+$wr+$wl,$h*$factor*$z);
	  $pdf->setColor(1,1,1);
	  $pdf->addText($x,$y-$th-$td,$ts,$text);
	  $pdf->setColor(0.6,0,0);
	  $pdf->addText($x,$y-$th-$td,$ts*0.1,'http://www.rocks.ie');
	  $pdf->restoreState();
	  return $height;
	}
	
	ros_logo($pdf,150,$pdf->y-100,80,150,200);
	$pdf->selectFont($mainFont);
	
	
	if (file_exists('ros.jpg')){
	  $pdf->addJpegFromFile('logo.jpg',199,$pdf->y-100,200,0);
	} else {
	  // comment out these two lines if you do not have GD jpeg support
	  // I couldn't quickly see a way to test for this support from the code.
	  // you could also copy the file from the locatioin shown and put it in the directory, then 
	  // the code above which doesn't use GD will be activated.
	  $img = ImageCreatefromjpeg('logo.jpg');
	  $pdf-> addImage($img,199,$pdf->y-100,200,0);
	}
	
	//-----------------------------------------------------------
	// load up the document content
	$data=file_get_contents('http://www.rocks.ie/admin/pdf_print.php?mid='.$_GET['mid']);
	
	// try adding the faq's to the document, this will not work for people re-building the file from the 
	// download as I am not going to put in the faq file with that
	
	$pdf->ezNewPage();
	
	$pdf->ezStartPageNumbers(500,28,10,'','',1);
	
	$size=12;
	$height = $pdf->getFontHeight($size);
	$textOptions = array('justification'=>'full');
	$collecting=0;
	$code='';
	
	foreach ($data as $line){
	  // go through each line, showing it as required, if it is surrounded by '<>' then 
	  // assume that it is a title
	  $line=chop($line);
	  if (strlen($line) && $line[0]=='#'){
		// comment, or new page request
		switch($line){
		  case '#NP':
			$pdf->ezNewPage();
			break;
		  case '#C':
			$pdf->selectFont($codeFont);
			$textOptions = array('justification'=>'left','left'=>20,'right'=>20);
			$size=10;
			break;
		  case '#c':
			$pdf->selectFont($mainFont);
			$textOptions = array('justification'=>'full');
			$size=12;
			break;
		  case '#X':
			$collecting=1;
			break;
		  case '#x':
			$pdf->saveState();
			eval($code);
			$pdf->restoreState();
			$pdf->selectFont($mainFont);
			$code='';
			$collecting=0;
			break;
		}
	  } else if ($collecting){
		$code.=$line;
	//  } else if (((strlen($line)>1 && $line[1]=='<') || (strlen($line) && $line[0]=='<')) && $line[strlen($line)-1]=='>') {
	  } else if (((strlen($line)>1 && $line[1]=='<') ) && $line[strlen($line)-1]=='>') {
		// then this is a title
		switch($line[0]){
		  case '1':
			$tmp = substr($line,2,strlen($line)-3);
			$tmp2 = $tmp.'<C:rf:1'.rawurlencode($tmp).'>';
			$pdf->ezText($tmp2,26,array('justification'=>'centre'));
			break;
		  default:
			$tmp = substr($line,2,strlen($line)-3);
			// add a grey bar, highlighting the change
			$tmp2 = $tmp.'<C:rf:2'.rawurlencode($tmp).'>';
			$pdf->transaction('start');
			$ok=0;
			while (!$ok){
			  $thisPageNum = $pdf->ezPageCount;
			  $pdf->saveState();
			  $pdf->setColor(0.9,0.9,0.9);
			  $pdf->filledRectangle($pdf->ez['leftMargin'],$pdf->y-$pdf->getFontHeight(18)+$pdf->getFontDecender(18),$pdf->ez['pageWidth']-$pdf->ez['leftMargin']-$pdf->ez['rightMargin'],$pdf->getFontHeight(18));
			  $pdf->restoreState();
			  $pdf->ezText($tmp2,18,array('justification'=>'left'));
			  if ($pdf->ezPageCount==$thisPageNum){
				$pdf->transaction('commit');
				$ok=1;
			  } else {
				// then we have moved onto a new page, bad bad, as the background colour will be on the old one
				$pdf->transaction('rewind');
				$pdf->ezNewPage();
			  }
			}
			break;
		}
	  } else {
		// then this is just text
		// the ezpdf function will take care of all of the wrapping etc.
		$pdf->ezText($line,$size,$textOptions);
	  }
	  
	}
	
	$pdf->ezStopPageNumbers(1,1);
	
	// now add the table of contents, including internal links
	$pdf->ezInsertMode(1,1,'after');
	$pdf->ezNewPage();
	$pdf->ezText("Contents\n",26,array('justification'=>'centre'));
	$xpos = 520;
	$contents = $pdf->reportContents;
	foreach($contents as $k=>$v){
	  switch ($v[2]){
		case '1':
		  $y=$pdf->ezText('<c:ilink:toc'.$k.'>'.$v[0].'</c:ilink><C:dots:1'.$v[1].'>',16,array('aright'=>$xpos));
	//      $y=$pdf->ezText($v[0].'<C:dots:1'.$v[1].'>',16,array('aright'=>$xpos));
		  break;
		case '2':
		  $pdf->ezText('<c:ilink:toc'.$k.'>'.$v[0].'</c:ilink><C:dots:2'.$v[1].'>',12,array('left'=>50,'aright'=>$xpos));
		  break;
	  }
	}
	
	
	if (isset($d) && $d){
	  $pdfcode = $pdf->ezOutput(1);
	  $pdfcode = str_replace("\n","\n<br>",htmlspecialchars($pdfcode));
	  echo '<html><body>';
	  echo trim($pdfcode);
	  echo '</body></html>';
	} else {
	  $pdf->ezStream();
	}
	
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Order for print</title>
</head>
<?
	require('db.php');
	$master_order_id = $_GET['mid'];
	$q = "SELECT insert_datetime, status FROM orders_master WHERE id=".$master_order_id;
	$r = mysql_query($q) or die(mysql_error());
	$f = mysql_fetch_row($r);
	$insert_datetime = $f[0];
	$status = $f[1];
	
	
?>
<body style="font-family:Arial, Helvetica, sans-serif;">
	<div style="width:900px;margin-left:auto; margin-right:auto;">
    	<p><img src="img/print.png" width="59" height="58" alt="print" style="position:absolute; cursor:pointer;" onclick="window.print()" />
        
        
        
        <img src="../images/logo.jpg" width="199" height="63" alt="logo" style="display:block; margin-left:auto; margin-right:auto;" /></p>
<table width="300" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>Order number</td>
    <td><strong><? echo(substr('4000000000',0,10-strlen($master_order_id)).$master_order_id); ?></strong></td>
</tr>
  <tr>
    <td>Order date</td>
    <td><strong><? echo($insert_datetime); ?></strong></td>
  </tr>
  <tr>
    <td>Purchased from</td>
    <td>www.ShanOre.com</td>
  </tr>
  <tr>
    <td>Order status</td>
    <td>
<?
	switch($status){
		case '0':
			$status = '0 - not confirmed';
			break;
		case '1':
			$status = '1 - confirmed and paid';
			break;
		case '0':
			$status = '2 - dispatched / resolved';
			break;
		case '8':
			$status = '8 - CANCELED';
			break;
		case '9':
			$status = '9 - payment failed';
			break;
	}
	
	echo($status);
	
?>
    </td>
  </tr>
</table>
        
    <hr />
        <table width="100%" border="0" cellspacing="0" cellpadding="5" class="bag_table">
				<tr>
				  <th height="40" width="24">&nbsp;</th>
				  <th align="center">Item</th>
				  <th>&nbsp;</th>
				  <th align="left">Description</th>
				  <th>&nbsp;</th>
				  <th align="center">gift wrap</th>
				  <th>&nbsp;</th>
				  <th align="center">size</th>
				  <th>&nbsp;</th>
				  <th align="center">price</th>
				  <th>&nbsp;</th>
				  <th align="center">quantity</th>
				  <th>&nbsp;</th>
				  <th align="center">total</th>
				  <th>&nbsp;</th>
				</tr>
			
<?
				$q = "SELECT product_id, id, qty, price, size, wrapping FROM orders WHERE order_master_id='".$master_order_id."'";
				$r = mysql_query($q) or die(mysql_error());
				$c = mysql_num_rows($r);
				for($i=0; $i<$c; $i++){
					$f = mysql_fetch_row($r);
					
					$product_id = $f[0];
					$order_id = $f[1];
					$price = $f[3];
					$quantity = $f[2];
					$total = floatval($price) * intval($quantity);
					$size = $f[4];
					if(!$size) $size = "-";
					$wrapping = $f[5];
					$wrap_note = $f[6];
					
					
					if(file_exists('../products_images/'.$product_id.'.jpg')){
						$image_size = getimagesize ("../products_images/".$product_id.".jpg");
						if($image_size[0] > $image_size[1]){
							$size_restriction = 'width="50"';
						}else{
							$size_restriction = 'height="50"';
						}
						$img = '<img src="../products_images/'.$product_id.'.jpg" '.$size_restriction.' alt="product" />';
					}else{
						$img = '<img src="../images/noimage_big.jpg" height="90" alt="no image" />';
					}
					
					$q_product = "SELECT brand, name, sku2 FROM products WHERE id=".$product_id;
					$r_product = mysql_query($q_product);
					$f_product = mysql_fetch_row($r_product);
					
					$q_brand = "SELECT name FROM brands WHERE id=".$f_product[0];
					$r_brand = mysql_query($q_brand);
					$f_brand = mysql_fetch_row($r_brand);
					
					if($wrapping){
						$checked = ' checked="yes"';
					}else{
						$checked = '';
					}
					
					 echo'<tr id="row_'.$order_id.'_1">
							  <td>&nbsp;</td>
							  <td width="10">'.$img.'</td>
							  <td>&nbsp;</td>
							  <td>
								<span class="brand_name_bag" style="display:block;font-size: 16px;color: #000;">'.strtoupper($f_brand[0]).'</span>
								<span class="product_name_bag" style="display:block;font-size: 14px;clear: both;color: #666666;">'.$f_product[1].'<br/>(P/N - <u>'.$f_product[2].'</u>)</span>
							  </td>
							  <td>&nbsp;</td>
							  <td align="center" valign="middle"><input type="checkbox" id="gift_'.$order_id.'" '.$checked.' DISABLED/></td>
							  <td>&nbsp;</td>
							  <td align="center" valign="middle" class="size_bag" style="color: #666666;font-size: 16px;">'.$size.'</td>
							  <td>&nbsp;</td>
							  <td align="right" valign="middle" class="price_bag" style="font-size: 16px;">&euro;&nbsp;<span id="price_'.$order_id.'">'.number_format($price,2).'</span></td>
							  <td>&nbsp;</td>
							  <td align="center" valign="middle"><input class="bag_quantity" style="width:10px;" id="quantity_'.$order_id.'" value="'.$quantity.'" disabled="disabled" /></td>
							  <td>&nbsp;</td>
							  <td align="right" valign="middle" class="price_bag" style="font-size: 16px;">&euro;&nbsp;<span class="total_price" id="total_'.$order_id.'">'.number_format($total,2).'</span></td>
							  <td>&nbsp;</td>
						</tr>';
				}
			
				$q = "SELECT sub_total, delivery, total, ship_address1, ship_address2, ship_city, ship_county, ship_post_code, ship_country, address1, address2, city, county, post_code, country, email, wrapping_note, title, name, surname, delivery_type FROM orders_master WHERE id=".$master_order_id;
				$r = mysql_query($q) or die(mysql_error());
				$f = mysql_fetch_row($r);
				
				$delivery_price = $f[1];
				$sub = $f[0];
				$total = $f[2];
				
				// SHIPPING ADDRESS
				$ship_address1 = $f[3];
				$ship_address2 = $f[4];
				$ship_city = $f[5];
				$ship_county = $f[6];
				$ship_post_code = $f[7];
				$ship_country = $f[8];
				
				$address1 = $f[9];
				$address2 = $f[10];
				$city = $f[11];
				$county = $f[12];
				$post_code = $f[13];
				$country = $f[14];
				$email = $f[15];
				$wrap_note = $f[16];
				$title = $f[17];
				$name = $f[18];
				$surname = $f[19];
				$delivery_type = $f[20];
				
				if($ship_address1){
					$address = "$title $name $surname<br/>$ship_address1<br>$ship_address2<br>$ship_city<br>$ship_county&nbsp;$ship_post_code<br>$ship_country<br/><br/><strong>$delivery_type</strong>";
				}else{
					$address = "$title $name $surname<br/>$address1<br>$address2<br>$city<br>$county&nbsp;$post_code<br>$country<br><br/><strong>$delivery_type</strong>";
				}
				
				$billing_address = "$title $name $surname<br/>$address1<br>$address2<br>$city<br>$county&nbsp;$post_code<br>$country";
?>		
		</table>
			<hr />
			<div class="total_third" style="width:100%;float:right;margin-top: 10px;padding:20px;padding-top:0;">
			  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
				  <td width="7%"><h2 style="color: #000;font-size: 14px;width: auto;">&nbsp;</h2></td>
				  <td width="22%"><h2 style="color: #000;font-size: 14px;width: auto;">WRAPPING NOTE</h2></td>
				  <td width="25%"><h2 style="color: #000;font-size: 14px;width: auto;">BILLING ADDRESS</h2></td>
				  <td width="25%"><h2 style="color: #000;font-size: 14px;width: auto;">DELIVERY ADDRESS</h2></td>
				  <td colspan="2"><h2 style="color: #000;font-size: 14px;width: auto;">TOTAL AMOUNT</h2></td>
			    </tr>
				<tr>
				  <td rowspan="4" style="font-size:14px;color: #666666;">&nbsp;</td>
				  <td rowspan="4" valign="top" style="font-size:14px;color: #666666;"><? echo($wrap_note); ?></td>
				  <td rowspan="4" valign="top" style="font-size:14px;color: #666666;"><? echo($billing_address); ?></td>
				  <td rowspan="4" valign="top" style="font-size:14px;color: #666666;"><? echo($address); ?></td>
				  <td width="12%" height="20" valign="top"><span class="nova_reg" style="font-size:14px;color: #666666;">Sub-Total </span></td>
				  <td width="9%" align="right" valign="top"><span class="nova_reg" style="font-size:14px;color: #666666;">&euro;<span id="veletotal"><? echo(number_format($sub,2)); ?></span></span></td>
				</tr>
				<tr>
				  <td height="20" valign="top"><span class="nova_reg" style="font-size:14px;color: #666666;">Delivery</span></td>
				  <td align="right" valign="top">
					<span class="nova_reg" style="font-size:14px;color: #666666;">&euro;<span id="delivery_price"><? echo(number_format($delivery_price,2)); ?></span></span>
				  </td>
				</tr>
				<tr>
				  <td height="25" valign="top">&nbsp;</td>
				  <td align="right" valign="top">&nbsp;</td>
				</tr>
				<tr>
				  <td valign="top"><span class="nova_reg_black"  style="font-size:14px;color: #000000;"><strong>TOTAL</strong></span></td>
				  <td align="right" valign="top"><span class="nova_reg_black"  style="font-size:14px;color: #000000;"><strong>&euro;<span id="master_total"><? echo(number_format($total,2)); ?></span></strong></span></td>
				</tr>
			</table>
	  </div>
    </div>
</body>
</html>