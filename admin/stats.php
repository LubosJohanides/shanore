<?
	require('../db.php');
	function find_rate($y,$m)
	{
		$sql="SELECT rate FROM calendar_rate WHERE MONTH(insert_datetime)='".$m."' AND YEAR(insert_datetime)='".$y."'";
		$resp  = mysqli_query($db,$sql) or die(mysqli_error($db));
		$result = mysqli_fetch_array($resp);
		if ($result[0] ==0 || $result[0] == NULL){
			$conv_rate =0.736918; //1USD = 0.736918€  by default
		} else {
			$conv_rate = $result[0];
		}
		return $conv_rate;
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Shanore statistics</title>
    <link href="screen.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
</head>
<body style="padding:25px;">
	<? if($_POST['values_type'] != "geo_data" AND $_POST['values_type'] != "ten_pop_prod"){?>
	<style type="text/css" media="print">
		/*@page {size: landscape}*/
		@media{
			body { 
				background: white; 
				font-size : 16pt;
			}
			body, .tab_content, #month_conv, #month_conv2 {
				margin : 0;
				padding : 0;
				border : 0;
				/*float:none;*/
				clear: both;
				width:100%;
			}
			#left_col{
				display: none;	
			}
			.content {
				width: 120%; margin: 0; float: none;	
				background: transparent; 
			}
			.infoprint {
				display : block !important;	
				margin-left: -80% !important;
			}
			/*
			body, html
			{
				margin:0 !important;
				padding : 0 !important;
				-webkit-transform: rotate(-45deg); -moz-transform:rotate(-45deg);
				filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
				width: 1000px;
				height: 1145px;
				position: absolute;
				bottom: -135px;
				right : 0;
			}
			#left_col {
				display : none;
			}
			div.content div.tab_content, #tab_content_0{
				margin:0 !important;
				padding : 0 !important;
			}
			div.onglet {
				display : none;	
			}
			.tab_content {
			    border: none !important;
			}
			*/
		}
	</style>
    <? } ?>
<div class="content">
	<div class="onglet sel"><a href="#">Statistics</a></div>
	<div class="onglet"><a href="conversion_rate.php">Fx rates</a></div> 
	<div class="onglet"><a href="visits.php">Visits</a></div> 
	<div class="tab_content" id="tab_content_0">
  	<div id="left_col">
	<?
		$month_2 = mktime(0,0,0,date('n'), date('j'), date('Y',strtotime("-1 year")));
		if(!$_POST['visits_1_from']) $_POST['visits_1_from'] = '01 '.date('M',strtotime("-1 month")).' '.date('Y');
		if(!$_POST['visits_1_to']) $_POST['visits_1_to'] = date('t').' '.date('M').' '.date('Y');
		if(!$_POST['visits_2_from']) $_POST['visits_2_from'] = '01 '.date('M', strtotime("-1 month")).' '.date('Y',strtotime("-1 year"));
		if(!$_POST['visits_2_to']) $_POST['visits_2_to'] =  date('t', $month_2).' '.date('M').' '.date('Y',strtotime("-1 year"));
		if(!$_POST['values_per']) $_POST['values_per'] = 'Monthly';
		if(!$_POST['values_type']) $_POST['values_type'] = 'Transactions';
	?>
    <form action="stats.php?tab=0" method="post" name="myform">
    	<div class="select_area" >
            <h1 style="margin-top:0;">Type</h1>
			<select id="values_type" name="values_type" style="font-size:24px; width:100%;"  onchange="greyForm()">
				<option value="Visits" <? if($_POST['values_type'] == 'Visits') echo('selected'); ?>>Visits</option>
                <option value="Transactions" <? if($_POST['values_type'] == 'Transactions') echo('selected'); ?>>No. of Transactions</option>
				<option value="Transaction_val" <? if($_POST['values_type'] == 'Transaction_val') echo('selected'); ?>>Sales Total €</option>
                <option value="Conversion" <? if($_POST['values_type'] == 'Conversion') echo('selected'); ?>>Conversion Rate</option>
				<option value="Products_sold" <? if($_POST['values_type'] == 'Products_sold') echo('selected'); ?>>No. Products Sold</option>
				<option value="Abandoned" <? if($_POST['values_type'] == 'Abandoned') echo('selected'); ?>>Abandoned Carts</option>
				<option value="Avg_transactions" <? if($_POST['values_type'] == 'Avg_transactions') echo('selected'); ?>>Avg. Trans. Value €</option>
				<option value="Avg_products" <? if($_POST['values_type'] == 'Avg_products') echo('selected'); ?>>Avg. Product Value €</option>
				<option value="ten_pop_prod" <? if($_POST['values_type'] == 'ten_pop_prod') echo('selected'); ?>>10 Popular Products</option>
				<option value="geo_data" <? if($_POST['values_type'] == 'geo_data') echo('selected'); ?>>Geographic Datas</option>
            </select>
        </div>
        <script type="text/javascript">
			//on page load
			window.onload = greyForm;
			 
			//and on change
			function greyForm(){
				var Index = document.getElementById("values_type").options[document.getElementById("values_type").selectedIndex].value;	
				var r1 = document.getElementById('select_areb');
				var f1 = document.getElementById('visits_2_from');
				var t1 = document.getElementById('visits_2_to');			
				//alert(Index);
				if(Index == "geo_data" || Index == "ten_pop_prod") {
					//r1.style.display = 'none';
					r1.style.backgroundColor = '#EEEEEE';
					r1.style.color = '#AAAAAA';					
					t1.disabled=true;
					f1.disabled=true;
					
				} else {
					//initialize
					//r1.style.display = 'block';
					r1.style.backgroundColor = '#FFFFCC';
					r1.style.color = '#000';	
					t1.disabled=false;
					f1.disabled=false;				
				}
			}
					
			function automatic_range() {
				if (document.getElementById('same_range').checked == true) {
					var froma = document.getElementById('visits_1_from').value;
					var toa = document.getElementById('visits_1_to').value;
					var fromb = document.getElementById('visits_2_from').value;
					var tob = document.getElementById('visits_2_to').value;
					document.getElementById('visits_2_from_hidden').value = fromb;
					document.getElementById('visits_2_to_hidden').value = tob;
					
					froma = new Date(froma);
					froma.setFullYear(froma.getFullYear(),froma.getMonth()-12); 
					froma = $.datepicker.formatDate('dd M yy',froma);
					toa = new Date(toa);
					toa.setFullYear(toa.getFullYear(),toa.getMonth()-12); 
					toa = $.datepicker.formatDate('dd M yy',toa);
					document.getElementById('visits_2_from').value = froma;
					document.getElementById('visits_2_to').value = toa;
					
				} if (document.getElementById('same_range').checked == false) {
					var fhb = document.getElementById('visits_2_from_hidden').value;
					var thb = document.getElementById('visits_2_to_hidden').value;
					document.getElementById('visits_2_from').value = fhb;
					document.getElementById('visits_2_to').value = thb;
				}
			}	
		</script>
        
        <div class="select_area range1">
           	<h1 style="margin-top:0;">Range 1</h1>
                FROM<br />
                <input id="visits_1_from" name="visits_1_from" value="<?=$_POST['visits_1_from'] ?>" style="font-size:24px; width:100%;" /><br />
                TO<br />
                <input id="visits_1_to" name="visits_1_to" value="<?=$_POST['visits_1_to'] ?>" style="font-size:24px; width:100%;" />
                <label for="same_range">Same 12 months ago :</label>
                <?
				if (isset($_POST['same_range']) AND $_POST['same_range'] == "ok"){
				?>	
				<input type="checkbox" name="same_range" id="same_range" value="ok" onclick="automatic_range()" checked="checked"/>	
                <? } else { ?>
                <input type="checkbox" name="same_range" id="same_range" value="ok" onclick="automatic_range()"/>
				<? }?>	
        </div>
         <script type="text/javascript">
			$(function() {
				$( "#visits_1_from" ).datepicker({
					defaultDate: "+1w",
					changeMonth: true,
					numberOfMonths: 3,
					dateFormat: "d M yy",
					onClose: function( selectedDate ) {
						$( "#visits_1_to" ).datepicker( "option", "minDate", selectedDate );
					}
				});
				
				$( "#visits_1_to" ).datepicker({
					defaultDate: "+1w",
					changeMonth: true,
					numberOfMonths: 3,
					dateFormat: "d M yy",
					onClose: function( selectedDate ) {
						$( "#visits_1_from" ).datepicker( "option", "maxDate", selectedDate );
					}
				});
			});
			
		</script>
                    
        <div id="select_areb" class="range2">
           	<h1 style="margin-top:0;">Range 2</h1>
                FROM<br />
                <input id="visits_2_from" name="visits_2_from" value="<?=$_POST['visits_2_from'] ?>" style="font-size:24px; width:100%;" /><br />
                <input id="visits_2_from_hidden" name="visits_2_from_hidden" value="<?=$_POST['visits_2_from'] ?>" type="hidden" />
                TO<br />
                <input id="visits_2_to" name="visits_2_to" value="<?=$_POST['visits_2_to'] ?>" style="font-size:24px; width:100%;" />
                <input id="visits_2_to_hidden" name="visits_2_to_hidden" value="<?=$_POST['visits_2_from'] ?>" type="hidden" />
         </div>
         <script type="text/javascript">
			$(function() {
				$( "#visits_2_from" ).datepicker({
					defaultDate: "+1w",
					changeMonth: true,
					numberOfMonths: 3,
					dateFormat: "d M yy",
					onClose: function( selectedDate ) {
						$( "#visits_2_to" ).datepicker( "option", "minDate", selectedDate );
					}
				});
				
				$( "#visits_2_to" ).datepicker({
					defaultDate: "+1w",
					changeMonth: true,
					numberOfMonths: 3,
					dateFormat: "d M yy",
					onClose: function( selectedDate ) {
						$( "#visits_2_from" ).datepicker( "option", "maxDate", selectedDate );
					}
				});
			});
		</script>
         
         <div class="select_area">
         	<h1 style="margin-top:0;">Period</h1>
			<select id="values_per" name="values_per" style="font-size:24px; width:100%;" >
            <option value="Monthly" <? if($_POST['values_per'] == 'Monthly') echo('selected="selected"'); ?>>monthly values</option>
            <option value="Daily" <? if($_POST['values_per'] == 'Daily') echo('selected'); ?>>daily values</option>
            </select>
         </div>
         <input type="submit" value="Submit" />
         </form>
    </div>
    
    <div id="right_col">
    	<div class="infoprint">
        	<? echo "<div class='total1'><h2>Range 1</h2>From : " .$_POST['visits_1_from']. " to : ".$_POST['visits_1_to'] ."</div>"; ?>
           	<? echo "<div class='total2'><h2>Range 2</h2>From : " .$_POST['visits_2_from']. " to : ".$_POST['visits_2_to'] ."</div>"; ?>
        </div>
		<?
	//10 most popular products sold
	if($_POST['values_type'] == 'ten_pop_prod'){
		echo "<section id ='graph_right'>";
		if($_POST['values_per'] == 'Monthly' || $_POST['values_per'] == 'Daily')
		{
			echo "<section id='range1'>";
			echo "<h2>Range1</h2>";
			function ten_pop_prod($state) {
				// get number of Months between dates
				$date1_stamp = strtotime($_POST['visits_1_from']);
				$date2_stamp = strtotime($_POST['visits_1_to']);
				
				$year1 = date('Y', $date1_stamp);
				$year2 = date('Y', $date2_stamp);
				
				$month1 = date('m', $date1_stamp);
				$month2 = date('m', $date2_stamp);
				
				$months = (($year2 - $year1) * 12) + ($month2 - $month1);
			
				$s = $state;
				$a=array();
				$Mytotal_tab =0;
				for($i=0; $i<=$months; $i++)
				{
						$disp_date = date("M",strtotime("+".$i." month", $date1_stamp));
						$month = date("n",strtotime("+".$i." month", $date1_stamp));
						$year = date("Y",strtotime("+".$i." month", $date1_stamp));
						
						if ($state == "United _Kindom" || $state == "UK" || $state == "New Zealand" || $state == "Australia" 
							|| $state == "United States" || $state == "US")
						{
							switch ($state){
								case "United Kindom" : $t = "UK";
								break;
								case "United States" : $t = "US";
								break;
								case "New Zealand" : $t = "Australia";
								break;
							}
							$state = "$s' OR orders_master.country = '$t";
						}
						
						
						// TRANSACTIONS
						//select all products sold in this period in this state
						$q = "SELECT orders.product_id FROM orders,orders_master 
								WHERE MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."'
								AND orders.order_master_id= orders_master.id 
								AND orders_master.status >0 AND orders_master.status <8
								AND (orders_master.country = '$state');";
						$r = mysqli_query($db,$q) or die(mysqli_error($db));
						while ($row = mysqli_fetch_array($r))
						{
							$a[] = $row[0];
						}
				}
			
				//calcul the frequence of values in array
				$b = array_count_values($a);
				//order and keep the 10 first
				arsort($b);
				$b = array_slice($b, 0, 10, true);
				if ($b !=0 && $b != NULL) 
				{
					?>
					<table id="ten_pop_prod">
						
						<? echo "<h3>$s</h3>"; ?>
						<tbody>
						<tr><td>Product Name</td><td>Number of products sold</td></tr>
						<?
						foreach ($b as $key=>$value)
						{
							$sql = "SELECT sku FROM products WHERE id = $key";
							$resp = mysqli_query($db,$sql) or die(mysqli_error($db));
							$res = mysqli_fetch_array($resp);
							echo "<tr><td>" .$res[0]."($key)</td> <td> $value </td></tr>";
						}
						?>
						</tbody>
					</table>
					<?
				}
			}
			
			ten_pop_prod("United States");
			ten_pop_prod("United Kingdom");
			ten_pop_prod("Mainland Europe");
			ten_pop_prod("Ireland");
			ten_pop_prod("Canada");
			ten_pop_prod("New Zealand");
			ten_pop_prod("South America");
			ten_pop_prod("Africa");
			ten_pop_prod("Asia");
			ten_pop_prod("Middle East");
			echo "</section>";
		
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//GRAPH2
		////////////////////////////////////////////////////////////////////////
		
		
			echo "<section id='range2'>";
			echo "<h2>Range2</h2>";
			function ten_pop_prodb($state) 
			{
				// get number of Months between dates
				$date1_stamp = strtotime($_POST['visits_2_from']);
				$date2_stamp = strtotime($_POST['visits_2_to']);
				
				$year1 = date('Y', $date1_stamp);
				$year2 = date('Y', $date2_stamp);
				
				$month1 = date('m', $date1_stamp);
				$month2 = date('m', $date2_stamp);
				
				$months = (($year2 - $year1) * 12) + ($month2 - $month1);
			
				$s = $state;
				$a=array();
				for($i=0; $i<=$months; $i++)
				{
						$disp_date = date("M",strtotime("+".$i." month", $date1_stamp));
						$month = date("n",strtotime("+".$i." month", $date1_stamp));
						$year = date("Y",strtotime("+".$i." month", $date1_stamp));
						
						if ($state == "United _Kindom" || $state == "UK" || $state == "New Zealand" || $state == "Australia" 
							|| $state == "United States" || $state == "US")
						{
							switch ($state){
								case "United Kindom" : $t = "UK";
								break;
								case "United States" : $t = "US";
								break;
								case "New Zealand" : $t = "Australia";
								break;
							}
							$state = "$s' OR orders_master.country = '$t";
						}
						
						// TRANSACTIONS
						//select all products sold in this period in this state
						$q = "SELECT orders.product_id FROM orders,orders_master 
								WHERE MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."'
								AND orders.order_master_id= orders_master.id 
								AND orders_master.status >0 AND orders_master.status <8
								AND (orders_master.country = '$state');";
						$r = mysqli_query($db,$q) or die(mysqli_error());
						while ($row = mysqli_fetch_array($r))
						{
							$a[] = $row[0];
						}
				}
			
				//calcul the frequence of values in array
				$b = array_count_values($a);
				//order and keep the 10 first
				arsort($b);
				$b = array_slice($b, 0, 10, true);
				if ($b !=0 && $b != NULL) 
				{
					?>
					<table id="ten_pop_prod" id="_ten_pop_prod_right">
						
						<? echo "<h3>$s</h3>"; ?>
						<tbody>
						<tr><td>Product Name</td><td>Number of products sold</td></tr>
						<?
						foreach ($b as $key=>$value)
						{
							$sql = "SELECT sku FROM products WHERE id = $key";
							$resp = mysqli_query($db,$sql) or die(mysqli_error($db));
							$res = mysqli_fetch_array($resp);
							echo "<tr><td>" .$res[0]."($key)</td> <td> $value </td></tr>";
						}
						?>
						</tbody>
					</table>
					<?
					}
			}
			ten_pop_prodb("United States");
			ten_pop_prodb("United Kingdom");
			ten_pop_prodb("Mainland Europe");
			ten_pop_prodb("Ireland");
			ten_pop_prodb("Canada");
			ten_pop_prodb("New Zealand");
			ten_pop_prodb("South America");
			ten_pop_prodb("Africa");
			ten_pop_prodb("Asia");
			ten_pop_prodb("Middle East");
			echo "</section>";
		}
		echo "</section>";
	}
	// END 10 most popular products
	?>
	
	<!-- geo data -->
	<?
	if($_POST['values_type'] == 'geo_data'){
	?>
    	<style>
			#right_col {
				margin-left : 18%;	
			}
		</style>
		<script type='text/javascript'>
		google.load('visualization', '1', {'packages': ['geochart']});
		google.setOnLoadCallback(drawRegionsMap);
		google.load('visualization', '1', {packages:['table']});
		google.setOnLoadCallback(drawTable);
		
		function drawRegionsMap() {
			var data = google.visualization.arrayToDataTable([
			  ['Country', 'Number of orders'],
			  <?
			  $total = 0;
			  $total3 = 0;
			  if($_POST['values_per'] == 'Monthly' || $_POST['values_per'] == 'Daily'){ //because it's the same
					// get number of Months between dates
					$date1_stamp = strtotime($_POST['visits_1_from']);
					$date2_stamp = strtotime($_POST['visits_1_to']);
					$date3_stamp = strtotime($_POST['visits_2_from']);
					$date4_stamp = strtotime($_POST['visits_2_to']);
					$year1 = date('Y', $date1_stamp);
					$year2 = date('Y', $date2_stamp);
					$month1 = date('m', $date1_stamp);
					$month2 = date('m', $date2_stamp);
					$months = (($year2 - $year1) * 12) + ($month2 - $month1);
					//states
					$q = "SELECT DISTINCT(country) FROM ip_to_country";
					$r = mysqli_query($db,$q) or die(mysqli_error($db));
					while ($row = mysqli_fetch_array($r))
					{	$states[] = $row[0]; }
				
					$a = array();
					$a3 = array();
					for($i=0; $i<=$months; $i++){
						$disp_date = date("M",strtotime("+".$i." month", $date1_stamp));
						$month = date("n",strtotime("+".$i." month", $date1_stamp));
						$year = date("Y",strtotime("+".$i." month", $date1_stamp));
						$month3 = date("n",strtotime("+".$i." month", $date3_stamp));
						$year3 = date("Y",strtotime("+".$i." month", $date4_stamp));
					
						//create array , states in keys, numbers of orders in values
						foreach ($states as $state) {
							$q = "SELECT orders_master.id FROM orders_master INNER JOIN ip_to_country ON ip_to_country.ip = orders_master.ip
									 WHERE ip_to_country.country='".$state."' AND YEAR(orders_master.insert_datetime)='".$year."'
									 AND MONTH(orders_master.insert_datetime)='".$month."';";
							$r = mysqli_query($db,$q) or die(mysqli_error($db));
							$number = mysqli_num_rows($r);
							if( $number !=0)
							{
								//if the country is in the array : concat
								if( array_key_exists($state, $a)==true) {
									$a[$state] += intval($number);
								}else {
								//if not, add new country
									$a[$state] = intval($number);
								}
							}
							
							$q3 = "SELECT orders_master.id FROM orders_master INNER JOIN ip_to_country ON ip_to_country.ip = orders_master.ip
									 WHERE ip_to_country.country='".$state."' AND YEAR(orders_master.insert_datetime)='".$year3."'
									 AND MONTH(orders_master.insert_datetime)='".$month3."';";
							$r3 = mysqli_query($db,$q3) or die(mysqli_error($db));
							$number3 = mysqli_num_rows($r3);
							if( $number3 !=0)
							{
								//if the country is in the array : concat
								if( array_key_exists($state, $a3)==true) {
									$a3[$state] += intval($number3);
								}else {
								//if not, add new country
									$a3[$state] = intval($number3);
								}
							}
							//total
							$total += $number;
							$total3 += $number3;
						}
					}
				}
				foreach ($a as $key=>$value)
				{
					$arr[] = "['".$key."', ".$value."]";
					//$Mytotal_geo += $value; 
					$percent = number_format(($value*100)/$total, 0, ',', '');
					$percent3 = number_format(($a3[$key]*100)/$total3, 0, ',', '');
					$arr3[] = "['".$key."', ".$value.", '".$percent."%', '".$a3[$key]."', '".$percent3."%']";
				}
				$output = implode(',',$arr);
				$output_tab = implode(',',$arr3);
				
				echo($output);
			?>
			]);
			var options = {
				vAxis: {
					title: '<? echo $xtitle; ?>',
					minValue: 0, 
					maxValue: <? echo intval($max_value); ?>
					},
				title: '<? echo "Total of orders : ".$Mytotal_geo; ?>',
				backgroundColor : '#D2D2D2',
				width: 950,
				legend: {textStyle: {fontSize: 18, bold: true}}
			};

			var chart = new google.visualization.GeoChart(document.getElementById('chart_diva'));
			chart.draw(data, options);
		}
		
		function drawTable() {
			var data = google.visualization.arrayToDataTable([
			  ['Country', 'Orders', 'Percent of the World', 'Orders Last Year', 'Percent Last Year'],
			  <? echo($output_tab);	?>
			]);
			var options = {
				width : 360	
			};
			var table = new google.visualization.Table(document.getElementById('table_diva'));
        	table.draw(data, options);
		}
		</script>
		<div id="chart_diva"></div>	
		<div id="table_diva"></div>	
        <!--<span class="t1">Total of orders : <? echo $Mytotal_geo; ?></span>-->
        
	<? } ?>
	<!--END geo map -->

	<?
        
                    var_dump($_POST);
        
	if($_POST['values_type'] != 'geo_data' AND $_POST['values_type'] != 'ten_pop_prod'){
		$xtitle = "Number of orders";
		switch($_POST['values_type']){
			case "Visits":	
			$xtitle = "Number of visits";
			break;
			case "Transactions":
			$xtitle = "Number of Transactions";
			break;
			case "Transaction_val":
			$xtitle = "Transaction values (€)";
			break;
			case "Conversion":
			$xtitle = "Conversion rate";
			break;
			case "Products_sold":
			$xtitle = "Number of products sold";
			break;
			case "Abandoned":
			$xtitle = "Abandoned cart (%)";
			break;
			case "Avg_transactions":
			$xtitle = "Average of transactions (€)";
			break;
			case "Avg_products":
			$xtitle = "Average of product sold (€)";
			break;
		}
		if ($xtitle =="€" || $xtitle =="%"){
		?>
            <style>
                    svg g g text{
                            /*
                            transform:rotate(0deg);
                            -ms-transform:rotate(0deg); 
                            -webkit-transform:rotate(0deg); */
                            font-size: 18px;
                    }
                    svg g g g text{
                            font-size: larger;
                    }
            </style>
        <?	
		}
		
		//datas
		$total = 0;
		$Mytotal1=0;
		$Mytotal2=0;
		function print_datas($date_from, $date_to){
	
			if($_POST['values_per'] == 'Daily')
			{
				// get number of days between dates
				$date1_stamp = strtotime($date_from);
				$date2_stamp = strtotime($date_to);
				$datediff = $date2_stamp - $date1_stamp;
				$days = floor($datediff/(60*60*24));
	
				$arr = array();
				for($i=0; $i<=$days; $i++){
					$day_date = date("j M",$date1_stamp + ($i * (60*60*24)));
					$day = date("j",$date1_stamp + ($i * (60*60*24)));
					$month = date("n",$date1_stamp + ($i * (60*60*24)));
					$year = date("Y",$date1_stamp + ($i * (60*60*24)));
			
					if($_POST['values_type'] == 'Visits'){
						// VISITS
						$q = "SELECT id FROM visits WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."'";
						$r = mysqli_query($db,$q) or die(mysqli_error($db));
						$c = mysqli_num_rows($r);
					}
			
					if($_POST['values_type'] == 'Transactions'){
						// TRANSACTIONS
						$q = "SELECT id FROM orders_master WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status IN (1,2)";
						$r = mysqli_query($db,$q) or die(mysqli_error($db));
						$c = mysqli_num_rows($r);
					}
			
					if($_POST['values_type'] == 'Conversion'){
						// VISITS
						$q = "SELECT id FROM visits WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."'";
						$r = mysqli_query($db,$q) or die(mysqli_error($db));
						$cv = mysqli_num_rows($r);
						// TRANSACTIONS
						$q = "SELECT id FROM orders_master WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status IN(1,2)";
						$r = mysqli_query($db,$q) or die(mysqli_error());
						$ct = mysqli_num_rows($r);
				
						$c = $ct / ($cv / 100);
						$c = number_format($c,3);
					}
			
					if($_POST['values_type'] == 'Products_sold'){
						// TRANSACTIONS
						$q = "SELECT orders.id FROM orders, orders_master WHERE DAY(orders_master.insert_datetime)='".$day."' AND MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."'
													AND orders_master.id = orders.order_master_id  
													AND orders_master.status >0 AND orders_master.status <8";
						$r = mysqli_query($db,$q) or die(mysqli_error($db));
						$c = mysqli_num_rows($r);
					}
					if($_POST['values_type'] == 'Abandoned'){
						// TRANSACTIONS
						$q = "SELECT id FROM orders_master WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status=0";
						$r = mysqli_query($db,$q) or die(mysqli_error());
						$abandon = intval(mysqli_num_rows($r));
				
						$q = "SELECT id FROM orders_master WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."'";
						$r = mysqli_query($db,$q) or die(mysqli_error());
						$all = intval(mysqli_num_rows($r));
				
						$c = (100*$abandon)/$all; //percent
						$c = number_format($c,2,'.','');
					}
					if($_POST['values_type'] == 'Avg_transactions'){
						// TRANSACTIONS
						//all the transaction (€ and $)
						$u=array();
						$w=array();
						$x=array();
						$y=0;
						$o= "SELECT id FROM orders_master WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND (currency='euro' OR currency='dolar')";
						$p = mysqli_query($db,$o) or die(mysqli_error($db));
						$divisor = mysqli_num_rows($p);
				
						//thirst array in €
						$q = "SELECT total FROM orders_master WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status IN (1,2) AND currency='euro'";
						$r = mysqli_query($db,$q) or die(mysqli_error($db));
						while ($row = mysqli_fetch_array($r))
						{
							$u[]= $row[0];
						}
				
						//second array in dolar, with conversion foreach transactions
						$s = "SELECT total FROM orders_master WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status IN (1,2) AND currency='dolar'";
						$t = mysqli_query($db,$s) or die(mysqli_error($db));
						while ($row = mysqli_fetch_array($t))
						{
							//convert to euros
							$conv_rate = find_rate($year, $month);
							$w[]= $row[0] * $conv_rate;
						}
						//merging the 2 arrays
						$x = array_merge($u , $w);
				
						//avarage of all transactions in the month
						$y = array_sum($x);
						$y = $y/$divisor;
				
						//not a decimal number
						$c = number_format($y,0,'','');
				
					}
					if($_POST['values_type'] == 'Avg_products'){
						// TRANSACTIONS
						//all the transaction (€ and $)
						$u=array();
						$w=array();
						$x=array();
						$y=0;
						$o = "SELECT orders.id FROM orders, orders_master 
											WHERE DAY(orders_master.insert_datetime)='".$day."' AND MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."'
											AND orders_master.id = orders.order_master_id  
											AND orders_master.status >0 AND orders_master.status <8
											AND (orders_master.currency='euro' OR orders_master.currency='dolar')";
						$p = mysqli_query($db,$o) or die(mysqli_error($db));
						$divisor = mysqli_num_rows($p);
				
						//thirst array in €
						$q = "SELECT orders.price FROM orders,orders_master 

								WHERE orders.order_master_id= orders_master.id 
								AND DAY(orders_master.insert_datetime)='".$day."' AND MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."'
								AND orders_master.status >0 AND orders_master.status <8 AND orders_master.currency='euro'";
						$r = mysqli_query($db,$q) or die(mysqli_error($db));
						while ($row = mysqli_fetch_array($r))
						{
							$u[]= $row[0];
						}
				
						//second array in dolar, with conversion foreach transactions
						$s = "SELECT orders.price FROM orders,orders_master 
								WHERE orders.order_master_id= orders_master.id 
								AND DAY(orders_master.insert_datetime)='".$day."' AND MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."'
								AND orders_master.status >0 AND orders_master.status <8 AND orders_master.currency='dolar'";
						$t = mysqli_query($db,$s) or die(mysqli_error($db));
						while ($row = mysqli_fetch_array($t))
						{
							//convert to euros
							$conv_rate = find_rate($year, $month);
							$w[]= $row[0] * $conv_rate;
						}
						//merging the 2 arrays
						$x = array_merge($u , $w);
				
						//avarage of all transactions in the month
						$y = array_sum($x);
						$y = $y/$divisor;
				
						//not a decimal number
						$c = number_format($y,0,'','');
					}
					if($_POST['values_type'] == 'Transaction_val'){
						// TRANSACTIONS
						//all the transaction (€ and $)
						$u=array();
						$w=array();
						$x=array();
						$y=0;
				
						//thirst array in €
						$q = "SELECT total FROM orders_master WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status IN (1,2) AND currency='euro'";
						$r = mysqli_query($db,$q) or die(mysqli_error($db));
						while ($row = mysqli_fetch_array($r))
						{
							$u[]= $row[0];
						}
				
						//second array in dolar, with conversion foreach transactions
						$s = "SELECT total FROM orders_master WHERE DAY(insert_datetime)='".$day."' AND MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status IN(1,2) AND currency='dolar'";
						$t = mysqli_query($db,$s) or die(mysqli_error($db));
						while ($row = mysqli_fetch_array($t))
						{

							//convert to euros
							$conv_rate = find_rate($year, $month);
							$w[]= $row[0] * $conv_rate;
						}
						//merging the 2 arrays
						$x = array_merge($u , $w);
				
						//avarage of all transactions in the month
						$y = array_sum($x);
				
						//not a decimal number
						$c = number_format($y,0,'','');
					}
					$arr[] = "['".$day_date."', ".$c."]";
					
					$total +=$c;
				} 
			}// daily
		
			if($_POST['values_per'] == 'Monthly'){
				// get number of Months between dates
				$date1_stamp = strtotime($date_from);
				$date2_stamp = strtotime($date_to);
			
				$year1 = date('Y', $date1_stamp);
				$year2 = date('Y', $date2_stamp);
			
				$month1 = date('m', $date1_stamp);
				$month2 = date('m', $date2_stamp);
			
				$months = (($year2 - $year1) * 12) + ($month2 - $month1);
		
				$arr = array();
			
				for($i=0; $i<=$months; $i++){
					$disp_date = date("M",strtotime("+".$i." month", $date1_stamp));
					$month = date("n",strtotime("+".$i." month", $date1_stamp));
					$year = date("Y",strtotime("+".$i." month", $date1_stamp));
				
					if($_POST['values_type'] == 'Visits'){
						// VISITS
						$q = "SELECT id FROM visits WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."'";
						$r = mysqli_query($db,$q) or die(mysqli_error($db));
						$c = mysqli_num_rows($r);
					}
				
					if($_POST['values_type'] == 'Transactions'){
						// TRANSACTIONS
						$q = "SELECT id FROM  orders_master WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status IN(1,2)";
						$r = mysqli_query($db,$q) or die(mysqli_error($db));
						$c = mysqli_num_rows($r);
						
					}
				
					if($_POST['values_type'] == 'Conversion'){
						// VISITS
						$q = "SELECT id FROM visits WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."'";
						$r = mysqli_query($db,$q) or die(mysqli_error($db));
						$cv = mysqli_num_rows($r);
						// TRANSACTIONS
						$q = "SELECT id FROM orders_master WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status IN(1,2)";
						$r = mysqli_query($db,$q) or die(mysqli_error($db));
						$ct = mysqli_num_rows($r);
					
						$c = $ct / ($cv / 100);
						$c = number_format($c,3);
					}
				
					if($_POST['values_type'] == 'Products_sold'){
						// TRANSACTIONS
						$q = "SELECT orders.id FROM orders, orders_master WHERE MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."' 
													AND orders_master.id = orders.order_master_id  
													AND orders_master.status >0 AND orders_master.status <8";
						$r = mysqli_query($db,$q) or die(mysqli_error($db));
						$c = mysqli_num_rows($r);
					
					}
					if($_POST['values_type'] == 'Abandoned'){
						// TRANSACTIONS
						$q = "SELECT id FROM orders_master WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status=0";
						$r = mysqli_query($db,$q) or die(mysqli_error($db));
						$abandon = intval(mysqli_num_rows($r));
					
						$t = "SELECT id FROM orders_master WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."'";
						$u = mysqli_query($db,$t) or die(mysqli_error($db));
						$all = intval(mysqli_num_rows($u));
					
						$c = (100*$abandon)/$all; //percent
						$c = number_format($c,2,'.','');
					}
					if($_POST['values_type'] == 'Avg_transactions'){
						// TRANSACTIONS
						//all the transaction (euro and $)
						$u=array();
						$w=array();
						$x=array();
						$y=0;
						$o= "SELECT id FROM orders_master WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status IN (1,2) AND (currency='euro' OR currency='dolar')";
						$p = mysqli_query($db,$o) or die(mysqli_error($db));
						$divisor = mysqli_num_rows($p);
					
						//thirst array in euro
						$q = "SELECT total FROM orders_master WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status IN (1,2) AND currency='euro'";
						$r = mysqli_query($db,$q) or die(mysqli_error($db));
						while ($row = mysqli_fetch_array($r))
						{
							$u[]= $row[0];
						}
					
						
						//second array in dolar, with conversion foreach transactions
						$s = "SELECT total FROM orders_master WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status IN(1,2) AND currency='dolar'";
						$t = mysqli_query($db,$s) or die(mysqli_error($db));
						while ($row = mysqli_fetch_array($t))
						{
							//convert to euros
							$conv_rate = find_rate($year, $month);
							$w[]= $row[0] * $conv_rate;
						}
						//merging the 2 arrays
						$x = array_merge($u , $w);
					
						//avarage of all transactions in the month
						$y = array_sum($x);
						$y = $y/$divisor; 
					
						//not a decimal number
						$c = number_format($y,0,'','');
					}
					if($_POST['values_type'] == 'Avg_products'){
						// TRANSACTIONS
						//all the transaction (€ and $)
						$u=array();
						$w=array();
						$x=array();
						$y=0;
						$o = "SELECT orders.id FROM orders, orders_master WHERE MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."' 
								AND orders_master.id = orders.order_master_id  
								AND orders_master.status >0 AND orders_master.status <8
								AND (orders_master.currency='euro' OR orders_master.currency='dolar')";
						$p = mysqli_query($db,$o) or die(mysqli_error($db));
						$divisor = mysqli_num_rows($p);
					
						//thirst array in €
						$q = "SELECT orders.price FROM orders,orders_master 
								WHERE MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."'  
								AND orders.order_master_id= orders_master.id  
								AND orders_master.status >0 AND orders_master.status <8 AND orders_master.currency='euro'";
						$r = mysqli_query($db,$q) or die(mysqli_error($db));
						while ($row = mysqli_fetch_array($r))
						{
							$u[]= $row[0];
						}
					
						//second array in dolar, with conversion foreach transactions
						$s = "SELECT orders.price FROM orders,orders_master 
								WHERE MONTH(orders_master.insert_datetime)='".$month."' AND YEAR(orders_master.insert_datetime)='".$year."'  
								AND orders.order_master_id= orders_master.id 
								AND orders_master.status >0 AND orders_master.status <8 AND orders_master.currency='dolar'";
						$t = mysqli_query($db,$s) or die(mysqli_error($db));
						while ($row = mysqli_fetch_array($t))
						{
							//convert to euros
							$conv_rate = find_rate($year, $month);
							$w[]= $row[0] * $conv_rate;
						}
						//merging the 2 arrays
						$x = array_merge($u , $w);
					
						//avarage of all transactions in the month
						$y = array_sum($x);
						$y = $y/$divisor;
					
						//not a decimal number
						$c = number_format($y,0,'','');
					}		
					if($_POST['values_type'] == 'Transaction_val'){
						// TRANSACTIONS
						//all the transaction (euro and $)
						$u=array();
						$w=array();
						$x=array();
						$y=0;
						//thirst array in euro
						$q = "SELECT total FROM orders_master WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status IN (1,2) AND currency='euro'";
						$r = mysqli_query($db,$q) or die(mysqli_error($db));
						while ($row = mysqli_fetch_array($r))
						{
							$u[]= $row[0];
						}
						
					
						//second array in dolar, with conversion foreach transactions
						$s = "SELECT total FROM orders_master WHERE MONTH(insert_datetime)='".$month."' AND YEAR(insert_datetime)='".$year."' AND status IN (1,2) AND currency='dolar'";
						$t = mysqli_query($db,$s) or die(mysqli_error($db));
						while ($row = mysqli_fetch_array($t))
						{
							//convert to euros
							$conv_rate = find_rate($year, $month);
							$w[]= $row[0] * $conv_rate;
						}
						
						echo('<br>cr = '.$conv_rate);
						
						//merging the 2 arrays
						$x = array_merge($u , $w);
					
						//avarage of all transactions in the month
						$y = array_sum($x);
					
						//not a decimal number
						$c = number_format($y,0,'','');
					}		
					$arr[] = "['".$disp_date."', ".$c."]";
					$total += $c;
					
				}
			} // monthly
			
			$output = implode(',',$arr);
			
			
			
			return $output."pokaentas".$total;
			
			
			
		} //end of the function print_data()
		$output1 = print_datas($_POST['visits_1_from'],$_POST['visits_1_to']);
		$output2 = print_datas($_POST['visits_2_from'],$_POST['visits_2_to']);
		$Mytotal1 = substr($output1 , (strpos($output1, "pokaentas")+9));
		$Mytotal2 = substr($output2 , (strpos($output2, "pokaentas")+9));
		$output1 = substr($output1 , 0, (strpos($output1, "pokaentas")));
		$output2 = substr($output2 , 0, (strpos($output2, "pokaentas")));
		
		//calcul total avg
		if ($_POST['values_type'] == 'Avg_transactions' || $_POST['values_type'] == 'Avg_products' 
		|| $_POST['values_type'] == 'Abandoned' || $_POST['values_type'] == 'Conversion'){
			//nb of values in the array
			$nb_output1 = (count(explode(',',$output1)))/2;
			$nb_output2 = (count(explode(',',$output2)))/2;
			
			$Mytotal1 = number_format((float)($Mytotal1 /$nb_output1), 2, ',', '');
			$Mytotal2 = number_format((float)($Mytotal2 /$nb_output2), 2, ',', '');
		}
			
		//select only the max number
		$merge = array_merge(explode(',',$output1), explode(',',$output2));
		rsort($merge, SORT_NUMERIC);
		$max_value = intval($merge[0]);
		
		//if one graphic
		if (isset($_POST['same_range']) AND $_POST['same_range'] == "ok"){
			$output1 = explode(",",$output1 );
			
			
			
			$output2 = explode(",",$output2);
			
			$o1_diff =array_diff($output1, $output2);
			
			$comptor = 0;
			$output3 = "";
			foreach ($output2 as $a=>$b){
				
				if ($comptor%2 != 1){
					$output3 .=$b;
				}
				if ($comptor%2 == 1){
					//$c = str_replace( "]", "", $o1_diff[$comptor]);
					
					$c = str_replace( "]", "", $output1[$comptor]);
					
					if ($c == "" ){ $c= 0; }
					$output3 .= ",".$c.",".$b.",";
				}
				$comptor++;
			}
			
			?>
			<script type="text/javascript">
			  google.load("visualization", "1", {packages:["corechart"]});
			  google.setOnLoadCallback(drawChart);
			  function drawChart() {
				var data = google.visualization.arrayToDataTable([
				<?
						echo "['".$_POST['values_type']."', 'Range 1', 'Range 2'],";
						echo $output3;
				?>
				]);
				var options = {
					title: '<? echo $xtitle; ?>',
					colors:['#FF8F3F', '#91911A'],
					tooltip: {trigger :getSelection().row},
					legend: 'none',
					chartArea: {width: '75%'}
				};
		
				var chart = new google.visualization.LineChart(document.getElementById('month_conv'));
				chart.draw(data, options);
				/*chart.getChartLayoutInterface().getVAxisValue(300);*/
			  }
			</script>
            <div class="total1" style="margin-top: 121px;">
				<? echo "Total : ".$Mytotal1; ?><? if ($_POST['values_type'] == 'Abandoned') { echo " %"; }?>
            </div>
            <div class="total2"><? echo "Total : ".$Mytotal2; ?><? if ($_POST['values_type'] == 'Abandoned') { echo " %"; }?></div>
			<div id="month_conv" style="height:670px;"></div>
            <? 
		}else {
		?>
        <script type="text/javascript">
		  google.load("visualization", "1", {packages:["corechart"]});
		  google.setOnLoadCallback(drawChart);
		  function drawChart() {
			var data = google.visualization.arrayToDataTable([
			<?
				echo  "['".$_POST['values_type']."', 'Range 1'],"; 
				echo $output1;
			?>
			]);
			var options = {
				/*vAxis: {
					title: '<? echo $xtitle; ?>',
					minValue: 0, 
					maxValue: <? echo intval($max_value); ?>
					},*/
				title: '<? echo $xtitle; ?>',
				colors:['#FF8F3F'],
				legend: {position :'none', width:0, margin:0, padding:0},
				chartArea: {width: '70%'}
			};
	
			var chart = new google.visualization.LineChart(document.getElementById('month_conv'));
			chart.draw(data, options);
			/*chart.getChartLayoutInterface().getVAxisValue(300);*/
		  }
		</script>
        <div class="total1"><? echo "Total : ".$Mytotal1; ?><? if ($_POST['values_type'] == 'Abandoned') { echo " %"; }?></div>
        <div id="month_conv" style="height:350px;"></div> 
        
       
		<script type="text/javascript">
		  google.load("visualization", "1", {packages:["corechart"]});
		  google.setOnLoadCallback(drawChart);
		  function drawChart() {
			var data = google.visualization.arrayToDataTable([
			<?
				echo  "['".$_POST['values_type']."', 'Range 2'],"; 
				echo $output2;
			?>
			]);
			var options = {
				title: '<? echo $xtitle; ?>',
				colors:['#91911A'],
				legend: 'none', 
				chartArea: {width: '70%'}
			};
	
			var chart = new google.visualization.LineChart(document.getElementById('month_conv2'));
			chart.draw(data, options);
		  }
		</script>
        <div class="total2"><? echo "Total : ".$Mytotal2; ?><? if ($_POST['values_type'] == 'Abandoned') { echo " %"; }?></div>
		<div id="month_conv2" style="height:350px;"></div>   
		<? 
		}//end else no one graphic
		}  //endif values_type = other
		?>
        
    </div>
    </div>
 <img src="print.png" alt="print" onclick="window.print();" style="width: 50px; float:right; cursor:pointer;" />
</div>
    
    
</body>
</html>
