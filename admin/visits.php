<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Shanore statistics</title>
    <link href="screen.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
</head>

<body style="padding:25px;">
<div class="content">
	<div class="onglet"><a href="stats.php">Statistics</a></div>
	<div class="onglet"><a href="conversion_rate.php">Fx rates</a></div> 
	<div class="onglet sel"><a href="#">Visits</a></div> 
	<div class="tab_content" id="tab_content_0">
  	<section id="left_col">
		<form action="csv/import.php" method="post" enctype="multipart/form-data">
			<!--<input type="file" name="file" value="file" />-->
			<input type="submit" value="Upload" />
		</form>
	</section>
    
    <table class="visits">
	<tr>
        <th>Number</th>
        <th>Day</th>
        <th>Month</th>
        <th>Year</th>
	</tr>
    <?
	require('db.php');
	
	$result = array();
	$n = 0;
	$q = "SELECT id, insert_datetime FROM visits ORDER BY insert_datetime DESC";
	$r = mysql_query($q) or die(mysql_error());
	while($l = mysql_fetch_row($r))
	{		
		$day = substr($l[1], 0, 10 );
		if(array_key_exists($day, $result)== FALSE){
			$n=1;
			$result[$day] = $n;
		} else {
			$nb = (intval($result[$day]) +1);
			$result[$day] = $nb;
		}
	}
	foreach($result as $date => $val)
	{
		echo "<tr><td>".$val."</td><td >".substr($date, 8,10)."</td><td>".substr($date, 5,2)."</td><td>".substr($date, 0,4)."</td></tr>";
		
	}
	?>
    </table>
</div>
    
</body>
</html>