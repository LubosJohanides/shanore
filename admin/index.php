﻿<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-language" content="en" />
        <meta name="robots" content="index,follow" />
        <meta name="googlebot" content="index,follow,snippet,archive" />
        <meta name="author" content="Matrix Internet, Tomas Herink" />

        <link rel="stylesheet" type="text/css" media="screen" href="css/login.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/smoothness/jquery-ui-1.7.2.custom.css" />
        
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
        <script type="text/javascript" src="js/index.js"></script>
        

        <script type="text/javascript">
            $(document).ready(function() {
                $('#forgottenPassword').dialog({
                    autoOpen: false,
                    title: 'Forgotten password'
                })
                $('a.forgotten').click(function() {
                    $('#forgottenPassword').dialog('open');
                });
            })
        </script>

        <title>Admin Login</title>
    </head>
    <body>
        

      <div id="content">
            <h1>Admin area</h1>
            <form id="login_frm" name="login_frm" >
              <h2>Login</h2>

                <div class="left">
                    <label for="email">name:</label>
                    <input type="text" name="name" id="name" class="text" />
                </div>

                <div class="right">
                    <label for="password">password:</label>
                    <input type="password" name="password" id="password" class="password"/>
                </div>

                <div class="submit">

                    <input type="button" name="login_btn" class="button "value="Log In" id="login_btn" />
                </div>
            </form>
            <div class="flash error" id="error">Nope, wrong login details.</div>
        </div>
<div id="forgottenPassword">
            <form method="post" action="">
                <label for="email">Your email:</label>
                <input type="text" name="email" class="text "id="email" />
                <div class="fpSubmit">
                    <input type="submit" class="button" value="Odoslat" />
                </div>
            </form>
        </div>
    </body>
</html>