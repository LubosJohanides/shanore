<?
	require('db.php');
	session_start();

	//logged?
	if($_GET['action']=='logout'){
		$q = "UPDATE admin SET session_id='' WHERE session_id='".session_id()."'";
		$r = mysqli_query($db,$q);
	}

	$q = "select login from admin where session_id='".session_id()."'";
	$r = mysqli_query($db,$q);
	$c = mysqli_num_rows($r);
	if($c){
		$logged = 1;
		$f = mysqli_fetch_row($r);
		$admin_name = $f[0];
	}else{
		$logged = 0;
	}



?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-language" content="en" />
        <meta name="robots" content="index,follow" />
        <meta name="googlebot" content="index,follow,snippet,archive" />
        <meta name="author" content="ELIVE CZ s.r.o. (c) 2010" />
		<?
			if(!$logged) {
				die('<meta http-equiv="refresh" content="0;url=index.php" />');
				exit();
			}
		?>


        <link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/smoothness/jquery-ui-1.7.2.custom.css" />

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
        <script type="text/javascript" src="js/jquery.ui.datepicker-cs.js"></script>
        <script type="text/javascript" src="js/jquery.simpleTooltip.js"></script>
        <script type="text/javascript" src="js/jquery.flot.pack.js"></script>
        <!--[if IE]><script language="javascript" type="text/javascript" src="js/excanvas.pack.js"></script><![endif]-->
        <script src="js/jquery.wysiwyg.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/jquery.wysiwyg.controls.js" type="text/javascript" charset="utf-8"></script>
        <script type="text/javascript" src="js/jquery.highlight-1.1.source.js"></script>


<script type="text/javascript" src="js/submenu.js"></script>
        <script type="text/javascript" src="js/xadmin.js"></script>
        <script type="text/javascript" src="js/jquery.simplemodal.js"></script>
    <script type="text/javascript" src="js/admin.js"></script>


    <title>ShanOre.com ADMIN</title>
    <link href="css/jquery.wysiwyg.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
		body,td,th {
			font-family: Arial, sans-serif;
		}
    </style>
    </head>
<body>
        <div id="header">
            <h1>ShanOre.com</h1>
            <div id="user"><a href="?action=logout">Logout</a>

                <br />
            </div>

            <div id="mainMenuWrapper">
<ul id="mainMenu">
                    <li><a href="admin.php">Catalog</a></li>
                    <li><a href="orders.php">ORDERS</a></li>
                    <li><a href="customers.php">Customers</a></li>
                    <li class="active"><a href="affiliates.php">Affiliates</a><a href="editor.html"></a></li>
</ul>
            </div>
        </div>
        <div id="contentWrapper">
            <div id="leftMenu">
                <strong>Menu</strong>
              <div id="menu">
                    <ul>
                        <li class="active"><a href="affiliates.php" class="item">Affiliates List</a></li>
                        <li><a href="affiliates-orders.php" class="item">Orders</a></li>
                        <li><a href="affiliates-orders-resolved.php" class="item">Resolved Orders</a></li>
                        <li><a href="affiliates-reports.php" class="item">Reports</a></li>
                        <li><a href="affiliates-links.php" class="item">Permanent Links</a></li>
                    </ul>
            </div>
            <br />
                <br />
                <br />
                <br />
                <br />
                <br />

                <div class="filter">
              <h3>Quick Search</h3>
              <form action="" method="get" enctype="multipart/form-data">
                    Word / Phrase
                    <input type="text" name="search_txt" style="width:165px;" value="<? echo($_GET['search_txt']); ?>" />
                    <br />
                    <input type="submit" value="Search" />

              </form>
            </div>

            </div>
            <div id="content">


              <div id="inner"><? echo($message); ?>
              <a href="#" class="add" id="addnew_clicker">Add a new affiliate</a>
           	    <div class="add">
                	<form action="?action=addnew" method="post" enctype="multipart/form-data">
                    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="13%">&nbsp;</td>
    <td width="24%">&nbsp;</td>
    <td width="18%">&nbsp;</td>
    <td width="45%">&nbsp;</td>
  </tr>
  <tr>
    <td>Affiliate Name</td>
    <td><input name="name" /></td>
    <td>web address</td>
    <td><input name="web" /></td>
  </tr>
  <tr>
    <td>Debtor No.</td>
    <td><input name="debtor" /></td>
    <td>email</td>
    <td><input name="email" /></td>
  </tr>
  <tr>
    <td>Address 1</td>
    <td><input name="address1" /></td>
    <td>contact name</td>
    <td><input name="contact" /></td>
  </tr>
  <tr>
    <td>Address 2</td>
    <td><input name="address2" /></td>
    <td>phone</td>
    <td><input name="phone" /></td>
  </tr>
  <tr>
    <td>City</td>
    <td><input name="address3" /></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>State / County</td>
    <td><input name="state" /></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>ZIP / Post Code</td>
    <td><input name="zip" /></td>
    <td>&nbsp;</td>
    <td><input type="submit" value="ADD" /></td>
  </tr>
  <tr>
    <td>Order only</td>
    <td><input type="checkbox" name="irish_aff" value="irish_aff" /></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" style="color:#000;">Tick the box above if you don't want this affiliate to benefit from orders or to be visible on store locator page (eg. non-US affiliate). The affiliate will only be able to create wholesale orders.</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>


                    </form>

                </div>


                <div class="datagrid">
       	  <h2>All Existing Affiliates</h2>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <th width="41%">Debtor No.</th>
            <th width="17%">Name</th>
            <th width="15%">Address</th>
            <th width="15%">contact</th>
						<th width="12%">action</th>
          </tr>

<?
		if($_GET['action'] == 'addnew'){

			if($_POST['web']){
				if(!strpos($_POST['web'],'http://')){
					$_POST['web'] = 'http://'.$_POST['web'];
				}
			}

			$name = mysql_real_escape_string($_POST['name']);

			// Classe google maps
			require_once('../classes/gmaps/gMaps.php');

	        // Object gmaps
	        $gmaps = new gMaps();

	        // Create address
	        $address = mysqli_real_escape_string($db,$_POST['address1'].", ".$_POST['address2'].", ".$_POST['address3'].", ".$_POST['state'].", ".$_POST['zip']);

	        // Array return lat and lng
	        $arrMap = $gmaps->getLatLng($address);

			// get LAT and LNG
	        $lat = $arrMap["lat"];
	        $lng = $arrMap["lng"];

			if($_POST['irish_aff'] == 'irish_aff'){
				$non_us = '1';
			}else{
				$non_us = '0';
			}

			// Insert SQL
			$q = "INSERT INTO affiliates (debtor_no, name, address1, address2, address3, state, zip, web, contact, phone, email, lat, lng, non_us) VALUES ('".$_POST['debtor']."', '".$name."', '".$_POST['address1']."', '".$_POST['address2']."', '".$_POST['address3']."', '".$_POST['state']."', '".$_POST['zip']."', '".$_POST['web']."', '".$_POST['contact']."', '".$_POST['phone']."', '".$_POST['email']."','".$lat."','".$lng."', ".$non_us.")";
			$r = mysqli_query($db,$q) or die(mysqli_error($db));

		}


		if($_GET['action'] == 'makelogin'){

			if(($_POST['login']=='delete') AND ($_POST['pass']=='delete')){
				$q = "DELETE FROM affiliates WHERE id=".$_POST['affid'];
				$r = mysqli_query($db,$q) or die(mysqli_error($db));
				echo('<script type="text/javascript">alert("Deleted.");</script>');

			}else{
				if(!$_POST['login'] OR !$_POST['pass']){
					$q = "DELETE from adminshop WHERE affiliates_id=".$_POST['affid'];
					$r = mysqli_query($db,$q) or die(mysqli_error($db));
					echo('<script type="text/javascript">alert("Account DELETED.");</script>');
				}else{
					// changing or creating?
					$q = "SELECT id FROM adminshop WHERE affiliates_id=".$_POST['affid'];
					$r = mysqli_query($db,$q) or die(mysqli_error($db));
					$c = mysqli_num_rows($r);

					if($c){
						// changing
						$q = "UPDATE adminshop SET login='".$_POST['login']."', password='".$_POST['pass']."' WHERE affiliates_id=".$_POST['affid'];
						$r = mysqli_query($db,$q) or die(mysqli_error($db));
						echo('<script type="text/javascript">alert("Changes saved.");</script>');
					}else{
						// creating
						$q = "INSERT INTO adminshop (login, password, affiliates_id) VALUES ('".$_POST['login']."', '".$_POST['pass']."',".$_POST['affid'].")";
						$r = mysqli_query($db,$q) or die(mysqli_error($db));
						echo('<script type="text/javascript">alert("Account CREATED.");</script>');
					}
				}
			}
		}

if($_GET['action']=='savechanges'){

	$address1 = $_POST['address1'];
	$address2 = $_POST['address2'];
	$address3 = $_POST['address3'];
	$contact = $_POST['contact'];
	$phone = $_POST['phone'];
	$state = $_POST['state'];
	$zip = $_POST['zip'];
	$web = $_POST['web'];
	$email = $_POST['email'];
	$affid = intval($_POST['affid']);

	$q = "UPDATE affiliates SET
			address1='".$address1."',
			address2='".$address2."',
			address3='".$address3."',
			contact='".$contact."',
			phone='".$phone."',
			state='".$state."',
			zip='".$zip."',
			web='".$web."',
			email='".$email."'
			WHERE id=".$affid;
			//echo($q);

	$r = MySQLi_Query($db,$q) or die(mysqli_error($db));

	echo('<script>alert("Changes saved")</script>');

}


		if($_GET['search_txt']){
			$_GET['search_txt'] = strtolower($_GET['search_txt']);
			$searchsql = " WHERE LOWER(name) like '%".$_GET['search_txt']."%' OR LOWER(address1) like '%".$_GET['search_txt']."%' OR LOWER(address2) like '%".$_GET['search_txt']."%' OR LOWER(address3) like '%".$_GET['search_txt']."%' OR LOWER(contact) like '%".$_GET['search_txt']."%' or LOWER(email) like '%".$_GET['search_txt']."%' or LOWER(state) like '%".$_GET['search_txt']."%' or LOWER(web) like '%".$_GET['search_txt']."%'";
		}

		$q = "SELECT id, debtor_no, name, address1, address2, address3, contact, email, phone, state, zip, web FROM affiliates".$searchsql;
		$r = mysqli_query($db,$q) or die(mysqli_error($db));
		$c = mysqli_num_rows($r);

		for($i=0; $i<$c; $i++){
			$f = mysqli_fetch_row($r);

			$q_log = "SELECT login, password FROM adminshop WHERE affiliates_id=".$f[0];
			$r_log = mysqli_query($db,$q_log);
			$c_log = mysqli_num_rows($r_log);
			if($c_log){
				$f_log = mysqli_fetch_row($r_log);
				$login = '<br /><form action="?action=makelogin" method="post" enctype="multipart/form-data">
							<input name="affid" type="hidden" value="'.$f[0].'" />
							login: <input name="login" value="'.$f_log[0].'" /><br />
							pass: <input name="pass" value="'.$f_log[1].'" /><br /><input type="submit" value="Save">
							</form>';
			}else{
				$login = '<br /><form action="?action=makelogin" method="post" enctype="multipart/form-data">
							<input name="affid" type="hidden" value="'.$f[0].'" />
							login: <input name="login" /><br />
							pass: <input name="pass" /><br /><input type="submit" value="Create account">
							</form>';
			}


			if($color == 'e0e0e0'){
				$color = 'f0f0f0';
			}else{
				$color = 'e0e0e0';
			}

			echo('<tr style="background-color:#'.$color.'" id="line_'.$f[0].'">
					<td valign="top" style="padding-top:10px;">'.$f[1].'</td>
					<td valign="top" style="padding-top:10px;"><h3 style="margin-top:0;">'.stripslashes($f[2]).'</h3>'.$login.'</td>
					<form action="?action=savechanges" enctype="multipart/form-data" method="post">
					<td valign="top" style="padding-top:10px;"><input name="address1" placeholder="address line 1" value="'.$f[3].'" /><br/><input name="address2" placeholder="address line 2" value="'.$f[4].'" /><br/><input placeholder="Address line 3" name="address3" value="'.$f[5].'" /></td>
					<td valign="top" style="padding-top:10px;"><input placeholder="contact name" name="contact" value="'.$f[6].'" /><br/><input placeholder="email" name="email" value="'.$f[7].'" /><br/><input placeholder="phone" name="phone" value="'.$f[8].'" /><br/><input placeholder="state" name="state" value="'.$f[9].'" /> <input placeholder="zip" name="zip" value="'.$f[10].'" /><input placeholder="web" name="web" value="'.$f[11].'"/><input name="affid" type="hidden" value="'.$f[0].'" /></td>
					<td valign="top" style="padding-top:10px;"><input type="submit" value="Save changes"></td>
					</form>
					</tr>');
			}

?>

             </table>

          </div>
              </div>
            </div>
            <hr class="cleaner" />

      <div id="basic-modal-content">
      		<!-- edit form here -->

	  </div>
      <div id="basic-modal-content-2">
      		<!-- edit form here -->

	  </div>


</div>

        <div id="footer">

        </div>
    </body>

<?
	if($_GET['search_txt']){
		echo("<script>
					$('td').highlight('".$_GET['search_txt']."','highlight');
			  </script>");
	}
?>

</html>
