<?
	require('db.php');

	session_start();
	
	//logged?
	if($_GET['action']=='logout'){
		$q = "UPDATE admin SET session_id='' WHERE session_id='".session_id()."'";
		$r = mysqli_query($db,$q);
	}
	
	$q = "select login from admin where session_id='".session_id()."'";
	$r = mysqli_query($db,$q);
	$c = mysqli_num_rows($r);
	if($c){
		$logged = 1;
		$f = mysqli_fetch_row($r);
		$admin_name = $f[0];
	}else{
		$logged = 0;
	}
	
	

?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-language" content="en" />
        <meta name="robots" content="index,follow" />
        <meta name="googlebot" content="index,follow,snippet,archive" />
        <meta name="author" content="ELIVE CZ s.r.o. (c) 2010" />
		<?
			if(!$logged) {
				die('<meta http-equiv="refresh" content="0;url=index.php" />');
				
			}
		?>


        <link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/smoothness/jquery-ui-1.7.2.custom.css" />

        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
        <script type="text/javascript" src="js/jquery.ui.datepicker-cs.js"></script>
        <script type="text/javascript" src="js/jquery.simpleTooltip.js"></script>
        <script type="text/javascript" src="js/jquery.flot.pack.js"></script>
        <!--[if IE]><script language="javascript" type="text/javascript" src="js/excanvas.pack.js"></script><![endif]-->
        <script type="text/javascript" src="js/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="js/submenu.js"></script>
        <script type="text/javascript" src="js/xadmin.js"></script>
        <script type="text/javascript" src="js/admin.js"></script>
        
        

        <title>Shanore Admin</title>
    </head>
    <body>
        <div id="header">
            <h1>ShanOre.com</h1>
            <div id="user"><a href="?action=logout">Logout</a>

                <br />
            </div>
            <div id="mainMenuWrapper">
              <ul id="mainMenu">
                <li class="active"><a href="admin.php">Catalog</a></li>
                <li><a href="orders.php">ORDERS</a></li>
                <li><a href="customers.php">Customers</a><a href="editor.html"></a></li>
                <li><a href="affiliates.php">Affiliates</a><a href="editor.html"></a></li>
              </ul>
            </div>
        </div>
        <div id="contentWrapper">
            <div id="leftMenu">
                <strong>Menu</strong>
                <div id="menu">
                  <ul>
                    <li><a href="admin.php" class="item">Products</a></li>
                    <li class="active"> <a href="#" class="item">Product Options</a>
                      <ul>
                        <li><a href="options.php?what=categories">Categories</a></li>
                        <li><a href="options.php?what=brands">Brands</a></li>
                        <?
									$q = "SELECT name, id FROM product_options_types";
									$r = mysqli_query($db,$q);
									$c = mysqli_num_rows($r);
									for($i=0; $i<$c; $i++){
										$f = mysqli_fetch_row($r);
										echo('<li><a href="options.php?what='.$f[1].'&multi=yes">'.$f[0].'</a></li>');
									}
								?>
                      </ul>
                    </li>
                    <li> <a href="#" class="item" id="collections_menu">Collections</a>
                      <ul>
                      	<li><a href="categories-seo.php">CATEGORIES SEO</a></li><hr />
                        <li><a href="collections-assign.php">COLLECTIONS ADMIN</a></li><hr />
                        <?
									$q = "SELECT name, id FROM collections";
									$r = mysqli_query($db,$q);
									$c = mysqli_num_rows($r);
									for($i=0; $i<$c; $i++){
										$f = mysqli_fetch_row($r);
										echo('<li><a href="collections.php?what='.$f[1].'">'.$f[0].'</a></li>');
									}
								?>
                      </ul>
                    </li>
                    <li><a href="brands.php" class="item">Brands</a></li>
                    <li><a href="deleted_products.php" class="item">Deleted Products</a></li>
                    <li><a href="pages.php" class="item">Pages</a></li>
                    <li><a href="currencies.php" class="item">Currencies</a></li>
                    <li><a href="images.php" class="item">Images</a></li>
                    <li><a href="sort.php" class="item">Sort Products</a></li>
                    <li> <a href="#" class="item" id="pdf_menu">PDF catalogues</a>
                      <ul>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=1" target="_new">Engagement Rings</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=10" target="_new">Wedding Rings</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=96" target="_new">Claddagh Rings</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=94" target="_new">Celtic Rings</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=16" target="_new">Pendants</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=37" target="_new">Celtic Crosses</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=95" target="_new">Bracelets</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=84" target="_new">Earrings</a></li>

                                
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=85" target="_new">Tara Beads</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=91" target="_new">Tara Charms</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=83" target="_new">Tara Bracelets</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=92" target="_new">Tara Necklaces</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=93" target="_new">Tara Stacking Rings</a></li>

                            </ul>
                    </li>
                  </ul>
                </div>
              <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <div class="contact">
                    <strong>Support</strong>
                    <address>
                        Matrix Internet Solutions<br />
                        <a href="http://www.matrixinternet.ie/">www.matrixinternet.ie</a><br />
                        <br />
                        info@matrixinternet.ie<br />
                        +353 (0) 1 6711884
                  </address>
              </div>
            </div>
            <div id="content">
              <div id="inner"><a href="#" class="add" id="addnew_clicker">Add a new option to <? if($_GET['multi'] == 'yes'){
			$q = "SELECT name FROM product_options_types WHERE id=".$_GET['what'];
			$r = mysqli_query($db,$q);
			$f = mysqli_fetch_row($r);
			echo($f[0]);
		}else{
			echo($_GET['what']);
		} ?></a>
           	    <div class="add">
<div class="textbox">
                    
    	            	<form action="?what=<? echo($_GET['what']); ?>&action=add&multi=<? echo($_GET['multi']); ?>" enctype="multipart/form-data" method="post" id="addform">
                			<table width="100%" border="0" cellspacing="0" cellpadding="0">
                			  <tr>
                			    <td valign="top">Option name <br />
                                  <input name="name" />
                             <?
							 	if($_GET['what'] == 'categories'){
							 ?>
                                  <select name="parent" />
                                  		<option value="0">none</option>
                             <?
							 		$q = "SELECT id, name FROM categories WHERE parent_id=0";
									$r = mysqli_query($db,$q);
									$c = mysqli_num_rows($r);
									for($i=0; $i<$c; $i++){
										$f = mysqli_fetch_row($r);
										echo('<option value="'.$f[0].'">'.$f[1].'</option>');
										$q_sub = "SELECT id, name FROM categories WHERE parent_id=".$f[0];
										$r_sub = mysqli_query($db,$q_sub);
										$c_sub = mysqli_num_rows($r_sub);
										for($j=0; $j<$c_sub; $j++){
											$f_sub = mysqli_fetch_row($r_sub);
											echo('<option value="'.$f_sub[0].'">- '.$f_sub[1].'</option>');
										}
									}
							 ?>
                                  </select>
                             <?
								}
							 ?>
                                  <input type="submit" value="Insert" /></td>
                			    <td width="37%" valign="top">&nbsp;</td>
              			      </tr>
              			  </table>
  </form>
                    <div class="textboxFooter"></div></div></div>
         
         <?
		 		if($_GET['deleteid']){
					if($_GET['multi']=='yes'){
						$q = "DELETE FROM product_options_values WHERE id='".$_GET['deleteid']."'";
						$r = mysqli_query($db,$q);
						echo('<div class="flash ok">
								  <div class="top"></div>
								  <div class="text">Option deleted</div>
								  <div class="bottom"></div>
								</div>');
					}else{
						$q = "DELETE FROM ".$_GET['what']." WHERE id='".$_GET['deleteid']."'";
						$r = mysqli_query($db,$q);
						echo('<div class="flash ok">
								  <div class="top"></div>
								  <div class="text">Option deleted</div>
								  <div class="bottom"></div>
								</div>');
					}
				}
				
				
		 		if($_GET['action'] == 'add'){
					if($_GET['multi']=='yes'){
						
						$_POST['what'] = addslashes($_POST['what']);
						
						if($_POST['name']){
							$q = "SELECT id FROM product_options_values WHERE name='".$_POST['name']."' AND type_id=".$_GET['what'];
							$r = mysqli_query($db,$q);
							if(mysqli_num_rows($r)){
								echo('<div class="flash error">
									  <div class="top"></div>
									  <div class="text">This option already exists</div>
									  <div class="bottom"></div>
									</div>');
							}else{
							
								$q = "INSERT INTO product_options_values (name, type_id) values ('".$_POST['name']."','".$_GET['what']."')";
								$r = mysqli_query($db,$q) or die('<div class="flash error"><div class="top"></div><div class="text">SQL error: '.mysqli_error($db).'</div><div class="bottom"></div></div>');
									
								echo('<div class="flash ok">
									  <div class="top"></div>
									  <div class="text">New option added</div>
									  <div class="bottom"></div>
									</div>');
							}
						}else{
							echo('<div class="flash error">
								  <div class="top"></div>
								  <div class="text">No text inserted</div>
								  <div class="bottom"></div>
								</div>');
						}
					}else{
						if($_POST['name']){
							
							if($_GET['what'] == 'categories'){  // pro kategorie specialni sekce protoze ma navic PARENT_ID pole v DB
								$q = "SELECT id FROM ".$_GET['what']." WHERE name='".$_POST['name']."' AND parent_id=".$_POST['parent'];
								$r = mysqli_query($db,$q);
								if(mysqli_num_rows($r)){
									echo('<div class="flash error">
										  <div class="top"></div>
										  <div class="text">This option already exists</div>
										  <div class="bottom"></div>
										</div>');
								}else{
								
									$q = "INSERT INTO ".$_GET['what']." (name, parent_id) values ('".$_POST['name']."','".$_POST['parent']."')";
									$r = mysqli_query($db,$q) or die('<div class="flash error"><div class="top"></div><div class="text">SQL error: '.mysqli_error($db).'</div><div class="bottom"></div></div>');
										
									echo('<div class="flash ok">
										  <div class="top"></div>
										  <div class="text">New option added</div>
										  <div class="bottom"></div>
										</div>');
								}
							}else{
								$q = "SELECT id FROM ".$_GET['what']." WHERE name='".$_POST['name']."'";
								$r = mysqli_query($db,$q);
								if(mysqli_num_rows($r)){
									echo('<div class="flash error">
										  <div class="top"></div>
										  <div class="text">This option already exists</div>
										  <div class="bottom"></div>
										</div>');
								}else{
								
									$q = "INSERT INTO ".$_GET['what']." (name) values ('".$_POST['name']."')";
									$r = mysqli_query($db,$q) or die('<div class="flash error"><div class="top"></div><div class="text">SQL error: '.mysqli_error($db).'</div><div class="bottom"></div></div>');
										
									echo('<div class="flash ok">
										  <div class="top"></div>
										  <div class="text">New option added</div>
										  <div class="bottom"></div>
										</div>');
								}
							}
						}else{
							echo('<div class="flash error">
								  <div class="top"></div>
								  <div class="text">No text inserted</div>
								  <div class="bottom"></div>
								</div>');
						}
					}
				}
				
		 ?>
  
<div class="datagrid">
<h2><? 
		if($_GET['multi'] == 'yes'){
			$q = "SELECT name FROM product_options_types WHERE id=".$_GET['what'];
			$r = mysqli_query($db,$q);
			$f = mysqli_fetch_row($r);
			echo($f[0]);
		}else{
			echo($_GET['what']);
		}
		
	?></h2>
                	  <form method="post" action="">
                	    <table>
                	      <tr>
                	        <th>#</th>
                	        <th align="left" style="text-align:left;">Option</th>
                	        <th>Actions</th>
              	        </tr>
                        
                        
                     	<?
						if($_GET['multi']=='yes'){
							$q = "SELECT id, name FROM product_options_values WHERE type_id=".$_GET['what']." ORDER BY name";
							$r = mysqli_query($db,$q);
							$c = mysqli_num_rows($r);
							$sudalicha = 'odd';
							for($i=0; $i<$c; $i++){
								$f = mysqli_fetch_row($r);
								if($sudalicha == 'odd'){
									$sudalicha = 'even';
								}else{
									$sudalicha = 'odd';
								}
								echo('<tr class="'.$sudalicha.'">
										<td>'.strval($i+1).'</td>
										<td align="left" style="text-align:left;">'.$f[1].'</td>
										<td class="actions"><a href="?what='.$_GET['what'].'&multi='.$_GET['multi'].'&deleteid='.$f[0].'" class="button delete"><span>Delete</span></a></td>
									</tr>');
							}
						}else{
							if($_GET['what']=='categories'){
								$q = "SELECT id, name FROM categories WHERE parent_id=0 ORDER BY name";
								$r = mysqli_query($db,$q);
								$c = mysqli_num_rows($r);
								$sudalicha = 'odd';
								
								$counter = 0;
								
								for($i=0; $i<$c; $i++){
									$f = mysqli_fetch_row($r);
									if($sudalicha == 'odd'){
										$sudalicha = 'even';
									}else{
										$sudalicha = 'odd';
									}
									$counter ++;
									echo('<tr class="'.$sudalicha.'">
											<td>'.strval($counter).'</td>
											<td align="left" style="text-align:left;">'.$f[1].'</td>
											<td class="actions"><a href="?what='.$_GET['what'].'&deleteid='.$f[0].'" class="button delete"><span>Delete</span></a></td>
										</tr>');
									
									$q_sub = "SELECT id, name FROM categories WHERE parent_id=".$f[0]." ORDER BY name";
									$r_sub = mysqli_query($db,$q_sub);
									$c_sub = mysqli_num_rows($r_sub);
									for($j=0; $j<$c_sub; $j++){
										$f_sub = mysqli_fetch_row($r_sub);
										$sudalicha = 'even';
										$counter ++;
										echo('<tr class="'.$sudalicha.'">
											<td>'.strval($counter).'</td>
											<td align="left" style="text-align:left;"><li style="margin-left:20px;">'.$f_sub[1].'</li></td>
											<td class="actions"><a href="?what='.$_GET['what'].'&deleteid='.$f_sub[0].'" class="button delete"><span>Delete</span></a></td>
										</tr>');
									}
									
								}
							}else{
								$q = "SELECT id, name FROM ".$_GET['what']." ORDER BY name";
								$r = mysqli_query($db,$q);
								$c = mysqli_num_rows($r);
								$sudalicha = 'odd';
								for($i=0; $i<$c; $i++){
									$f = mysqli_fetch_row($r);
									if($sudalicha == 'odd'){
										$sudalicha = 'even';
									}else{
										$sudalicha = 'odd';
									}
									echo('<tr class="'.$sudalicha.'">
											<td>'.strval($i+1).'</td>
											<td align="left" style="text-align:left;">'.$f[1].'</td>
											<td class="actions"><a href="?what='.$_GET['what'].'&deleteid='.$f[0].'" class="button delete"><span>Delete</span></a></td>
										</tr>');
								}
							}
						}
							
						?>
                	      
                        
                        
              	      </table>
              	    </form>
          </div>
              </div>
            </div>
            <hr class="cleaner" />
        </div>

        <div id="footer">

        </div>
    </body>
</html>
