<?
	require('db.php');
	session_start();
	
	//logged?
	if($_GET['action']=='logout'){
		$q = "UPDATE admin SET session_id='' WHERE session_id='".session_id()."'";
		$r = mysqli_query($db,$q);
	}
	
	$q = "select login from admin where session_id='".session_id()."'";
	$r = mysqli_query($db,$q);
	$c = mysqli_num_rows($r);
	if($c){
		$logged = 1;
		$f = mysqli_fetch_row($r);
		$admin_name = $f[0];
	}else{
		$logged = 0;
	}
	
	

?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-language" content="en" />
        <meta name="robots" content="index,follow" />
        <meta name="googlebot" content="index,follow,snippet,archive" />
        <meta name="author" content="ELIVE CZ s.r.o. (c) 2010" />
		<?
			if(!$logged) {
				die('<meta http-equiv="refresh" content="0;url=index.php" />');
				
			}
		?>


        <link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/smoothness/jquery-ui-1.7.2.custom.css" />

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
        <script type="text/javascript" src="js/jquery.ui.datepicker-cs.js"></script>
        <script type="text/javascript" src="js/jquery.simpleTooltip.js"></script>
        <script type="text/javascript" src="js/jquery.flot.pack.js"></script>
        <!--[if IE]><script language="javascript" type="text/javascript" src="js/excanvas.pack.js"></script><![endif]-->
        <script type="text/javascript" src="js/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="js/submenu.js"></script>
        <script type="text/javascript" src="js/xadmin.js"></script>
        <script type="text/javascript" src="js/admin.js"></script>
        
        

        <title>Shanore Admin</title>
    </head>
    <body>
        <div id="header">
            <h1>ShanOre.com</h1>
            <div id="user"><a href="?action=logout">Logout</a>

                <br />
            </div>
            <div id="mainMenuWrapper">
              <ul id="mainMenu">
                <li class="active"><a href="admin.php">Catalog</a></li>
                <li><a href="orders.php">ORDERS</a></li>
                <li><a href="customers.php">Customers</a><a href="editor.html"></a></li>
                <li><a href="affiliates.php">Affiliates</a><a href="editor.html"></a></li>
              </ul>
            </div>
        </div>
        <div id="contentWrapper" style="width:1200px;">
          <div id="leftMenu"> <strong>Menu</strong>
            <div id="menu">
              <ul>
                <li><a href="admin.php" class="item">Products</a></li>
                <li> <a href="#" class="item">Product Options</a>
                  <ul>
                    <li><a href="options.php?what=categories">Categories</a></li>
                    <li><a href="options.php?what=brands">Brands</a></li>
                    <?
									$q = "SELECT name, id FROM product_options_types";
									$r = mysqli_query($db,$q);
									$c = mysqli_num_rows($r);
									for($i=0; $i<$c; $i++){
										$f = mysqli_fetch_row($r);
										echo('<li><a href="options.php?what='.$f[1].'&multi=yes">'.$f[0].'</a></li>');
									}
								?>
                  </ul>
                </li>
                <li> <a href="#" class="item" id="collections_menu">Collections</a>
                  <ul>
                  	<li><a href="categories-seo.php">CATEGORIES SEO</a></li><hr />
                    <li><a href="collections-assign.php">COLLECTIONS ADMIN</a></li><hr />
                    <?
									$q = "SELECT name, id FROM collections";
									$r = mysqli_query($db,$q);
									$c = mysqli_num_rows($r);
									for($i=0; $i<$c; $i++){
										$f = mysqli_fetch_row($r);
										echo('<li><a href="collections.php?what='.$f[1].'">'.$f[0].'</a></li>');
									}
								?>
                  </ul>
                </li>
                <li class="active"><a href="brands.php" class="item">Brands</a></li>
                <li><a href="deleted_products.php" class="item">Deleted Products</a></li>
                <li><a href="pages.php" class="item">Pages</a></li>
                <li><a href="currencies.php" class="item">Currencies</a></li>
                <li><a href="images.php" class="item">Images</a></li>
                <li><a href="sort.php" class="item">Sort Products</a></li>
                <li> <a href="#" class="item" id="pdf_menu">PDF catalogues</a>
                  <ul>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=1" target="_new">Engagement Rings</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=10" target="_new">Wedding Rings</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=96" target="_new">Claddagh Rings</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=94" target="_new">Celtic Rings</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=16" target="_new">Pendants</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=37" target="_new">Celtic Crosses</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=95" target="_new">Bracelets</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=84" target="_new">Earrings</a></li>

                                
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=85" target="_new">Tara Beads</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=91" target="_new">Tara Charms</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=83" target="_new">Tara Bracelets</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=92" target="_new">Tara Necklaces</a></li>
                                <li><a href="https://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=93" target="_new">Tara Stacking Rings</a></li>

                            </ul>
                </li>
              </ul>
            </div>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <strong> </strong></div>
          <div id="content">
              <div id="inner">
  
<div class="datagrid" style="width:850px;">
<h2 style="background-color:#666">Collections administration</h2>
                	    <table>
                	      <tr>
                	        <th>Brand</th>
                	        <th>Jewelry Image</th>
                            <th>Watches Image</th>
                            <th>Frontpage Image</th>
              	          </tr>
                        
                        
<?
						
	if($_FILES['img']['tmp_name']){
		
		$category_name = str_replace('\'','_',$_POST['category_name']);
		$category_name = str_replace(' ','_',$category_name);
		$category_name = str_replace('&','_',$category_name);
		
		if(file_exists("../brand_images/".$category_name.".jpg")) unlink("../brand_images/".$category_name."jpg");
		$filename = $category_name.".jpg";
		copy ($_FILES['img']['tmp_name'], "../brand_images/".$filename) or print_r(error_get_last());
		
	}
	
	
	$q = "SELECT name, id FROM brands";
	$r = mysqli_query($db,$q);
	$c = mysqli_num_rows($r);
	$sudalicha = 'odd';
	for($i=0; $i<$c; $i++){
		$f = mysqli_fetch_row($r);
		
		$category_name = str_replace(' ','_',$f[0]);
		$category_name = str_replace('\'','_',$category_name);
		$category_name = str_replace('&','_',$category_name);
		
		if(file_exists('../brand_images/'.$category_name.'.jpg')){
			$image = '<img src="../brand_images/'.$category_name.'.jpg" width="100" /><br />
						<form action="" id="'.$category_name.'" enctype="multipart/form-data" method="post">
							<input name="img" type="file" onchange="this.form.submit()" />
							<input type="hidden" name="category_name" value="'.$category_name.'" />
						</form>';
		}else{
			$image = '<form action="" id="'.$category_name.'" enctype="multipart/form-data" method="post">
							<input name="img" type="file" onchange="this.form.submit()" />
							<input type="hidden" name="category_name" value="'.$category_name.'" />
						</form>';
		}
		
		if(file_exists('../brand_images/'.$category_name.'_w.jpg')){
			$wimage = '<img src="../brand_images/'.$category_name.'_w.jpg" width="100" /><br />
						<form action="" id="'.$category_name.'" enctype="multipart/form-data" method="post">
							<input name="img" type="file" onchange="this.form.submit()" />
							<input type="hidden" name="category_name" value="'.$category_name.'_w" />
						</form>';
		}else{
			$wimage = '<form action="" id="'.$category_name.'" enctype="multipart/form-data" method="post">
							<input name="img" type="file" onchange="this.form.submit()" />
							<input type="hidden" name="category_name" value="'.$category_name.'_w" />
						</form>';
		}
		
		if(file_exists('../brand_images/'.$category_name.'_small.jpg')){
			$simage = '<img src="../brand_images/'.$category_name.'_small.jpg" width="100" /><br />
						<form action="" id="'.$category_name.'" enctype="multipart/form-data" method="post">
							<input name="img" type="file" onchange="this.form.submit()" />
							<input type="hidden" name="category_name" value="'.$category_name.'_small" />
						</form>';
		}else{
			$simage = '<form action="" id="'.$category_name.'" enctype="multipart/form-data" method="post">
							<input name="img" type="file" onchange="this.form.submit()" />
							<input type="hidden" name="category_name" value="'.$category_name.'_small" />
						</form>';
		}
		
		echo('<tr class="odd">
				<td><h3 style="color:red; margin-bottom:0;">'.$f[0].'</h3>
				<img src="img/seo.png" width="59" height="58" alt="SEO" class="onoff_btn" style="cursor:pointer;" onclick=\'window.open("seo.php?brand_id='.$f[1].'&ssid='.session_id().'","mywindow","menubar=1,resizable=1,width=880,height=570");\' />
				<td class="actions" valign="bottom">'.$image.'</td>
				<td class="actions" valign="bottom">'.$wimage.'</td>
				<td class="actions" valign="bottom">'.$simage.'</td>
			</tr>');
	}
	
		
?>
                	      
                        
                        
              	      </table>
          </div>
              </div>
            </div>
            <hr class="cleaner" />
        </div>

        <div id="footer">

        </div>
    </body>
</html>
