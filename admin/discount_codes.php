<?
	require('db.php');
	mysqli_query($db,"SET NAMES UTF8");
	session_start();
	
	//logged?
	if($_GET['action']=='logout'){
		$q = "UPDATE admin SET session_id='' WHERE session_id='".session_id()."'";
		$r = mysqli_query($db,$q);
	}
	
	$q = "select login from admin where session_id='".session_id()."'";
	$r = mysqli_query($db,$q);
	$c = mysqli_num_rows($r);
	if($c){
		$logged = 1;
		$f = mysqli_fetch_row($r);
		$admin_name = $f[0];
	}else{
		$logged = 0;
	}
	 

?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-language" content="en" />
        <meta name="robots" content="index,follow" />
        <meta name="googlebot" content="index,follow,snippet,archive" />
        <meta name="author" content="ELIVE CZ s.r.o. (c) 2010" />
		<?
			if(!$logged) {
				die('<meta http-equiv="refresh" content="0;url=index.php" />');
				
			}
		?>


        <link rel="stylesheet" type="text/css" media="screen" href="css/screen.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/smoothness/jquery-ui-1.7.2.custom.css" />

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
        <script type="text/javascript" src="js/jquery.ui.datepicker-cs.js"></script>
        <script type="text/javascript" src="js/jquery.simpleTooltip.js"></script>
        <script type="text/javascript" src="js/jquery.flot.pack.js"></script>
        <!--[if IE]><script language="javascript" type="text/javascript" src="js/excanvas.pack.js"></script><![endif]-->
        <script type="text/javascript" src="js/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="js/submenu.js"></script>
        <script type="text/javascript" src="js/xadmin.js"></script>
        <script type="text/javascript" src="js/admin.js"></script>
        
        

        <title>Shanore admin</title>
    </head>
    <body>
        <div id="header">
            <h1>Shanore.com</h1>
            <div id="user"><a href="?action=logout">Logout</a>

                <br />
            </div>
            <div id="mainMenuWrapper">
              <ul id="mainMenu">
                <li class="active"><a href="admin.php">Catalog</a></li>
                <li><a href="orders.php">ORDERS</a></li>
                <li><a href="customers.php">Customers</a><a href="editor.html"></a></li>
              </ul>
            </div>
        </div>
        <div id="contentWrapper" style="width:1200px;">
          <div id="leftMenu"> <strong>Menu</strong>
            <div id="menu">
              <ul>
                <li><a href="admin.php" class="item">Products</a></li>
                <li> <a href="#" class="item">Product Options</a>
                  <ul>
                    <li><a href="options.php?what=categories">Categories</a></li>
                    <li><a href="options.php?what=brands">Brands</a></li>
                    <?
									$q = "SELECT name, id FROM product_options_types";
									$r = mysqli_query($db,$q);
									$c = mysqli_num_rows($r);
									for($i=0; $i<$c; $i++){
										$f = mysqli_fetch_row($r);
										echo('<li><a href="options.php?what='.$f[1].'&multi=yes">'.$f[0].'</a></li>');
									}
								?>
                  </ul>
                </li>
                <li> <a href="#" class="item" id="collections_menu">Collections</a>
                  <ul>
                    <li><a href="categories-seo.php">CATEGORIES SEO</a></li>
                    <hr />
                    <li><a href="collections-assign.php">COLLECTIONS ADMIN</a></li>
                    <hr />
                    <?
									$q = "SELECT name, id FROM collections";
									$r = mysqli_query($db,$q);
									$c = mysqli_num_rows($r);
									for($i=0; $i<$c; $i++){
										$f = mysqli_fetch_row($r);
										echo('<li><a href="collections.php?what='.$f[1].'">'.$f[0].'</a></li>');
									}
								?>
                  </ul>
                </li>
                <li><a href="brands.php" class="item">Brands</a></li>
                <li><a href="deleted_products.php" class="item">Deleted Products</a></li>
                <li><a href="pages.php" class="item">Pages</a></li>
                <li><a href="currencies.php" class="item">Currencies</a></li>
                <li><a href="images.php" class="item">Images</a></li>
                <li><a href="sort.php" class="item">Sort Products</a></li>
                <li><a href="reports.php" class="item">Reports</a></li>
                <li> <a href="#" class="item" id="pdf_menu">PDF catalogues</a>
                  <ul>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=1" target="_new">Engagement Rings</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=10" target="_new">Wedding Rings</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=96" target="_new">Claddagh Rings</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=94" target="_new">Celtic Rings</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=16" target="_new">Pendants</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=37" target="_new">Celtic Crosses</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=95" target="_new">Bracelets</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/shanore.php?catalog=84" target="_new">Earrings</a></li>

                                
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=85" target="_new">Tara Beads</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=91" target="_new">Tara Charms</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=83" target="_new">Tara Bracelets</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=92" target="_new">Tara Necklaces</a></li>
                                <li><a href="http://www.shanore.com/admin/shanore_pdf_cmyk/taras.php?catalog=93" target="_new">Tara Stacking Rings</a></li>

                            </ul>
                </li>
                <li  class="active"><a href="discount_codes.php" class="item">Discount codes</a></li>
              </ul>
            </div>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <strong> </strong></div>
          <div id="content">
              <div id="inner">
  <a href="#" class="add" id="addnew_clicker">Add a new discount code</a>
  <div class="add">
<div class="textbox">
<?

			if($_GET['action'] == 'newcode'){
				$q = "INSERT INTO discount_codes (code, percent) VALUES ('".$_POST['name']."','".$_POST['perc']."')";
				$r = mysqli_query($db,$q) or die(mysqli_error($db));
				
				$q = "SELECT id FROM discount_codes WHERE code='".$_POST['name']."' ORDER BY id DESC";
				$r = mysqli_query($db,$q) or die(mysqli_error($db));
				$f = mysqli_fetch_row($r);
				
				$q = "UPDATE discount_products SET discount_code_id=".$f[0].", temp_ssid='' WHERE temp_ssid='".session_id()."'";
				$r = mysqli_query($db,$q) or die(mysqli_error($db));
			}
			
?>  
    	            	<form action="?action=newcode" enctype="multipart/form-data" method="post" id="addform">
                			<table width="100%" border="0" cellspacing="0" cellpadding="0">
                			  <tr>
                			    <td width="23%" rowspan="2" valign="top">Code <br />
                                  <input name="name" id="name" />
                                  <br /> 
                                  % discount
<br />
<input name="perc" id="perc" />
<br /></td>
<td width="42%" valign="top"><!--<strong>brands</strong><br />
  <select name="brands" id="brands" onchange="brandselected();">
    <option value="">select</option>
    <?
                                    $q = "SELECT id, name FROM brands ORDER BY name";
									$r = mysqli_query($db,$q);
									$c = mysqli_num_rows($r);
									for($i=0; $i<$c; $i++){
										$f = mysqli_fetch_row($r);
										echo('<option value="'.$f[0].'">'.$f[1].'</option>');
									}
?>
  </select>--><strong>
  products</strong><br />
    hold CTRL and click for multi selection<br /></td>
                			    <td width="35%" style="vertical-align:bottom;"><strong>Apply to following products only</strong><br />
                			      leave empty for  &quot;apply to all&quot;</td>
              			      </tr>
                			  <tr>
                			    <td colspan="2" valign="top"><select name="products" size="10" multiple="multiple" id="products" style="width:250px;"><option value="" disabled="disabled">Select brand first</option></select><img src="img/add.jpg" alt="add" id="addbtn" style="cursor:pointer;" onclick="add_it()"/><select name="selected" size="10" multiple="multiple" id="selected" style="width:250px;"></select></td>
               			      </tr>
                			  <tr>
                			    <td><img src="img/loading.gif" alt="loading" width="16" height="16" class="loading" style="margin-top:3px; display:none;" id="loading" /></td>
                			    <td>&nbsp;</td>
                			    <td align="right"><input type="submit" id="submitbtn" value="Add Code" style="width:120px; background-color:#F90;" /></td>
              			      </tr>
              			  </table><br />
       	  </form>
                    <div class="textboxFooter"></div></div></div>
  <div class="datagrid">
<h2 style="background-color:#666">Discount Codes Builder</h2>
                	    <table>
                	      <tr>
                	        <th>Code</th>
                	        <th>% discount</th>
                            <th>Applies to</th>
              	          </tr>
                        
                        
<?
	
	if($_GET['action']=='percent_change'){
		$q = "UPDATE discount_codes SET percent=".$_POST['percent']." WHERE id=".$_POST['codeid'];
		$r = mysqli_query($db,$q) or die(mysqli_error($db));
		?>
<script type="text/javascript">
	alert('Change has been saved...');
</script>
        <?
		
	}
	
	if($_GET['action']=='name_change'){
		$q = "UPDATE discount_codes SET code='".$_POST['codename']."' WHERE id=".$_POST['codeid'];
		$r = mysqli_query($db,$q) or die(mysqli_error($db));
		?>
<script type="text/javascript">
	alert('Discount code name has been saved...');
</script>
        <?
		
	}
	
	if($_GET['delid']){
		$r = mysqli_query($db,"DELETE FROM discount_codes WHERE id=".$_GET['delid']);
	}

	$q = "SELECT id, code, percent FROM discount_codes order by id DESC";
	$r = mysqli_query($db,$q);
	$c = mysqli_num_rows($r);
	for($i=0; $i<$c; $i++){
		$f = mysqli_fetch_row($r);
		
		// let's check product dependencies
		$q2 = "SELECT id, product_id FROM discount_products WHERE discount_code_id=".$f[0];
		$r2 = mysqli_query($db,$q2);
		$c2 = mysqli_num_rows($r2);
		if($c2){
			$applies = $c2." products.";
		}else{
			$applies = "ALL products.";
		}
		
		
		echo('<tr class="odd">
				<td><form action="?action=name_change" enctype="multipart/form-data" method="post"><input type="hidden" name="codeid" value="'.$f[0].'" /><input name="codename" style="width:50px;" value="'.$f[1].'" /><input type="submit" value="save" /></form><a href="?delid='.$f[0].'"><img src="img/datagridDel.png" /></a>
				<td><form action="?action=percent_change" enctype="multipart/form-data" method="post"><input type="hidden" name="codeid" value="'.$f[0].'" /><input name="percent" style="width:50px;" value="'.$f[2].'" /><input type="submit" value="save" /></form> </td>
				<td>'.$applies.'</td>
			</tr>');
	}
	
		
?>
                	      
                        
                        
              	      </table>
        </div>
              </div>
            </div>
            <hr class="cleaner" />
        </div>

        <div id="footer">
<div id="copyright">
<br />
                Copyright 2011<br />
				matrixinternet.ie<br />
                info@matrixinternet.ie
            </div>
        </div>
        
        
	<script type="text/javascript">
	
	brandselected();
	
	
        function brandselected(){
			var brand = $('#brands').val();
			$('#products').attr('disabled','disabled');
			$('#products').html('<option value="">Loading...</option>');
			
			$.ajax({
				type: 'POST',
				//data: "brand=" + brand,
				url: "ajax_get_products_discount.php",
				success: function(msg){
					$('#products').attr('disabled','');
					$('#products').html(msg);
				}
			});
		}
		
		function add_it(){
			var products = $('#products').val();
			if(products == null){
				alert('No products selected');
				return false;
			}
			
			$('#selected').html('<option value="">Loading....</option>');
			
			$.ajax({
				type: 'POST',
				data: "products=" + products,
				url: "ajax_set_products_discount.php",
				success: function(msg){
					$('#selected').html(msg);
				}
			});
		}
		
		$('#addbtn').hover(
			function(){
				$(this).attr('src','img/add_over.jpg');
			},
			function(){
				$(this).attr('src','img/add.jpg');
			}
		);
		
    </script>

    </body>
</html>
